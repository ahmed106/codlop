@extends('admin.layouts.layout')

@php
    $page_title = isset($page_titles['create']) ? $page_titles['create'] : setting('title');
@endphp


@section('page-title')
    {{ $page_title }}
@endsection


@push('admin_css')


@endpush



{{-------------------- start content --------------------}}
@section('content')

    <div class="main-content side-content pt-0">
        <div class="container-fluid">
            <div class="inner-body">

                <div class="page-header">
                    @include('admin.inc.page_header', ['page_title' => $page_title])

                </div>
                <!-- End Page Header -->

                <!--Row-->
                <div class="row">

                    <form action="{{ route('service_hosts.update',$service_host->id) }}" method="post" enctype="multipart/form-data">

                        @csrf
                        @method('put')
                        <div class="col-sm-12">
                            <div class="card">
                                <div class="card-body">

                                    <div class="row">


                                        @if(count($language_allows) > 0)
                                            @foreach($language_allows as $language_allow)
                                                <div class="col-md-6 form-group">
                                                    <label> اسم الباقه {{ "(". $language_allow->code .")"}} </label>
                                                    <input name="titles[{{$language_allow->code}}]" value="{{ isset($view_languages[$language_allow->code]) ? $view_languages[$language_allow->code]->title : null }}" class="form-control" type="text" placeholder="" required>
                                                </div>
                                            @endforeach
                                        @endif

                                        @if(count($language_allows) > 0)
                                            @foreach($language_allows as $language_allow)
                                                <div class="col-md-6 form-group">
                                                    <label>{{'التفاصيل'}} {{ "(". $language_allow->code .")"}} </label>
                                                    <textarea name="texts[{{$language_allow->code}}]" class="form-control" required>{{ isset($view_languages[$language_allow->code]) ? $view_languages[$language_allow->code]->text : null }}</textarea>
                                                </div>
                                            @endforeach
                                        @endif

                                    </div>


                                    <input type="hidden" name="service_id" value="{{request()->get('service_id')}}">


                                    <div class="form-group">

                                        <label for="">السعر</label>

                                        <input type="number" name="price" value="{{$service_host->price}}" required class="form-control">
                                    </div>

                                    {{--                                    <div class="form-group">--}}

                                    {{--                                        <label for="">النوع</label>--}}

                                    {{--                                        <input type="string" name="host_type" value="{{$service_host->host_type}}" required class="form-control">--}}
                                    {{--                                    </div>--}}

                                </div>

                                <div class="card-footer">
                                    <div class="col-sm-12">
                                        <button id="submit-form" type="submit" class="btn ripple btn-primary btn-lg">
                                            <i class="fa fa-save loading-submit"></i>
                                            حفظ
                                        </button>
                                    </div>
                                </div>

                            </div>
                        </div>

                    </form>

                </div>
            </div>
        </div>
    </div>

@endsection
{{-------------------- end content --------------------}}


{{-------------------- scripts --------------------}}
@push('admin_css')


@endpush


