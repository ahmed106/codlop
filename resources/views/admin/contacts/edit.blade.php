@extends('admin.layouts.layout')

@php
    $page_title = isset($page_titles['edit']) ? $page_titles['edit'] : setting('title');
@endphp


@section('page-title')
    {{ $page_title }}
@endsection


@push('admin_css')


@endpush



{{-------------------- start content --------------------}}
@section('content')

    <div class="main-content side-content pt-0">
        <div class="container-fluid">
            <div class="inner-body">

                <div class="page-header">
                @include('admin.inc.page_header', ['page_title' => $page_title])

</div>
            <!-- End Page Header -->

                <!--Row-->
                <div class="row">




                    <form action="{{ aurl('update-contact') }}" method="post" enctype="multipart/form-data">

                        @csrf

                        <input type="hidden" name="contact_id" value="{{ $contact->id }}">

                        <div class="col-sm-12">
                            <div class="card">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <h5>الاسم :</h5>
                                            <p>{{ $contact->name }}</p>
                                        </div>

                                        <div class="col-md-6">
                                            <h5>الايميل :</h5>
                                            <p>{{ $contact->email }}</p>
                                        </div>


                                        <div class="col-md-12">
                                            <h5>الموضوع :</h5>
                                            <p>{{ $contact->subject }}</p>
                                        </div>


                                        <div class="col-md-12">
                                            <h5>الرسالة :</h5>
                                            <p>{{ $contact->message }}</p>
                                        </div>

                                    </div>
                                </div>

                                @if($contact->is_read == 'read')
                                    <div class="app-details" style="padding: 15px">
                                        <h3>الرد :</h3>
                                        <p>{{ $contact->replay }}</p>
                                    </div>
                                @else
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <label> {{ $page_title }} </label>
                                                <textarea name="replay" rows="3" class="form-control min-unset" required></textarea>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="card-footer">
                                            <button id="submit-form" type="submit" class="btn ripple btn-primary btn-lg">
                                                <i class="fa fa-save loading-submit"></i>
                                                حفظ
                                            </button>
                                    </div>
                               @endif

                            </div>
                        </div>

                    </form>

                </div>
            </div>
        </div>
    </div>

@endsection
{{-------------------- end content --------------------}}


{{-------------------- scripts --------------------}}
@push('admin_css')


@endpush


