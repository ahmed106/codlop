@extends('admin.layouts.layout')

@php
    $page_title = isset($page_titles['index']) ? $page_titles['index'] : setting('title');
@endphp


@section('page-title')
    {{ $page_title }}
@endsection


@push('admin_css')


@endpush



{{-------------------- start content --------------------}}
@section('content')

    <!-- Main Content-->
    <div class="main-content side-content pt-0">
        <div class="container-fluid">
            <div class="inner-body">

                <div class="page-header">

                    @include('admin.inc.page_header', ['page_title' => $page_title])

                    {{--                    <div class="d-flex flex-end">--}}
                    {{--                        <div class="justify-content-center">--}}
                    {{--                            <a href="{{ aurl('contact/create') }}" class="btn ripple btn-primary btn-icon-text">--}}
                    {{--                                <i class="fe fe-plus me-2"></i>{{ isset($page_titles['create']) ? $page_titles['create'] : "#" }}</a>--}}
                    {{--                        </div>--}}
                    {{--                    </div>--}}
                </div>
                <!-- End Page Header -->


                <!--Row-->
                <div class="row sidemenu-height">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    @if(count($contacts) > 0)
                                        <div class="col-md-12">
                                            <div class="table-responsive">
                                                <table id="example3"
                                                       class="table table-striped table-bordered">
                                                    <thead>

                                                    <tr>
                                                        <th>مقرؤه</th>
                                                        <th>الرد</th>
                                                        <th>التاريخ</th>
{{--                                                        <th>الاسم</th>--}}
                                                        <th>رمز الهاتف</th>
                                                        <th>الهاتف</th>
                                                        <th>الايميل</th>
{{--                                                        <th>الموضوع</th>--}}
{{--                                                        <th>الرسالة</th>--}}
                                                        <th>التحكم</th>
                                                    </tr>

                                                    </thead>
                                                    <tbody>

                                                    @foreach($contacts as $key => $contact)
                                                        <tr class="delete-row-{{ $contact->id }}">
                                                            <td><label class="ckbox my-auto me-4 mt-1"><input {{ $contact->is_read == 'read' ? 'checked' : '' }} type="checkbox" disabled><span></span></label></td>
                                                            <td><a href="{{ aurl('contact').'/'.$contact->id.'/edit' }}" class="btn btn-success">رد</a></td>
                                                            <td>{{ change_date_to_new_format($contact->created_at) }}</td>
{{--                                                            <td>{{ $contact->name }}</td>--}}
                                                            <td>{{ $contact->phone_code }}</td>
                                                            <td>{{ $contact->phone }}</td>
                                                            <td>{{ $contact->email }}</td>
{{--                                                            <td>{{ $contact->subject }}</td>--}}
{{--                                                            <td title="{{ $contact->message }}">{{ \Illuminate\Support\Str::limit($contact->message, 100 , '...') }}</td>--}}
                                                            <td>
                                                                <div class="button-list">
                                                                    <a href="#" id="delete-element" data-id="{{ $contact->id }}" data-title="{{ $contact->name }}" class="btn"><i class="ti ti-trash"></i></a>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    @endforeach

                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    @else
                                        @include('admin.inc.there_is_no_elements_to_show')
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


            </div>
        </div>
    </div>
    <!-- End Main Content-->

    {{-- ============================== Modal ============================== --}}
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">New message</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form>
                        <div class="form-group">
                            <label for="recipient-name" class="col-form-label">Recipient:</label>
                            <input type="text" class="form-control" id="recipient-name">
                        </div>
                        <div class="form-group">
                            <label for="message-text" class="col-form-label">Message:</label>
                            <textarea class="form-control" id="message-text"></textarea>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary">Send message</button>
                </div>
            </div>
        </div>
    </div>


@endsection
{{-------------------- end content --------------------}}


{{-------------------- scripts --------------------}}
@push('admin_js')

    <script !src="">
        //===================================================================
        $(document).on('click', '#delete-element', function () {

            var url = '{{ aurl('delete-contact') }}';
            var element_id = $(this).attr('data-id');
            var element_title = $(this).attr('data-title');

            console.log(element_title);
            var sure = confirm("هل تريد بالفعل حذف " + element_title);

            if (sure == true) {
                $.ajax({
                    url: url,
                    dataType: 'json',
                    type: "POST",
                    data: {_token: '{{ @csrf_token() }}', id: element_id},
                    beforeSend: function () {
                    },
                    success: function (data) {
                        console.log(data);
                        $('.delete-row-' + data.id).fadeOut().remove();
                    },
                    error(response) {
                    }
                });
            }

            return false;
        });
        //====================================================================
    </script>

@endpush


