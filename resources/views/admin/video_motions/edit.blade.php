@extends('admin.layouts.layout')

@php
    $page_title = isset($page_titles['edit']) ? $page_titles['edit'] : setting('title');
@endphp


@section('page-title')
    {{ $page_title }}
@endsection


@push('admin_css')


@endpush



{{-------------------- start content --------------------}}
@section('content')

    <div class="main-content side-content pt-0">
        <div class="container-fluid">
            <div class="inner-body">

                <div class="page-header">
                    @include('admin.inc.page_header', ['page_title' => $page_title])

                </div>
                <!-- End Page Header -->

                <!--Row-->
                <div class="row">

                    <form id="video_motion_form" action="{{ aurl('update-video_motion') }}" method="post" enctype="multipart/form-data">

                        @csrf


                        <input type="hidden" name="video_motion_id" value="{{ $video_motion->id }}">

                        <div class="col-sm-12">
                            <div class="card">
                                <div class="card-body">

                                    <div class="row">

                                        <div class="col-md-12 form-group">
                                            <select name="type" class="form-select" aria-label="Default select example">
                                                <option selected disabled>اختر القسم</option>
                                                <option value="video_motions" {{ $video_motion->type == 'video_motions' ? 'selected' : '' }}>الموشن جرافيك</option>
                                                <option value="mobile_application" {{ $video_motion->type == 'mobile_application' ? 'selected' : '' }}>تطبيقات الموبيل</option>
                                                <option value="web_design" {{ $video_motion->type == 'web_design' ? 'selected' : '' }}>تصميم المواقع</option>
                                                <option value="stores" {{ $video_motion->type == 'stores' ? 'selected' : '' }}>متاجر</option>
                                            </select>
                                        </div>


                                        @if(count($language_allows) > 0)
                                            @foreach($language_allows as $language_allow)
                                                <div class="col-md-6 form-group">
                                                    <label>{{'العنوان'}} {{ "(". $language_allow->code .")"}} </label>
                                                    <input name="titles[{{$language_allow->code}}]"
                                                           value="{{ isset($view_languages[$language_allow->code]) ? $view_languages[$language_allow->code]->title : null }}"
                                                           class="form-control" type="text" placeholder="" required>
                                                </div>
                                            @endforeach
                                        @endif

                                        @if(count($language_allows) > 0)
                                            @foreach($language_allows as $language_allow)
                                                <div class="col-md-6 form-group">
                                                    <label>{{'التفاصيل'}} {{ "(". $language_allow->code .")"}} </label>
                                                    <textarea name="texts[{{$language_allow->code}}]"
                                                              class="form-control"
                                                              required>{{ isset($view_languages[$language_allow->code]) ? $view_languages[$language_allow->code]->text : null }}</textarea>
                                                </div>
                                            @endforeach
                                        @endif

                                        <div class="col-md-12 form-group">
                                            <label>@lang('web_lang.website_link')</label>
                                            <input type="url" class="form-control" value="{{$video_motion->website_link}}" name="website_link">
                                        </div>

                                        <div class="col-md-12 form-group">
                                            <label>@lang('web_lang.android_link')</label>
                                            <input type="url" class="form-control" value="{{$video_motion->android_link}}" name="android_link">
                                        </div>

                                        <div class="col-md-12 form-group">
                                            <label>@lang('web_lang.ios_link')</label>
                                            <input type="url" class="form-control" value="{{$video_motion->ios_link}}" name="ios_link">
                                        </div>

                                        <div class="col-md-12 form-group">
                                            <label>الصورة (الابعاد 320 * 264)</label>

                                            <input type="file" class="dropify" name="image" data-default-file="{{ GetImg($video_motion->image) }}">
                                        </div>

                                        <div class="col-md-12 form-group">
                                            <label>صورة التفاصيل</label>
                                            <input type="file" class="dropify" name="background" data-default-file="{{ GetImg($video_motion->background) }}">
                                        </div>

                                        <div class="col-md-12 form-group">
                                            <label>الفيديو</label>
                                            <input type="file" class="video-dropify" name="video"
                                                   data-default-file="{{ GetImg($video_motion->video) }}">
                                        </div>

                                    </div>

                                    <div class="card card-primary card-outline video_motion_card d-none ">
                                        <div class="container mt-5 mb-4">
                                            <h2>loading....</h2>
                                            <div class="progress">
                                                <div id="video_progress_bar" class="progress-bar" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" style="width:0%">
                                                    <span class="sr-only">70% Complete</span>
                                                </div>
                                            </div>
                                        </div>


                                    </div>


                                </div>

                                <div class="card-footer">
                                    <div class="col-sm-12">
                                        <button id="submit-form" type="submit" class="btn ripple btn-primary btn-lg">
                                            <i class="fa fa-save loading-submit"></i>
                                            حفظ
                                        </button>
                                    </div>
                                </div>

                            </div>
                        </div>

                    </form>

                </div>
            </div>
        </div>
    </div>

@endsection
{{-------------------- end content --------------------}}


{{-------------------- scripts --------------------}}
@push('admin_js')
    {{--    <script>--}}


    {{--        $('#video_motion_form').on('submit', function (e) {--}}
    {{--            e.preventDefault();--}}

    {{--            let data = new FormData($('#video_motion_form')[0]),--}}
    {{--                url = $(this).attr('action');--}}

    {{--            $('.video_motion_card').addClass('d-none').removeClass('d-block')--}}
    {{--            $('#video_progress_bar').css('width', 0 + '%').html(0 + '%')--}}


    {{--            $.ajax({--}}
    {{--                type: 'post',--}}
    {{--                url: url,--}}
    {{--                contentType: false,--}}
    {{--                cache: false,--}}
    {{--                processData: false,--}}
    {{--                data: data,--}}
    {{--                success: function (response) {--}}
    {{--                    //window.location = '{{aurl('video_motion')}}'--}}
    {{--                },--}}
    {{--                xhr: function () {--}}
    {{--                    var xhr = new window.XMLHttpRequest();--}}
    {{--                    xhr.upload.addEventListener("progress", function (evt) {--}}
    {{--                        if (evt.lengthComputable) {--}}
    {{--                            var percentComplete = Math.round(evt.loaded / evt.total * 100) + "%";--}}
    {{--                            $('.video_motion_card').removeClass('d-none').addClass('d-block')--}}
    {{--                            $('#video_progress_bar').css('width', percentComplete).html(percentComplete)--}}

    {{--                        }--}}
    {{--                    }, false)--}}
    {{--                    return xhr;--}}
    {{--                },--}}
    {{--            })--}}

    {{--        })--}}
    {{--    </script>--}}

@endpush

