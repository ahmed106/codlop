@extends('admin.layouts.layout')

@php
    $page_title = isset($page_titles['index']) ? $page_titles['index'] : setting('title');
@endphp


@section('page-title')
    {{ $page_title }}
@endsection


@push('admin_css')


@endpush



{{-------------------- start content --------------------}}
@section('content')

    <!-- Main Content-->
    <div class="main-content side-content pt-0">
        <div class="container-fluid">
            <div class="inner-body">
                <div class="page-header">

                    @include('admin.inc.page_header', ['page_title' => $page_title])

                    <div class="d-flex flex-end">
                        <div class="justify-content-center">
                            <a href="{{ aurl('video_motion/create') }}" class="btn ripple btn-primary btn-icon-text">
                                <i class="fe fe-plus me-2"></i>{{ isset($page_titles['create']) ? $page_titles['create'] : "#" }}
                            </a>
                        </div>
                    </div>
                </div>
                <!-- End Page Header -->


                <!--Row-->
                <div class="row sidemenu-height">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">

                                    <div class="col-md-12">

                                        <select
                                            onchange="$(this).val() && (window.location = $(this).val());"
                                            style="width: 50%" class="form-select" aria-label="Default select example">
                                            <option selected disabled>اختر القسم</option>
                                            <option {{ request()->get('type') == 'video_motions' ? 'selected' : '' }} value="{{ aurl('video_motion').'?type=video_motions' }}">الموشن
                                                جرافيك
                                            </option>
                                            <option {{ request()->get('type') == 'mobile_application' ? 'selected' : '' }} value="{{ aurl('video_motion').'?type=mobile_application' }}">
                                                تطبيقات الموبيل
                                            </option>
                                            <option {{ request()->get('type') == 'web_design' ? 'selected' : '' }} value="{{ aurl('video_motion').'?type=web_design' }}">تصميم
                                                المواقع
                                            </option>
                                            <option {{ request()->get('type') == 'stores' ? 'selected' : '' }} value="{{ aurl('video_motion').'?type=stores' }}">
                                                متاجر
                                            </option>
                                        </select>


                                        <br>
                                        <br>
                                        @if(count($video_motions) > 0)

                                            <div class="table-responsive">
                                                <table id="example3"
                                                       class="table table-striped table-bordered">
                                                    <thead>

                                                    <tr>
                                                        <th>#</th>
                                                        <th>القسم</th>
                                                        <th>العنوان</th>
                                                        <th>التفاصيل</th>
                                                        <th>فيديو الموشن</th>
                                                        <th>التحكم</th>
                                                    </tr>

                                                    </thead>
                                                    <tbody>

                                                    @php
                                                        $arr = ['video_motions' => 'الموشن جرافيك', 'mobile_application' => 'تطبيقات الموبيل', 'web_design' => 'تصميم المواقع','stores'=>'المتاجر'];
                                                    @endphp


                                                    @foreach($video_motions as $key => $video_motion)
                                                        <tr class="delete-row-{{ $video_motion->id }}">
                                                            <td>{{ ++$key }}</td>
                                                            <td>{{ @$arr[$video_motion->type] }}</td>
                                                            <td>{{ $video_motion->title }}</td>
                                                            <td title="{{ $video_motion->text }}">{!! \Illuminate\Support\Str::limit(strip_tags($video_motion->text), 100 , '...') !!}</td>
                                                            <td>
                                                                <video width="220" height="140" controls>
                                                                    <source src="{{ GetImg($video_motion->video) }}"
                                                                            type="video/mp4">
                                                                    <source src="{{ GetImg($video_motion->video) }}"
                                                                            type="video/ogg">
                                                                </video>
                                                            </td>
                                                            <td>
                                                                <div class="button-list">
                                                                    <a href="{{ aurl('video_motion').'/'.$video_motion->id.'/edit' }}"
                                                                       class="btn"><i class="ti ti-pencil"></i></a>
                                                                    <a href="#" id="delete-element"
                                                                       data-id="{{ $video_motion->id }}"
                                                                       data-title="{{ $video_motion->title }}"
                                                                       class="btn"><i class="ti ti-trash"></i></a>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    @endforeach

                                                    </tbody>
                                                </table>
                                            </div>

                                        @else
                                            @include('admin.inc.there_is_no_elements_to_show')
                                        @endif
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>


            </div>
        </div>
    </div>
    <!-- End Main Content-->

@endsection
{{-------------------- end content --------------------}}


{{-------------------- scripts --------------------}}
@push('admin_js')

    <script !src="">
        //===================================================================
        $(document).on('click', '#delete-element', function () {

            var url = '{{ aurl('delete-video_motion') }}';
            var element_id = $(this).attr('data-id');
            var element_title = $(this).attr('data-title');

            console.log(url);
            var sure = confirm("هل تريد بالفعل حذف " + element_title);

            if (sure == true) {
                $.ajax({
                    url: url,
                    dataType: 'json',
                    type: "POST",
                    data: {_token: '{{ @csrf_token() }}', id: element_id},
                    beforeSend: function () {
                    },
                    success: function (data) {
                        console.log(data);
                        $('.delete-row-' + data.id).fadeOut().remove();
                    },
                    error(response) {
                    }
                });
            }

            return false;
        });
        //====================================================================
    </script>

@endpush


