@extends('admin.layouts.layout')

@php
    $page_title = isset($page_titles['index']) ? $page_titles['index'] : setting('title');
@endphp


@section('page-title')
    {{ $page_title }}
@endsection


@push('admin_css')


@endpush



{{-------------------- start content --------------------}}
@section('content')

    <!-- Main Content-->
    <div class="main-content side-content pt-0">
        <div class="container-fluid">
            <div class="inner-body">

                <div class="page-header">

                    @include('admin.inc.page_header', ['page_title' => $page_title])

                    <div class="d-flex flex-end">
                        <div class="justify-content-center">
                            <a href="{{ aurl('site_text/create') }}" class="btn ripple btn-primary btn-icon-text">
                                <i class="fe fe-plus me-2"></i>{{ isset($page_titles['create']) ? $page_titles['create'] : "#" }}
                            </a>
                        </div>
                    </div>
                </div>
                <!-- End Page Header -->


                <!--Row-->
                <div class="row sidemenu-height">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    @if(count($site_texts) > 0)
                                        <div class="col-md-12">
                                            <div class="table-responsive">
                                                <table id="example3"
                                                       class="table table-striped table-bordered">
                                                    <thead>

                                                    <tr>
                                                        <th>#</th>
                                                        <th>الصفحة</th>
                                                        <th>العنوان</th>
                                                        <th>التفاصيل</th>
                                                        <th>الصورة</th>
                                                        <th>التحكم</th>
                                                    </tr>

                                                    </thead>
                                                    <tbody>

                                                    @php
                                                        $page_type_arr = ['home'=> 'الصفحة الرئيسية', 'about'=> 'عن الشركة', 'service'=> 'الخدمات', 'offer'=> '', 'blog'=> 'العروض', 'contact'=> 'اتصل بنا'];
                                                    @endphp

                                                    @foreach($site_texts as $key => $site_text)
                                                        <tr class="delete-row-{{ $site_text->id }}">
                                                            <td>{{ ++$key }}</td>
                                                            <td>{{ @$page_type_arr[$site_text->page_type] }}</td>
                                                            <td>{{ $site_text->title }}</td>
                                                            <td title="{{ $site_text->text }}">{!! \Illuminate\Support\Str::limit(strip_tags($site_text->text), 100 , '...') !!}</td>
                                                            <td>
                                                                @if($site_text->text_type == "features_of_codlop" || $site_text->page_type == 'service')
                                                                    لا يوجد صورة
                                                                @else
                                                                    <img class="img-table" src="{{ GetImg($site_text->image) }}" alt="">
                                                                @endif
                                                            </td>
                                                            <td>
                                                                <div class="button-list">
                                                                    <a href="{{ aurl('site_text').'/'.$site_text->id.'/edit?page_title='.@$page_type_arr[$site_text->page_type] }}"
                                                                       class="btn"><i class="ti ti-pencil"></i></a>
                                                                    <a href="#" id="delete-element"
                                                                       data-id="{{ $site_text->id }}"
                                                                       data-title="{{ $site_text->title }}" class="btn"><i
                                                                            class="ti ti-trash"></i></a>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    @endforeach

                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    @else
                                        @include('admin.inc.there_is_no_elements_to_show')
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


            </div>
        </div>
    </div>
    <!-- End Main Content-->

@endsection
{{-------------------- end content --------------------}}


{{-------------------- scripts --------------------}}
@push('admin_js')

    <script !src="">
        //===================================================================
        $(document).on('click', '#delete-element', function () {

            var url = '{{ aurl('delete-site_text') }}';
            var element_id = $(this).attr('data-id');
            var element_title = $(this).attr('data-title');

            console.log(url);
            var sure = confirm("هل تريد بالفعل حذف " + element_title);

            if (sure == true) {
                $.ajax({
                    url: url,
                    dataType: 'json',
                    type: "POST",
                    data: {_token: '{{ @csrf_token() }}', id: element_id},
                    beforeSend: function () {
                    },
                    success: function (data) {
                        console.log(data);
                        $('.delete-row-' + data.id).fadeOut().remove();
                    },
                    error(response) {
                    }
                });
            }

            return false;
        });
        //====================================================================
    </script>

@endpush


