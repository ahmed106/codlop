@extends('admin.layouts.layout')

@php
    $page_title = isset($page_titles['edit']) ? $page_titles['edit'] : setting('title');
@endphp


@section('page-title')
    {{ $page_title }}
@endsection


@push('admin_css')


@endpush



{{-------------------- start content --------------------}}
@section('content')

    <div class="main-content side-content pt-0">
        <div class="container-fluid">
            <div class="inner-body">

                <div class="page-header">
                @include('admin.inc.page_header', ['page_title' => $page_title])

</div>
            <!-- End Page Header -->

                <!--Row-->
                <div class="row">

                    <form action="{{ aurl('update-client') }}" method="post" enctype="multipart/form-data">

                        @csrf

                        <input type="hidden" name="client_id" value="{{ $client->id }}">

                        <div class="col-sm-12">
                            <div class="card">
                                <div class="card-body">

                                    <div class="row">

{{--                                        @if(count($language_allows) > 0)--}}
{{--                                            @foreach($language_allows as $language_allow)--}}
{{--                                                <div class="col-md-6 form-group">--}}
{{--                                                    <label>{{'العنوان'}} {{ "(". $language_allow->code .")"}} </label>--}}
{{--                                                    <input name="titles[{{$language_allow->code}}]" value="{{ isset($view_languages[$language_allow->code]) ? $view_languages[$language_allow->code]->title : null }}" class="form-control" type="text" placeholder="" required>--}}
{{--                                                </div>--}}
{{--                                            @endforeach--}}
{{--                                        @endif--}}

{{--                                        @if(count($language_allows) > 0)--}}
{{--                                            @foreach($language_allows as $language_allow)--}}
{{--                                            <div class="col-md-6 form-group">--}}
{{--                                                <label>{{'التفاصيل'}} {{ "(". $language_allow->code .")"}} </label>--}}
{{--                                                <textarea name="texts[{{$language_allow->code}}]" class="form-control" required>{{ isset($view_languages[$language_allow->code]) ? $view_languages[$language_allow->code]->text : null }}</textarea>--}}
{{--                                            </div>--}}
{{--                                            @endforeach--}}
{{--                                        @endif--}}

                                        <div class="col-md-12 form-group">
                                            <label>{{'اللينك'}}</label>
                                            <input name="link" value="{{ $client->link }}" class="form-control" type="text" placeholder="" required>
                                        </div>

                                        <div class="col-md-12 form-group">
                                            <label>الصورة</label>
                                            <input type="file" class="dropify" name="image"
                                                   data-default-file="{{ GetImg($client->image) }}">
                                        </div>

                                    </div>


                                </div>

                                <div class="card-footer">
                                    <div class="col-sm-12">
                                        <button id="submit-form" type="submit" class="btn ripple btn-primary btn-lg">
                                            <i class="fa fa-save loading-submit"></i>
                                            حفظ
                                        </button>
                                    </div>
                                </div>

                            </div>
                        </div>

                    </form>

                </div>
            </div>
        </div>
    </div>

@endsection
{{-------------------- end content --------------------}}


{{-------------------- scripts --------------------}}
@push('admin_css')


@endpush


