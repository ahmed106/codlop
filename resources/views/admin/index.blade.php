@extends('admin.layouts.layout')

@php
    $page_title = '';
@endphp

@section('page-title')
    الصفحة الرئيسية
@endsection


@push('admin_css')


@endpush



{{-------------------- start content --------------------}}
@section('content')


    <div class="main-content side-content pt-0">
        <div class="container-fluid">
            <div class="inner-body">

                <div class="page-header">
                    @include('admin.inc.page_header' , ['page_title' => $page_title])

                </div>
                <!-- End Page Header -->


                <!--Row-->
                <div class="row row-sm">


                    <div class="col-sm-12 col-lg-12 col-xl-8">

                        <!--Row-->
                        <div class="row row-sm">

                            {{-- =========================================== --}}
                            <div class="col-sm-12 col-md-6 col-lg-6 col-xl-4">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="card-item">
                                            <div class="card-item-icon card-icon">
                                                <svg class="text-primary"
                                                     xmlns="http://www.w3.org/2000/svg"
                                                     enable-background="new 0 0 24 24" height="24"
                                                     viewBox="0 0 24 24" width="24">
                                                    <g>
                                                        <rect height="14" opacity=".3" width="14" x="5" y="5">
                                                        </rect>
                                                        <g>
                                                            <rect fill="none" height="24" width="24"></rect>
                                                            <g>
                                                                <path
                                                                    d="M19,3H5C3.9,3,3,3.9,3,5v14c0,1.1,0.9,2,2,2h14c1.1,0,2-0.9,2-2V5C21,3.9,20.1,3,19,3z M19,19H5V5h14V19z">
                                                                </path>
                                                                <rect height="5" width="2" x="7" y="12"></rect>
                                                                <rect height="10" width="2" x="15" y="7"></rect>
                                                                <rect height="3" width="2" x="11" y="14"></rect>
                                                                <rect height="2" width="2" x="11" y="10"></rect>
                                                            </g>
                                                        </g>
                                                    </g>
                                                </svg>
                                            </div>
                                            <div class="card-item-title mb-2">
                                                <label class="main-content-label tx-13 font-weight-bold mb-1">Total
                                                    Revenue</label>
                                                <span
                                                    class="d-block tx-12 mb-0 text-muted">Previous month vs thismonths</span>
                                            </div>
                                            <div class="card-item-body">
                                                <div class="card-item-stat">
                                                    <h4 class="font-weight-bold">$5,900.00</h4>
                                                    <small><b class="text-success">55%</b> higher</small>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            {{-- =========================================== --}}
                            <div class="col-sm-12 col-md-6 col-lg-6 col-xl-4">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="card-item">
                                            <div class="card-item-icon card-icon">
                                                <svg class="text-primary"
                                                     xmlns="http://www.w3.org/2000/svg"
                                                     enable-background="new 0 0 24 24" height="24"
                                                     viewBox="0 0 24 24" width="24">
                                                    <g>
                                                        <rect height="14" opacity=".3" width="14" x="5" y="5">
                                                        </rect>
                                                        <g>
                                                            <rect fill="none" height="24" width="24"></rect>
                                                            <g>
                                                                <path
                                                                    d="M19,3H5C3.9,3,3,3.9,3,5v14c0,1.1,0.9,2,2,2h14c1.1,0,2-0.9,2-2V5C21,3.9,20.1,3,19,3z M19,19H5V5h14V19z">
                                                                </path>
                                                                <rect height="5" width="2" x="7" y="12"></rect>
                                                                <rect height="10" width="2" x="15" y="7"></rect>
                                                                <rect height="3" width="2" x="11" y="14"></rect>
                                                                <rect height="2" width="2" x="11" y="10"></rect>
                                                            </g>
                                                        </g>
                                                    </g>
                                                </svg>
                                            </div>
                                            <div class="card-item-title mb-2">
                                                <label class="main-content-label tx-13 font-weight-bold mb-1">Total
                                                    Revenue</label>
                                                <span
                                                    class="d-block tx-12 mb-0 text-muted">Previous month vs thismonths</span>
                                            </div>
                                            <div class="card-item-body">
                                                <div class="card-item-stat">
                                                    <h4 class="font-weight-bold">$5,900.00</h4>
                                                    <small><b class="text-success">55%</b> higher</small>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-6 col-lg-6 col-xl-4">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="card-item">
                                            <div class="card-item-icon card-icon">
                                                <svg class="text-primary"
                                                     xmlns="http://www.w3.org/2000/svg"
                                                     enable-background="new 0 0 24 24" height="24"
                                                     viewBox="0 0 24 24" width="24">
                                                    <g>
                                                        <rect height="14" opacity=".3" width="14" x="5" y="5">
                                                        </rect>
                                                        <g>
                                                            <rect fill="none" height="24" width="24"></rect>
                                                            <g>
                                                                <path
                                                                    d="M19,3H5C3.9,3,3,3.9,3,5v14c0,1.1,0.9,2,2,2h14c1.1,0,2-0.9,2-2V5C21,3.9,20.1,3,19,3z M19,19H5V5h14V19z">
                                                                </path>
                                                                <rect height="5" width="2" x="7" y="12"></rect>
                                                                <rect height="10" width="2" x="15" y="7"></rect>
                                                                <rect height="3" width="2" x="11" y="14"></rect>
                                                                <rect height="2" width="2" x="11" y="10"></rect>
                                                            </g>
                                                        </g>
                                                    </g>
                                                </svg>
                                            </div>
                                            <div class="card-item-title mb-2">
                                                <label class="main-content-label tx-13 font-weight-bold mb-1">Total
                                                    Revenue</label>
                                                <span
                                                    class="d-block tx-12 mb-0 text-muted">Previous month vs thismonths</span>
                                            </div>
                                            <div class="card-item-body">
                                                <div class="card-item-stat">
                                                    <h4 class="font-weight-bold">$5,900.00</h4>
                                                    <small><b class="text-success">55%</b> higher</small>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-6 col-lg-6 col-xl-4">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="card-item">
                                            <div class="card-item-icon card-icon">
                                                <svg class="text-primary"
                                                     xmlns="http://www.w3.org/2000/svg"
                                                     enable-background="new 0 0 24 24" height="24"
                                                     viewBox="0 0 24 24" width="24">
                                                    <g>
                                                        <rect height="14" opacity=".3" width="14" x="5" y="5">
                                                        </rect>
                                                        <g>
                                                            <rect fill="none" height="24" width="24"></rect>
                                                            <g>
                                                                <path
                                                                    d="M19,3H5C3.9,3,3,3.9,3,5v14c0,1.1,0.9,2,2,2h14c1.1,0,2-0.9,2-2V5C21,3.9,20.1,3,19,3z M19,19H5V5h14V19z">
                                                                </path>
                                                                <rect height="5" width="2" x="7" y="12"></rect>
                                                                <rect height="10" width="2" x="15" y="7"></rect>
                                                                <rect height="3" width="2" x="11" y="14"></rect>
                                                                <rect height="2" width="2" x="11" y="10"></rect>
                                                            </g>
                                                        </g>
                                                    </g>
                                                </svg>
                                            </div>
                                            <div class="card-item-title mb-2">
                                                <label class="main-content-label tx-13 font-weight-bold mb-1">Total
                                                    Revenue</label>
                                                <span
                                                    class="d-block tx-12 mb-0 text-muted">Previous month vs thismonths</span>
                                            </div>
                                            <div class="card-item-body">
                                                <div class="card-item-stat">
                                                    <h4 class="font-weight-bold">$5,900.00</h4>
                                                    <small><b class="text-success">55%</b> higher</small>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-6 col-lg-6 col-xl-4">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="card-item">
                                            <div class="card-item-icon card-icon">
                                                <svg class="text-primary"
                                                     xmlns="http://www.w3.org/2000/svg"
                                                     enable-background="new 0 0 24 24" height="24"
                                                     viewBox="0 0 24 24" width="24">
                                                    <g>
                                                        <rect height="14" opacity=".3" width="14" x="5" y="5">
                                                        </rect>
                                                        <g>
                                                            <rect fill="none" height="24" width="24"></rect>
                                                            <g>
                                                                <path
                                                                    d="M19,3H5C3.9,3,3,3.9,3,5v14c0,1.1,0.9,2,2,2h14c1.1,0,2-0.9,2-2V5C21,3.9,20.1,3,19,3z M19,19H5V5h14V19z">
                                                                </path>
                                                                <rect height="5" width="2" x="7" y="12"></rect>
                                                                <rect height="10" width="2" x="15" y="7"></rect>
                                                                <rect height="3" width="2" x="11" y="14"></rect>
                                                                <rect height="2" width="2" x="11" y="10"></rect>
                                                            </g>
                                                        </g>
                                                    </g>
                                                </svg>
                                            </div>
                                            <div class="card-item-title mb-2">
                                                <label class="main-content-label tx-13 font-weight-bold mb-1">Total
                                                    Revenue</label>
                                                <span
                                                    class="d-block tx-12 mb-0 text-muted">Previous month vs thismonths</span>
                                            </div>
                                            <div class="card-item-body">
                                                <div class="card-item-stat">
                                                    <h4 class="font-weight-bold">$5,900.00</h4>
                                                    <small><b class="text-success">55%</b> higher</small>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-6 col-lg-6 col-xl-4">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="card-item">
                                            <div class="card-item-icon card-icon">
                                                <svg class="text-primary"
                                                     xmlns="http://www.w3.org/2000/svg"
                                                     enable-background="new 0 0 24 24" height="24"
                                                     viewBox="0 0 24 24" width="24">
                                                    <g>
                                                        <rect height="14" opacity=".3" width="14" x="5" y="5">
                                                        </rect>
                                                        <g>
                                                            <rect fill="none" height="24" width="24"></rect>
                                                            <g>
                                                                <path
                                                                    d="M19,3H5C3.9,3,3,3.9,3,5v14c0,1.1,0.9,2,2,2h14c1.1,0,2-0.9,2-2V5C21,3.9,20.1,3,19,3z M19,19H5V5h14V19z">
                                                                </path>
                                                                <rect height="5" width="2" x="7" y="12"></rect>
                                                                <rect height="10" width="2" x="15" y="7"></rect>
                                                                <rect height="3" width="2" x="11" y="14"></rect>
                                                                <rect height="2" width="2" x="11" y="10"></rect>
                                                            </g>
                                                        </g>
                                                    </g>
                                                </svg>
                                            </div>
                                            <div class="card-item-title mb-2">
                                                <label class="main-content-label tx-13 font-weight-bold mb-1">Total
                                                    Revenue</label>
                                                <span
                                                    class="d-block tx-12 mb-0 text-muted">Previous month vs thismonths</span>
                                            </div>
                                            <div class="card-item-body">
                                                <div class="card-item-stat">
                                                    <h4 class="font-weight-bold">$5,900.00</h4>
                                                    <small><b class="text-success">55%</b> higher</small>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-6 col-lg-6 col-xl-4">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="card-item">
                                            <div class="card-item-icon card-icon">
                                                <svg class="text-primary"
                                                     xmlns="http://www.w3.org/2000/svg"
                                                     enable-background="new 0 0 24 24" height="24"
                                                     viewBox="0 0 24 24" width="24">
                                                    <g>
                                                        <rect height="14" opacity=".3" width="14" x="5" y="5">
                                                        </rect>
                                                        <g>
                                                            <rect fill="none" height="24" width="24"></rect>
                                                            <g>
                                                                <path
                                                                    d="M19,3H5C3.9,3,3,3.9,3,5v14c0,1.1,0.9,2,2,2h14c1.1,0,2-0.9,2-2V5C21,3.9,20.1,3,19,3z M19,19H5V5h14V19z">
                                                                </path>
                                                                <rect height="5" width="2" x="7" y="12"></rect>
                                                                <rect height="10" width="2" x="15" y="7"></rect>
                                                                <rect height="3" width="2" x="11" y="14"></rect>
                                                                <rect height="2" width="2" x="11" y="10"></rect>
                                                            </g>
                                                        </g>
                                                    </g>
                                                </svg>
                                            </div>
                                            <div class="card-item-title mb-2">
                                                <label class="main-content-label tx-13 font-weight-bold mb-1">Total
                                                    Revenue</label>
                                                <span
                                                    class="d-block tx-12 mb-0 text-muted">Previous month vs thismonths</span>
                                            </div>
                                            <div class="card-item-body">
                                                <div class="card-item-stat">
                                                    <h4 class="font-weight-bold">$5,900.00</h4>
                                                    <small><b class="text-success">55%</b> higher</small>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-6 col-lg-6 col-xl-4">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="card-item">
                                            <div class="card-item-icon card-icon">
                                                <svg class="text-primary"
                                                     xmlns="http://www.w3.org/2000/svg"
                                                     enable-background="new 0 0 24 24" height="24"
                                                     viewBox="0 0 24 24" width="24">
                                                    <g>
                                                        <rect height="14" opacity=".3" width="14" x="5" y="5">
                                                        </rect>
                                                        <g>
                                                            <rect fill="none" height="24" width="24"></rect>
                                                            <g>
                                                                <path
                                                                    d="M19,3H5C3.9,3,3,3.9,3,5v14c0,1.1,0.9,2,2,2h14c1.1,0,2-0.9,2-2V5C21,3.9,20.1,3,19,3z M19,19H5V5h14V19z">
                                                                </path>
                                                                <rect height="5" width="2" x="7" y="12"></rect>
                                                                <rect height="10" width="2" x="15" y="7"></rect>
                                                                <rect height="3" width="2" x="11" y="14"></rect>
                                                                <rect height="2" width="2" x="11" y="10"></rect>
                                                            </g>
                                                        </g>
                                                    </g>
                                                </svg>
                                            </div>
                                            <div class="card-item-title mb-2">
                                                <label class="main-content-label tx-13 font-weight-bold mb-1">Total
                                                    Revenue</label>
                                                <span
                                                    class="d-block tx-12 mb-0 text-muted">Previous month vs thismonths</span>
                                            </div>
                                            <div class="card-item-body">
                                                <div class="card-item-stat">
                                                    <h4 class="font-weight-bold">$5,900.00</h4>
                                                    <small><b class="text-success">55%</b> higher</small>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-6 col-lg-6 col-xl-4">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="card-item">
                                            <div class="card-item-icon card-icon">
                                                <svg class="text-primary"
                                                     xmlns="http://www.w3.org/2000/svg"
                                                     enable-background="new 0 0 24 24" height="24"
                                                     viewBox="0 0 24 24" width="24">
                                                    <g>
                                                        <rect height="14" opacity=".3" width="14" x="5" y="5">
                                                        </rect>
                                                        <g>
                                                            <rect fill="none" height="24" width="24"></rect>
                                                            <g>
                                                                <path
                                                                    d="M19,3H5C3.9,3,3,3.9,3,5v14c0,1.1,0.9,2,2,2h14c1.1,0,2-0.9,2-2V5C21,3.9,20.1,3,19,3z M19,19H5V5h14V19z">
                                                                </path>
                                                                <rect height="5" width="2" x="7" y="12"></rect>
                                                                <rect height="10" width="2" x="15" y="7"></rect>
                                                                <rect height="3" width="2" x="11" y="14"></rect>
                                                                <rect height="2" width="2" x="11" y="10"></rect>
                                                            </g>
                                                        </g>
                                                    </g>
                                                </svg>
                                            </div>
                                            <div class="card-item-title mb-2">
                                                <label class="main-content-label tx-13 font-weight-bold mb-1">Total
                                                    Revenue</label>
                                                <span
                                                    class="d-block tx-12 mb-0 text-muted">Previous month vs thismonths</span>
                                            </div>
                                            <div class="card-item-body">
                                                <div class="card-item-stat">
                                                    <h4 class="font-weight-bold">$5,900.00</h4>
                                                    <small><b class="text-success">55%</b> higher</small>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <!--End row-->

                    </div>
                    <!-- col end -->


                    <div class="col-sm-12 col-lg-12 col-xl-4">

                        <div class="card card-dashboard-calendar pb-0"><label
                                class="main-content-label mb-2 pt-1">Recent transcations</label> <span
                                class="d-block tx-12 mb-2 text-muted">Projects where development work is on
                                    completion</span>
                            <div class="table-responsive">
                                <table class="table table-hover m-b-0 transcations mt-2">
                                    <tbody>


                                    {{-- clients --}}
                                    <tr>
                                        <td class="wd-5p">
                                            <div class="main-img-user avatar-md"><img alt="avatar"
                                                                                      class="rounded-circle me-3"
                                                                                      src="{{asset('dashboard')}}/assets/img/users/5.jpg">
                                            </div>
                                        </td>
                                        <td>
                                            <div class="d-flex align-middle ms-3">
                                                <div class="d-inline-block">
                                                    <h6 class="mb-1">Flicker</h6>
                                                    <p class="mb-0 tx-13 text-muted">App improvement</p>
                                                </div>
                                            </div>
                                        </td>
                                        <td class="text-end">
                                            <div class="d-inline-block">
                                                <h6 class="mb-2 tx-15 font-weight-semibold">$45.234<i
                                                        class="fas fa-level-up-alt ms-2 text-success m-l-10"></i>
                                                </h6>
                                                <p class="mb-0 tx-11 text-muted">12 Jan 2020</p>
                                            </div>
                                        </td>
                                    </tr>
                                    {{-- clients --}}

                                    <tr>
                                        <td class="wd-5p">
                                            <div class="main-img-user avatar-md"><img alt="avatar"
                                                                                      class="rounded-circle me-3"
                                                                                      src="{{asset('dashboard')}}/assets/img/users/5.jpg">
                                            </div>
                                        </td>
                                        <td>
                                            <div class="d-flex align-middle ms-3">
                                                <div class="d-inline-block">
                                                    <h6 class="mb-1">Flicker</h6>
                                                    <p class="mb-0 tx-13 text-muted">App improvement</p>
                                                </div>
                                            </div>
                                        </td>
                                        <td class="text-end">
                                            <div class="d-inline-block">
                                                <h6 class="mb-2 tx-15 font-weight-semibold">$45.234<i
                                                        class="fas fa-level-up-alt ms-2 text-success m-l-10"></i>
                                                </h6>
                                                <p class="mb-0 tx-11 text-muted">12 Jan 2020</p>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="wd-5p">
                                            <div class="main-img-user avatar-md"><img alt="avatar"
                                                                                      class="rounded-circle me-3"
                                                                                      src="{{asset('dashboard')}}/assets/img/users/5.jpg">
                                            </div>
                                        </td>
                                        <td>
                                            <div class="d-flex align-middle ms-3">
                                                <div class="d-inline-block">
                                                    <h6 class="mb-1">Flicker</h6>
                                                    <p class="mb-0 tx-13 text-muted">App improvement</p>
                                                </div>
                                            </div>
                                        </td>
                                        <td class="text-end">
                                            <div class="d-inline-block">
                                                <h6 class="mb-2 tx-15 font-weight-semibold">$45.234<i
                                                        class="fas fa-level-up-alt ms-2 text-success m-l-10"></i>
                                                </h6>
                                                <p class="mb-0 tx-11 text-muted">12 Jan 2020</p>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="wd-5p">
                                            <div class="main-img-user avatar-md"><img alt="avatar"
                                                                                      class="rounded-circle me-3"
                                                                                      src="{{asset('dashboard')}}/assets/img/users/5.jpg">
                                            </div>
                                        </td>
                                        <td>
                                            <div class="d-flex align-middle ms-3">
                                                <div class="d-inline-block">
                                                    <h6 class="mb-1">Flicker</h6>
                                                    <p class="mb-0 tx-13 text-muted">App improvement</p>
                                                </div>
                                            </div>
                                        </td>
                                        <td class="text-end">
                                            <div class="d-inline-block">
                                                <h6 class="mb-2 tx-15 font-weight-semibold">$45.234<i
                                                        class="fas fa-level-up-alt ms-2 text-success m-l-10"></i>
                                                </h6>
                                                <p class="mb-0 tx-11 text-muted">12 Jan 2020</p>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="wd-5p">
                                            <div class="main-img-user avatar-md"><img alt="avatar"
                                                                                      class="rounded-circle me-3"
                                                                                      src="{{asset('dashboard')}}/assets/img/users/5.jpg">
                                            </div>
                                        </td>
                                        <td>
                                            <div class="d-flex align-middle ms-3">
                                                <div class="d-inline-block">
                                                    <h6 class="mb-1">Flicker</h6>
                                                    <p class="mb-0 tx-13 text-muted">App improvement</p>
                                                </div>
                                            </div>
                                        </td>
                                        <td class="text-end">
                                            <div class="d-inline-block">
                                                <h6 class="mb-2 tx-15 font-weight-semibold">$45.234<i
                                                        class="fas fa-level-up-alt ms-2 text-success m-l-10"></i>
                                                </h6>
                                                <p class="mb-0 tx-11 text-muted">12 Jan 2020</p>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="wd-5p">
                                            <div class="main-img-user avatar-md"><img alt="avatar"
                                                                                      class="rounded-circle me-3"
                                                                                      src="{{asset('dashboard')}}/assets/img/users/5.jpg">
                                            </div>
                                        </td>
                                        <td>
                                            <div class="d-flex align-middle ms-3">
                                                <div class="d-inline-block">
                                                    <h6 class="mb-1">Flicker</h6>
                                                    <p class="mb-0 tx-13 text-muted">App improvement</p>
                                                </div>
                                            </div>
                                        </td>
                                        <td class="text-end">
                                            <div class="d-inline-block">
                                                <h6 class="mb-2 tx-15 font-weight-semibold">$45.234<i
                                                        class="fas fa-level-up-alt ms-2 text-success m-l-10"></i>
                                                </h6>
                                                <p class="mb-0 tx-11 text-muted">12 Jan 2020</p>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="wd-5p">
                                            <div class="main-img-user avatar-md"><img alt="avatar"
                                                                                      class="rounded-circle me-3"
                                                                                      src="{{asset('dashboard')}}/assets/img/users/5.jpg">
                                            </div>
                                        </td>
                                        <td>
                                            <div class="d-flex align-middle ms-3">
                                                <div class="d-inline-block">
                                                    <h6 class="mb-1">Flicker</h6>
                                                    <p class="mb-0 tx-13 text-muted">App improvement</p>
                                                </div>
                                            </div>
                                        </td>
                                        <td class="text-end">
                                            <div class="d-inline-block">
                                                <h6 class="mb-2 tx-15 font-weight-semibold">$45.234<i
                                                        class="fas fa-level-up-alt ms-2 text-success m-l-10"></i>
                                                </h6>
                                                <p class="mb-0 tx-11 text-muted">12 Jan 2020</p>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="wd-5p">
                                            <div class="main-img-user avatar-md"><img alt="avatar"
                                                                                      class="rounded-circle me-3"
                                                                                      src="{{asset('dashboard')}}/assets/img/users/5.jpg">
                                            </div>
                                        </td>
                                        <td>
                                            <div class="d-flex align-middle ms-3">
                                                <div class="d-inline-block">
                                                    <h6 class="mb-1">Flicker</h6>
                                                    <p class="mb-0 tx-13 text-muted">App improvement</p>
                                                </div>
                                            </div>
                                        </td>
                                        <td class="text-end">
                                            <div class="d-inline-block">
                                                <h6 class="mb-2 tx-15 font-weight-semibold">$45.234<i
                                                        class="fas fa-level-up-alt ms-2 text-success m-l-10"></i>
                                                </h6>
                                                <p class="mb-0 tx-11 text-muted">12 Jan 2020</p>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="wd-5p">
                                            <div class="main-img-user avatar-md"><img alt="avatar"
                                                                                      class="rounded-circle me-3"
                                                                                      src="{{asset('dashboard')}}/assets/img/users/5.jpg">
                                            </div>
                                        </td>
                                        <td>
                                            <div class="d-flex align-middle ms-3">
                                                <div class="d-inline-block">
                                                    <h6 class="mb-1">Flicker</h6>
                                                    <p class="mb-0 tx-13 text-muted">App improvement</p>
                                                </div>
                                            </div>
                                        </td>
                                        <td class="text-end">
                                            <div class="d-inline-block">
                                                <h6 class="mb-2 tx-15 font-weight-semibold">$45.234<i
                                                        class="fas fa-level-up-alt ms-2 text-success m-l-10"></i>
                                                </h6>
                                                <p class="mb-0 tx-11 text-muted">12 Jan 2020</p>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="wd-5p">
                                            <div class="main-img-user avatar-md"><img alt="avatar"
                                                                                      class="rounded-circle me-3"
                                                                                      src="{{asset('dashboard')}}/assets/img/users/5.jpg">
                                            </div>
                                        </td>
                                        <td>
                                            <div class="d-flex align-middle ms-3">
                                                <div class="d-inline-block">
                                                    <h6 class="mb-1">Flicker</h6>
                                                    <p class="mb-0 tx-13 text-muted">App improvement</p>
                                                </div>
                                            </div>
                                        </td>
                                        <td class="text-end">
                                            <div class="d-inline-block">
                                                <h6 class="mb-2 tx-15 font-weight-semibold">$45.234<i
                                                        class="fas fa-level-up-alt ms-2 text-success m-l-10"></i>
                                                </h6>
                                                <p class="mb-0 tx-11 text-muted">12 Jan 2020</p>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="wd-5p">
                                            <div class="main-img-user avatar-md"><img alt="avatar"
                                                                                      class="rounded-circle me-3"
                                                                                      src="{{asset('dashboard')}}/assets/img/users/5.jpg">
                                            </div>
                                        </td>
                                        <td>
                                            <div class="d-flex align-middle ms-3">
                                                <div class="d-inline-block">
                                                    <h6 class="mb-1">Flicker</h6>
                                                    <p class="mb-0 tx-13 text-muted">App improvement</p>
                                                </div>
                                            </div>
                                        </td>
                                        <td class="text-end">
                                            <div class="d-inline-block">
                                                <h6 class="mb-2 tx-15 font-weight-semibold">$45.234<i
                                                        class="fas fa-level-up-alt ms-2 text-success m-l-10"></i>
                                                </h6>
                                                <p class="mb-0 tx-11 text-muted">12 Jan 2020</p>
                                            </div>
                                        </td>
                                    </tr>

                                    </tbody>
                                </table>
                            </div>
                        </div>

                    </div>
                    <!-- col end -->


                </div><!-- Row end -->
            </div>
        </div>
    </div>


@endsection
{{-------------------- end content --------------------}}


{{-------------------- scripts --------------------}}
@push('admin_js')


@endpush


