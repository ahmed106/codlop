<!DOCTYPE html>
<html lang="en" id="demo">

<head>
    @include('admin.inc._css')
</head>

<body class="main-body" cz-shortcut-listen="true">

<!-- Loader -->
@include('admin.inc._loader')
<!-- End Loader -->

<!-- Page -->
<div class="page">

    {{--Sidemenu--}}
    @if(request()->segment(1) !== 'power' && request()->segment(1) !='power-qr' )
        @include('admin.inc.side_menu')
    @endif
    {{--End Sidemenu--}}


    {{-- Main Header--}}
    @if(request()->segment(1) != 'power'&& request()->segment(1) !='power-qr' )
        @include('admin.inc.main_header')
    @endif
    {{-- Mobile-header --}}


    {{-- Main Content--}}
    @yield('content')
    {{-- End Main Content--}}


    {{-- Main Footer--}}
    @if(request()->segment(1) != 'power'&& request()->segment(1) !='power-qr' )
        @include('admin.inc.main_footer')
    @endif
    {{--End Footer--}}

</div>
{{-- End Page --}}

{{-- Back-to-top --}}
@include('admin.inc.back_to_top')

@include('admin.inc._js')

</body>

</html>
