<meta charset="utf-8">
<meta content="width=device-width, initial-scale=1, shrink-to-fit=no" name="viewport">
<meta name="description" content="Codlop - Dashboard">
<link rel="icon" href="{{asset('dashboard')}}/assets/img/logo-icon.svg" type="image/x-icon">
<title>@yield('page-title')</title>
<link id="style" href="{{asset('dashboard')}}/assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
<link href="{{asset('dashboard')}}/assets/plugins/web-fonts/icons.css" rel="stylesheet">
<link href="{{asset('dashboard')}}/assets/plugins/web-fonts/font-awesome/font-awesome.min.css" rel="stylesheet">
<link href="{{asset('dashboard')}}/assets/plugins/web-fonts/plugin.css" rel="stylesheet">
<link href="{{asset('dashboard')}}/assets/plugins/select2/css/select2.min.css" rel="stylesheet">
<link rel="stylesheet" href="{{asset('dashboard')}}/assets/plugins/multipleselect/multiple-select.css">
<link rel="stylesheet" href="{{asset('dashboard')}}/assets/plugins/datatable/css/jquery.dataTables.css"/>
<link rel="stylesheet" href="{{asset('dashboard')}}/assets/plugins/filepond/filepond.css"/>
<link rel="stylesheet" href="{{asset('dashboard')}}/assets/plugins/filepond/filepond-plugin-image-preview.css"/>

{{-- upload image --}}
<link href="https://cdnjs.cloudflare.com/ajax/libs/Dropify/0.2.2/css/dropify.min.css" rel="stylesheet" type="text/css"/>


<link href="{{asset('dashboard')}}/assets/css/style.css" rel="stylesheet">
<link href="{{asset('dashboard')}}/assets/css/skins.css" rel="stylesheet">
<link href="{{asset('dashboard')}}/assets/css/colors/default.css" rel="stylesheet">
<link href="{{asset('dashboard')}}/assets/css/colors/color2.css" rel="stylesheet">
@toastr_css

<style>
    .alert-message {
        font-size: 15px !important;
        color: #666;
        /* font-family: "Open Sans", sans-serif; */
        font-weight: 400;
        font-size: 15px;
        text-align: center;
        margin-bottom: 35px;
        line-height: 1.6;
        align-self: center;
        font-size: 15px !important;
    }

    .check-div {
        display: flex;
        align-items: center;
        gap: 8px;
        margin-top: 6px;
    }

    .check-div input{
        width: 24px;
        height: 24px;
    }

    .check-div label {
        font-size: 21px;
        margin: 0;
        color: #f44336;
    }
</style>

{{-- cute-alert --}}
<link id="layoutstyle" rel="stylesheet" href="{{asset('toastr')}}/cute-alert.css">

@stack('admin_css')
