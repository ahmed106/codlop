<!-- NotFound -->
<div class="card">
    <div class="card-body">
        <div class="NotFound justify-content-center" style="max-width: 300px;margin: 0 auto;text-align: center;">
            <img src="{{asset('website')}}/default-images/not-found.jpg">
            <h3>لا يوجد عناصر لعرضها</h3>
        </div>
    </div>
</div>
<!-- NotFound -->
