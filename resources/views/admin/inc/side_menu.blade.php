<div class="main-sidebar main-sidebar-sticky side-menu">
    <div class="sidemenu-logo">
        <a class="main-logo" href="{{ aurl('/') }}">
            <img src="{{asset('dashboard')}}/assets/img/logo.svg" class="header-brand-img" alt="logo">
        </a>
    </div>
    <div class="main-sidebar-body">
        <ul class="nav">

            <li class="nav-item {{ in_array(request()->segment(2), ['slider', 'contract_advantage','polices','privacy','roles-conditions']) || ( in_array(request()->segment(2), ['site_text']) && request()->get('site_text_id') ) ? 'show active' : '' }}">
                <a class="nav-link with-sub" href="#">
                    <span class="shape1"></span>
                    <span class="shape2"></span>
                    <i class="ti-home sidemenu-icon"></i>
                    <span class="sidemenu-label">الصفحة الرئيسية</span>
                    <i class="angle fe fe-chevron-left"></i>
                </a>
                <ul class="nav-sub">
                    <li class="nav-sub-item {{ request()->segment(2) == 'slider' ? 'show active' : '' }}"><a class="nav-sub-link" href="{{ aurl('slider') }}">الاسلايدر</a></li>
                    @if(count(site_texts('home')) > 0)
                        @foreach(site_texts('home') as $site_text)
                            <li class="nav-sub-item {{ request()->get('site_text_id') == $site_text->id ? 'active' : '' }}"><a class="nav-sub-link" href="{{ aurl('site_text').'/'.$site_text->id.'/edit?page=home&site_text_id='.$site_text->id }}">{{ $site_text->title }}</a></li>
                        @endforeach
                    @endif
                    <li class="nav-sub-item {{ request()->segment(2) == 'contract_advantage' ? 'show active' : '' }}"><a class="nav-sub-link" href="{{ aurl('contract_advantage') }}">ميزات التعاقد</a></li>
                    <li class="nav-sub-item {{ request()->segment(2) == 'polices' ? 'show active' : '' }}"><a class="nav-sub-link" href="{{aurl('polices')}}">سياسه الإستخدام</a></li>
                    <li class="nav-sub-item {{ request()->segment(2) == 'privacy' ? 'show active' : '' }}"><a class="nav-sub-link" href="{{aurl('privacy')}}">سياسه الخصوصيه</a></li>
                    <li class="nav-sub-item {{ request()->segment(2) == 'roles-conditions' ? 'show active' : '' }}"><a class="nav-sub-link" href="{{route('roles-conditions.index')}}">الشروط و الأحكام</a></li>
                </ul>
            </li>

            <li class="nav-item {{ request()->segment(2)  == 'admin' ? 'show active' : '' }}">
                <a class="nav-link" href="{{ aurl('admin') }}">
                    <span class="shape1"></span>
                    <span class="shape2"></span>
                    <i class="ti-user sidemenu-icon"></i>
                    <span class="sidemenu-label">المشرفين</span>
                </a>
            </li>

            <li class="nav-item {{ request()->segment(2)  == 'teams' ? 'show active' : '' }}">
                <a class="nav-link" href="{{ aurl('teams') }}">
                    <span class="shape1"></span>
                    <span class="shape2"></span>
                    <i class="ti-user sidemenu-icon"></i>
                    <span class="sidemenu-label">فريق العمل</span>
                </a>
            </li>

            <li class="nav-item {{ request()->segment(2)  == 'settings' ? 'show active' : '' }}">
                <a class="nav-link" href="{{ aurl('settings') }}">
                    <span class="shape1"></span>
                    <span class="shape2"></span>
                    <i class="ti-settings sidemenu-icon"></i>
                    <span class="sidemenu-label">اعدادات الموقع</span></a>
            </li>

            <li class="nav-item {{ request()->segment(2) == 'site_text'  && !request()->get('site_text_id') ? 'show active' : '' }}">
                <a class="nav-link with-sub" href="#">
                    <span class="shape1"></span>
                    <span class="shape2"></span>
                    <i class="ti-text sidemenu-icon"></i>
                    <span class="sidemenu-label">نصوص الموقع</span>
                    <i class="angle fe fe-chevron-left"></i>
                </a>
                <ul class="nav-sub">
                    <li class="nav-sub-item {{ request()->segment(2) == 'site_text' ? 'active' : '' }}"><a class="nav-sub-link" href="{{ aurl('site_text') }}">نصوص الموقع</a></li>
                </ul>
            </li>

            <li class="nav-item {{ in_array(request()->segment(2), ['service', 'service_text'])  ? 'show active' : '' }}">
                <a class="nav-link with-sub" href="#">
                    <span class="shape1"></span>
                    <span class="shape2"></span>
                    <i class="ti-layout sidemenu-icon"></i>
                    <span class="sidemenu-label">الخدمات</span>
                    <i class="angle fe fe-chevron-left"></i>
                </a>
                <ul class="nav-sub">
                    <li class="nav-sub-item {{ in_array(request()->segment(2), ['service', 'service_text']) && !request()->get('service_id') ? 'active' : '' }}"><a class="nav-sub-link" href="{{ aurl('service') }}">ادارة الخدمات</a></li>
                    {{--<li class="nav-sub-item"><a class="nav-sub-link" href="{{ aurl('service/create') }}">اضافة خدمة جديدة</a></li>--}}
                    @if(count(all_services()) > 0)
                        @foreach(all_services() as $service)
                            <li class="nav-sub-item {{ request()->segment(2)  == 'service' && request()->get('service_id') == $service->id ? 'active' : '' }}"><a class="nav-sub-link" href="{{ aurl('service').'/'.$service->id.'/edit?page=home&service_id='.$service->id }}">{{ $service->title }}</a></li>
                        @endforeach
                    @endif
                </ul>
            </li>

            <li class="nav-item  {{ request()->segment(2)  == 'offer_detail' ? 'show active' : '' }}">
                <a class="nav-link" href="{{ aurl('offer_detail') }}">
                    <span class="shape1"></span>
                    <span class="shape2"></span>
                    <i class="ti-gift sidemenu-icon"></i>
                    <span class="sidemenu-label"> الأسعار و (الباقات)</span></a>
            </li>

            <li class="nav-item  {{ request()->segment(2)  == 'blog' ? 'show active' : '' }}">
                <a class="nav-link" href="{{ aurl('blog') }}">
                    <span class="shape1"></span>
                    <span class="shape2"></span>
                    <i class="ti-agenda sidemenu-icon"></i>
                    <span class="sidemenu-label">المدونة</span></a>
            </li>

            <li class="nav-item {{ request()->segment(2)  == 'language_allow' ? 'show active' : '' }}">
                <a class="nav-link" href="{{ aurl('language_allow') }}">
                    <span class="shape1"></span>
                    <span class="shape2"></span>
                    <i class="ti-world sidemenu-icon"></i>
                    <span class="sidemenu-label">اللغات</span></a>
            </li>

            <li class="nav-item {{ request()->segment(2)  == 'phone_code_allow' ? 'show active' : '' }}">
                <a class="nav-link" href="{{ aurl('phone_code_allow') }}">
                    <span class="shape1"></span>
                    <span class="shape2"></span>
                    <i class="ti-map-alt sidemenu-icon"></i>
                    <span class="sidemenu-label">الدول</span></a>
            </li>

            <li class="nav-item {{ request()->segment(2)  == 'contact' ? 'show active' : '' }}">
                <a class="nav-link" href="{{ aurl('contact') }}">
                    <span class="shape1"></span>
                    <span class="shape2"></span>
                    <i class="ti-headphone-alt sidemenu-icon"></i>
                    <span class="sidemenu-label">اتصل بنا</span></a>
            </li>

            <li class="nav-item  {{ request()->segment(2) == 'programs' ? 'show active' : '' }}">
                <a class="nav-link" href="{{ aurl('programs') }}">
                    <span class="shape1"></span>
                    <span class="shape2"></span>
                    <i class="ti-shopping-cart-full sidemenu-icon "></i>
                    <span class="sidemenu-label">خدمات اطلب الآن</span></a>
            </li>
            <li class="nav-item {{  in_array(request()->segment(2), ['user', 'user_program']) ? 'show active' : '' }}">
                <a class="nav-link" href="{{ aurl('user') }}">
                    <span class="shape1"></span>
                    <span class="shape2"></span>
                    <i class="ti-user sidemenu-icon"></i>
                    <span class="sidemenu-label">العملاء</span></a>
            </li>

            <li class="nav-item {{  in_array(request()->segment(2), ['client']) ? 'show active' : '' }}">
                <a class="nav-link" href="{{ aurl('client') }}">
                    <span class="shape1"></span>
                    <span class="shape2"></span>
                    <i class="ti-user sidemenu-icon"></i>
                    <span class="sidemenu-label">عملاء نفتخر بهم</span></a>
            </li>

            <li class="nav-item ">
                <a class="nav-link" href="{{route('invoices.index')}}">
                    <span class="shape1"></span>
                    <span class="shape2"></span>
                    <i class="fa fa-paper-plane sidemenu-icon"></i>
                    <span class="sidemenu-label">الفاتوره الالكترونيه </span></a>
            </li>

            <li class="nav-item  {{ request()->segment(2) == 'video_motion' ? 'show active' : '' }}">
                <a class="nav-link" href="{{ aurl('video_motion') }}">
                    <span class="shape1"></span>
                    <span class="shape2"></span>
                    <i class="ti-agenda sidemenu-icon"></i>
                    <span class="sidemenu-label">اعمالنا</span></a>
            </li>


            <li class="nav-item">
                <a class="nav-link" href="{{ aurl('logout') }}">
                    <span class="shape1"></span>
                    <span class="shape2"></span>
                    <i class="ti-share sidemenu-icon"></i>
                    <span class="sidemenu-label">تسجيل خروج</span></a>
            </li>

        </ul>
    </div>
</div>
