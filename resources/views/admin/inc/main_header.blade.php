<div class="main-header side-header sticky sticky-pin">
    <div class="container-fluid">
        <div class="main-header-left"><a class="main-header-menu-icon" href="#" id="mainSidebarToggle"><span></span></a></div>
        <div class="main-header-center">
            <div class="responsive-logo">
                <a href="#">
                    <img src="{{asset('dashboard')}}/assets/img/logo.svg" class="mobile-logo" alt="logo">
                </a>
            </div>
        </div>
        <div class="main-header-right">
            <div class="dropdown d-md-flex"><a class="nav-link icon full-screen-link" href="#"> <i class="fe fe-maximize fullscreen-button fullscreen header-icons"></i> <i class="fe fe-minimize fullscreen-button exit-fullscreen header-icons"></i> </a></div>

            <div class="dropdown main-header-notification notif"><a class="nav-link icon" href="#"> <i class="fe fe-bell header-icons"></i> <span class="badge bg-danger nav-link-badge">{{auth('admin')->user()->unreadNotifications->count() }}</span> </a>
                <div class="dropdown-menu">
                    <div class="header-navheading">
                        <p class="main-notification-text">You have {{auth('admin')->user()->unreadNotifications->count()}} unread notification
                            {{--                            <span class="badge bg-pill bg-primary ms-3">View all</span>--}}
                        </p>
                    </div>

                    <div class="main-notification-list">
                        @foreach(auth('admin')->user()->unreadNotifications as $notification)
                            <a href="{{url('admin/user/'.$notification->data['user_id'])}}">
                                <div class="media new">
                                    <div class="main-img-user online"><img alt="avatar" src="{{asset('dashboard')}}/assets/img/users/5.jpg">
                                    </div>
                                    <div class="media-body">
                                        <p><b> {{$notification->data['author']}}</b> {{$notification->data['message']}}</p>
                                        <span>{{$notification->data['date']}}</span>
                                    </div>
                                </div>
                            </a>

                        @endforeach


                    </div>
                    {{--                    <div class="dropdown-footer"><a href="#">View All Notifications</a></div>--}}
                </div>
            </div>
            <div class="dropdown main-profile-menu">
                <a class="d-flex" href="">
                            <span class="main-img-user">
                                <img alt="avatar" src="{{ GetImg( admin()->image ) }}">
                            </span>
                </a>
                <div class="dropdown-menu">
                    <div class="header-navheading">
                        <h6 class="main-notification-title">{{ admin()->name }}</h6>
                        <p class="main-notification-text">{{ admin()->email }}</p>
                    </div>

                    <a class="dropdown-item" href="#"> <i class="fe fe-edit"></i>تعديل البروفايل</a>
                    <a class="dropdown-item" href="{{ aurl('logout') }}"> <i class="fe fe-power"></i> تسجيل الخروج </a>

                </div>
            </div>
            <!-- Navresponsive closed -->
        </div>
    </div>
</div>
