    <div>
        <h2 class="main-content-title tx-24 mg-b-5"> مرحبا بك في {{ setting('title') }}</h2>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ aurl('/') }}">الرئيسية</a></li>
            @if($page_title)
                <li class="breadcrumb-item active" aria-current="page">{{ $page_title }}</li>
            @endif
        </ol>
    </div>
