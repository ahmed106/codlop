<script src="{{asset('dashboard')}}/assets/plugins/jquery/jquery.min.js"></script>
<script src="{{asset('dashboard')}}/assets/plugins/bootstrap/js/popper.min.js"></script>
<script src="{{asset('dashboard')}}/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
<script src="{{asset('dashboard')}}/assets/plugins/chart.js/Chart.bundle.min.js"></script>
<script src="{{asset('dashboard')}}/assets/plugins/peity/jquery.peity.min.js"></script>
<script src="{{asset('dashboard')}}/assets/plugins/select2/js/select2.min.js"></script>
<script src="{{asset('dashboard')}}/assets/js/select2.js"></script>
<script src="{{asset('dashboard')}}/assets/plugins/perfect-scrollbar/perfect-scrollbar.min.js"></script>
{{--<script src="{{asset('dashboard')}}/assets/plugins/sidemenu/sidemenu.js"></script>--}}
<script src="{{asset('dashboard')}}/assets/plugins/sidebar/sidebar.js"></script>
<script src="{{asset('dashboard')}}/assets/plugins/raphael/raphael.min.js"></script>
<script src="{{asset('dashboard')}}/assets/plugins/morris.js/morris.min.js"></script>
<script src="{{asset('dashboard')}}/assets/js/circle-progress.min.js"></script>
<script src="{{asset('dashboard')}}/assets/js/chart-circle.js"></script>
<script src="{{asset('dashboard')}}/assets/plugins/datatable/js/jquery.dataTables.js"></script>
<script src="{{asset('dashboard')}}/assets/plugins/filepond/filepond.min.js"></script>
<script src="{{asset('dashboard')}}/assets/plugins/filepond/filepond-plugin-image-preview.min.js"></script>
<script src="{{asset('dashboard')}}/assets/plugins/filepond/filepond.jquery.js"></script>

<!-- cute alert -->
<script src="{{asset('toastr')}}/cute-alert.js"></script>

{{--upload image --}}
<script src="https://cdnjs.cloudflare.com/ajax/libs/Dropify/0.2.2/js/dropify.min.js"></script>

<script>
    FilePond.registerPlugin(FilePondPluginImagePreview);
    $('.my-pond').filepond({
        allowMultiple: true,
    });
</script>

<script>

    $('.dropify').dropify({
        messages: {
            'default': 'اضف الصورة من هنا',
            'replace': 'قم بالسحب والإفلات أو النقر للاستبدال',
            'remove': 'ازاله',
            'error': 'عفوًا ، حدث خطأ ما.'
        }
    });


    $('.video-dropify').dropify({
        messages: {
            'default': 'اضف الفيديو من هنا',
            'replace': 'قم بالسحب والإفلات أو النقر للاستبدال',
            'remove': 'ازاله',
            'error': 'عفوًا ، حدث خطأ ما.'
        }
    });

    $(document).on('click', '#submit-form', function () {
        $('.loading-submit').removeClass('fa-save').addClass('fa-spinner fa-spin');
    });

</script>

{{-- ========================  toastr ======================== --}}

<script>

    function makeCuteAlert(title = null, text = null, type = null, redirect_url = '#', timeout = 50000) {
        cuteAlert({
            title: title,
            message: text,
            type: type, // or 'info', 'error', 'warning' , 'question'
            confirmText: "{{ trans('web_lang.Okay') }}",
            cancelText: "{{ trans('web_lang.Cancel') }}"
        }).then((e) => {
            if (e == ("confirm")) {
                window.location.href = redirect_url;
            } else {
            }
        })
    }

    function makeCuteToast(title = null, text = null, type = null, timeout = 50000) {
        cuteToast({
            title: title,
            message: text,
            type: type, // or 'success', 'info', 'error', 'warning'
            timer: timeout
        });
    }

</script>

<script>
    // ______________ PAGE LOADING
    $("#global-loader").fadeOut("slow");


    $(document).on('click', '.nav-link', function (e) {
        // $('.nav-link').parent().removeClass('show');
        $(this).parent().toggleClass('show');
    });


    $(document).on('click', '#mainSidebarToggle', function (event) {
        event.preventDefault();
        if (window.matchMedia('(min-width: 992px)').matches) {
            $('body').toggleClass('main-sidebar-hide');
        } else {
            $('body').toggleClass('main-sidebar-show');
        }
    });
    $(".side-menu").hover(function () {
        if ($('body').hasClass('main-sidebar-hide')) {
            $('body').addClass('main-sidebar-open');
        }
    }, function () {
        if ($('body').hasClass('main-sidebar-hide')) {
            $('body').removeClass('main-sidebar-open');
        }
    });

</script>

{{--
<script src="{{asset('dashboard')}}/assets/plugins/ckeditor/ckeditor.js"></script>
--}}
<script src="https://cdn.ckeditor.com/4.17.1/standard/ckeditor.js"></script>


<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>

@foreach(language_allows() as $index => $code)
    <script>
        CKEDITOR.replace('texts[{{$code}}]', {
            language: '{{$code}}'
        });
    </script>
@endforeach



{{-- ========================  toastr ======================== --}}


<script src="{{asset('dashboard')}}/assets/js/index.js"></script>
<script src="{{asset('dashboard')}}/assets/js/sticky.js"></script>
<script src="{{asset('dashboard')}}/assets/js/custom.js"></script>

@toastr_js
@toastr_render
<script>

    $('.table').DataTable({

        "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
        'language':{!! yajra_lang() !!}


    });
</script>
@stack('admin_js')
