<!--Row-->
<div class="row sidemenu-height">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    @if(count($service->service_texts) > 0)
                        <div class="col-md-12">

                            @if(count($service->service_text_lists) > 0)
                                <div class="d-flex flex-end">
                                    <div class="justify-content-center">
                                        <a href="{{ aurl('service_text/create?service_id='.$service->id) }}" class="btn ripple btn-primary btn-icon-text">
                                            <i class="fe fe-plus me-2"></i>{{ isset($page_titles['create']) ? $page_titles['create'] : "#" }}</a>
                                    </div>
                                </div>
                                <br>
                            @endif


                            <div class="table-responsive">
                                <table id="example3"
                                       class="table table-striped table-bordered">
                                    <thead>

                                    <tr>
                                        <th>#</th>
                                        <th>العنوان</th>
                                        <th>التفاصيل</th>
                                        <th>الصورة</th>
                                        <th></th>
                                    </tr>

                                    </thead>
                                    <tbody>

                                    @foreach($service->service_texts as $key => $service_text)
                                        <tr class="delete-row-{{ $service_text->id }}">
                                            <td>{{ ++$key }}</td>
                                            <td>{{ isset($service_text->title) ? $service_text->title : '#' }}</td>
                                            <td title="{{ $service_text->text }}">{!! \Illuminate\Support\Str::limit(strip_tags($service_text->text), 100 , '...')  !!}</td>
                                            <td><img class="img-table"
                                                     src="{{ GetImg($service_text->image) }}" alt=""></td>
                                            <td>
                                                <div class="button-list">
                                                    <a href="{{ aurl('service_text').'/'.$service_text->id.'/edit?service_id='.$service->id }}" class="btn"><i class="ti ti-pencil"></i></a>
                                                    @if($service_text->type == 'list')
                                                        <a href="#" id="delete-element" data-id="{{ $service_text->id }}" data-title="{{ $service_text->title }}" class="btn"><i class="ti ti-trash"></i></a>
                                                    @endif
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    @else
                        @include('admin.inc.there_is_no_elements_to_show')
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>


{{-------------------- scripts --------------------}}
@push('admin_js')

    <script !src="">
        //===================================================================
        $(document).on('click', '#delete-element', function () {

            var url = '{{ aurl('delete-service_text') }}';
            var element_id = $(this).attr('data-id');
            var element_title = $(this).attr('data-title');

            console.log(url);
            var sure = confirm("هل تريد بالفعل حذف " + element_title);

            if (sure == true) {
                $.ajax({
                    url: url,
                    dataType: 'json',
                    type: "POST",
                    data: {_token: '{{ @csrf_token() }}', id: element_id},
                    beforeSend: function () {
                    },
                    success: function (data) {
                        console.log(data);
                        $('.delete-row-' + data.id).fadeOut().remove();
                    },
                    error(response) {
                    }
                });
            }

            return false;
        });
        //====================================================================
    </script>

@endpush



