@extends('admin.layouts.layout')

@php
    $page_title = isset($page_titles['edit']) ? $page_titles['edit'] : setting('title');
@endphp


@section('page-title')
    {{ $page_title }}
@endsection


@push('admin_css')


@endpush



{{-------------------- start content --------------------}}
@section('content')

    <div class="main-content side-content pt-0">
        <div class="container-fluid">
            <div class="inner-body">

                <div class="page-header">
                @include('admin.inc.page_header', ['page_title' => $page_title])
      
</div>
            <!-- End Page Header -->

                <!--Row-->
                <div class="row">

                    <form action="{{ aurl('update-admin') }}" method="post" enctype="multipart/form-data">

                        @csrf

                        <input type="hidden" name="admin_id" value="{{ $admin->id }}">

                        <div class="col-sm-12">
                            <div class="card">
                                <div class="card-body">

                                    <div class="row">

                                        <div class="col-md-12 form-group">
                                            <label>الصورة</label>
                                            <input type="file" class="dropify" name="image" data-default-file="{{ GetImg($admin->image) }}">
                                        </div>

                                        <div class="col-md-4 form-group">
                                            <label>الاسم</label>
                                            <input name="name" value="{{ $admin->name }}" class="form-control" type="text" placeholder="" autocomplete="off" required>
                                        </div>

                                        <div class="col-md-4 form-group">
                                            <label>البريد الالكتروني</label>
                                            <input name="email" value="{{ $admin->email }}" class="form-control" type="text" placeholder="" autocomplete="off" required>
                                        </div>

                                        <div class="col-md-4 form-group">
                                            <label>كلمة السر</label>
                                            <input name="password" value="" class="form-control" type="password" placeholder="" autocomplete="new-password" required>
                                        </div>

                                    </div>


                                </div>

                                <div class="card-footer">
                                    <div class="col-sm-12">
                                        <button id="submit-form" type="submit" class="btn ripple btn-primary btn-lg">
                                            <i class="fa fa-save loading-submit"></i>
                                            حفظ
                                        </button>
                                    </div>
                                </div>

                            </div>
                        </div>

                    </form>

                </div>
            </div>
        </div>
    </div>

@endsection
{{-------------------- end content --------------------}}


{{-------------------- scripts --------------------}}
@push('admin_css')


@endpush


