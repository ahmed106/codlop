@extends('admin.layouts.layout')

@php
    $page_title = isset($page_titles['create']) ? $page_titles['create'] : setting('title');
@endphp


@section('page-title')
    {{ $page_title }}
@endsection


@push('admin_css')


@endpush



{{-------------------- start content --------------------}}
@section('content')

    <div class="main-content side-content pt-0">
        <div class="container-fluid">
            <div class="inner-body">

                <div class="page-header">
                @include('admin.inc.page_header', ['page_title' => $page_title])
   
</div>
            <!-- End Page Header -->

                <!--Row-->
                <div class="row">

                    <form action="{{ route('admin.store') }}" method="post" enctype="multipart/form-data">

                        @csrf

                        <div class="col-sm-12">
                            <div class="card">
                                <div class="card-body">

                                    <div class="row">

                                        <div class="col-md-12 form-group">
                                            <label>الصورة</label>
                                            <input type="file" class="dropify" name="image" data-default-file="">
                                        </div>

                                        <div class="col-md-4 form-group">
                                            <label>الاسم</label>
                                            <input name="name" value="{{ old('name') }}" class="form-control" type="text" placeholder="" autocomplete="off" required>
                                            @if(Session::has('errors_keys') && in_array('name', Session::get('errors_keys')))
                                               <span style="color: red"><i class="fa fa-info-circle" aria-hidden="true"></i></span>
                                            @endif
                                        </div>

                                        <div class="col-md-4 form-group">
                                            <label>البريد الالكتروني</label>
                                            <input name="email" value="{{ old('email') }}" class="form-control" type="text" placeholder="" autocomplete="off" required>
                                            @if(Session::has('errors_keys') && in_array('email', Session::get('errors_keys')))
                                                <span style="color: red"><i class="fa fa-info-circle" aria-hidden="true"></i></span>
                                            @endif
                                        </div>

                                        <div class="col-md-4 form-group">
                                            <label>كلمة السر</label>
                                            <input name="password" value="" class="form-control" type="password" placeholder="" autocomplete="new-password" required>
                                            @if(Session::has('errors_keys') && in_array('password', Session::get('errors_keys')))
                                                <span style="color: red"><i class="fa fa-info-circle" aria-hidden="true"></i></span>
                                            @endif
                                        </div>

                                    </div>


                                </div>

                                <div class="card-footer">
                                    <div class="col-sm-12">
                                        <button id="submit-form" type="submit" class="btn ripple btn-primary btn-lg">
                                            <i class="fa fa-save loading-submit"></i>
                                            حفظ
                                        </button>
                                    </div>
                                </div>

                            </div>
                        </div>

                    </form>

                </div>
            </div>
        </div>
    </div>

@endsection
{{-------------------- end content --------------------}}


{{-------------------- scripts --------------------}}
@push('admin_css')


@endpush


