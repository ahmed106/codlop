@extends('admin.layouts.layout')

@php
    $page_title = isset($page_titles['index']) ? $page_titles['index'] : setting('title');
@endphp


@section('page-title')
    {{ $page_title }}
@endsection


@push('admin_css')


@endpush



{{-------------------- start content --------------------}}
@section('content')

    <!-- Main Content-->
    <div class="main-content side-content pt-0">
        <div class="container-fluid">
            <div class="inner-body">

                <div class="page-header">

                    @include('admin.inc.page_header', ['page_title' => $page_title])

                    <div class="d-flex flex-end">
                        <div class="justify-content-center">
                            <a href="{{ aurl('offer_detail/create') }}" class="btn ripple btn-primary btn-icon-text">
                                <i class="fe fe-plus me-2"></i>{{ isset($page_titles['create']) ? $page_titles['create'] : "#" }}
                            </a>
                        </div>
                    </div>
                </div>
                <!-- End Page Header -->


                <!--Row-->
                <div class="row sidemenu-height">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    @if(count($offer_details) > 0)
                                        <div class="col-md-12">
                                            <div class="table-responsive">
                                                <table id="example3"
                                                       class="table table-striped table-bordered">
                                                    <thead>

                                                    <tr>
                                                        <th>#</th>
                                                        <th>التفاصيل</th>
                                                        <th>الباقة الاساسية</th>
                                                        <th>الباقة الاحترافية</th>
                                                        <th>الباقة الذهبية</th>
                                                        <th>التحكم</th>
                                                    </tr>

                                                    </thead>
                                                    <tbody>


                                                    @foreach($offer_details as $key => $offer_detail)
                                                        <tr class="delete-row-{{ $offer_detail->id }}">
                                                            <td>{{ ++$key }}</td>
                                                            <td title="{{ $offer_detail->text }}">{{ \Illuminate\Support\Str::limit($offer_detail->text, 100 , '...') }}</td>
                                                            <td><span
                                                                    style="color: {{ $offer_detail->main_package == 'yes' ? '#2ca01c' : '#fc0a0c' }}"> {{ $offer_detail->main_package == 'yes' ? '✔' : "✘" }} </span>
                                                            </td>
                                                            <td><span
                                                                    style="color: {{ $offer_detail->professional_package == 'yes' ? '#2ca01c' : '#fc0a0c' }}"> {{ $offer_detail->professional_package == 'yes' ? '✔' : "✘" }} </span>
                                                            </td>
                                                            <td><span
                                                                    style="color: {{ $offer_detail->golden_package == 'yes' ? '#2ca01c' : '#fc0a0c' }}"> {{ $offer_detail->golden_package == 'yes' ? '✔' : "✘" }} </span>
                                                            </td>
                                                            <td>
                                                                <div class="button-list">
                                                                    <a href="{{ aurl('offer_detail').'/'.$offer_detail->id.'/edit' }}"
                                                                       class="btn"><i class="ti ti-pencil"></i></a>
                                                                    <a href="#" id="delete-element"
                                                                       data-id="{{ $offer_detail->id }}"
                                                                       data-title="{{ $offer_detail->title }}"
                                                                       class="btn"><i class="ti ti-trash"></i></a>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    @endforeach


                                                    <tr style="text-align: center;">
                                                        <th></th>
                                                        <th>الاسعار</th>
                                                        <th><input id="main_package" value="{{ $offer_main_package->price }}" class="form-control" type="text" placeholder="" required></th>
                                                        <th><input id="professional_package" value="{{ $offer_professional_package->price }}" class="form-control" type="text" placeholder="" required></th>
                                                        <th><input id="golden_package" value="{{ $offer_golden_package->price }}" class="form-control" type="text" placeholder="" required></th>
                                                        <th><a id="update-prices" href="#!" class="btn btn-success"><i class="ti ti-pencil"></i></a></th>
                                                    </tr>


                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    @else
                                        @include('admin.inc.there_is_no_elements_to_show')
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


            </div>
        </div>
    </div>
    <!-- End Main Content-->

@endsection
{{-------------------- end content --------------------}}


{{-------------------- scripts --------------------}}
@push('admin_js')

    <script !src="">

        //===================================================================
        $(document).on('click', '#delete-element', function () {

            var url = '{{ aurl('delete-offer_detail') }}';
            var element_id = $(this).attr('data-id');
            var element_title = $(this).attr('data-title');

            console.log(url);
            var sure = confirm("هل تريد بالفعل حذف " + element_title);

            if (sure == true) {
                $.ajax({
                    url: url,
                    dataType: 'json',
                    type: "POST",
                    data: {_token: '{{ @csrf_token() }}', id: element_id},
                    beforeSend: function () {
                    },
                    success: function (data) {
                        Swal.fire({
                            title: 'data Deleted successfully ',
                            icon: 'success',
                            confirmButtonText: 'OK'
                        })
                        $('.delete-row-' + data.id).fadeOut().remove();
                    },
                    error(response) {
                    }
                });
            }

            return false;
        });
        //===================================================================

        //===================================================================
        $(document).on('click', '#update-prices', function () {

            let url = '{{ aurl('update-prices') }}';

            let main_package = $('#main_package').val();
            let professional_package = $('#professional_package').val();
            let golden_package = $('#golden_package').val();


            console.log(url);
            var sure = confirm("هل تريد بالفعل تحديث الاسعار ");

            if (sure == true) {
                $.ajax({
                    url: url,
                    dataType: 'json',
                    type: "POST",
                    data: {_token: '{{ @csrf_token() }}', main_package: main_package, professional_package: professional_package, golden_package: golden_package},
                    beforeSend: function () {
                    },
                    success: function (data) {
                        Swal.fire({
                            title: 'data updated successfully ',
                            icon: 'success',
                            confirmButtonText: 'OK'
                        })
                    },
                    error(response) {
                    }
                });
            }

            return false;
        });
        //===================================================================

    </script>

@endpush


