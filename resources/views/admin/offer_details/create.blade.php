@extends('admin.layouts.layout')

@php
    $page_title = isset($page_titles['create']) ? $page_titles['create'] : setting('title');
@endphp


@section('page-title')
    {{ $page_title }}
@endsection


@push('admin_css')


@endpush



{{-------------------- start content --------------------}}
@section('content')

    <div class="main-content side-content pt-0">
        <div class="container-fluid">
            <div class="inner-body">

                <div class="page-header">
                @include('admin.inc.page_header', ['page_title' => $page_title])

</div>
            <!-- End Page Header -->

                <!--Row-->
                <div class="row">

                    <form action="{{ route('offer_detail.store') }}" method="post" enctype="multipart/form-data">

                        @csrf

                        <input type="hidden" name="service_id" value="{{ request()->get('service_id') }}">

                        <div class="col-sm-12">
                            <div class="card">
                                <div class="card-body">

                                    <div class="row">


                                        @if(count($language_allows) > 0)
                                            @foreach($language_allows as $language_allow)
                                                <div class="col-md-6 form-group">
                                                    <label>{{'التفاصيل'}} {{ "(". $language_allow->code .")"}} </label>
                                                    <input name="texts[{{$language_allow->code}}]" value="" class="form-control" type="text" placeholder="" required>
                                                </div>
                                            @endforeach
                                        @endif

                                        <br>

                                        <div class="col-md-4 form-group">
                                            <label class="custom-switch">
                                                <label> الباقة الاساسية </label> &nbsp;&nbsp;&nbsp;
                                                <input type="checkbox" name="main_package" value="yes" class="custom-switch-input">
                                                <span class="custom-switch-indicator custom-switch-indicator-md"></span>
                                            </label>
                                        </div>


                                        <div class="col-md-4 form-group">
                                            <label class="custom-switch">
                                                <label> الباقة الاحترافية </label> &nbsp;&nbsp;&nbsp;
                                                <input type="checkbox" name="professional_package" value="yes" class="custom-switch-input">
                                                <span class="custom-switch-indicator custom-switch-indicator-md"></span>
                                            </label>
                                        </div>


                                        <div class="col-md-4 form-group">
                                            <label class="custom-switch">
                                                <label> الباقة الذهبية </label> &nbsp;&nbsp;&nbsp;
                                                <input type="checkbox" name="golden_package" value="yes" class="custom-switch-input">
                                                <span class="custom-switch-indicator custom-switch-indicator-md"></span>
                                            </label>
                                        </div>


                                    </div>


                                </div>

                                <div class="card-footer">
                                    <div class="col-sm-12">
                                        <button id="submit-form" type="submit" class="btn ripple btn-primary btn-lg">
                                            <i class="fa fa-save loading-submit"></i>
                                            حفظ
                                        </button>
                                    </div>
                                </div>

                            </div>
                        </div>

                    </form>

                </div>
            </div>
        </div>
    </div>

@endsection
{{-------------------- end content --------------------}}


{{-------------------- scripts --------------------}}
@push('admin_css')


@endpush


