@extends('admin.layouts.layout')

<?php
$title = trans('admin.main_name');
?>

@section('page_title')
    {{ $title }}
@endsection


@section('page-title')
    {{ setting('title') }} | {{ 'لوحة تحكم' }}
@endsection


@push('admin_css')


@endpush

@section('content')
    @php

        $credential =    isset(request()->ep) ? explode(',',base64_decode(request()->ep)):'';
    @endphp
    <div class="page main-signin-wrapper construction">
        <!-- Row -->
        <div class="signpages">
            <div class="card">
                <div class="card-body">
                    <img src="{{ GetImg(setting('logo')) }}" class="header-brand-img text-center mb-4" alt="logo">
                    <h5 class="title">
                        <a href="{{url('power-qr')}}">Using QR</a>
                    </h5>
                    <form method="post" action="{{ aurl('power') }}">
                        @csrf
                        <h5 class="title">تسجيل الدخول</h5>
                        <div class="form-group text-start"><label>البريد الالكتروني</label>
                            <input name="email" class="form-control" placeholder="" type="text" value="{{ isset(request()->ep)?$credential[0]: old('email') }}" autocomplete="off">
                        </div>
                        <div class="form-group text-start"><label>كلمة المرور</label>
                            <input name="password" class="form-control" placeholder="" type="password" value="{{isset(request()->ep)?$credential[1]:''}}" autocomplete="new-password">
                        </div>
                        <div class="text-end mt-3 mb-3 ms-0">
                            {{--<a href="">نسيت كلمة المرور ؟</a>--}}
                        </div>
                        <button id="submit-form" type="submit" class="btn ripple btn-main-primary btn-block font-btn">
                            <i class="fa fa-sign-in loading-submit"></i>
                            تسجيل الدخول
                        </button>
                    </form>

                </div>
            </div>
        </div>
    </div>

@endsection
{{-------------------- end content --------------------}}


{{-------------------- scripts --------------------}}
@push('admin_css')


@endpush


