@extends('admin.layouts.layout')

<?php
$title = trans('admin.main_name');
?>

@section('page_title')
    {{ $title }}
@endsection


@section('page-title')
    {{ setting('title') }} | {{ 'لوحة تحكم' }}
@endsection


@push('admin_css')


@endpush

@section('content')

    <div class="page main-signin-wrapper construction">
        <!-- Row -->
        <div class="row sidemenu-height">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col">
                                <div style="width:500px;" id="reader"></div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



@endsection
{{-------------------- end content --------------------}}


{{-------------------- scripts --------------------}}
@push('admin_js')
    <script src="https://reeteshghimire.com.np/wp-content/uploads/2021/05/html5-qrcode.min_.js"></script>

    <style>
        .result {
            background-color: green;
            color: #fff;
            padding: 20px;
        }

        .row {
            display: flex;
        }
    </style>

    <script type="text/javascript">
        function onScanSuccess(qrCodeMessage) {
            let url_string = new URL(qrCodeMessage);

            $.ajax({
                type: "post",
                url: "{{route('admin.login')}}",
                data: {
                    '_token': "{{csrf_token()}}",
                    search: url_string.search
                },
                success: function () {
                    window.location.href = '{{url('/admin')}}'
                },
                error: function (data) {

                    cuteToast({
                        type: "error",
                        message: "الايميل و / او كلمة المرور غير صحيح",
                        timer: 5000
                    })

                }
            })

            // document.getElementById('result').innerHTML = '<span class="result">' + qrCodeMessage + '</span>';
        }

        function onScanError(errorMessage) {
            //handle scan error
        }

        var html5QrcodeScanner = new Html5QrcodeScanner(
            "reader", {fps: 10, qrbox: 250});
        html5QrcodeScanner.render(onScanSuccess, onScanError);
    </script>

@endpush


