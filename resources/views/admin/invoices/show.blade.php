<!DOCTYPE html>
<html lang="ar">


<head>
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1, shrink-to-fit=no" name="viewport">
    <meta name="description" content="Codlop - Dashboard">

    <title>Invoice</title>
    <style>
        @import url('https://fonts.googleapis.com/css2?family=Noto+Kufi+Arabic&display=swap');

        *,
        ::after,
        ::before {
            -webkit-box-sizing: border-box;
            box-sizing: border-box;
            margin: 0;
        }

        html {
            line-height: 1.15;
            -webkit-text-size-adjust: 100%;
        }


        body,
        html {
            color: #777777;
            font-family: 'Noto Kufi Arabic', sans-serif;
            font-size: 14px;
            font-weight: 400;
            line-height: 1.5em;
            overflow-x: hidden;
            direction: rtl;
            background: #e3e3e3;
        }

        h1,
        h2,
        h3,
        h4,
        h5,
        h6 {
            clear: both;
            color: #111111;
            padding: 0;
            margin: 0 0 20px 0;
            font-weight: 500;
            line-height: 1.2em;
        }

        h1 {
            font-size: 60px;
        }

        h2 {
            font-size: 48px;
        }

        h3 {
            font-size: 30px;
        }

        h4 {
            font-size: 24px;
        }

        h5 {
            font-size: 18px;
        }

        h6 {
            font-size: 16px;
        }

        p,
        div {
            margin-top: 0;
            line-height: 1.5em;
        }

        p {
            margin-bottom: 15px;
        }

        ul {
            margin: 0 0 25px 0;
            padding-left: 20px;
            list-style: square outside none;
        }

        ol {
            padding-left: 20px;
            margin-bottom: 25px;
        }

        dfn,
        cite,
        em,
        i {
            font-style: italic;
        }

        blockquote {
            margin: 0 15px;
            font-style: italic;
            font-size: 20px;
            line-height: 1.6em;
            margin: 0;
        }

        address {
            margin: 0 0 15px;
        }

        img {
            border: 0;
            max-width: 100%;
            height: auto;
            vertical-align: middle;
        }

        a {
            color: inherit;
            text-decoration: none;
            -webkit-transition: all 0.3s ease;
            transition: all 0.3s ease;
        }

        a:hover {
            color: #2ad19d;
        }

        button {
            color: inherit;
            -webkit-transition: all 0.3s ease;
            transition: all 0.3s ease;
        }

        a:hover {
            text-decoration: none;
            color: inherit;
        }

        table {
            width: 100%;
            caption-side: bottom;
            border-collapse: collapse;
        }

        th {
            text-align: right;
        }

        td {
            border-top: 1px solid #eaeaea;
        }

        td,
        th {
            padding: 10px 15px;
            line-height: 1.55em;
        }

        dl {
            margin-bottom: 25px;
        }

        dl dt {
            font-weight: 600;
        }

        b,
        strong {
            font-weight: bold;
        }

        pre {
            color: #777777;
            border: 1px solid #eaeaea;
            font-size: 18px;
            padding: 25px;
            border-radius: 5px;
        }

        kbd {
            font-size: 100%;
            background-color: #777777;
            border-radius: 5px;
        }

        /*--------------------------------------------------------------
        3. Invoice General Style
        ----------------------------------------------------------------*/
        .cs-f10 {
            font-size: 10px;
        }

        .cs-f11 {
            font-size: 11px;
        }

        .cs-f12 {
            font-size: 12px;
        }

        .cs-f13 {
            font-size: 13px;
        }

        .cs-f14 {
            font-size: 14px;
        }

        .cs-f15 {
            font-size: 15px;
        }

        .cs-f16 {
            font-size: 16px;
        }

        .cs-f17 {
            font-size: 17px;
        }

        .cs-f18 {
            font-size: 18px;
        }

        .cs-f19 {
            font-size: 19px;
        }

        .cs-f20 {
            font-size: 20px;
        }

        .cs-f21 {
            font-size: 21px;
        }

        .cs-f22 {
            font-size: 22px;
        }

        .cs-f23 {
            font-size: 23px;
        }

        .cs-f24 {
            font-size: 24px;
        }

        .cs-f25 {
            font-size: 25px;
        }

        .cs-f26 {
            font-size: 26px;
        }

        .cs-f27 {
            font-size: 27px;
        }

        .cs-f28 {
            font-size: 28px;
        }

        .cs-f29 {
            font-size: 29px;
        }

        .cs-light {
            font-weight: 300;
        }

        .cs-normal {
            font-weight: 400;
        }

        .cs-medium {
            font-weight: 500;
        }

        .cs-semi_bold {
            font-weight: 600;
        }

        .cs-bold {
            font-weight: 700;
        }

        .cs-m0 {
            margin: 0px;
        }

        .cs-mb0 {
            margin-bottom: 0px;
        }

        .cs-mb1 {
            margin-bottom: 1px;
        }

        .cs-mb2 {
            margin-bottom: 2px;
        }

        .cs-mb3 {
            margin-bottom: 3px;
        }

        .cs-mb4 {
            margin-bottom: 4px;
        }

        .cs-mb5 {
            margin-bottom: 5px;
        }

        .cs-mb6 {
            margin-bottom: 6px;
        }

        .cs-mb7 {
            margin-bottom: 7px;
        }

        .cs-mb8 {
            margin-bottom: 8px;
        }

        .cs-mb9 {
            margin-bottom: 9px;
        }

        .cs-mb10 {
            margin-bottom: 10px;
        }

        .cs-mb11 {
            margin-bottom: 11px;
        }

        .cs-mb12 {
            margin-bottom: 12px;
        }

        .cs-mb13 {
            margin-bottom: 13px;
        }

        .cs-mb14 {
            margin-bottom: 14px;
        }

        .cs-mb15 {
            margin-bottom: 15px;
        }

        .cs-mb16 {
            margin-bottom: 16px;
        }

        .cs-mb17 {
            margin-bottom: 17px;
        }

        .cs-mb18 {
            margin-bottom: 18px;
        }

        .cs-mb19 {
            margin-bottom: 19px;
        }

        .cs-mb20 {
            margin-bottom: 20px;
        }

        .cs-mb21 {
            margin-bottom: 21px;
        }

        .cs-mb22 {
            margin-bottom: 22px;
        }

        .cs-mb23 {
            margin-bottom: 23px;
        }

        .cs-mb24 {
            margin-bottom: 24px;
        }

        .cs-mb25 {
            margin-bottom: 20px;
        }

        .cs-mb26 {
            margin-bottom: 26px;
        }

        .cs-mb27 {
            margin-bottom: 27px;
        }

        .cs-mb28 {
            margin-bottom: 28px;
        }

        .cs-mb29 {
            margin-bottom: 29px;
        }

        .cs-mb30 {
            margin-bottom: 30px;
        }

        .cs-pt25 {
            padding-top: 25px;
        }

        .cs-width_1 {
            width: 8.33333333%;
        }

        .cs-width_2 {
            width: 16.66666667%;
        }

        .cs-width_3 {
            width: 25%;
        }

        .cs-width_4 {
            width: 33.33333333%;
        }

        .cs-width_5 {
            width: 41.66666667%;
        }

        .cs-width_6 {
            width: 50%;
        }

        .cs-width_7 {
            width: 58.33333333%;
        }

        .cs-width_8 {
            width: 66.66666667%;
        }

        .cs-width_9 {
            width: 75%;
        }

        .cs-width_10 {
            width: 83.33333333%;
        }

        .cs-width_11 {
            width: 91.66666667%;
        }

        .cs-width_12 {
            width: 100%;
        }

        .cs-accent_color,
        .cs-accent_color_hover:hover {
            color: #2ad19d;
        }

        .cs-accent_bg,
        .cs-accent_bg_hover:hover {
            background-color: #2ad19d;
        }

        .cs-primary_color {
            color: #111111;
        }

        .cs-secondary_color {
            color: #777777;
        }

        .cs-ternary_color {
            color: #353535;
        }

        .cs-ternary_color {
            border-color: #eaeaea;
        }

        .cs-focus_bg {
            background: #f6f6f6;
        }

        .cs-accent_10_bg {
            background-color: rgba(42, 209, 157, 0.1);
        }

        .cs-container {
            max-width: 992px;
            padding: 15px;
            margin-left: auto;
            margin-right: auto;
            background: #fff;
        }

        .cs-text_center {
            text-align: center;
        }

        .cs-text_right {
            text-align: right;
        }

        .cs-border_bottom_0 {
            border-bottom: 0;
        }

        .cs-border_top_0 {
            border-top: 0;
        }

        .cs-border_bottom {
            border-bottom: 1px solid #eaeaea;
        }

        .cs-border_top {
            border-top: 1px solid #eaeaea;
        }

        .cs-border_left {
            border-left: 1px solid #eaeaea;
        }

        .cs-border_right {
            border-right: 1px solid #eaeaea;
        }

        .cs-table_baseline {
            vertical-align: baseline;
        }

        .cs-round_border {
            border: 1px solid #eaeaea;
            overflow: hidden;
            border-radius: 6px;
        }

        .cs-border_none {
            border: none;
        }

        .cs-border_left_none {
            border-left-width: 0;
        }

        .cs-border_right_none {
            border-right-width: 0;
        }

        .cs-invoice.cs-style1 .cs-invoice_head {
            display: -webkit-box;
            display: -ms-flexbox;
            display: flex;
            -webkit-box-pack: justify;
            -ms-flex-pack: justify;
            justify-content: space-between;
        }

        .cs-invoice.cs-style1 .cs-invoice_head.cs-type1 {
            padding-bottom: 20px;
            border-bottom: 1px solid #eaeaea;
            align-items: center;
        }

        .cs-invoice.cs-style1 .cs-invoice_footer {
            display: -webkit-box;
            display: -ms-flexbox;
            display: flex;
        }

        .cs-invoice.cs-style1 .cs-invoice_footer table {
            margin-top: -1px;
        }

        .cs-invoice.cs-style1 .cs-left_footer {
            width: 50%;
            padding: 10px 15px;
            padding-right: 0;
        }

        .cs-invoice.cs-style1 .cs-right_footer {
            width: 50%;
        }

        .cs-invoice.cs-style1 .cs-note {
            display: -webkit-box;
            display: -ms-flexbox;
            display: flex;
            -webkit-box-align: start;
            -ms-flex-align: start;
            align-items: flex-start;
            margin-top: 40px;
        }

        .cs-invoice.cs-style1 .cs-note_left {
            margin-right: 10px;
            margin-top: 6px;
            margin-left: -5px;
            display: -webkit-box;
            display: -ms-flexbox;
            display: flex;
        }

        .cs-invoice.cs-style1 .cs-note_left svg {
            width: 32px;
        }

        .cs-invoice.cs-style1 .cs-invoice_left {
            max-width: 55%;
        }

        .cs-invoice_btns {
            display: -webkit-box;
            display: -ms-flexbox;
            display: flex;
            -webkit-box-pack: center;
            -ms-flex-pack: center;
            justify-content: center;
            margin-top: 20px;
            padding-top: 20px;
            border-top: 1px solid #eaeaea;
        }

        .cs-invoice_btns .cs-invoice_btn:first-child {
        }

        .cs-invoice_btns .cs-invoice_btn:last-child {
        }

        .cs-invoice_btn {
            /*display: -webkit-inline-box;*/
            /*display: -ms-inline-flexbox;*/
            display: inline-flex;
            -webkit-box-align: center;
            -ms-flex-align: center;
            align-items: center;
            border: none;
            font-weight: 600;
            padding: 8px 20px;
            cursor: pointer;
            border-radius: 5px;
            width: 130px;
            justify-content: center;
        }

        .cs-invoice_btn svg {
            width: 24px;
            margin-left: 7px;
        }

        .cs-invoice_btn.cs-color1 {
            color: #111111;
            background: rgba(42, 209, 157, 0.15);
        }

        .cs-invoice_btn.cs-color1:hover {
            background-color: rgba(42, 209, 157, 0.3);
        }

        .cs-invoice_btn.cs-color2 {
            color: #fff;
            background: #2ad19d;
        }

        .cs-invoice_btn.cs-color2:hover {
            background-color: rgba(42, 209, 157, 0.8);
        }

        .cs-table_responsive {
            overflow-x: auto;
        }

        .cs-table_responsive > table {
            min-width: 600px;
        }

        .cs-50_col > * {
            width: 50%;
            -webkit-box-flex: 0;
            -ms-flex: none;
            flex: none;
        }

        .cs-bar_list {
            margin: 0;
            padding: 0;
            list-style: none;
            position: relative;
        }

        .cs-bar_list::before {
            content: '';
            height: 75%;
            width: 2px;
            position: absolute;
            left: 4px;
            top: 50%;
            -webkit-transform: translateY(-50%);
            transform: translateY(-50%);
            background-color: #eaeaea;
        }

        .cs-bar_list li {
            position: relative;
            padding-left: 25px;
        }

        .cs-bar_list li:before {
            content: '';
            height: 10px;
            width: 10px;
            border-radius: 50%;
            background-color: #eaeaea;
            position: absolute;
            left: 0;
            top: 6px;
        }

        .cs-bar_list li:not(:last-child) {
            margin-bottom: 10px;
        }

        .cs-table.cs-style1.cs-type1 {
            padding: 10px 30px;
        }

        .cs-table.cs-style1.cs-type1 tr:first-child td {
            border-top: none;
        }

        .cs-table.cs-style1.cs-type1 tr td:first-child {
            padding-left: 0;
        }

        .cs-table.cs-style1.cs-type1 tr td:last-child {
            padding-right: 0;
        }

        .cs-table.cs-style1.cs-type2 > * {
            padding: 0 10px;
        }

        .cs-table.cs-style1.cs-type2 .cs-table_title {
            padding: 20px 0 0 15px;
            margin-bottom: -5px;
        }

        .cs-table.cs-style2 td {
        }

        .cs-table.cs-style2 td,
        .cs-table.cs-style2 th {
            padding: 10px 8px;
            line-height: 1.55em;
        }

        .cs-table.cs-style2 tr:not(:first-child) {
            border-top: 1px dashed #eaeaea;
        }

        .cs-list.cs-style1 {
            list-style: none;
            padding: 0;
            margin: 0;
        }

        .cs-list.cs-style1 li {
            display: -webkit-box;
            display: -ms-flexbox;
            display: flex;
        }

        .cs-list.cs-style1 li:not(:last-child) {
            border-bottom: 1px dashed #eaeaea;
        }

        .cs-list.cs-style1 li > * {
            -webkit-box-flex: 0;
            -ms-flex: none;
            flex: none;
            width: 50%;
            padding: 7px 0px;
        }

        .cs-list.cs-style2 {
            list-style: none;
            margin: 0 0 30px 0;
            padding: 12px 0;
            border: 1px solid #eaeaea;
            border-radius: 5px;
        }

        .cs-list.cs-style2 li {
            display: -webkit-box;
            display: -ms-flexbox;
            display: flex;
        }

        .cs-list.cs-style2 li > * {
            -webkit-box-flex: 1;
            -ms-flex: 1;
            flex: 1;
            padding: 5px 25px;
        }

        .cs-heading.cs-style1 {
            line-height: 1.5em;
            border-top: 1px solid #eaeaea;
            border-bottom: 1px solid #eaeaea;
            padding: 10px 0;
        }

        .cs-no_border {
            border: none !important;
        }

        .cs-grid_row {
            display: -ms-grid;
            display: grid;
            grid-gap: 20px;
            list-style: none;
            padding: 0;
            width: 100%;
        }

        .cs-col_2 {
            /*-ms-grid-columns: (1fr);*/
            grid-template-columns: repeat(2, 1fr);
        }

        .cs-col_3 {
            /*-ms-grid-columns: (1fr);*/
            grid-template-columns: repeat(3, 1fr);
        }

        .cs-col_4 {
            /*-ms-grid-columns: (1fr)*/
            grid-template-columns: repeat(4, auto);
        }

        .cs-border_less td {
            border-color: transparent;
        }

        .cs-special_item {
            position: relative;
        }

        .cs-special_item:after {
            content: '';
            height: 52px;
            width: 1px;
            background-color: #eaeaea;
            position: absolute;
            top: 50%;
            -webkit-transform: translateY(-50%);
            transform: translateY(-50%);
            right: 0;
        }

        .cs-table.cs-style1 .cs-table.cs-style1 tr:not(:first-child) td {
            border-color: #eaeaea;
        }

        .cs-table.cs-style1 .cs-table.cs-style2 td {
            padding: 12px 0px;
        }

        .cs-ticket_wrap {
            display: -webkit-box;
            display: -ms-flexbox;
            display: flex;
        }

        .cs-ticket_left {
            -webkit-box-flex: 1;
            -ms-flex: 1;
            flex: 1;
        }

        .cs-ticket_right {
            -webkit-box-flex: 0;
            -ms-flex: none;
            flex: none;
            width: 215px;
        }

        .cs-box.cs-style1 {
            border: 2px solid #eaeaea;
            border-radius: 5px;
            padding: 20px 10px;
            min-width: 150px;
        }

        .cs-box.cs-style1.cs-type1 {
            padding: 12px 10px 10px;
        }

        .cs-max_w_150 {
            max-width: 150px;
        }

        .cs-left_auto {
            margin-left: auto;
        }

        .cs-title_1 {
            display: inline-block;
            border-bottom: 1px solid #eaeaea;
            padding-bottom: 5px;
            margin-bottom: 10px;
            width: 100%;
        }

        .cs-box2_wrap {
            display: -ms-grid;
            display: grid;
            grid-gap: 30px;
            list-style: none;
            padding: 0;
            /*-ms-grid-columns: (1fr);*/
            grid-template-columns: repeat(2, 1fr);
        }

        .cs-box.cs-style2 {
            border: 1px solid #eaeaea;
            padding: 25px 30px;
            border-radius: 5px;
        }

        .cs-box.cs-style2 .cs-table.cs-style2 td {
            padding: 12px 0;
        }

        .cs-logo img {
            height: 30px;
        }

        @media print {
            .cs-hide_print {
                display: none !important;
            }
        }

        .com-invoice b {
            margin-bottom: 5px;
            display: block;
        }

        .com-invoice p {
            margin: 0;
        }

        .cs-grid_row li.com-invoice {
            width: 100%;
            min-width: 250px;
        }

        .cs-list.cs-style1 li span {
            direction: ltr;
            display: inline-block;
        }

        .cs-text_left {
            text-align: left;
            padding-left: 15px !important;
        }

        .cs-table.cs-style2 th {
            font-size: 13px;
            white-space: nowrap;
        }

        .cs-left_footer img {
            height: 165px;
        }

    </style>
</head>

<body>
<div class="cs-container">
    <div class="cs-invoice cs-style1">
        <div class="cs-invoice_in">
            <div class="cs-invoice_head cs-type1 cs-mb25">
                <div class="cs-text_right">
                    <div class="cs-logo"><img src="{{asset('dashboard/assets/img/logo.svg')}}" alt="Logo"></div>
                </div>
                <div class="cs-invoice_left">
                    <p class="cs-invoice_number cs-primary_color cs-mb0 cs-f16"><b class="cs-primary_color">الرقم الضريبي :</b>
                        {{$invoice->tax_number}}</p>
                </div>
            </div>
            <div class="cs-invoice_head">
                <ul class="cs-grid_row cs-col_4 cs-mb30">
                    <li class="com-invoice cs-text_right">
                        <b class="cs-primary_color">شركة كود لوب للبرمجيات</b>
                        <p>
                            القصيم - بريدة - السعودية <br>
                            +966548541214 <br>
                            Codlop@gmail.com
                        </p>
                    </li>

                    <li>
                        <span class="cs-title_1 cs-semi_bold">تاريخ الفاتورة:</span><br>
                        <span class="cs-primary_color">{{change_date_to_new_format($invoice->created_at,'full')}}</span>
                    </li>
                    <li>
                        <span class="cs-title_1 cs-semi_bold">  رقم الفاتوره:</span><br>
                        <span class="cs-primary_color">{{$invoice->getInvoiceNumber($invoice->invoice_number)}}</span>
                    </li>

                </ul>
            </div>
            <div class="cs-mb20">
                <ul class="cs-list cs-style1">
                    <li>
                        <div class="cs-list_left"><b class="cs-primary_color cs-semi_bold">اسم العميل :</b> <span>{{$invoice->customer_name}}</span></div>
                        @if($invoice_customer_tax_number)
                            <div class="cs-list_right"><b class="cs-primary_color cs-semi_bold">الرقم الضريبي للعميل :</b>
                                <span>{{$invoice->tax_number}}</span></div>
                        @endif

                    </li>
                    <li>
                        <div class="cs-list_left"><b class="cs-primary_color cs-semi_bold">العنوان :</b> <span>{{$invoice->address}}</span></div>
                        <div class="cs-list_right"><b class="cs-primary_color cs-semi_bold">رقم الهاتف :</b> <span>{{$invoice->phone}}</span></div>
                    </li>
                    <li>


                    </li>
                </ul>
            </div>
            <div class="cs-table cs-style2 cs-mb15">
                <div class="cs-round_border">
                    <div class="cs-table_responsive">
                        <table>
                            <thead>
                            <tr class="cs-focus_bg">
                                <th class="cs-width-custom cs-semi_bold cs-primary_color"> المنتج</th>
                                <th class="cs-semi_bold cs-primary_color">الكمية</th>
                                <th class="cs-semi_bold cs-primary_color">السعر بدون ضريبة</th>

                                <th class="cs-semi_bold cs-primary_color">سعر الضريبة</th>
                                <th class="cs-semi_bold cs-primary_color cs-text_left">السعر شامل الضريبة</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>
                                    {{$invoice->service_details}}
                                </td>
                                <td>1</td>
                                <td>{{$invoice->total}} ر.س</td>
                                <td>{{$invoice->tax_amount}}</td>
                                <td class="cs-text_left">{{$invoice->total_tax}} ر.س</td>
                            </tr>

                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="cs-invoice_footer">
                    <div class="cs-left_footer">
                        {!! $invoice->qr !!}
                    </div>
                    <div class="cs-right_footer">
                        <table>
                            <tbody>
                            <tr class="cs-border_none">
                                <td class="cs-border_top_0 cs-bold cs-primary_color">المجموع</td>
                                <td class="cs-border_top_0 cs-primary_color cs-text_left">{{$invoice->total_tax}} ر.س</td>
                            </tr>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="cs-invoice_btns cs-hide_print">
            <a href="javascript:window.print()" class="cs-invoice_btn cs-color1">
                <svg xmlns="http://www.w3.org/2000/svg" class="ionicon" viewBox="0 0 512 512">
                    <path
                        d="M384 368h24a40.12 40.12 0 0040-40V168a40.12 40.12 0 00-40-40H104a40.12 40.12 0 00-40 40v160a40.12 40.12 0 0040 40h24"
                        fill="none" stroke="currentColor" stroke-linejoin="round" stroke-width="32"></path>
                    <rect x="128" y="240" width="256" height="208" rx="24.32" ry="24.32" fill="none" stroke="currentColor"
                          stroke-linejoin="round" stroke-width="32"></rect>
                    <path d="M384 128v-24a40.12 40.12 0 00-40-40H168a40.12 40.12 0 00-40 40v24" fill="none"
                          stroke="currentColor" stroke-linejoin="round" stroke-width="32"></path>
                    <circle cx="392" cy="184" r="24"></circle>
                </svg>
                <span>طباعة</span>
            </a>
        </div>
    </div>
</div>


</body>

</html>
