@extends('admin.layouts.layout')

@php
    $page_title = isset($page_titles['index']) ? $page_titles['index'] : setting('title');
@endphp


@section('page-title')
    {{ $page_title }}
@endsection


@push('admin_css')


@endpush



{{-------------------- start content --------------------}}
@section('content')

    <!-- Main Content-->
    <div class="main-content side-content pt-0">
        <div class="container-fluid">
            <div class="inner-body">

                <div class="page-header">

                    @include('admin.inc.page_header', ['page_title' => $page_title])

                    <div class="d-flex flex-end">
                        <div class="justify-content-center">
                            <a href="{{ aurl('invoices/create') }}" class="btn ripple btn-primary btn-icon-text">
                                <i class="fe fe-plus me-2"></i>{{ isset($page_titles['create']) ? $page_titles['create'] : "#" }}</a>
                        </div>
                    </div>
                </div>
                <!-- End Page Header -->


                <!--Row-->
                <div class="row sidemenu-height">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    @if(count($invoices) > 0)
                                        <div class="col-md-12">
                                            <div class="table-responsive">
                                                <table id="example3"
                                                       class="table table-striped table-bordered">
                                                    <thead>

                                                    <tr>
                                                        <th>#</th>
                                                        <th>Invoice Number</th>
                                                        <th>اسم العميل</th>
                                                        <th>رقم الهاتف</th>
                                                        <th>تاريخ الفاتوره</th>
                                                        <th>Qr Code</th>
                                                        <th>التحكم</th>
                                                    </tr>

                                                    </thead>
                                                    <tbody>

                                                    @foreach($invoices as $key => $invoice)
                                                        <tr class="delete-row-{{ $invoice->id }}">
                                                            <td>{{ ++$key }}</td>
                                                            <td>{{ $invoice->getInvoiceNumber($invoice->invoice_number) }}</td>
                                                            <td>{{ $invoice->customer_name }}</td>
                                                            <td>{{$invoice->phone}}</td>
                                                            <td>{{ change_date_to_new_format($invoice->created_at,'full') }}</td>
                                                            <td>
                                                                {!! $invoice->qr !!}
                                                            </td>

                                                            <td>
                                                                <a href="{{route('invoices.show',$invoice->id)}}" class="btn btn-primary"><i class="fa fa-eye"></i></a>
                                                                <a href="" class="btn btn-danger"><i class="fa fa-trash"></i></a>
                                                            </td>

                                                        </tr>
                                                    @endforeach

                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    @else
                                        @include('admin.inc.there_is_no_elements_to_show')
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


            </div>
        </div>
    </div>
    <!-- End Main Content-->

@endsection
{{-------------------- end content --------------------}}


{{-------------------- scripts --------------------}}
@push('admin_js')

    <script !src="">
        //===================================================================
        $(document).on('click', '#delete-element', function () {

            var url = '{{ aurl('delete-blog') }}';
            var element_id = $(this).attr('data-id');
            var element_title = $(this).attr('data-title');

            console.log(url);
            var sure = confirm("هل تريد بالفعل حذف " + element_title);

            if (sure == true) {
                $.ajax({
                    url: url,
                    dataType: 'json',
                    type: "POST",
                    data: {_token: '{{ @csrf_token() }}', id: element_id},
                    beforeSend: function () {
                    },
                    success: function (data) {
                        console.log(data);
                        $('.delete-row-' + data.id).fadeOut().remove();
                    },
                    error(response) {
                    }
                });
            }

            return false;
        });
        //====================================================================
    </script>

@endpush


