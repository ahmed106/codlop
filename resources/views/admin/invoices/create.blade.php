@extends('admin.layouts.layout')

@php
    $page_title = isset($page_titles['create']) ? $page_titles['create'] : setting('title');
@endphp


@section('page-title')
    {{ $page_title }}
@endsection






{{-------------------- start content --------------------}}
@section('content')

    <div class="main-content side-content pt-0">
        <div class="container-fluid">
            <div class="inner-body">

                <div class="page-header">
                    @include('admin.inc.page_header', ['page_title' => $page_title])

                </div>
                <!-- End Page Header -->

                <!--Row-->
                <div class="row">

                    <form action="{{ route('invoices.store') }}" method="post" enctype="multipart/form-data">

                        @csrf

                        <div class="col-sm-12">
                            <div class="card">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-md-6 form-group">
                                            <label>إسم العميل </label>
                                            <input name="customer_name" value="" class="form-control" type="text" placeholder="" required>
                                        </div>
                                        @error('customer_name')
                                        <span class="text-danger">{{$message}}</span>
                                        @enderror
                                        <div class="col-md-6 form-group">
                                            <label>رقم الهاتف</label>
                                            <input name="phone" value="" class="form-control" type="number" placeholder="" required>
                                        </div>
                                        @error('phone')
                                        <span class="text-danger">{{$message}}</span>
                                        @enderror
                                    </div>

                                    <div class="row">
                                        <div class="col-md-6 form-group">
                                            <label> تفاصيل الخدمه</label>
                                            <textarea name="service_details" value="" class="form-control" type="number" placeholder="" required></textarea>
                                        </div>
                                        <div class="col-md-6 form-group">
                                            <label>العنوان</label>
                                            <textarea name="address" value="" class="form-control" type="number" placeholder="" required></textarea>
                                        </div>
                                        @error('address')
                                        <span class="text-danger">{{$message}}</span>
                                        @enderror

                                    </div>


                                    <div class="row">
                                        <div class="col-md-6 form-group">
                                            <label>الإجمالي</label>
                                            <input name="total" value="0" class="form-control total" type="number" placeholder="" required>
                                        </div>
                                        <div class="col-md-6 form-group">
                                            <label>قيمه الضريبه</label>
                                            <input name="tax_amount" value="0" class="form-control tax" type="number" placeholder="" required>
                                        </div>

                                        @error('tax_amount')
                                        <span class="text-danger">{{$message}}</span>
                                        @enderror
                                    </div>

                                    <div class="row">
                                        <div class="col-md-6 form-group">
                                            <label>الإجمالي شامل الضريبه </label>
                                            <input readonly name="total_tax" value="0" class="form-control" type="text" placeholder="" required>
                                            @error('total_tax')
                                            <span class="text-danger">{{$message}}</span>
                                            @enderror
                                        </div>


                                    </div>
                                </div>

                                <div class="card-footer">
                                    <div class="col-sm-12">
                                        <button id="submit-form" type="submit" class="btn ripple btn-primary btn-lg">
                                            <i class="fa fa-save loading-submit"></i>
                                            حفظ
                                        </button>
                                    </div>
                                </div>

                            </div>
                        </div>

                    </form>

                </div>
            </div>
        </div>
    </div>

@endsection


@push('admin_js')
    <script>

        $('.tax , .total').on('change', function () {
            calcTotalTax();
        });

        function calcTotalTax() {
            let total = parseFloat($('input[name="total"]').val()),
                tax = parseFloat($('input[name="tax_amount"]').val());

            console.log(total, tax)
            $('input[name="total_tax"]').val(0)
            $('input[name="total_tax"]').val(total + tax)

        }
    </script>
@endpush

