@extends('admin.layouts.layout')

@php
    $page_title = isset($page_titles['edit']) ? $page_titles['edit'] : setting('title');
@endphp


@section('page-title')
    {{ $page_title }}
@endsection


@push('admin_css')


@endpush



{{-------------------- start content --------------------}}
@section('content')

    <div class="main-content side-content pt-0">
        <div class="container-fluid">
            <div class="inner-body">

                <div class="page-header">
                @include('admin.inc.page_header', ['page_title' => $page_title])
 
</div>
            <!-- End Page Header -->

                <!--Row-->
                <div class="row">

                    <form action="{{ aurl('update-service_text') }}" method="post" enctype="multipart/form-data">

                        @csrf

                        <input type="hidden" name="service_text_id" value="{{ $service_text->id }}">

                        <div class="col-sm-12">
                            <div class="card">
                                <div class="card-body">

                                    <div class="row">

                                        @if($service_text->service_id != 8)
                                            <div class="col-md-12 form-group">
                                                <label>الصورة</label>
                                                <input type="file" class="dropify" name="image"
                                                       data-default-file="{{ GetImg($service_text->image) }}">
                                            </div>
                                        @endif


                                        @if(count($language_allows) > 0)
                                            @foreach($language_allows as $language_allow)
                                                <div class="col-md-6 form-group">
                                                    <label>{{'العنوان'}} {{ "(". $language_allow->code .")"}} </label>
                                                    <input name="titles[{{$language_allow->code}}]"
                                                           value="{{ isset($view_languages[$language_allow->code]) ? $view_languages[$language_allow->code]->title : null }}"
                                                           class="form-control" type="text" placeholder="" required>
                                                </div>
                                            @endforeach
                                        @endif


                                        @if(count($language_allows) > 0)
                                            @foreach($language_allows as $language_allow)
                                                <div class="col-md-6 form-group" {{ $service_text->service_id == 8 && $service_text->type == 'list' ? 'hidden' : '' }}>
                                                    <label>{{'التفاصيل'}} {{ "(". $language_allow->code .")"}} </label>
                                                    <textarea name="texts[{{$language_allow->code}}]"
                                                              class="form-control"
                                                              required>{{ isset($view_languages[$language_allow->code]) ? $view_languages[$language_allow->code]->text : null }}</textarea>
                                                </div>
                                            @endforeach

                                        @endif

                                    </div>


                                </div>

                                <div class="card-footer">
                                    <div class="col-sm-12">
                                        <button id="submit-form" type="submit" class="btn ripple btn-primary btn-lg">
                                            <i class="fa fa-save loading-submit"></i>
                                            حفظ
                                        </button>
                                    </div>
                                </div>

                            </div>
                        </div>

                    </form>

                </div>
            </div>
        </div>
    </div>

@endsection
{{-------------------- end content --------------------}}


{{-------------------- scripts --------------------}}
@push('admin_css')


@endpush


