@extends('admin.layouts.layout')

@php
    $page_title = isset($page_titles['create']) ? $page_titles['create'] : setting('title');
@endphp


@section('page-title')
    {{ $page_title }}
@endsection


@push('admin_css')


@endpush



{{-------------------- start content --------------------}}
@section('content')

    <div class="main-content side-content pt-0">
        <div class="container-fluid">
            <div class="inner-body">

                <div class="page-header">
                @include('admin.inc.page_header', ['page_title' => $page_title])
  
</div>
            <!-- End Page Header -->

                <!--Row-->
                <div class="row">

                    <form action="{{ route('language_allow.store') }}" method="post" enctype="multipart/form-data">

                        @csrf

                        <div class="col-sm-12">
                            <div class="card">
                                <div class="card-body">

                                    <div class="row">

                                        <div class="col-md-12 form-group">
                                            <select name="language_id" class="form-select" aria-label="Default select example">
                                                <option selected>اختر اللغة</option>
                                                @foreach($languages as $language)
                                                    <option value="{{ $language->id }}">{{ $language->native_name }}</option>
                                                @endforeach
                                            </select>
                                        </div>


                                        <div class="col-md-12 form-group">
                                            <label>اللوجو</label>
                                            <input type="file" class="dropify" name="icon" data-default-file="">
                                        </div>

                                    </div>


                                </div>

                                <div class="card-footer">
                                    <div class="col-sm-12">
                                        <button id="submit-form" type="submit" class="btn ripple btn-primary btn-lg">
                                            <i class="fa fa-save loading-submit"></i>
                                            حفظ
                                        </button>
                                    </div>
                                </div>

                            </div>
                        </div>

                    </form>

                </div>
            </div>
        </div>
    </div>

@endsection
{{-------------------- end content --------------------}}


{{-------------------- scripts --------------------}}
@push('admin_css')


@endpush


