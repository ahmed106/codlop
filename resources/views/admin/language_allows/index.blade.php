@extends('admin.layouts.layout')

@php
    $page_title = isset($page_titles['index']) ? $page_titles['index'] : setting('title');
@endphp


@section('page-title')
    {{ $page_title }}
@endsection


@push('admin_css')


@endpush



{{-------------------- start content --------------------}}
@section('content')

    <!-- Main Content-->
    <div class="main-content side-content pt-0">
        <div class="container-fluid">
            <div class="inner-body">

                <div class="page-header">

                    @include('admin.inc.page_header', ['page_title' => $page_title])

{{--                    <div class="d-flex flex-end">--}}
{{--                        <div class="justify-content-center">--}}
{{--                            <a href="{{ aurl('language_allow/create') }}" class="btn ripple btn-primary btn-icon-text">--}}
{{--                                <i class="fe fe-plus me-2"></i>{{ isset($page_titles['create']) ? $page_titles['create'] : "#" }}</a>--}}
{{--                        </div>--}}
{{--                    </div>--}}
                </div>
                <!-- End Page Header -->


                <!--Row-->
                <div class="row sidemenu-height">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    @if(count($language_allows) > 0)
                                        <div class="col-md-12">
                                            <div class="table-responsive">
                                                <table id="example3"
                                                       class="table table-striped table-bordered">
                                                    <thead>

                                                    <tr>
                                                        <th>الايقونة</th>
                                                        <th>الاسم</th>
                                                        <th>الكود</th>
                                                        <th>التحكم</th>
                                                    </tr>

                                                    </thead>
                                                    <tbody>

                                                    @foreach($language_allows as $key => $language_allow)
                                                        <tr class="delete-row-{{ $language_allow->id }}">
                                                            <td><img class="img-table" src="{{ GetImg($language_allow->icon) }}" alt=""></td>
                                                            <td>{{ $language_allow->native_name }}</td>
                                                            <td>{{ $language_allow->code }}</td>
                                                            <td>
                                                                <div class="button-list">
                                                                    <a href="{{ aurl('language_allow').'/'.$language_allow->id.'/edit' }}" class="btn"><i class="ti ti-pencil"></i></a>
                                                                    <a href="#" id="delete-element" data-id="{{ $language_allow->id }}" data-title="{{ $language_allow->native_name }}" class="btn"><i class="ti ti-trash"></i></a>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    @endforeach

                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    @else
                                        @include('admin.inc.there_is_no_elements_to_show')
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


            </div>
        </div>
    </div>
    <!-- End Main Content-->

@endsection
{{-------------------- end content --------------------}}


{{-------------------- scripts --------------------}}
@push('admin_js')

    <script !src="">
        //===================================================================
        $(document).on('click', '#delete-element', function () {

            var url = '{{ aurl('delete-language_allow') }}';
            var element_id = $(this).attr('data-id');
            var element_title = $(this).attr('data-title');

            console.log(element_title);
            var sure = confirm("هل تريد بالفعل حذف " + element_title);

            if (sure == true) {
                $.ajax({
                    url: url,
                    dataType: 'json',
                    type: "POST",
                    data: {_token: '{{ @csrf_token() }}', id: element_id},
                    beforeSend: function () {
                    },
                    success: function (data) {
                        console.log(data);
                        $('.delete-row-'+data.id).fadeOut().remove();
                    },
                    error(response) {
                    }
                });
            }

            return false;
        });
        //====================================================================
    </script>

@endpush


