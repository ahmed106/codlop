@extends('admin.layouts.layout')

@php
    $page_title = isset($page_titles['create']) ? $page_titles['create'] : setting('title');
@endphp


@section('page-title')
    {{ $page_title }}
@endsection


@push('admin_css')


@endpush



{{-------------------- start content --------------------}}
@section('content')

    <div class="main-content side-content pt-0">
        <div class="container-fluid">
            <div class="inner-body">


                <div class="page-header">
                    @include('admin.inc.page_header', ['page_title' => $page_title])
                </div>
                <!-- End Page Header -->

                <!--Row-->
                <div class="row">

                    <form action="{{ route('teams.update',$team->id) }}" method="post" enctype="multipart/form-data">

                        @csrf
                        @method('put')

                        <div class="col-sm-12">
                            <div class="card">
                                <div class="card-body">

                                    <div class="row">

                                        @if(count($language_allows) > 0)
                                            @foreach($language_allows as $language_allow)
                                                <div class="col-md-6 form-group">
                                                    <label>{{'اسم الموظف'}} {{ "(". $language_allow->code .")"}} </label>
                                                    <input  name="name[{{$language_allow->code}}]" value="{{$team->team_tran->name}}" class="form-control" type="text" placeholder="" required>
                                                </div>
                                            @endforeach
                                        @endif

                                        @if(count($language_allows) > 0)
                                            @foreach($language_allows as $language_allow)
                                                <div class="col-md-6 form-group">
                                                    <label>الوظيفه {{ "(". $language_allow->code .")"}} </label>
                                                    <input type="text" name="job_title[{{$language_allow->code}}]" value="{{$team->team_tran->job_title}}" class="form-control">
                                                </div>
                                            @endforeach
                                        @endif

                                            <div class="form-group check-div">
                                                <input type="checkbox" id="ceo" class="checkbox_field" value="1" {{$team->is_ceo ? 'checked':''}} name="is_ceo">
                                                <label for="ceo">مدير ؟</label>
                                            </div>

                                            <div class="col-md-12 form-group">
                                                <label>الصورة</label>
                                                <input type="file" class="dropify" name="photo"
                                                       data-default-file="{{ GetImg($team->photo) }}">
                                            </div>

                                    </div>


                                </div>

                                <div class="card-footer">
                                    <div class="col-sm-12">
                                        <button id="submit-form" type="submit" class="btn ripple btn-primary btn-lg">
                                            <i class="fa fa-save loading-submit"></i>
                                            حفظ
                                        </button>
                                    </div>
                                </div>

                            </div>
                        </div>

                    </form>

                </div>
            </div>
        </div>
    </div>

@endsection
{{-------------------- end content --------------------}}


{{-------------------- scripts --------------------}}
@push('admin_js')


@endpush


