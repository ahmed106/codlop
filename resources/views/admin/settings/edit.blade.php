@extends('admin.layouts.layout')

@php
    $page_title = isset($page_titles['edit']) ? $page_titles['edit'] : setting('title');
@endphp

@section('page-title')
    {{ $page_title }}
@endsection


@push('admin_css')


@endpush



{{-------------------- start content --------------------}}
@section('content')

    <div class="main-content side-content pt-0">
        <div class="container-fluid">
            <div class="inner-body">

                <div class="page-header">
                @include('admin.inc.page_header', ['page_title' => $page_title])

</div>
            <!-- End Page Header -->


                <!--Row-->
                <div class="row">

                    <form action="{{ aurl('update-setting') }}" method="post" enctype="multipart/form-data">

                        @csrf

                        <div class="col-sm-12">
                            <div class="card">
                                <div class="card-body">

                                    <div class="row">

                                        @if(count($language_allows) > 0)
                                            @foreach($language_allows as $language_allow)
                                                <div class="col-md-6 form-group">
                                                    <label>{{'العنوان'}} {{ "(". $language_allow->code .")"}} </label>
                                                    <input name="titles[{{$language_allow->code}}]" value="{{ isset($view_languages[$language_allow->code]) ? $view_languages[$language_allow->code]->title : null }}" class="form-control" type="text" placeholder="" required>
                                                </div>
                                            @endforeach
                                        @endif

                                        @if(count($language_allows) > 0)
                                            @foreach($language_allows as $language_allow)
                                                <div class="col-md-6 form-group">
                                                    <label>{{'عنوان الشركة'}} {{ "(". $language_allow->code .")"}} </label>
                                                    <input name="address[{{$language_allow->code}}]" value="{{ isset($view_languages[$language_allow->code]) ? $view_languages[$language_allow->code]->address : null }}" class="form-control" type="text" placeholder="" required>
                                                </div>
                                            @endforeach
                                        @endif


                                        <div class="col-md-4 form-group">
                                            <label>البريد الالكتروني</label>
                                            <input name="email" value="{{ $setting->email }}" class="form-control"
                                                   type="email" placeholder="" required>
                                        </div>

                                        <div class="col-md-4 form-group">
                                            <label>رقم الهاتف</label>
                                            <input name="phone" value="{{ $setting->phone }}" class="form-control"
                                                   type="text" placeholder="" required>
                                        </div>

                                        <div class="col-md-4 form-group">
                                            <label>رقم الواتساب</label>
                                            <input name="whatsapp" value="{{ $setting->whatsapp }}" class="form-control"
                                                   type="text" placeholder="" required>
                                        </div>

                                        <div class="col-md-12 form-group">
                                            <label>اللوجو</label>
                                            <input type="file" class="dropify" name="logo" data-default-file="{{ GetImg(setting('logo')) }}">
                                        </div>

                                    </div>

                                    <hr>

                                    <div class="row">
                                        <div class="col-md-6 form-group">
                                            <label>لينك Facebook</label>
                                            <input name="facebook" value="{{ $setting->facebook }}" class="form-control"
                                                   type="text" placeholder="" required>
                                        </div>
                                        <div class="col-md-6 form-group">
                                            <label>لينك Twitter</label>
                                            <input name="twitter" value="{{ $setting->twitter }}" class="form-control"
                                                   type="text" placeholder="" required>
                                        </div>
                                        <div class="col-md-6 form-group">
                                            <label>لينك youtube</label>
                                            <input name="gmail" value="{{ $setting->gmail }}" class="form-control"
                                                   type="text" placeholder="" required>
                                        </div>
                                        <div class="col-md-6 form-group">
                                            <label>لينك linked-in</label>
                                            <input name="linkedin" value="{{ $setting->linkedin }}" class="form-control"
                                                   type="text" placeholder="" required>
                                        </div>
                                    </div>

                                    <hr>

                                    <div class="row">

                                        <div class="col-sm-12">
                                            <h4>العنوان</h4>
                                        </div>

                                        <div class="col-md-4 form-group">
                                            <label> خط العرض (latitude) </label>
                                            <input name="latitude" value="{{ $setting->latitude }}" class="form-control"
                                                   type="text" placeholder="" required>
                                        </div>

                                        <div class="col-md-4 form-group">
                                            <label> خط الطول (longitude)</label>
                                            <input name="longitude" value="{{ $setting->longitude }}"
                                                   class="form-control" type="text" placeholder="" required>
                                        </div>

                                        <div class="col-md-4 form-group">
                                            <label> العنوان يدويا </label>
                                            <input name="location" value="{{ $setting->location }}" class="form-control"
                                                   type="text" placeholder="" required>
                                        </div>

                                    </div>

                                </div>

                                <div class="card-footer">
                                    <div class="col-sm-12">
                                        <button id="submit-form" type="submit" class="btn ripple btn-primary btn-lg">
                                            <i class="fa fa-save loading-submit"></i>
                                            حفظ
                                        </button>
                                    </div>
                                </div>

                            </div>
                        </div>

                    </form>

                </div>
            </div>
        </div>
    </div>

@endsection
{{-------------------- end content --------------------}}


{{-------------------- scripts --------------------}}
@push('admin_js')


@endpush


