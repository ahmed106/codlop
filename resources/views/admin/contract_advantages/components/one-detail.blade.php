@if(isset($contract_advantage_detail) && $contract_advantage_detail != null)

    @csrf

    <tr id="update_raw_{{$contract_advantage_detail->id}}" class="delete-row-{{ $contract_advantage_detail->id }}">
        <td style="width: 15%">
            <img src="{{ GetImg($contract_advantage_detail->icon) }}" class="img img-thumbnail" alt="icon" style="width: 60%;">
        </td>
        <td>
            @if(count($language_allows) > 0)
                @foreach($language_allows as $language_allow)
                    <label>{{'العنوان'}} {{ "(". $language_allow->code .")"}} </label>
                    <input value="{{ isset(view_language($contract_advantage_detail->contract_advantage_detail_trans)[$language_allow->code]) ? view_language($contract_advantage_detail->contract_advantage_detail_trans)[$language_allow->code]->title : null }}" class="form-control" type="text" placeholder="" readonly required>
                @endforeach
            @endif
        </td>
        <td>

            @if(count($language_allows) > 0)
                @foreach($language_allows as $language_allow)

                    <label>{{'التفاصيل'}} {{ "(". $language_allow->code .")"}} </label>
                    <textarea class="form-control" id="" cols="30" rows="3" required>{{ isset(view_language($contract_advantage_detail->contract_advantage_detail_trans)[$language_allow->code]) ? strip_tags(view_language($contract_advantage_detail->contract_advantage_detail_trans)[$language_allow->code]->text) : null  }}</textarea>
                @endforeach
            @endif
        </td>
        <td>
            <a href="{{route('edit_contract_advantage_detail',$contract_advantage_detail->id)}}" class="btn"><i class="ti fa-2x ti-pencil"></i> </a>

            <button type="button" data-id="{{ $contract_advantage_detail->id }}" data-title="{{ $contract_advantage_detail->title }}" class="btn     delete-row">
                <i class="ti ti-trash fa-2x "></i>

            </button>

        </td>
    </tr>




@endif

@push('admin_js')

    <script>

        $('#edit_' +{{$contract_advantage_detail->id}}).on('click', function (e) {
            e.preventDefault();

            let data = new FormData($('#update_raw_' + '{{$contract_advantage_detail->id}}')[0])

            console.log(data)

            $.ajax({
                type: 'get',
                url: '{{route('update_contract_advantage_detail',$contract_advantage_detail->id)}}',
                contentType: false,
                cache: false,
                processData: false,
                data: data,

            })
        })
    </script>
@endpush
