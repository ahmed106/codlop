<form action="{{ aurl('add-contract_advantage-row') }}" method="post"
      enctype="multipart/form-data">
    @csrf
    <input type="hidden" name="contract_advantage_id" value="{{ $contract_advantage->id }}">


    <div class="col-sm-12">
        <div class="card">
            <div class="card-body">

                <div class="row">

                    @if(count($language_allows) > 0)
                        @foreach($language_allows as $language_allow)
                            <div class="col-md-6 form-group">
                                <label>{{'العنوان'}} {{ "(". $language_allow->code .")"}} </label>
                                <input name="titles[{{$language_allow->code}}]" value="" class="form-control" type="text" placeholder="" required>
                            </div>
                        @endforeach
                    @endif

                    @if(count($language_allows) > 0)
                        @foreach($language_allows as $language_allow)
                            <div class="col-md-6 form-group">
                                <label>{{'التفاصيل'}} {{ "(". $language_allow->code .")"}} </label>
                                <textarea name="texts[{{$language_allow->code}}]" class="form-control" required></textarea>
                            </div>
                        @endforeach
                    @endif

                    <div class="col-md-12 form-group">
                        <label>اللوجو</label>
                        <input type="file" class="dropify" name="icon" data-default-file="">
                    </div>

                </div>


            </div>

            <div class="card-footer">
                <div class="col-sm-12">
                    <button id="submit-form" type="submit" class="btn ripple btn-primary btn-lg">
                        <i class="fa fa-plus loading-submit"></i>
                        إضافه
                    </button>
                </div>
            </div>

        </div>
    </div>


</form>


