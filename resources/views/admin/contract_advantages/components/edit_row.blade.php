@extends('admin.layouts.layout')

@php
    $page_title = isset($page_titles['add_details']) ? $page_titles['add_details'] : setting('title');
@endphp


@section('page-title')
    {{ $page_title }}
@endsection


@push('admin_css')

    <style>
        .dropify-wrapper {
            height: 140px;
        }

        .add-row {
            line-height: 34px;
            width: 100%;
            margin-top: 50px;
        }
    </style>
@endpush

@section('content')

    <div class="main-content side-content pt-0">
        <div class="container-fluid">
            <div class="inner-body">

                <div class="page-header">
                    @include('admin.inc.page_header', ['page_title' => $page_title])

                </div>
                <!-- End Page Header -->

                <!--Row-->
                <div class="row">

                    <div class="card">
                        <div class="card-header d-flex justify-content-between">
                            <div class="header-title">
                                <h4 class="card-title">{{ $page_title }}</h4>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="row">

                                <form action="{{ route('update_contract_advantage_detail',$contract_detail->id) }}" method="post" enctype="multipart/form-data">
                                    @csrf
                                    @method('put')
                                    <input type="hidden" name="contract_advantage_id" value="{{ $contract_advantage->id }}">


                                    <div class="col-sm-12">
                                        <div class="card">
                                            <div class="card-body">

                                                <div class="row">

                                                    @if(count($language_allows) > 0)
                                                        @foreach($language_allows as $language_allow)
                                                            <div class="col-md-6 form-group">
                                                                <label>{{'العنوان'}} {{ "(". $language_allow->code .")"}} </label>
                                                                <input name="titles[{{$language_allow->code}}]" value="{{ isset(view_language($contract_detail->contract_advantage_detail_trans)[$language_allow->code]) ? view_language($contract_detail->contract_advantage_detail_trans)[$language_allow->code]->title : null }}" class="form-control" type="text" placeholder="" required>
                                                            </div>
                                                        @endforeach
                                                    @endif

                                                    @if(count($language_allows) > 0)
                                                        @foreach($language_allows as $language_allow)
                                                            <div class="col-md-6 form-group">
                                                                <label>{{'التفاصيل'}} {{ "(". $language_allow->code .")"}} </label>
                                                                <textarea name="texts[{{$language_allow->code}}]" class="form-control" required>{{ isset(view_language($contract_detail->contract_advantage_detail_trans)[$language_allow->code]) ? view_language($contract_detail->contract_advantage_detail_trans)[$language_allow->code]->text : null }}</textarea>
                                                            </div>
                                                        @endforeach
                                                    @endif

                                                    <div class="col-md-12 form-group">
                                                        <label>اللوجو</label>
                                                        <input type="file" class="dropify" style="height: 2000px" name="icon" data-default-file="{{GetImg($contract_detail->icon)}}">
                                                    </div>

                                                </div>


                                            </div>

                                            <div class="card-footer">
                                                <div class="col-sm-12">
                                                    <button id="submit-form" type="submit" class="btn ripple btn-primary btn-lg">
                                                        <i class="fa fa-save loading-submit"></i>
                                                        حفظ
                                                    </button>
                                                </div>
                                            </div>

                                        </div>
                                    </div>


                                </form>


                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

@endsection



