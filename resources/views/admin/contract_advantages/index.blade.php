@extends('admin.layouts.layout')

@php
    $page_title = isset($page_titles['index']) ? $page_titles['index'] : setting('title');
@endphp


@section('page-title')
    {{ $page_title }}
@endsection


@push('admin_css')


@endpush



{{-------------------- start content --------------------}}
@section('content')

    <!-- Main Content-->
    <div class="main-content side-content pt-0">
        <div class="container-fluid">
            <div class="inner-body">
                <div class="page-header">

                    @include('admin.inc.page_header', ['page_title' => $page_title])

                    <div class="d-flex flex-end">
                        <div class="justify-content-center">
                            <a href="{{ aurl('contract_advantage/create') }}" class="btn ripple btn-primary btn-icon-text">
                                <i class="fe fe-plus me-2"></i>{{ isset($page_titles['create']) ? $page_titles['create'] : "#" }}</a>
                        </div>
                    </div>

                </div>
                <!-- End Page Header -->


                <!--Row-->
                <div class="row sidemenu-height">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    @if(count($contract_advantages) > 0)
                                        <div class="col-md-12">
                                            <div class="table-responsive">
                                                <table id="example3"
                                                       class="table table-striped table-bordered">
                                                    <thead>

                                                    <tr>
                                                        <th>#</th>
                                                        <th>العنوان</th>
                                                        <th>التفاصيل</th>
                                                        <th>صورة الميزة</th>
                                                        <th>التحكم</th>
                                                    </tr>

                                                    </thead>
                                                    <tbody>

                                                    @foreach($contract_advantages as $key => $contract_advantage)
                                                        <tr class="delete-row-{{ $contract_advantage->id }}">
                                                            <td>{{ ++$key }}</td>
                                                            <td>{{ $contract_advantage->title }}</td>
                                                            <td title="{{ $contract_advantage->text }}">{!! \Illuminate\Support\Str::limit(strip_tags($contract_advantage->text), 100 , '...') !!}</td>
                                                            <td><img class="img-table"
                                                                     src="{{ GetImg($contract_advantage->image) }}" alt=""></td>
                                                            <td>
                                                                <div class="button-list">
                                                                    <a href="{{ aurl('contract_advantage/add_details').'/'.$contract_advantage->id }}" class="btn"><i class="ti ti-plus"></i></a>
                                                                    <a href="{{ aurl('contract_advantage').'/'.$contract_advantage->id.'/edit' }}" class="btn"><i class="ti ti-pencil"></i></a>
                                                                    <a href="#" id="delete-element" data-id="{{ $contract_advantage->id }}" data-title="{{ $contract_advantage->title }}" class="btn"><i class="ti ti-trash"></i></a>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    @endforeach

                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    @else
                                        @include('admin.inc.there_is_no_elements_to_show')
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


            </div>
        </div>
    </div>
    <!-- End Main Content-->

@endsection
{{-------------------- end content --------------------}}


{{-------------------- scripts --------------------}}
@push('admin_js')

    <script !src="">
        //===================================================================
        $(document).on('click', '#delete-element', function () {
            var url = '{{ aurl('delete-contract_advantage') }}';
            var element_id = $(this).attr('data-id');
            var element_title = $(this).attr('data-title');
            // console.log(url);
            var sure = confirm("هل تريد بالفعل حذف " + element_title);
            if (sure == true) {
                $.ajax({
                    url: url,
                    dataType: 'json',
                    type: "POST",
                    data: {_token: '{{ @csrf_token() }}', id: element_id},
                    beforeSend: function () {
                    },
                    success: function (data) {
                        console.log(data);
                        $('.delete-row-' + data.id).fadeOut().remove();
                    },
                    error(response) {
                    }
                });
            }
            return false;
        });
        //====================================================================
    </script>

@endpush


