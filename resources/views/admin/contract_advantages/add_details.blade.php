@extends('admin.layouts.layout')

@php
    $page_title = isset($page_titles['add_details']) ? $page_titles['add_details'] : setting('title');
@endphp


@section('page-title')
    {{ $page_title }}
@endsection


@push('admin_css')

    <style>
        .dropify-wrapper {
            height: 140px;
        }

        .add-row {
            line-height: 34px;
            width: 100%;
            margin-top: 50px;
        }
    </style>
@endpush



{{-------------------- start content --------------------}}
@section('content')


    {{-- add row --}}
    {{-- add row --}}
    {{-- add row --}}
    {{-- add row --}}

    <div class="main-content side-content pt-0">
        <div class="container-fluid">
            <div class="inner-body">

                <div class="page-header">
                    @include('admin.inc.page_header', ['page_title' => $page_title])

                </div>
                <!-- End Page Header -->

                <!--Row-->
                <div class="row">

                    <div class="card">
                        <div class="card-header d-flex justify-content-between">
                            <div class="header-title">
                                <h4 class="card-title">{{ $page_title }}</h4>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="row">

                                @include('admin.contract_advantages.components.add-new-row')

                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>


    {{-- table --}}
    {{-- table --}}
    {{-- table --}}
    {{-- table --}}


    <div class="main-content side-content pt-0">
        <div class="container-fluid">
            <div class="inner-body">

                <!--Row-->
                <div class="row">

                    <div class="card">

                        <div class="card-body">
                            <div class="row">

                                <div class="col-md-12">
                                    <div class="table-responsive">
                                        <table class="table table-striped table-bordered">

                                            <thead>
                                            <tr>
                                                <th>الصورة</th>
                                                <th>العنوان</th>
                                                <th>التفاصيل</th>
                                                <th>التحكم</th>
                                            </tr>
                                            </thead>

                                            <tbody class="append-row">

                                            @if(count($contract_advantage_details) > 0)
                                                @foreach($contract_advantage_details as $contract_advantage_detail)
                                                    @include('admin.contract_advantages.components.one-detail', ['contract_advantage_detail' => $contract_advantage_detail])
                                                @endforeach
                                            @else
                                                @include('admin.inc.there_is_no_elements_to_show')
                                            @endif

                                            </tbody>

                                        </table>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

@endsection
{{-------------------- end content --------------------}}


{{-------------------- scripts --------------------}}
@push('admin_js')

    <script !src="">


        //====================================================================
        $(document).on('click', '.delete-row', function () {

            var url = '{{ aurl('delete-contract_advantage_details') }}';
            var element_id = $(this).attr('data-id');
            var element_title = $(this).attr('data-title');

            let delete_row_length = $('.delete-row').length;
            if (delete_row_length <= 1) {
                alert('لا يمكن حذف اخر عنصر (على الاقل عنصر واحد فقط)');
                return false;
            }

            console.log(url);
            var sure = confirm("هل تريد بالفعل حذف " + element_title);

            if (sure == true) {
                $.ajax({
                    url: url,
                    dataType: 'json',
                    type: "POST",
                    data: {_token: '{{ @csrf_token() }}', id: element_id},
                    beforeSend: function () {
                    },
                    success: function (data) {
                        console.log(data);
                        $('.delete-row-' + data.id).fadeOut().remove();
                    },
                    error(response) {
                    }
                });
            }

            return false;
        });
        //====================================================================

        //====================================================================
        $(document).on('click', '.delete-single-row', function () {
            let delete_row_length = $('.delete-row').length;
            if (delete_row_length <= 1) {
                alert('لا يمكن حذف اخر عنصر (على الاقل عنصر واحد فقط)');
                return false;
            }
            $(this).closest('tr').remove();
            return false;
        });
        //====================================================================

    </script>
@endpush
