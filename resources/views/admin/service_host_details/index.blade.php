@extends('admin.layouts.layout')

@php
    $page_title = isset($page_titles['index']) ? $page_titles['index'] : setting('title');
@endphp


@section('page-title')
    {{ $page_title }}
@endsection


@push('admin_css')


@endpush



{{-------------------- start content --------------------}}
@section('content')

    <!-- Main Content-->
    <div class="main-content side-content pt-0">
        <div class="container-fluid">
            <div class="inner-body">

                <div class="page-header">

                    @include('admin.inc.page_header', ['page_title' => $page_title])


                </div>
                <!-- End Page Header -->

                <!--Row-->
                <div class="row sidemenu-height">
                    <div class="col-lg-12">
                        <div class="card">

                            <div class="card-body">
                                <div class="row">

                                    @if($service_host_details->count() > 0)
                                        <div class="col-md-12">

                                            <select onchange="this.options[this.selectedIndex].value && (window.location = this.options[this.selectedIndex].value);" style="width: 20%" class="form-select" aria-label="Default select example">
                                                <option value="" selected disabled>اختر نوع الباقة</option>
                                                @foreach($service_hosts as $service_host)
                                                    <option value="{{ aurl('service').'/'.$service_host->service_id.'?service_host_id='.$service_host->id }}" {{ request()->get('service_host_id') == $service_host->id ? 'selected' : '' }}>{{ $service_host->title }}</option>
                                                @endforeach
                                            </select>


                                            <div class="d-flex flex-end">
                                                <div class="justify-content-center">
                                                    <a href="{{ aurl('service_host_detail/create') }}"
                                                       class="btn ripple btn-primary btn-icon-text">
                                                        <i class="fe fe-plus me-2"></i>{{ isset($page_titles['create']) ? $page_titles['create'] : "#" }}
                                                    </a>
                                                </div>
                                            </div>
                                            <br>


                                            <div class="table-responsive">
                                                <table id="example3"
                                                       class="table table-striped table-bordered">
                                                    <thead>

                                                    <tr>
                                                        <th>#</th>
                                                        <th>العنوان</th>
                                                        <th>نوع الباقة</th>
                                                        <th></th>
                                                    </tr>

                                                    </thead>
                                                    <tbody>

                                                    @foreach($service_host_details as $key => $service_host_detail)
                                                        <tr class="delete-row-{{ $service_host_detail->id }}">
                                                            <td>{{ ++$key }}</td>
                                                            <td>{{ $service_host_detail->title }}</td>
                                                            <td>{{ isset($service_host_detail) && isset($service_host_detail->service_host) ? $service_host_detail->service_host->title : '#' }}</td>
                                                            <td>
                                                                <div class="button-list">
                                                                    {{--<a href="{{ aurl('service_host_detail').'/'.$service_host_detail->id.'/edit' }}"
                                                                       class="btn"><i class="ti ti-pencil"></i></a>--}}
                                                                    <a href="#" id="delete-element"
                                                                       data-id="{{ $service_host_detail->id }}"
                                                                       data-title="{{ $service_host_detail->title }}"
                                                                       class="btn"><i class="ti ti-trash"></i></a>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    @endforeach

                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    @else
                                        @include('admin.inc.there_is_no_elements_to_show')
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


            </div>
        </div>
    </div>
    <!-- End Main Content-->

@endsection
{{-------------------- end content --------------------}}


{{-------------------- scripts --------------------}}
@push('admin_js')

    <script !src="">
        //===================================================================
        $(document).on('click', '#delete-element', function () {

            var url = '{{ aurl('delete-service_host_detail') }}';
            var element_id = $(this).attr('data-id');
            var element_title = $(this).attr('data-title');

            console.log(url);
            var sure = confirm("هل تريد بالفعل حذف " + element_title);

            if (sure == true) {
                $.ajax({
                    url: url,
                    dataType: 'json',
                    type: "POST",
                    data: {_token: '{{ @csrf_token() }}', id: element_id},
                    beforeSend: function () {
                    },
                    success: function (data) {
                        console.log(data);
                        $('.delete-row-'+data.id).fadeOut().remove();
                    },
                    error(response) {
                    }
                });
            }

            return false;
        });
        //====================================================================
    </script>

@endpush


