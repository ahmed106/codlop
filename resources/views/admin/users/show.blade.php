@extends('admin.layouts.layout')

@php
    $page_title = isset($page_titles['show']) ? $page_titles['show'] : setting('title');
@endphp


@section('page-title')
    {{ $page_title }}
@endsection


@push('admin_css')


@endpush



{{-------------------- start content --------------------}}
@section('content')

    <!-- Main Content-->
    <div class="main-content side-content pt-0">
        <div class="container-fluid">
            <div class="inner-body">

                <div class="page-header">
                    @include('admin.inc.page_header', ['page_title' => $page_title])
                    <div class="d-flex flex-end">
                        <div class="justify-content-center">
                            <a href="{{ aurl('user_program/create?user_id='.$user->id) }}"
                               class="btn ripple btn-primary btn-icon-text">
                                <i class="fe fe-plus me-2"></i>{{ isset($page_titles['create']) ? $page_titles['create'] : "#" }}
                            </a>
                        </div>
                    </div>
                </div>
                <!-- End Page Header -->

                <div class="app-details">
                    {{--                    <h3>تفاصيل اكثر :</h3>--}}
                    {{--                    <p>{{ $user->more_details }}</p>--}}
                </div>

                <!--Row-->
                <div class="row sidemenu-height mb-3">
                    <div class="col-lg-12">
                                    @if(count($user_programs) > 0)
                                            <ul class="apps">
                                                @foreach($user_programs as $user_program)

                                                    <li class="delete-row-{{ $user_program->id }}">
                                                        <span class="pull-left">{{change_date_to_new_format($user_program->created_at,'full')}}</span>

                                                        <div class="app-details">
                                                            <h3>
                                                                التطبيق :
                                                                <span>{{ @$user_program->program->title }}</span>
                                                            </h3>
                                                            <h3>
                                                                حالة التطبيق :
                                                                <span>{{ $user_program->status == 'new' ? 'جديد' : 'تم' }}</span>
                                                            </h3>

                                                            <h3>
                                                                عرض التفاصيل :
                                                                <span data-details="{{$user_program->details}}" class="btn show_details">
                                                                    <i class=" fa fa-eye"></i>

                                                                </span>

                                                            </h3>

                                                        </div>


                                                        <div class="main-traffic-detail-item">
                                                            <div><span>نسبة الانجاز</span> <span>{{ $user_program->progress_bar }}%</span>
                                                            </div>
                                                            <div class="progress">
                                                                <div style="width: {{ $user_program->progress_bar }}%;"
                                                                     aria-valuemax="100" aria-valuemin="0"
                                                                     aria-valuenow="70"
                                                                     class="progress-bar progress-bar-xs"
                                                                     role="progressbar"></div>
                                                            </div>
                                                        </div>

                                                        <div class="btns-bttm">
                                                                <a href="{{ aurl('user_program'.'/'.$user_program->id) }}">
                                                                    <i class="fa fa-plus"></i>
                                                                    اضافة مهام
                                                                </a>


                                                                <a href="{{ aurl('chat').'?user_program_id='.$user_program->id }}" class=""
                                                                   style="background: #2d93c1"
                                                                   data-id="{{ $user_program->id }}"
                                                                   data-title="{{ @$user_program->program->title }}"><i
                                                                        class="ti ti-comments"></i> دردشة </a>

                                                                <a href="#" class="delete-element"
                                                                   style="background: #ef4b4b"
                                                                   data-id="{{ $user_program->id }}"
                                                                   data-title="{{ @$user_program->program->title }}"><i
                                                                        class="ti ti-trash"></i> حذف </a>

                                                            @if($user_program->status == 'finished')
                                                                    <a href="#" class="update-status delete-btn-{{ $user_program->id }}"
                                                                       style="background: #3f51b5"
                                                                       data-id="{{ $user_program->id }}"
                                                                       data-title="{{ @$user_program->program->title }}"><i
                                                                            class="ti ti-close"></i> الغاء الاتمام </a>
                                                            @endif

                                                        </div>

                                                    </li>
                                                @endforeach
                                            </ul>
                                    @else
                                        @include('admin.inc.there_is_no_elements_to_show')
                                    @endif
                    </div>
                </div>


            </div>
        </div>


    </div>
    <!-- End Main Content-->


@endsection
{{-------------------- end content --------------------}}


{{-------------------- scripts --------------------}}
@push('admin_js')

    <script !src="">
        //===================================================================
        $(document).on('click', '.delete-element', function () {

            var url = '{{ aurl('delete-user_program') }}';
            var element_id = $(this).attr('data-id');
            var element_title = $(this).attr('data-title');

            console.log(url);
            var sure = confirm("هل تريد بالفعل حذف " + element_title);

            if (sure == true) {
                $.ajax({
                    url: url,
                    dataType: 'json',
                    type: "POST",
                    data: {_token: '{{ @csrf_token() }}', id: element_id},
                    beforeSend: function () {
                    },
                    success: function (data) {
                        console.log(data);
                        $('.delete-row-' + data.id).fadeOut().remove();
                    },
                    error(response) {
                    }
                });
            }

            return false;
        });
        //====================================================================

        // ===================================================================
        $(document).on('click', '.update-status', function () {

            var url = '{{ aurl('update-update_user_program_status') }}';
            var element_id = $(this).attr('data-id');
            var element_title = $(this).attr('data-title');

            console.log(url);
            var sure = confirm("هل تريد بالفعل الغاء اتمام " + element_title);

            if (sure == true) {
                $.ajax({
                    url: url,
                    dataType: 'json',
                    type: "POST",
                    data: {_token: '{{ @csrf_token() }}', id: element_id},
                    beforeSend: function () {
                    },
                    success: function (data) {
                        console.log(data);
                        $('.delete-btn-' + data.id).fadeOut().remove();
                    },
                    error(response) {
                    }
                });
            }

            return false;
        });

        //====================================================================

        $('.show_details').on('click', function () {

            swal.fire({
                input: 'textarea',

                showConfirmButton: true,
                confirmButtonText: '{{__('web_lang.close')}}',
                inputValue: $(this).data('details') != '' ? $(this).data('details') : '{{trans('admin.no_data_found')}} !',
                inputAttributes: {
                    readonly: true,
                },

            })
        })
    </script>

@endpush


