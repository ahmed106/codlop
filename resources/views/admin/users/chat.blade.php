@extends('admin.layouts.layout')

@php
    $page_title = isset($page_titles['index']) ? $page_titles['index'] : setting('title');
@endphp


@section('page-title')
    {{ $page_title }}
@endsection


@push('admin_css')


@endpush



{{-------------------- start content --------------------}}
@section('content')

    <!-- Main Content-->
    <div class="main-content side-content pt-0">
        <div class="container-fluid">
            <div class="inner-body">

                <div class="page-header">
                    <div>
                        <h2 class="main-content-title tx-24 mg-b-5">المحادثة</h2>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">الرئيسية</a></li>
                            <li class="breadcrumb-item active" aria-current="page">المحادثة</li>
                        </ol>
                    </div>
                </div> <!-- End Page Header -->
                <!--Row-->
                <div class="row row-sm">
                    <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12">
                        <div class="card custom-card">
                            <div class="main-content-app pt-0">
                                <div class="main-content-body main-content-body-chat">

                                    <div class="main-chat-body append-message ChatBody" id="ChatBody" style="padding-top: 20px; overflow: scroll;">

                                        @if( isset($user_program_room->user_program_chats) && count($user_program_room->user_program_chats) > 0)

                                            @foreach($user_program_room->user_program_chats as $user_program_chat)

                                                @if(isset($user_program_chat->user))
                                                    <div class="media">
                                                        <div class="media-body">
                                                            <h6> {{ @$user_program_chat->user->name }} </h6>
                                                            <div class="main-msg-wrapper"> {{ $user_program_chat->message }} </div>
                                                            <div>
                                                                <span>
                                                                    {{ change_date_to_new_format($user_program_chat->created_at, 'time_ar_pm_am') }}
                                                                    {{ change_date_to_new_format($user_program_chat->created_at, 'small_date') }}
                                                                </span>
                                                                <a href=""><i class="icon ion-android-more-horizontal"></i></a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                @else
                                                    <div class="media flex-row-reverse">
                                                        <div class="media-body">
                                                            <h6> الادارة </h6>
                                                            <div class="main-msg-wrapper"> {{ $user_program_chat->message }}  </div>
                                                            <div>
                                                            <span>
                                                                {{ change_date_to_new_format($user_program_chat->created_at, 'time_ar_pm_am') }}
                                                                {{ change_date_to_new_format($user_program_chat->created_at, 'small_date') }}
                                                            </span>
                                                                <a href=""><i class="icon ion-android-more-horizontal"></i></a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                @endif

                                            @endforeach

                                        @endif

                                    </div>

                                    <form id="chat-submit" action="{{ route('msg.send') }}" enctype="multipart/form-data">
                                        <div class="main-chat-footer chat-input">
                                            @csrf
                                            <input name="user_program_room_id" value="{{ $user_program_room->id }}" type="hidden">
                                            <input name="user_type" value="admin" type="hidden">
                                            <input name="message" type="text" id="chat-input" class="form-control" placeholder="Type your message here...">
                                            <button type="button" class="btn main-msg-send chat-submit"><i class="far fa-paper-plane"></i></button>
                                        </div>
                                    </form>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Main Content-->

@endsection
{{-------------------- end content --------------------}}


{{-------------------- scripts --------------------}}
@push('admin_js')

    <script !src="">

        $(document).on('click', '.chat-submit', function () {
            $('#chat-submit').submit();
        });

        $(function () {
            $(".ChatBody").stop().animate({scrollTop: $(".ChatBody")[0].scrollHeight}, 1000);
        });

        //===================================================================
        $(document).on('submit', '#chat-submit', function (e) {
            e.preventDefault();
            var msg = $("#chat-input").val();
            if (msg.trim() == '') {
                return false;
            }
            formData = $(this).serialize();

            $.ajax({
                type: 'POST',
                url: "{{ route('msg.send') }}",
                data: formData, // here $(this) refers to the ajax object not form
                success: function (data) {
                    console.log(data);
                    generate_message(data, 'self');
                },
            });
            return false;
        });


        function generate_message(data, type) {

            // console.log( data );

            var str = `<div class="media flex-row-reverse">
                            <div class="media-body">
                                <h6> الادارة </h6>
                                <div class="main-msg-wrapper"> ${data.msg} </div>
                                <div>
                                <span>
                                  {{ change_date_to_new_format(date('Y-m-d'), 'time_ar_pm_am') }}
            {{ change_date_to_new_format(date('Y-m-d'), 'small_date') }}
            </span>
                <a href=""><i class="icon ion-android-more-horizontal"></i></a>
            </div>
        </div>
    </div>`;

            $(".append-message").append(str);

            if (type == 'self') {
                $("#chat-input").val('');
            }
            try {
                $(".ChatBody").stop().animate({scrollTop: $(".ChatBody")[0].scrollHeight}, 1000);
            } catch (e) {
            }
        }

        //====================================================================
    </script>



@endpush


