@extends('admin.layouts.layout')

@php
    $page_title = isset($page_titles['index']) ? $page_titles['index'] : setting('title');
@endphp


@section('page-title')
    {{ $page_title }}
@endsection


@push('admin_css')


@endpush



{{-------------------- start content --------------------}}
@section('content')

    <!-- Main Content-->
    <div class="main-content side-content pt-0">
        <div class="container-fluid">
            <div class="inner-body">

                <div class="page-header">

                    @include('admin.inc.page_header', ['page_title' => $page_title])

                    {{--                    <div class="d-flex flex-end">--}}
                    {{--                        <div class="justify-content-center">--}}
                    {{--                            <a href="{{ aurl('user/create') }}" class="btn ripple btn-primary btn-icon-text">--}}
                    {{--                                <i class="fe fe-plus me-2"></i>{{ isset($page_titles['create']) ? $page_titles['create'] : "#" }}</a>--}}
                    {{--                        </div>--}}
                    {{--                    </div>--}}
                </div>
                <!-- End Page Header -->


                <!--Row-->
                <div class="row sidemenu-height">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    @if(count($users) > 0)
                                        <div class="col-md-12">
                                            <div class="table-responsive">
                                                <table id="example3"
                                                       class="table table-striped table-bordered">
                                                    <thead>

                                                    <tr>
                                                        <th>#</th>
                                                        <th>مقرؤه</th>
                                                        <th>اسم العميل</th>
                                                        <th>ايميل العميل</th>
                                                        <th>جوال العميل</th>
                                                        <th>التاريخ</th>
                                                        <th>نوع الباقة</th>
                                                        <th>التطبيقات</th>
                                                        <th>حذف</th>
                                                    </tr>

                                                    </thead>
                                                    <tbody>

                                                    @foreach($users as $key => $user)
                                                        <tr class="delete-row-{{ $user->id }}">
                                                            <td>{{ ++$key }}</td>
                                                            <td><label class="ckbox my-auto me-4 mt-1"><input {{ $user->is_read == 'read' ? 'checked' : '' }} type="checkbox" disabled><span></span></label></td>
                                                            <td>{{ $user->name }}</td>
                                                            <td>{{ $user->email }}</td>
                                                            <td>{{ $user->phone.'('.$user->phone_code.')' }}</td>
                                                            <td>{{ change_date_to_new_format($user->created_at) }}</td>
                                                            <td>
                                                                @if(isset($user->offer) && $user->offer->offer_type == 'professional_package')
                                                                    الباقة الاحترافية
                                                                @elseif(isset($user->offer) && $user->offer->offer_type == 'golden_package')
                                                                    الباقة الذهبية
                                                                @else
                                                                    الباقة الاساسية
                                                                @endif
                                                            </td>
                                                            <td>
                                                                <div class="button-list">
                                                                    <a href="{{ aurl('user'.'/'.$user->id) }}" class="btn btn-primary flex">
                                                                        <i class="ti ti-eye"></i>
                                                                        عرض
                                                                    </a>
                                                                </div>
                                                            </td>
                                                            <td>
                                                                <div class="button-list">
                                                                    <a href="#" id="delete-element" data-id="{{ $user->id }}" data-title="{{ $user->name }}" class="btn"><i class="ti ti-trash"></i></a>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    @endforeach

                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    @else
                                        @include('admin.inc.there_is_no_elements_to_show')
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


            </div>
        </div>
    </div>
    <!-- End Main Content-->

@endsection
{{-------------------- end content --------------------}}


{{-------------------- scripts --------------------}}
@push('admin_js')

    <script !src="">
        //===================================================================
        $(document).on('click', '#delete-element', function () {

            var url = '{{ aurl('delete-user') }}';
            var element_id = $(this).attr('data-id');
            var element_title = $(this).attr('data-title');

            console.log(url);
            var sure = confirm("هل تريد بالفعل حذف " + element_title);

            if (sure == true) {
                $.ajax({
                    url: url,
                    dataType: 'json',
                    type: "POST",
                    data: {_token: '{{ @csrf_token() }}', id: element_id},
                    beforeSend: function () {
                    },
                    success: function (data) {
                        console.log(data);
                        $('.delete-row-' + data.id).fadeOut().remove();
                    },
                    error(response) {
                    }
                });
            }

            return false;
        });
        //====================================================================
    </script>

@endpush


