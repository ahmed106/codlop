@extends('admin.layouts.layout')

@php
    $page_title = isset($page_titles['create']) ? $page_titles['create'] : setting('title');
@endphp


@section('page-title')
    {{ $page_title }}
@endsection


@push('admin_css')


@endpush



{{-------------------- start content --------------------}}
@section('content')

    <div class="main-content side-content pt-0">
        <div class="container-fluid">
            <div class="inner-body">

                <div class="page-header">
                @include('admin.inc.page_header', ['page_title' => $page_title])

</div>
            <!-- End Page Header -->


                <!--Row-->
                <div class="row sidemenu-height">
                    <div class="col-lg-12">
                        <div class="card">

                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-12">

                                        <form action="{{ route('user_program.store') }}" method="post"
                                              enctype="multipart/form-data">
                                            @csrf

                                            <input type="hidden" value="{{ request()->get('user_id') }}" name="user_id">

                                            <div class="table-responsive tasks apps-list">
                                                <ul>
                                                    @if(count($programs) > 0)
                                                        @foreach($programs as $program)
                                                            <li>
                                                                <input name="programs[]" value="{{ $program->id }}"
                                                                       type="checkbox" id="program-{{ $program->id }}">
                                                                <label for="program-{{ $program->id }}">
                                                                    <img src="{{ GetImg($program->icon) }}" alt="">
                                                                    {{ $program->title }}
                                                                </label>
                                                            </li>
                                                        @endforeach
                                                    @else
                                                        @include('website.handle-pages.not-found')
                                                    @endif

                                                </ul>
                                            </div>

                                            <div class="card-footer pd-0 pt-3">
                                                    <button type="submit" class="btn ripple btn-primary btn-lg">
                                                        <i class="fa fa-save"></i>
                                                        حفظ
                                                    </button>
                                            </div>

                                        </form>

                                    </div>
                                </div>
                            </div>


                        </div>
                    </div>
                </div>


            </div>
        </div>
    </div>

@endsection
{{-------------------- end content --------------------}}


{{-------------------- scripts --------------------}}
@push('admin_css')


@endpush


