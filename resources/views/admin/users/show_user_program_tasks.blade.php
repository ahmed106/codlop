@extends('admin.layouts.layout')

@php
    $page_title = isset($page_titles['index']) ? $page_titles['index'] : setting('title');
@endphp


@section('page-title')
    {{ $page_title }}
@endsection


@push('admin_css')


@endpush



{{-------------------- start content --------------------}}
@section('content')

    <!-- Main Content-->
    <div class="main-content side-content pt-0">
        <div class="container-fluid">
            <div class="inner-body">

                <div class="page-header">

                    @include('admin.inc.page_header', ['page_title' => $page_title])

                </div>
                <!-- End Page Header -->


                <!--Row-->
                <div class="row sidemenu-height">
                    <div class="col-lg-12">
                        <div class="card">

                            <div class="card-header d-flex justify-content-between">
                                <div class="header-title">
                                    <h4 class="card-title">اضافة مهام</h4>
                                </div>
                            </div>

                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-12">


                                        <div class="main-traffic-detail-item">
                                            <div><span>نسبة الانجاز</span> <span>{{ $user_program->progress_bar }}%</span></div>
                                            <div class="progress">
                                                <div style="width: {{ $user_program->progress_bar }}%;" aria-valuemax="100" aria-valuemin="0"
                                                     aria-valuenow="70"
                                                     class="progress-bar progress-bar-xs"
                                                     role="progressbar"></div>
                                            </div>
                                        </div>
                                        <br>
                                        <form action="{{ aurl('update-user_program') }}" method="post" enctype="multipart/form-data">
                                            @csrf

                                            <input type="hidden" value="{{ $user_program->id }}" name="user_program_id">

                                            <div class="col-md-6 form-group">
                                                <label>{{'اللينك'}} </label>
                                                <input name="link" value="{{ $user_program->link }}" class="form-control" type="text" placeholder="اللينك">
                                            </div>

                                            <hr>
                                            <div class="table-responsive tasks mb-3">
                                                <table class="table task-table">
                                                    <thead>
                                                    <tr>
                                                        <th>المهام</th>
                                                        <th>اتمام</th>
                                                        <th>حذف</th>
                                                    </tr>
                                                    </thead>

                                                    <tbody id="append-task">
                                                    @if(count($user_program_tasks) > 0)
                                                        @foreach($user_program_tasks as $key => $user_program_task)
                                                            <tr>
                                                                <td>
                                                                    <input name="tasks[{{ $user_program_task->id }}]" placeholder="اكتب هنا" value="{{ $user_program_task->task }}" class="form-control" type="text" required>
                                                                </td>

                                                                <td>
                                                                    <label class="ckbox">
                                                                        <input name="status[{{ $user_program_task->id }}]" type="checkbox" value="check" {{$user_program_task->status == 'check' ? 'checked' : ''}}><span></span>
                                                                    </label>
                                                                </td>

                                                                <td>
                                                                    <a href="#" data-id="id" data-title="title" class="btn btn-danger delete-task"><i class="ti ti-trash"></i></a>
                                                                </td>
                                                            </tr>
                                                        @endforeach
                                                    @else
                                                        <tr>
                                                            <td>
                                                                <input name="tasks[]" placeholder="اكتب هنا" class="form-control" type="text">
                                                            </td>

                                                            <td>
                                                                <label class="ckbox">
                                                                    <input name="status[]" type="checkbox" value="check"><span></span>
                                                                </label>
                                                            </td>

                                                            <td>
                                                                <a href="#" data-id="id" data-title="title" class="btn btn-danger delete-task"><i class="ti ti-trash"></i></a>
                                                            </td>
                                                        </tr>
                                                    @endif

                                                    </tbody>

                                                    <tfoot>
                                                    <tr>
                                                        <td colspan="2">
                                                            <button type="submit" id="add-new-task" class="btn ripple btn-success btn-sm">
                                                                <i class="fa fa-plus"></i>
                                                                اضافة مهمة جديدة
                                                            </button>


                                                        </td>
                                                    </tr>
                                                    </tfoot>

                                                </table>
                                            </div>

                                            <div class="card-footer">
                                                    <button type="submit" class="btn ripple btn-primary btn-lg">
                                                        <i class="fa fa-save"></i>
                                                        حفظ
                                                    </button>

                                                    <a href="{{ aurl('user/'.$user->id) }}" class="btn ripple btn-danger btn-lg">
                                                        <i class="fa fa-backward"></i>
                                                        عودة
                                                    </a>
                                            </div>

                                        </form>

                                    </div>
                                </div>
                            </div>


                        </div>
                    </div>
                </div>


            </div>
        </div>
    </div>
    <!-- End Main Content-->

@endsection
{{-------------------- end content --------------------}}


{{-------------------- scripts --------------------}}
@push('admin_js')

    <script !src="">
        //===================================================================
        $(document).on('click', '#add-new-task', function () {

            var append_task = `<tr>
                                    <td>
                                        <input name="tasks[]" placeholder="اكتب هنا" value="" class="form-control" type="text" required>
                                    </td>

                                    <td></td>

                                    <td>
                                        <a href="#" data-id="id" data-title="title" class="btn btn-danger delete-task"><i class="ti ti-trash"></i></a>
                                    </td>
                                </tr>`;

            $('#append-task').append(append_task);

            return false;
        });
        //====================================================================

        //===================================================================
        $(document).on('click', '.delete-task', function () {

            if ($('.delete-task').length <= 1) {
                alert('لا يمكن مسح اخر عنصر');
                return false;
            }
            $(this).closest('tr').remove();
            return false;
        });
        //====================================================================
    </script>

@endpush


