@extends('admin.layouts.layout')
@section('content')
    <!-- Main Content-->
    <div class="main-content side-content pt-0">
        <div class="container-fluid">
            <div class="inner-body">
                <div class="page-header">


                    <div class="d-flex flex-end">
                        <div class="justify-content-center">

                        </div>
                    </div>
                </div>
                <!-- End Page Header -->


                <!--Row-->
                <div class="row sidemenu-height">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col">
                                        <div style="width:500px;" id="reader"></div>
                                    </div>
                                    <div class="col" style="padding:30px;">
                                        <h4>SCAN RESULT</h4>
                                        <div id="result">Result Here</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <script src="https://reeteshghimire.com.np/wp-content/uploads/2021/05/html5-qrcode.min_.js"></script>

    <style>
        .result {
            background-color: green;
            color: #fff;
            padding: 20px;
        }

        .row {
            display: flex;
        }
    </style>

    <script type="text/javascript">
        function onScanSuccess(qrCodeMessage) {
            let url_string = new URL(qrCodeMessage);
            console.log(url_string.search)
            $.ajax({
                type: "get",
                url: "{{url('admin/test-qr')}}",
                data: {
                    search: url_string.search
                },
                success: function () {
                    window.location.href = '{{url('/admin')}}'
                },
                error: function (data) {

                    alert(data.responseText);
                }
            })

            document.getElementById('result').innerHTML = '<span class="result">' + qrCodeMessage + '</span>';
        }

        function onScanError(errorMessage) {
            //handle scan error
        }

        var html5QrcodeScanner = new Html5QrcodeScanner(
            "reader", {fps: 10, qrbox: 250});
        html5QrcodeScanner.render(onScanSuccess, onScanError);
    </script>

@endsection
