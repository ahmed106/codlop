@extends('website.layouts.layout')

@section('page_title')
    {{ setting('title') }}
@endsection


@section('content')

    <!-- ================ content ================= -->

    <!-- START SECTION BANNER -->
    @include('website.pages.component.pages-banner', ['page_title' => trans('web_lang.Chat with admin')])
    <!-- END SECTION BANNER -->

    <section class="small_pt">
        <div class="container">
            <div class="contact-us chat">
                <div class="row">
                    <div class="col-md-5">

                        <h3>@lang('web_lang.more details')</h3>
                        <hr>
                        <lable>@lang('web_lang.service_type') :</lable>
                        <img src="{{GetImg($program->program->icon)}}" alt="">

                        <span>{{$program->program->title}}</span>
                        <br>
                        <br>
                        <label for="">{{__('web_lang.start_date_service')}} :</label>

                        {{change_date_to_new_format($program->created_at)}}


                        <br>

                        @if($program->details !=null)
                            <lable>@lang('web_lang.details') :</lable>
                            <br>
                            <br>
                            <div class="overflow-scroll" style="width: 400px;height: 280px; overflow: auto">

                                {{$program->details}}
                            </div>

                            <br>
                            <br>
                        @endif

                    </div>
                    <div class="col-lg-7">
                        <div class="msger" style="height: 500px;">
                            <div class="msger-header">
                                <h3 class="msger-header-title" style="font-size: 1.75rem">
                                    <i class="fa fa-comment-o"></i>
                                    {{ trans('web_lang.Chat with admin') }}
                                </h3>
                            </div>
                            <main class="msger-chat chatBody" id="append-message">

                                @if( isset($user_program_room->user_program_chats) && count($user_program_room->user_program_chats) > 0)

                                    @foreach($user_program_room->user_program_chats as $user_program_chat)

                                        <div style="" class="msg {{$user_program_chat->user_type == 'user' ? 'right' : 'left'}}-msg">
                                            <div class="msg-bubble">
                                                <div class="msg-info">
                                                    <div class="msg-info-name">{{ isset($user_program_chat->user) ? $user_program_chat->user->name : trans('web_lang.admin') }}</div>
                                                    <div class="msg-info-time ml-2">{{ change_date_to_new_format($user_program_chat->created_at, 'small_date') }}</div>
                                                    <div class="msg-info-time ">{{ change_date_to_new_format($user_program_chat->created_at, 'time_ar_pm_am') }}</div>
                                                </div>

                                                <div class="msg-text">
                                                    {{ $user_program_chat->message }}
                                                </div>

                                            </div>
                                        </div>

                                    @endforeach
                                @endif

                            </main>

                            <div class="chat-input">
                                <form id="chat-submit" class="msger-inputarea" action="{{ route('msg.send') }}" enctype="multipart/form-data">
                                    @csrf
                                    {{--hidden inputs --}}
                                    <input name="user_program_room_id" value="{{ $user_program_room->id }}" type="hidden">
                                    <input name="from_user_id" value="{{ auth()->user() ? auth()->user()->id : null }}" type="hidden">
                                    <input name="user_type" value="user" type="hidden">
                                    {{--hidden inputs--}}

                                    <input name="message" type="text" id="chat-input" class="msger-input" placeholder="{{ trans('web_lang.send a message ...') }}">
                                    <button type="submit" class="btn btn-default msger-send-btn chat-submit">{{ trans('web_lang.send') }}</button>
                                </form>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- ================ content ================= -->

@endsection

@push('web_js')

    <script>
        $(function () {
            $(".chatBody").stop().animate({scrollTop: $(".chatBody")[0].scrollHeight}, 1000);

            var INDEX = 0;

            $(document).on("submit", "#chat-submit", function (e) {
                e.preventDefault();
                var msg = $("#chat-input").val();
                if (msg.trim() == '') {
                    return false;
                }
                formData = $(this).serialize();
                console.log(formData);
                $.ajax({
                    type: 'POST',
                    url: "{{ route('msg.send_msg') }}",
                    data: formData, // here $(this) refers to the ajax object not form
                    success: function (data) {
                        generate_message(data, 'self');
                    },
                });
            });


            function generate_message(data, type) {

                // console.log( data );

                var str = `<div class="msg right-msg">
                                            <div class="msg-bubble">
                                                <div class="msg-info">
                                                    <div class="msg-info-name">{{ auth()->user() ? auth()->user()->name : null }}</div>
                                                          <div class="msg-info-time ml-2">{{ change_date_to_new_format(date('Y-m-d H:i:s'), 'small_date') }}</div>
                                                    <div class="msg-info-time">{{ change_date_to_new_format(date('Y-m-d H:i:s'), 'time_ar_pm_am') }}</div>

                                                </div>
                                                <div class="msg-text">
                                                    ${data.msg}
                                                </div>
                                            </div>
                                        </div>`;

                $("#append-message").append(str);

                if (type == 'self') {
                    $("#chat-input").val('');
                }
                try {
                    $(".chatBody").stop().animate({scrollTop: $(".chatBody")[0].scrollHeight}, 1000);
                } catch (e) {
                }
            }


        })
    </script>

@endpush






