@extends('website.layouts.layout')

@section('page_title')
    {{ setting('title') }}
@endsection


@section('content')

    <!-- ================ content ================= -->

    <!-- START SECTION BANNER -->
    @include('website.pages.component.pages-banner', ['page_title' => trans('web_lang.add apps')])
    <!-- END SECTION BANNER -->


    <section class="small_pt contact-us pb-220">
        <div class="container">
            <div class="row">
                <div class="col-lg-8">
                    <form method="post" action="{{ route('add-apps') }}" class="field_form_s2">
                        @csrf
                        <div class="row">
                            @if(count($programs) > 0)
                                <div class="form-group col-md-12 animation " data-animation="fadeInUp"
                                     data-animation-delay=".5s">
                                    <label class="label-custom"
                                           for="">{{ trans('web_lang.Choose the desired programme') }}</label>

                                    <ul class="check-box-div2">
                                        @foreach($programs as $program)
                                            <li>
                                                <input name="programs[]" value="{{ $program->id }}" type="checkbox"
                                                       id="program-{{ $program->id }}">
                                                <label for="program-{{ $program->id }}">
                                                    <img style="max-height: 30px;"
                                                         src="{{ GetImg($program->icon) }}" alt="">
                                                    {{ $program->title }}
                                                </label>
                                            </li>
                                        @endforeach
                                    </ul>

                                </div>

                                <div class="col-md-12 text-center animation " data-animation="fadeInUp"
                                     data-animation-delay=".9s">
                                    <button type="submit" class="btn btn-default btn-radius" name="submit" value="">
                                        {{ trans('web_lang.add') }} <i class="fa fa-long-arrow-left"></i></button>
                                </div>

                            @else
                                @include('website.handle-pages.not-found')
                                <div class="col-md-12 text-center animation " data-animation="fadeInUp"
                                     data-animation-delay=".9s">
                                    <a href="{{ url('profile') }}" class="btn btn-default btn-radius" name="submit" value="">
                                    {{ trans('web_lang.back') }}
                                    {{--                                        <i class="fa fa-long-arrow-left"></i></a>--}}
                                </div>
                            @endif

                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>


    <!-- ================ content ================= -->

@endsection

@push('web_js')


@endpush






