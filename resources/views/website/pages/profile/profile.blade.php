@extends('website.layouts.layout')

@section('page_title')
    {{ setting('title') }}
@endsection


@section('content')

    <!-- ================ content ================= -->

    <!-- START SECTION BANNER -->
    @include('website.pages.component.pages-banner', ['page_title' => trans('web_lang.My_services')])
    <!-- END SECTION BANNER -->


    <section class="my-apps pb-220">
        <div class="container">

            <div class="row">


                <div class="add-app mb-5">
                    <a href="{{ url('order-now') }}" class="btn btn-default btn-radius"> <i class="fa fa-plus"></i> {{ trans('web_lang.add new service') }}</a>
                </div>


                {{--                <div class="col-sm-12">--}}
                {{--                    <div class="choose-project">--}}
                {{--                        <h3>{{ trans('web_lang.Choose the project') }}</h3>--}}
                {{--                        <select name="" id="">--}}
                {{--                            <option value="">تست</option>--}}
                {{--                            <option value="">تست</option>--}}
                {{--                        </select>--}}
                {{--                    </div>--}}
                {{--                </div>--}}

                <div class="col-sm-12">
                    <div class="apps">
                        <ul class="app">


                            @if(count($user_programs) > 0)
                                @foreach($user_programs as $index => $user_program)

                                    {{-- programs --}}
                                    <li>

                                        <div class="app-box">
                                            <div class="app-type">
                                                <img style="/*width: 100%;height: 100%;*/" src="{{ GetImg(@$user_program->program->icon) }}" alt="">
                                                <p>{{ @$user_program->program->title }}</p>
                                                <p>

                                                    <button href="#" id="showDetails-{{$index}}" data-user_program_id="{{$user_program->id}}" data-user_id="{{$user_program->user_id}}" data-toggle="modal" data-target="#exampleModal" class="showDetails "><i class=" fa  fa-eye"></i> @lang('web_lang.more details')</button>
                                                </p>
                                            </div>


                                        </div>

                                        <div class="link-btns">
                                            @if($user_program->link == null || $user_program->link == "#!")
                                                <a class="app-link" href="{{ $user_program->link }}" style="color: red !important;"><i class="fa fa-link"></i>
                                                    {{ trans('web_lang.The link is not available now') }}
                                                </a>
                                            @else
                                                <a target="_blank" class="app-link" href="{{ $user_program->link }}"><i class="fa fa-link"></i>
                                                    {{ trans('web_lang.The link is available now') }}
                                                </a>
                                            @endif

                                            <div class="btns-div">
                                                <a href="{{ url('chat?user_program_id='.$user_program->id.'&&program_id='.$user_program->id) }}" class="btn-custom-style "> <i class="fa fa-comment-o"></i>
                                                    {{ trans('web_lang.Chat with admin') }}
                                                </a>
                                                @if($user_program->status == 'new')

                                                    <button id="{{ $user_program->id }}" title="{{ @$user_program->program->title }}" class="btn-custom-style check-box-div2 confirm-program confirm-program-{{ $user_program->id }}">
                                                        <input type="checkbox" id="user_program-{{ $user_program->id }}">
                                                        <label for="user_program-{{ $user_program->id }}">
                                                            {{ trans('web_lang.completion') }}
                                                        </label>
                                                    </button>
                                                @else


                                                    <button class="{{--btn-custom-style--}} btn-outline-success  check-box-div2">
                                                        <input class="btn btn-success" type="checkbox" id="user_program-{{ $user_program->id }}" checked disabled>
                                                        <label id="user-program-{{ $user_program->id }}" for="user_program-{{ $user_program->id }}">
                                                            {{ trans('web_lang.It was received') }}
                                                        </label>
                                                    </button>
                                                @endif


                                            </div>
                                        </div>


                                        <div class="progress-div text-center">

                                            <div class="mb-3">
                                                @lang('web_lang.start_date_service')
                                                <br>
                                                <span class="text-center">
                                                    {{change_date_to_new_format($user_program->created_at)}}
                                                </span>
                                            </div>
                                            <div class="progress-bar" data-percent="{{ $user_program->progress_bar > 0 ? $user_program->progress_bar : '0.0' }}" data-duration="1000">
                                                <span class="progress_min_val">
                                                    {{ trans('web_lang.Completion rate') }}
                                                </span>
                                            </div>
                                        </div>
                                    </li>
                                    {{-- programs --}}
                                @endforeach
                            @else
                                @include('website.handle-pages.not-found')
                            @endif


                        </ul>
                    </div>
                    <br>


                </div>

            </div>
        </div>
    </section>

    <!-- ================ content ================= -->


    <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                {{--                <div class="modal-header">--}}
                {{--                    <h5 class="modal-title" id="exampleModalLabel">@lang('web_lang.more details')</h5>--}}

                {{--                </div>--}}
                <div class="modal-body">

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">@lang('web_lang.close')</button>

                </div>
            </div>
        </div>
    </div>
@endsection

@push('web_js')

    <script>
        $(".progress-bar").loading();

        //===================================================================
        $(document).on('click', '.confirm-program', function () {

            var url = '{{ route('confirm-program') }}';
            var user_program_id = $(this).attr('id');
            var user_program_title = $(this).attr('title');

            console.log(user_program_id);

            var sure = confirm(" {{ trans('web_lang.Do you really want to confirm receipt of the application?') }} " + user_program_title);

            if (sure == true) {
                $.ajax({
                    url: url,
                    dataType: 'json',
                    type: "POST",
                    data: {_token: '{{ @csrf_token() }}', user_program_id: user_program_id},
                    beforeSend: function () {
                    },
                    success: function (data) {

                        $('.confirm-program-' + data.user_program_id).removeClass('confirm-program').addClass('btn-success');
                        //$('.chat-program-' + data.user_program_id).remove();
                        $('.confirm-program-' + data.user_program_id + ' input').attr('disabled', true).attr('checked', 'checked');
                        $('.confirm-program-' + data.user_program_id + ' label').text('{{ trans('web_lang.It was received') }}');
                    },
                    error(response) {
                    }
                });
            }

            return false;
        });
        //====================================================================
    </script>

    @foreach($user_programs as $index => $program)
        <script>
            $(document).on('click', '#showDetails-{{$index}}', function () {

                $('#exampleModal').modal().hide()

                let program_id = '{{$program->id}}',
                    user_program_id = $(this).data('user_program_id');
                $.ajax({
                    type: 'get',
                    url: '{{route('getUserServiceDescription')}}',
                    dataType: 'json',
                    data: {
                        program_id: program_id
                    },
                    success: function (response) {
                        $('.modal-body').empty();
                        if (response.data) {
                            let btn = '';
                            if (response.data.length > 500) {
                                btn = `....<br><br><a href="{{url('/chat?user_program_id='.$program->id.'&&program_id='.$program->id)}}" class="btn btn-default"> قراءه المزيد</a>`;
                            }
                            $('.modal-body').append(response.data.slice(0, 500) + btn);
                        } else {
                            $('.modal-body').append('{{__('admin.no_data_found')}}');

                        }

                        $('#exampleModal').modal().show()
                    }

                })

            })
        </script>
    @endforeach


@endpush






