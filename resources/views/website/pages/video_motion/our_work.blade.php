@extends('website.layouts.layout')

@section('page_title')
    {{ setting('title') }}
@endsection


@section('content')

    <!-- ================ content ================= -->

    <!-- START SECTION BANNER -->
    @include('website.pages.component.pages-banner', ['page_title' => trans('web_lang.our work')])
    <!-- END SECTION BANNER -->


    <section class="portfolio-section small_pt">
        <div class="container">

            <div class="portfolio-tab">
                <ul>
                    <li class="filter " data-filter="all">{{ trans('web_lang.all_works') }}</li>
                    @foreach($categories as $index  => $cat)
                        <li class="filter" data-filter=".{{$cat[0]->type}}">{{__('web_lang.'.$index)}}</li>
                    @endforeach


                </ul>
            </div>

            <div class="portfolio-grid">


                @foreach($all_video_motions as $videos)
                    <div class="portfolio mix {{$videos->type}}">
                        <div class="card">
                            <div class="content">

                                <div class="front">
                                    <img class="profile" src="{{ GetImg( $videos->image ) }}" alt="">
                                    <h2>{{ $videos->title }}</h2>
                                </div>

                                <a href="{{ url('work_details').'/'. $videos->id }}" class="back from-bottom">
                                    <h2>{{ $videos->title }}</h2>
                                    <p class="des"> {!! strip_tags(\Illuminate\Support\Str::limit($videos->text,100)) !!} </p>
                                    <span>{{ trans("web_lang.View details") }}</span>
                                </a>

                            </div>
                        </div>
                    </div>
                @endforeach


            </div>
        </div>

    </section>


    <!-- ================ content ================= -->

@endsection




@push('web_js')

@endpush


