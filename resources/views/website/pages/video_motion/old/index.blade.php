@extends('website.layouts.layout')

@section('page_title')
    {{ setting('title') }}
@endsection


@section('content')

    <!-- ================ content ================= -->

    <!-- START SECTION BANNER -->
    @include('website.pages.component.pages-banner', ['page_title' => trans('web_lang.Video motions')])
    <!-- END SECTION BANNER -->


     <section class="pb-220 pt-50">

        {{-- loader --}}
        @include('website.pages.component.loader')

        <div class="container">

            {{--<button id="button" type="button">Click</button>--}}

            @if(count($video_motions) > 0)

                <ul class="motion">

                    @foreach($video_motions as $video_motion)

                        <li>
                            <a video-src="{{ GetImg($video_motion->video) }}" title-src="{{ $video_motion->title }}" text-src="{{ $video_motion->text }}" class="show-video" href="javascript:;" data-toggle="modal" data-target="#staticBackdrop">
                                <div class="img-div">
                                    <img src="{{ GetImg($video_motion->image) }}" alt="">
                                    <i class="fa fa-play"></i>
                                </div>
                                <h3>{{ $video_motion->title }}</h3>
                            </a>
                        </li>

                    @endforeach

                </ul>

                <nav>
                    @if ($video_motions->lastPage() > 1)
                        <ul class="pagination justify-content-center">

                            <li class="{{ ($video_motions->currentPage() == 1) ? ' disabled' : '' }}">
                                @if($video_motions->currentPage() == 1)
                                    <a href="#!" style="cursor: default">{{ trans('web_lang.previous') }}</a>
                                @else
                                    <a href="{{ $video_motions->url($video_motions->currentPage()-1) }}">{{ trans('web_lang.previous') }}</a>
                                @endif
                            </li>

                            @for ($i = 1; $i <= $video_motions->lastPage(); $i++)
                                <li class="{{ ($video_motions->currentPage() == $i) ? ' active' : '' }}">
                                    <a href="{{ $video_motions->url($i) }}">{{ $i }}</a>
                                </li>
                            @endfor

                            <li class="{{ ($video_motions->currentPage() == $video_motions->lastPage()) ? ' disabled' : '' }}">
                                @if($video_motions->currentPage() == $video_motions->lastPage())
                                    <a href="#!" style="cursor: default">{{ trans('web_lang.next') }}</a>
                                @else
                                    <a href="{{ $video_motions->url($video_motions->currentPage()+1) }}">{{ trans('web_lang.next') }}</a>
                                @endif
                            </li>

                        </ul>
                    @endif
                </nav>

            @else
                @include('website.handle-pages.not-found')
            @endif

        </div>
    </section>


    <!-- Modal -->
    <div class="modal fade modal-motion" id="staticBackdrop" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg">
            <div class="modal-content">
                <div class="modal-body">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>


                    <div class="change-content">
{{--                        <video class="video-fluid z-depth-1 video-data" autoplay loop controls si>--}}
{{--                            <source class="video-data" src="https://mdbootstrap.com/img/video/Sail-Away.mp4" type="video/mp4" />--}}
{{--                        </video>--}}
{{--                        <div class="details-motion">--}}
{{--                            <h3 class="title-desc">عنوان الفيديو</h3>--}}
{{--                            <p class="text-desc">--}}
{{--                                لوريم ايبسوم هو نموذج افتراضي يوضع في التصاميم لتعرض على العميل ليتصور طريقه وضع النصوص بالتصاميم سواء كانت تصاميم مطبوعه ... بروشور او فلاير على سبيل المثال ... او نماذج مواقع انترنت ...--}}

{{--                                وعند موافقه العميل المبدئيه على التصميم يتم ازالة هذا النص من التصميم ويتم وضع النصوص النهائية المطلوبة للتصميم ويقول البعض ان وضع النصوص التجريبية بالتصميم قد تشغل المشاهد عن وضع الكثير من الملاحظات او الانتقادات للتصميم الاساسي.--}}

{{--                                وخلافاَ للاعتقاد السائد فإن لوريم إيبسوم ليس نصاَ عشوائياً، بل إن له جذور في الأدب اللاتيني الكلاسيكي منذ العام 45 قبل الميلاد. من كتاب "حول أقاصي الخير والشر"--}}
{{--                            </p>--}}
{{--                        </div>--}}
                    </div>


                </div>
            </div>
        </div>
    </div>

    <!-- ================ content ================= -->

@endsection




@push('web_js')
    <script !src="">
        //===================================================================
        $(document).on('click', '.show-video', function () {

            var url = '{{ url('get-video-content') }}';
            var video_src = $(this).attr('video-src');
            var title_src = $(this).attr('title-src');
            var text_src = $(this).attr('text-src');

            console.log(video_src);

            $.ajax({
                url: url,
                dataType: 'json',
                type: "GET",
                data: { video_src: video_src, title_src: title_src, text_src: text_src},
                beforeSend: function () {
                },
                success: function (data) {
                    /*console.log(data);*/
                    $('.change-content').html(data.html);
                    $('#overlay').hide();
                },
                error(response) {
                }
            });

            return false;
        });
        //====================================================================
    </script>
@endpush


