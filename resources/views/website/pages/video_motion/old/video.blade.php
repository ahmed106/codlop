<video class="video-fluid z-depth-1 video-data" autoplay loop controls si>
    <source class="video-data" src="{{ $data->video_src }}" type="video/mp4"/>
</video>


<div class="details-motion">
    <h3 class="title-desc">{{ $data->title_src }}</h3>
    <p class="text-desc">
        {{$data->text_src}}
    </p>
</div>
