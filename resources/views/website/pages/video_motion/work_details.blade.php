@extends('website.layouts.layout')

@section('page_title')
    {{ setting('title') }}
@endsection


@section('content')

    <!-- ================ content ================= -->

    <!-- START SECTION BANNER -->
    @include('website.pages.component.pages-banner', ['page_title' => trans('web_lang.our work')])
    <!-- END SECTION BANNER -->

    <section class="portfolio-section small_pt">

        <div class="container">
            <div class="project-details">

                <div class="details-sec">
                    <img src="{{ GetImg($work_detail->background) }}"
                         alt="">
                    <h3 class="project-title">{{ $work_detail->title }}</h3>
                    <div class="desc">
                        {!! $work_detail->text !!}
                    </div>
                </div>

                @if($work_detail->website_link !='' && $work_detail->android_link !='' && $work_detail->ios_link !='')

                    <hr>
                @endif
                <ul class="btn-apps">
                    @if($work_detail->website_link !='')
                        <li>
                            <a href="{{$work_detail->website_link}}" target="_blank">
                                <span><img src="{{asset('website')}}/assets/images/icons/www.png" alt=""></span>
                                @lang('web_lang.visit_website')
                            </a>
                        </li>
                    @endif

                    @if($work_detail->android_link !='')
                        <li>
                            <a href="{{$work_detail->android_link}}" target="_blank">
                                <span><img src="{{asset('website')}}/assets/images/icons/android.png" alt=""></span>
                                @lang('web_lang.show_android_app')
                            </a>
                        </li>
                    @endif
                    @if($work_detail->ios_link !='')
                        <li>
                            <a href="{{$work_detail->ios_link}}" target="_blank">
                                <span><img src="{{asset('website')}}/assets/images/icons/apple.png" alt=""></span>
                                @lang('web_lang.show_ios_app')
                            </a>
                        </li>
                    @endif

                </ul>

                <div class="video-sec">
                    <h3 class="project-title">{{ trans("web_lang.Application video") }}</h3>

                    <div class="row flex">
                        <div class="col-sm-10">
                            <div class="divider"></div>

                            <div class="video_wrap animation" data-animation="fadeInUp" data-animation-delay="0.4s">

                                <img src="{{asset('website')}}/assets/images/videoplay.png" alt=""/>

                                <div class="video_text">

                                    <a href="#video-01" class="video">
                                        <i class="ion-ios-play gradient_box"></i>
                                        <span>{{ trans("web_lang.View the video") }}</span>
                                    </a>

                                    <div id="video-01" class="video-popup mfp-hide">
                                        <video id="video_uploaded__" preload="false" controls="controls" controlslist="nodownload">
                                            <source src="{{ GetImg($work_detail->video) }}" type="video/mp4">
                                            {{--
                                                                                        <source src="{{ GetImg($work_detail->video) }}" type="video/oog">
                                            --}}
                                        </video>
                                    </div>

                                </div>

                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-12 mb-5 mt-5">
                    <a href="{{ url('order-now') }}" class="webs-c-more btn-contact" title="">{{ trans('web_lang.order now') }}</a>
                </div>

            </div>
        </div>

    </section>


    <!-- ================ content ================= -->

@endsection




@push('web_js')
    <script>

        $(function () {
            $('#video_uploaded__').bind('contextmenu', function () {
                return false
            })
        })
    </script>
@endpush


