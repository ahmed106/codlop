@extends('website.layouts.layout')

@section('page_title')
    {{ setting('title') }}
@endsection


@section('content')

<!-- ================ content ================= -->

    <!-- START SECTION SLIDER -->
         @include('website.pages.home.slider')
    <!-- END SECTION SLIDER -->

    <!-- START SECTION ABOUT US -->
        @include('website.pages.home.about')
    <!-- END SECTION ABOUT US -->

    <!-- START SECTION SERVICES -->
        @include('website.pages.home.services')
    <!-- END SECTION SERVICES -->

    <!-- SECTION MOBILE APP -->
        @include('website.pages.home.contract_advantages')
    <!-- END SECTION MOBILE APP -->

    <!-- SECTION MOBILE APP -->
        @include('website.pages.home.clients')
    <!-- END SECTION MOBILE APP -->

<!-- ================ content ================= -->

@endsection







