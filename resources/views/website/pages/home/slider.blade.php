<section id="home" class="banner-home">

    <ul class="social-header">
        <li><a href="{{ setting('facebook') }}" class="facebook"><i class="fa fa-facebook"></i></a></li>
        <li><a href="{{ setting('twitter') }}" class="twitter"><i class="fa fa-twitter"></i></a></li>
        <li><a href="{{ setting('gmail') }}" class="google"><i class="fa fa-youtube"></i></a></li>
        <li><a href="{{ setting('linkedin') }}" class="linkedin"><i class="fa fa-linkedin"></i></a></li>
    </ul>


    <div class="container-fluid p-0">
        <!-- START REVOLUTION SLIDER 5.0.7 -->
        <div id="rev_slider_home_wrapper" class="rev_slider_wrapper fullwidthbanner-container"
             data-alias="news-gallery34">
            <!-- START REVOLUTION SLIDER 5.0.7 fullwidth mode -->

            <div id="rev_slider_home" class="rev_slider fullwidthabanner" data-version="5.0.7">
                <ul>
                @if(count($sliders) > 0)
                    @foreach($sliders as $key => $slider)
                        <!-- SLIDE 1 -->
                            <li data-index="rs-{{ $key+1 }}" data-transition="fade" data-slotamount="default"
                                data-easein="default"
                                data-easeout="default" data-masterspeed="default" data-rotate="0"
                                data-fstransition="fade"
                                data-fsmasterspeed="1500" data-fsslotamount="7" data-saveperformance="off"
                                data-title="Make an Impact">
                                <!-- MAIN IMAGE -->
                                <img src="{{asset('website')}}/assets/images/1.png" alt="" data-bgposition="top"
                                     data-bgfit="cover"
                                     data-bgrepeat="no-repeat" data-bgparallax="10" class="rev-slidebg" data-no-retina>
                                <!-- LAYERS -->
                                <div class="tp-caption tp-resizeme text-white rs-parallaxlevel-0" id="slide-1-layer-1"
                                     data-x="['right','right','right','right']" data-hoffset="['0','0','0','0']"
                                     data-y="['top','top','top','top']" data-voffset="['50','0','0','0']"
                                     data-textalign="['right','right','right','right']"
                                     data-width="['500','400','0','0']" data-height="none" data-whitespace="normal"
                                     data-transform_idle="o:1;" data-transform_in="opacity:0;s:1500;e:Power3.easeInOut;"
                                     data-transform_out="opacity:0;s:1000;e:Power3.easeInOut;s:1000;e:Power3.easeInOut;"
                                     data-start="1000" data-splitin="none" data-splitout="none"
                                     data-responsive_offset="on"
                                     style="z-index: 6; white-space: normal">
                                    <div class="banner_text text_md_center">
                                        <h1 class="animation " data-animation="fadeInUp"
                                            data-animation-delay="1.1s">

                                            {{ $slider->title }}

                                        </h1>
                                        <p class="animation " data-animation="fadeInUp"
                                           data-animation-delay="1.3s">

                                            {!! strip_tags($slider->text) !!}

                                        </p>
                                        <div class="btn_group animation " data-animation="fadeInUp"
                                             data-animation-delay="1.4s">
                                            <a href="{{ url('order-now') }}" class="btn btn-default btn-radius">{{ trans('web_lang.start your website') }}
                                                {{--                                                 <i class="fa fa-long-arrow-left"></i>--}}
                                            </a>
                                            <a href="{{ url('about') }}" class="btn btn-border-white btn-radius">{{ trans('web_lang.about company') }}
                                                {{--  <i class="fa fa-long-arrow-left"></i>--}}
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="tp-caption tp-img" id="slide-1-layer-1"
                                     data-x="['left','left','left','left']" data-hoffset="['0','0','0','0']"
                                     data-y="['top','top','top','top']" data-voffset="['0','0','0','0']"
                                     data-textalign="['left','left','left','left']"
                                     data-width="['400','300','200','100']"
                                     data-height="none" data-whitespace="normal" data-transform_idle="o:1;"
                                     data-transform_in="opacity:0;s:1500;e:Power3.easeInOut;"
                                     data-transform_out="opacity:0;s:1000;e:Power3.easeInOut;s:1000;e:Power3.easeInOut;"
                                     data-start="1000" data-splitin="none" data-splitout="none"
                                     data-responsive_offset="on"
                                     style="z-index: 6; white-space: normal">
                                    <div class="banner_image_right res_md_mb_50 res_xs_mb_20 animation"
                                         data-animation-delay="1.4s" data-animation="fadeInLeft">
                                        <img alt="banner_vector6" src="{{ GetImg($slider->image) }}">
                                    </div>
                                </div>
                            </li>
                    @endforeach
                @else
                    <!-- SLIDE 1 -->
                        <li data-index="rs-1" data-transition="fade" data-slotamount="default" data-easein="default"
                            data-easeout="default" data-masterspeed="default" data-rotate="0" data-fstransition="fade"
                            data-fsmasterspeed="1500" data-fsslotamount="7" data-saveperformance="off"
                            data-title="Make an Impact">
                            <!-- MAIN IMAGE -->
                            <img src="{{asset('website')}}/assets/images/1.png" alt="" data-bgposition="top" data-bgfit="cover"
                                 data-bgrepeat="no-repeat" data-bgparallax="10" class="rev-slidebg" data-no-retina>
                            <!-- LAYERS -->
                            <div class="tp-caption tp-resizeme text-white rs-parallaxlevel-0" id="slide-1-layer-1"
                                 data-x="['right','right','right','right']" data-hoffset="['0','0','0','0']"
                                 data-y="['top','top','top','top']" data-voffset="['0','0','0','0']"
                                 data-textalign="['right','right','right','right']"
                                 data-width="['500','400','0','0']" data-height="none" data-whitespace="normal"
                                 data-transform_idle="o:1;" data-transform_in="opacity:0;s:1500;e:Power3.easeInOut;"
                                 data-transform_out="opacity:0;s:1000;e:Power3.easeInOut;s:1000;e:Power3.easeInOut;"
                                 data-start="1000" data-splitin="none" data-splitout="none" data-responsive_offset="on"
                                 style="z-index: 6; white-space: normal">
                                <div class="banner_text text_md_center">
                                    <h1 class="animation " data-animation="fadeInUp"
                                        data-animation-delay="1.1s">
                                        empty title
                                    </h1>
                                    <p class="animation " data-animation="fadeInUp"
                                       data-animation-delay="1.3s">
                                        empty title
                                    </p>
                                    <div class="btn_group animation " data-animation="fadeInUp"
                                         data-animation-delay="1.4s">
                                        <a href="{{ url('subscribe') }}" class="btn btn-default btn-radius">
                                            {{--                                                 <i class="fa fa-long-arrow-left"></i>--}}
                                        </a>
                                        <a href="{{ url('about') }}" class="btn btn-border-white btn-radius">
                                            {{--                                                <i class="fa fa-long-arrow-left"></i>--}}
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="tp-caption tp-img" id="slide-1-layer-1"
                                 data-x="['left','left','left','left']" data-hoffset="['0','0','0','0']"
                                 data-y="['top','top','top','top']" data-voffset="['0','0','0','0']"
                                 data-textalign="['left','left','left','left']" data-width="['500','400','300','200']"
                                 data-height="none" data-whitespace="normal" data-transform_idle="o:1;"
                                 data-transform_in="opacity:0;s:1500;e:Power3.easeInOut;"
                                 data-transform_out="opacity:0;s:1000;e:Power3.easeInOut;s:1000;e:Power3.easeInOut;"
                                 data-start="1000" data-splitin="none" data-splitout="none" data-responsive_offset="on"
                                 style="z-index: 6; white-space: normal">
                                <div class="banner_image_right res_md_mb_50 res_xs_mb_20 animation"
                                     data-animation-delay="1.4s" data-animation="fadeInLeft">
                                    <img alt="banner_vector6" src="{{asset('website')}}/assets/images/header1.png">
                                </div>
                            </div>
                        </li>
                    @endif
                </ul>
            </div>

        </div>

        <!-- END REVOLUTION SLIDER -->
    </div>
    <img src="{{asset('website')}}/assets/images/curv.png" class="curv" alt="">

</section>
