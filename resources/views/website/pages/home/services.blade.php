<section id="service" class="service gradient_box">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-8 col-md-10 col-sm-12 m-auto">
                <div class="title_default_light text-center">
                    <h4 class="animation" data-animation="fadeInUp" data-animation-delay="0.2s">
                        {{ trans('web_lang.services_company') }}
                    </h4>
                    <p class="animation" data-animation="fadeInUp" data-animation-delay="0.4s">
                        {!!   isset($site_text_service) ? strip_tags($site_text_service->text) : trans('web_lang.services_company') !!}
                    </p>
                </div>
            </div>
        </div>

        <div class="row">
            @if(count($services) > 0)
                @foreach($services as $service)
                    @include('website.pages.service.component.one-service')
                @endforeach
            @else
                @include('website.handle-pages.not-found')
            @endif
        </div>
    </div>

    <div class="divider small_divider"></div>

    <div class="text-center mg-b">
        <a href="{{ url('service') }}" class="btn btn-default btn-white1 btn-radius animation "
           data-animation="fadeInUp" data-animation-delay=".7s"> {{ trans('web_lang.More services') }}
            {{--                <i class="fa fa-long-arrow-left"></i>--}}
        </a>
    </div>

    </div>

    <div class="">
        <div class="col-12">
            <svg class="hero-svg hero-svg2" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 10"
                 preserveAspectRatio="none">
                <path d="M0 10 0 0 A 90 59, 0, 0, 0, 100 0 L 100 10 Z"></path>
            </svg>
        </div>
    </div>

</section>
