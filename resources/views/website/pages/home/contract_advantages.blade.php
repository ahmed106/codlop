<section id="mobileapp" class="why-us">
    <div class="container">

        @if(count($contract_advantages) > 0)
            @foreach($contract_advantages as $contract_advantage)
                <div class="row">
                    <div class="col-lg-8 col-md-10 com-sm 12 m-auto">
                        <div class="title_default_light text-center">
                            <h4 class="animation" data-animation="fadeInUp"
                                data-animation-delay="0.2s">{{ $contract_advantage->title }}</h4>
                            <p class="animation" data-animation="fadeInUp" data-animation-delay="0.4s">
                                {!! strip_tags($contract_advantage->text)  !!}
                            </p>
                        </div>
                    </div>
                </div>

                <div class="row align-items-center small_space">
                    <div class="col-lg-6 col-md-5 col-sm-12">
                        <div class="res_md_mt_40 res_md_mb_40 res_sm_mt_20 res_sm_mb_20 text-center animation"
                             data-animation="zoomIn" data-animation-delay="0.2s">
                            <img class="bounceimg small-width" src="{{ GetImg($contract_advantage->image) }}" alt="mobile_app5"/>
                        </div>
                    </div>

                    <div class="col-lg-6 col-md-7">
                        <div class="row">
                            <div class="col-md-12">
                                <ul class="list_none app_content">
                                    @if(isset($contract_advantage))
                                        @foreach($contract_advantage->contract_advantage_details as $key => $contract_advantage)
                                            <li class="animation" data-animation="fadeInUp"
                                                data-animation-delay="0.{{ $key+1 }}s">
                                                <div class="app_icon">
                                                    <img src="{{ GetImg($contract_advantage->icon) }}" alt=""/>
                                                </div>
                                                <div class="app_desc">
                                                    <h6>
                                                        {{ $contract_advantage->title }}
                                                    </h6>
                                                    <p>
                                                        {!! strip_tags($contract_advantage->text) !!}
                                                    </p>
                                                </div>
                                            </li>
                                        @endforeach
                                    @else
                                        <li class="animation" data-animation="fadeInUp" data-animation-delay="0.2s">
                                            <div class="app_icon">
                                                <img src="{{asset('website')}}/assets/images/icons/trust.png" alt=""/>
                                            </div>
                                            <div class="app_desc">
                                                <h6>الدقة والإحترافية
                                                </h6>
                                                <p>لا ينتهي عملنا حتى نتأكد من رضائك عن الخدمات التي نقدمها لك تماماً ,
                                                    نحن نضمن
                                                    لك الجودة العالية والخدمة الموثوقة وبأسعار مناسبة .</p>
                                            </div>
                                        </li>
                                    @endif
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        @else
            @include('website.handle-pages.not-found')
        @endif
    </div>
</section>
