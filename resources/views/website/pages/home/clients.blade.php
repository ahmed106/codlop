@if(count($clients) > 0)
    <!-- START CLIENTS SECTION -->
    <section class="client_logo small_pt">
        <div class="container">

            <div class="row">
                <div class="col-md-12">
                    <div class="title_default_light text-center">
                        <h4 class="animation" data-animation="fadeInUp" data-animation-delay="0.2s">{{ trans('web_lang.Clients we are proud of') }}</h4>
                    </div>
                </div>
            </div>

            <div class="owl-carousel owl-theme small_space clients-img">

                @foreach($clients as $client)
                    <div class="animation item" data-animation="fadeInUp" data-animation-delay="0.3s">
                        <a href="{{$client->link}}" target="_blank">
                            <img src="{{ GetImg($client->image) }}"
                                 alt="client_logo_wt1"/>
                        </a>
                    </div>
                @endforeach

            </div>

        </div>
    </section>
@else
    @include('website.handle-pages.not-found')
@endif
<!-- END CLIENTS SECTION -->
