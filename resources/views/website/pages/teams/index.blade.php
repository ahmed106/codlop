@extends('website.layouts.layout')
@section('page_title')
    {{ __('web_lang.team') }}
@endsection
@section('content')

<!-- START SECTION BANNER -->
@include('website.pages.component.pages-banner', ['page_title' => trans('web_lang.team')])
<!-- END SECTION BANNER -->

<!-- START SECTION TEAM -->
<section class="section_team small_pt">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-md-10 col-sm-12 m-auto">
                <div class="heading">
                    <h2>{{__('web_lang.code_loop_company_details')}}</h2>
                </div>
            </div>
        </div>
        <div class="row small_space justify-content-center">

            @foreach($teams as $team)
                  @if($team->is_ceo)
                    <div class="col-lg-3 col-md-6 col-sm-6 mb-20">
                        <div class="team-box animation" data-animation="fadeInUp" data-animation-delay="0.4s">
                            <div class="border-color"><span></span><span></span></div>
                            <div class="content-box">
                                <div class="img-user">
                                    <img src="{{GetImg($team->photo)}}" alt="team1" />
                                </div>
                                <div class="team_info text-center">
                                    <h4><a href="javascript:;" class="content-popup">{{$team->team_tran->name}}</a></h4>
                                    <p>{{$team->team_tran->job_title}}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                  @endif
            @endforeach

        </div>
        <div class="divider small_divider"></div>
        <div class="row">
          <div class="col-md-12">
            <div class="title_default_dark title_border text-center">
              <h4 class="animation animated" data-animation="fadeInUp" data-animation-delay="0.2s">{{__('web_lang.team')}}</h4>
            </div>
          </div>
        </div>
        <div class="row small_space justify-content-center mt-20">

            @foreach($teams as $team)
                  @if(!$team->is_ceo)
                    <div class="col-lg-3 col-md-6 col-sm-6 mb-20">
                        <div class="team-box animation" data-animation="fadeInUp" data-animation-delay="0.4s">
                            <div class="border-color"><span></span><span></span></div>
                            <div class="content-box">
                                <div class="img-user">
                                    <img src="{{GetImg($team->photo)}}" alt="team1" />
                                </div>
                                <div class="team_info text-center">
                                    <h4><a href="javascript:;" class="content-popup">{{$team->team_tran->name}}</a></h4>
                                    <p>{{$team->team_tran->job_title}}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                  @endif
            @endforeach

        </div>
    </div>
</section>
<!-- END SECTION TEAM -->

@endsection
