<!-- START SECTION SERVICES -->
<section id="service" class="service service-page">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-8 col-md-10 col-sm-12 m-auto">
                <div class="text-center">
                    <h2 class="animation title-service" data-animation="fadeInUp" data-animation-delay="0.2s">
                        {{ $service->title }}


                    </h2>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- END SECTION SERVICES -->

<section class="webs-cart color-div">
    <div class="container">
        <div class="row">

            @if($service->service_text_normals && count($service->service_text_normals) > 0)
                @foreach($service->service_text_normals as $key => $service_text_normal)

                    @if($key % 2 != 0)

                        <div class="col-lg-6 col-md-12">
                            <div class="webs-c-img hidden-xs animation" data-animation="fadeInUp"
                                 data-animation-delay="0.1s">

                                <img src="{{ GetImg($service_text_normal->image) }}"
                                     alt="{{ $service_text_normal->title }}" title="{{ $service_text_normal->title }}">
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-12">
                            <div class="webs-c-block animation" data-animation="fadeInUp" data-animation-delay="0.2s">
                                <h2> {{ $service_text_normal->title }} </h2>
                                <p>

                                    {!! $service_text_normal->text !!}
                                </p>
                                @include('website.pages.service.component.contact-btn')
                            </div>
                        </div>

                    @else

                        <div class="col-lg-6 col-md-12">
                            <div class="webs-c-block animation" data-animation="fadeInUp" data-animation-delay="0.2s">
                                <h2> {{ $service_text_normal->title }} </h2>
                                <p>
                                    {!! $service_text_normal->text !!}
                                </p>
                                @include('website.pages.service.component.contact-btn')
                            </div>
                        </div>

                        <div class="col-lg-6 col-md-12">
                            <div class="webs-c-img hidden-xs animation" data-animation="fadeInUp"
                                 data-animation-delay="0.1s">
                                <img src="{{ GetImg($service_text_normal->image) }}"
                                     alt="{{ $service_text_normal->title }}" title="{{ $service_text_normal->title }}">
                            </div>
                        </div>

                    @endif

                @endforeach
            @endif

        </div>
    </div>
</section>


<section class="gradient_box pt-50 lg_pb_40">

    <div class="container-fluid">

        <div class="row">
            <div class="col-lg-8 m-auto">
                <div class="title_default_light text-center">
                    <h4 class="animation custom-title mb-5" data-animation="fadeInUp" data-animation-delay="0.2s">
                        {{ trans('web_lang.Add vitality to the advertising content of your project') }}
                    </h4>
                </div>
            </div>
        </div>

        <div class="announc_feats">
            <div class="row">


                <div class="col-xl-4 col-md-6 col-sm-12 col-xs-12">

                    @if($service->service_text_lists && count($service->service_text_lists) > 0)
                        @foreach($service->service_text_lists as $key2 => $service_text_list)
                            @if($key2 % 2 != 0)
                                <div class="announc_feat">
                                    <div class="announc_pic">
                                        <img class="img-responsive" src="{{ GetImg($service_text_list->image) }}"
                                             alt="{{ GetImg($service_text_list->title) }}">
                                    </div>
                                    <div class="announc_info">
                                        <h4> {{ $service_text_list->title }}</h4>
                                        <p>{!! strip_tags($service_text_list->text) !!}</p>
                                    </div>
                                </div>
                            @endif
                        @endforeach
                    @else
                        @include('website.handle-pages.not-found')
                    @endif

                </div>


                <div class="flex-center justify-content-center col-md-4 hidden-lg-w">
                    <div class="mob-features-img wow fadeIn">
                        <img class="img-responsive" src="{{ GetImg($service->image) }}" alt="{{ $service->title }}">
                    </div>
                </div>


                <div class="col-xl-4 col-md-6 col-sm-12 col-xs-12">


                    @if($service->service_text_lists && count($service->service_text_lists) > 0)
                        @foreach($service->service_text_lists as $key2 => $service_text_list)
                            @if($key2 % 2 == 0)
                                <div class="announc_feat announc_feat-left">
                                    <div class="announc_pic">
                                        <img class="img-responsive" src="{{ GetImg($service_text_list->image) }}" alt="{{ GetImg($service_text_list->title) }}">
                                    </div>
                                    <div class="announc_info">
                                        <h4> {{ $service_text_list->title }}</h4>
                                        <p>{!! $service_text_list->text !!}</p>
                                    </div>
                                </div>
                            @endif
                        @endforeach
                    @else
                        @include('website.handle-pages.not-found')
                    @endif


                </div>

            </div>
        </div>
    </div>


    <div class="col-12">
        <svg class="hero-svg svg-breadcrumb" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 10"
             preserveAspectRatio="none">
            <path d="M0 10 0 0 A 90 59, 0, 0, 0, 100 0 L 100 10 Z"></path>
        </svg>
    </div>

</section>


@include('website.pages.component.contact')
