<section class="webs-cart color-div">


    <div class="container">
        <div class="row">

            @if($service->service_text_normals && count($service->service_text_normals) > 0)

                @foreach($service->service_text_normals as $key => $service_text_normal)
                    @if($key % 2 == 0)
                        <div class="col-lg-7 col-md-12 col-sm-12">
                            <div class="webs-c-block animation" data-animation="fadeInUp" data-animation-delay="0.1s">
                                <h2> {{ $service_text_normal->title }} </h2>
                                <p class="font-s">
                                    {!! $service_text_normal->text !!}
                                </p>
                            </div>
                        </div>

                        <div class="col-lg-5 col-md-5 col-sm-3 hidden-md-w">
                            <div class="animation webs-c-img " data-animation="fadeInUp" data-animation-delay="0.2s">
                                <img src="{{ GetImg($service_text_normal->image) }}" alt="" title="{{ $service_text_normal->title }}">
                            </div>
                        </div>

                    @else

                        <div class="col-lg-5 col-md-5 col-sm-3 hidden-md-w">
                            <div class="animation webs-c-img " data-animation="fadeInUp" data-animation-delay="0.2s">
                                <img src="{{ GetImg($service_text_normal->image) }}" alt="" title="{{ $service_text_normal->title }}">
                            </div>
                        </div>

                        <div class="col-lg-7 col-md-12 col-sm-12">
                            <div class="webs-c-block animation" data-animation="fadeInUp" data-animation-delay="0.1s">
                                <h2> {{ $service_text_normal->title }} </h2>
                                <p class="font-s">
                                    {!! $service_text_normal->text !!}
                                </p>
                            </div>
                        </div>
                    @endif

                @endforeach
            @else
                @include('website.handle-pages.not-found')
            @endif


        </div>
    </div>


    <div class="col-12">
        <svg class="hero-svg svg3" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 10" preserveAspectRatio="none">
            <defs>
                <linearGradient id="grad1" x1="0%" y1="0%" x2="100%" y2="0%">
                    <stop offset="0%" style="stop-color:#0d249d;stop-opacity:1"></stop>
                    <stop offset="100%" style="stop-color:#4b2596;stop-opacity:1"></stop>
                </linearGradient>
            </defs>
            <path d="M0 10 0 0 A 90 59, 0, 0, 0, 100 0 L 100 10 Z" fill="url(#grad1)"></path>
        </svg>
    </div>


</section>

<section class="gradient_box pt-50 lg_pb_40">
    <div class="container">

        <div class="row">
            <div class="col-lg-8 m-auto">
                <div class="title_default_light text-center">
                    <h4 class="animation mb-5" data-animation="fadeInUp" data-animation-delay="0.2s">
                        {{ trans('web_lang.Advantages of our email marketing services') }}
                    </h4>
                </div>
            </div>
        </div>

        <div class="row align-items-center">

            <div class="col-lg-4 col-md-12 col-sm-12">
                <div class="text-center animation" data-animation="zoomIn" data-animation-delay="0.2s">
                    <img class="bounceimg small-width2" src="{{ GetImg($service->image) }}" alt="">
                </div>
            </div>


            <div class="col-lg-8">
                <div class="row">

                    @if($service->service_text_lists && count($service->service_text_lists) > 0)

                        @foreach($service->service_text_lists as $key => $service_text_list)

                            <div class="col-md-6 animation mb-md" data-animation="fadeInUp" data-animation-delay="0.9s">
                                <div class="app_icon">
                                    <img src="{{ GetImg($service_text_list->image) }}" alt="">
                                </div>
                                <div class="app_desc">
                                    <h6>{{ $service_text_list->title }}</h6>
                                    <p> {!! strip_tags($service_text_list->text) !!} </p>
                                </div>
                            </div>

                        @endforeach
                    @else
                        @include('website.handle-pages.not-found')
                    @endif

                </div>
            </div>


        </div>
    </div>


    <div class="col-12">
        <svg class="hero-svg svg-breadcrumb cust-svg" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 10"
             preserveAspectRatio="none">
            <path d="M0 10 0 0 A 90 59, 0, 0, 0, 100 0 L 100 10 Z"></path>
        </svg>
    </div>


</section>


<!-- START SECTION Contact -->
@include('website.pages.component.contact')
<!-- END SECTION Contact -->
