<!-- START SECTION SERVICES -->
<section id="service" class="service service-page">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-8 col-md-10 col-sm-12 m-auto">
                <div class="text-center mb-5">
                    <h2 class="animation title-service mb-3" data-animation="fadeInUp" data-animation-delay="0.2s">
                        {{ $service->title }}
                    </h2>
                    <p class="animation title-service-small text" data-animation="fadeInUp" data-animation-delay="0.4s">
                        {!! $service->text !!}
                    </p>
                </div>
            </div>
        </div>


        <div class="row">

            @if(isset($service->service_hosts) && count($service->service_hosts) > 0)
                @foreach($service->service_hosts as $service_host)
                    <div class="col-lg-4 col-md-6 col-xs-12 has-color">
                        <div class="pricing-table hosting-table bg-white text-center">
                            <div class="host-conten">

                                <div class="bg-theme-colored-2 bkg-2 top-price position-relative">
                                    <p class="text-white  line-bottom-centered">{{ trans("web_lang.hosting plan") }}</p>
                                    <h2 class="package-type text-uppercase text-white">{{ $service_host->title }}</h2>
                                    <div class="package-icon"><i class="fa fa-cubes"></i></div>
                                </div>

                                <h1 class="price text-theme-colored bg-white">{{ $service_host->price }}<span
                                        class="text-theme-colored currency">{{ trans("web_lang.SAR") }}</span>
                                </h1>
                                <h5 class="text-theme-colored per-year">{{ trans("web_lang.annually") }}</h5>

                                @if(isset($service_host->service_host_details) && count($service_host->service_host_details) > 0)
                                    <ul class="table-list">
                                        @foreach($service_host->service_host_details as $service_host_detail)
                                            <li>{{ $service_host_detail->title }}</li>
                                        @endforeach
                                    </ul>
                                @endif

                                {{--<a class="btn btn-default btn-radius" href="#">اطلب الان</a>--}}

                            </div>
                        </div>
                    </div>
                @endforeach
            @endif

        </div>

    </div>

</section>
<!-- END SECTION SERVICES -->

