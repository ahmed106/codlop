<!-- START SECTION SERVICES -->
<section id="service" class="service service-page">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-8 col-md-10 col-sm-12 m-auto">
                <div class="text-center">
                    <h2 class="animation title-service mb-5" data-animation="fadeInUp" data-animation-delay="0.2s">
                        {{ $service->title }}
                    </h2>
                </div>
            </div>
        </div>
        <div class="row flex-img">

            <div class="col-xl-4 col-md-6 col-sm-12 col-xs-12">
                @if($service->service_text_lists && count($service->service_text_lists) > 0)
                    <ul class="mob-feat mob-feat-r s-ul">

                        @foreach($service->service_text_lists as $key1 => $service_text_list)
                            @if($key1 % 2 == 0)
                                <li class="animation" data-animation="fadeInLeftBig" data-animation-delay="0.4s">
                                    <div class="motion-ser-img">
                                        <div class="mot-s-im">
                                            <img class="img-responsive center-block" src="{{ GetImg($service_text_list->image) }}" alt="{{ $service_text_list->title }}">
                                        </div>
                                    </div>
                                    <div class="motion-ser-info">
                                        <h2> {{ $service_text_list->title }} </h2>
                                        <p> {!! strip_tags($service_text_list->text) !!} </p>
                                    </div>
                                </li>
                            @endif
                        @endforeach

                    </ul>
                @else
                    @include('website.handle-pages.not-found')
                @endif
            </div>


            <div class="flex-center justify-content-center col-md-4 hidden-lg-w">
                <div class="mob-features-img wow fadeIn">
                    <img class="img-responsive" src="{{ GetImg($service->image) }}" alt="mob">
                </div>
            </div>


            <div class="col-xl-4 col-md-6 col-sm-12 col-xs-12">
                @if($service->service_text_lists && count($service->service_text_lists) > 0)
                    <ul class="mob-feat mob-feat-r s-ul reverse">

                        @foreach($service->service_text_lists as $key2 => $service_text_list)
                            @if($key2 % 2 != 0)
                                <li class="animation" data-animation="fadeInLeftBig" data-animation-delay="0.4s">
                                    <div class="motion-ser-info">
                                        <h2> {{ $service_text_list->title }} </h2>
                                        <p> {!! strip_tags($service_text_list->text) !!} </p>
                                    </div>
                                    <div class="motion-ser-img">
                                        <div class="mot-s-im">
                                            <img class="img-responsive center-block" src="{{ GetImg($service_text_list->image) }}" alt="{{ $service_text_list->title }}">
                                        </div>
                                    </div>
                                </li>
                            @endif
                        @endforeach

                    </ul>
                @else
                    @include('website.handle-pages.not-found')
                @endif
            </div>

            <div class="col-12 mb-5 mt-5">
                @include('website.pages.service.component.contact-btn', ['btn_contact' => 'btn-contact'])
            </div>

        </div>

    </div>

</section>
<!-- END SECTION SERVICES -->

