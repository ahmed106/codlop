<!-- START SECTION SERVICES -->
<section id="service" class="service service-page">
    <div class="container">

        <div class="row align-items-center">
            <div class="col-lg-8 col-md-10 col-sm-12 m-auto">
                <div class="text-center">

                    <h2 class="animation title-service" data-animation="fadeInUp" data-animation-delay="0.2s">
                        {{ $service->title }}
                    </h2>

                    <p>{!! $service->text !!}</p>
                </div>
            </div>
        </div>

        <div class="slider-top">


            @if($service->service_text_lists && count($service->service_text_lists) > 0)
                <ul class="webs-ser-cont owl-carousel owl-theme mark-socials-ul layer">

                    @foreach($service->service_text_lists as $service_text_list)
                        <li class="animation" data-animation="fadeInUp" data-animation-delay="0.2s">
                            <a href="#service-{{ $service_text_list->id }}" class="motion-ser-info webs-services-smoth">
                                <div class="motion-ser-img">
                                    <div class="smot-s-im">
                                        <img class="img-responsive center-block"
                                             src="{{ GetImg($service_text_list->image) }}"
                                             alt="{{$service_text_list->title}}" title="{{$service_text_list->title}}">
                                    </div>
                                </div>
                                <h2> {{ $service_text_list->title }} </h2>
                            </a>
                        </li>
                    @endforeach

                </ul>
            @endif

        </div>
    </div>

    {{--    <div class="col-12">--}}
    {{--        <svg class="hero-svg svg4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 10" preserveAspectRatio="none">--}}
    {{--            <path d="M0 10 0 0 A 90 59, 0, 0, 0, 100 0 L 100 10 Z" fill="#5640a4e0"></path>--}}
    {{--        </svg>--}}
    {{--    </div>--}}

</section>
<!-- END SECTION SERVICES -->

<?php $increment = 1 ?>

@if($service->service_text_lists && count($service->service_text_lists) > 0)
    @foreach($service->service_text_lists as $key => $service_text_list)
        <section class="webs-cart {{ $increment % 2 != 0 ? 'color-div' : 'bg'.$increment }}"
                 id="service-{{ $service_text_list->id }}">
            <div class="container">
                <div class="row">
                    @if($increment % 2 == 0)
                        <div class="col-lg-6 col-md-12">
                            <div class="webs-c-block animation" data-animation="fadeInUp" data-animation-delay="0.1s">
                                <h2> {{ $service_text_list->title }} </h2>
                                <p>
                                    {!! strip_tags($service_text_list->text) !!}
                                </p>
                                @include('website.pages.service.component.contact-btn')
                            </div>
                        </div>

                        <div class="col-lg-6 col-md-12">
                            <div class="webs-c-img animation" data-animation="fadeInUp" data-animation-delay="0.2s">
                                <img src="{{ GetImg($service_text_list->image) }}" alt="{{ $service_text_list->title }}"
                                     title="{{ $service_text_list->title }}">
                            </div>
                        </div>

                    @else
                        <div class="col-lg-6 col-md-12">
                            <div class="webs-c-img animation" data-animation="fadeInUp" data-animation-delay="0.2s">
                                <img src="{{ GetImg($service_text_list->image) }}" alt="{{ $service_text_list->title }}"
                                     title="{{ $service_text_list->title }}">
                            </div>
                        </div>

                        <div class="col-lg-6 col-md-12">
                            <div class="webs-c-block animation" data-animation="fadeInUp" data-animation-delay="0.1s">
                                <h2> {{ $service_text_list->title }} </h2>
                                <p>
                                    {!! strip_tags($service_text_list->text) !!}
                                </p>
                                @include('website.pages.service.component.contact-btn')
                            </div>
                        </div>
                    @endif
                </div>
            </div>

            <?php if ($increment >= 4) {
                $increment = 0;
            } $increment++; ?>

        </section>
    @endforeach
@endif
