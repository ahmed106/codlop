<!-- START SECTION SERVICES -->
<section id="service" class="service service-page">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-12 col-md-12 col-sm-12 m-auto">
                <div class="text-center">


                    @if($service->service_text_normals && count($service->service_text_normals) > 0)
                        @foreach($service->service_text_normals as $key => $service_text_normal)

                            <h2 class="animation title-service mb-4" data-animation="fadeInUp" data-animation-delay="0.2s">
                                {{$service_text_normal->title}}
                            </h2>

                            {{--                            <p class="animation font-s" data-animation="fadeInUp" data-animation-delay="0.4s">--}}
                            {{--                                {!! $service_text_normal->text !!}--}}
                            {{--                            </p>--}}


                            <p>{!! $service_text_normal->text !!}</p>
                        @endforeach
                    @endif

                </div>
            </div>
        </div>
    </div>
</section>
<!-- END SECTION SERVICES -->


<section class="webs-cart color-div">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-md-12">
                <div class="webs-c-img hidden-xs animation" data-animation="fadeInUp" data-animation-delay="0.1s">
                    <img src="{{ GetImg($service->image) }}" alt="{{ $service->title }}" title="{{ $service->title }}">
                </div>
            </div>

            <div class="col-lg-6 col-md-12">
                <div class="webs-c-block animation" data-animation="fadeInUp" data-animation-delay="0.2s">

                    @if($service->service_text_lists && count($service->service_text_lists) > 0)
                        <ul class="prog-feat-bl s-ul">
                            @foreach($service->service_text_lists as $key2 => $service_text_list)
                                <li>{{ $service_text_list->title }}</li>
                            @endforeach
                        </ul>
                    @else
                        @include('website.handle-pages.not-found')
                    @endif

                    @include('website.pages.service.component.contact-btn')
                </div>
            </div>

        </div>
    </div>
</section>
