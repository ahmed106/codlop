<!-- START SECTION SERVICES -->
<section id="service" class="service service-page">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-8 col-md-10 col-sm-12 m-auto">
                <div class="text-center">
                    <h2 class="animation title-service" data-animation="fadeInUp" data-animation-delay="0.2s">
                        {{ $service->title }}
                    </h2>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- END SECTION SERVICES -->

<section class="webs-cart color-div">
    <div class="container">
        <div class="row">


            @if($service->service_text_normals && count($service->service_text_normals) > 0)
                @foreach($service->service_text_normals as $key => $service_text_normal)

                    @if($key % 2 == 0)

                        <div class="col-lg-6 col-md-12">
                            <div class="webs-c-img hidden-xs animation" data-animation="fadeInUp"
                                 data-animation-delay="0.1s">
                                <img src="{{ GetImg($service_text_normal->image) }}"
                                     alt="{{ $service_text_normal->title }}" title="{{ $service_text_normal->title }}">
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-12">
                            <div class="webs-c-block animation" data-animation="fadeInUp" data-animation-delay="0.2s">
                                <h2> {{ $service_text_normal->title }} </h2>
                                <p>
                                    {!! $service_text_normal->text !!}
                                </p>
                                @include('website.pages.service.component.contact-btn')
                            </div>
                        </div>

                    @else

                        <div class="col-lg-6 col-md-12">
                            <div class="webs-c-block animation" data-animation="fadeInUp" data-animation-delay="0.2s">
                                <h2> {{ $service_text_normal->title }} </h2>
                                <p>
                                    {!! $service_text_normal->text !!}
                                </p>
                                @include('website.pages.service.component.contact-btn')
                            </div>
                        </div>

                        <div class="col-lg-6 col-md-12">
                            <div class="webs-c-img hidden-xs animation" data-animation="fadeInUp"
                                 data-animation-delay="0.1s">
                                <img src="{{ GetImg($service_text_normal->image) }}"
                                     alt="{{ $service_text_normal->title }}" title="{{ $service_text_normal->title }}">
                            </div>
                        </div>

                    @endif

                @endforeach
            @endif

        </div>
    </div>
</section>

