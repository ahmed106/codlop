<div class="col-lg-4 col-md-4 col-sm-6 col-12">
    <a href="{{ url('service-detail/'.$service->id)}}" class="box_wrap text-center animation" data-animation="fadeInUp"
       data-animation-delay="0.1s">
        <img src="{{ GetImg($service->icon) }}" alt=""/>
        <h4>{{ $service->title }}</h4>
        <p>
            {!!  $service->text !!}
        </p>
    </a>
</div>
