@extends('website.layouts.layout')

@section('page_title')
    {{ setting('title') }}
@endsection


@section('content')

    <!-- ================ content ================= -->

    <!-- START SECTION BANNER -->
    @include('website.pages.component.pages-banner', ['page_title' =>($service ? $service->title : '#')])
    <!-- END SECTION BANNER -->

    @include('website.pages.service.service_pages.'.($service ? $service->service_page : 'service_other'))

    <!-- ================ content ================= -->

@endsection







