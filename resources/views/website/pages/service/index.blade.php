@extends('website.layouts.layout')

@section('page_title')
    {{ setting('title') }}
@endsection


@section('content')

    <!-- ================ content ================= -->

    <!-- START SECTION BANNER -->
    @include('website.pages.component.pages-banner', ['page_title' => trans('web_lang.services_company')])
    <!-- END SECTION BANNER -->


    <!-- START SECTION SERVICES -->
    <section id="service" class="service service-page">
        <div class="container">

            <div class="row align-items-center">
                <div class="col-lg-8 col-md-10 col-sm-12 m-auto">
                    <div class="text-center">
                        <p class="animation text" data-animation="fadeInUp" data-animation-delay="0.2s">
                            {!!  isset($site_text_service) ? strip_tags($site_text_service->text) : trans('web_lang.services_company')  !!}
                        </p>
                    </div>
                </div>
            </div>

            <div class="row row-flx">

                @if(count($services_) > 0)
                    @foreach($services_ as $service)
                        @include('website.pages.service.component.one-service')
                    @endforeach
                @else
                    @include('website.handle-pages.not-found')
                @endif
            </div>

        </div>
    </section>
    <!-- END SECTION SERVICES -->



    <!-- START SECTION Contact -->
    @include('website.pages.component.contact')
    <!-- END SECTION Contact -->


    <!-- ================ content ================= -->

@endsection







