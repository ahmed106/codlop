@extends('website.layouts.layout')

@section('page_title')
    {{ setting('title') }}
@endsection


@section('content')

    <!-- ================ content ================= -->

    <!-- START SECTION BANNER -->
    @include('website.pages.component.pages-banner', ['page_title' =>__('web_lang.blog')])
    <!-- END SECTION BANNER -->


    <section class="blog-page small_pt">
        <div class="container">
            <div class="row">

                <div class="col-lg-8 col-md-12">
                    <div class="post-details">
                        <div class="row">
                            @include('website.pages.blog.components.one-blog-detail')
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 col-md-12 ">
                    <div class="sidebar_block">
                        <div class="widget widget_popular_post">
                            <h5 class="widget_title">احدث المقالات</h5>

                            @if(count($new_blogs) > 0)
                                @foreach($new_blogs as $new_blog)
                                    @include('website.pages.blog.components.latest_articles')
                                @endforeach
                            @else
                                @include('website.handle-pages.not-found')
                            @endif
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>

    <!-- ================ content ================= -->

@endsection







