@extends('website.layouts.layout')

@section('page_title')
    {{ setting('title') }}
@endsection


@section('content')

    <!-- ================ content ================= -->

    <!-- START SECTION BANNER -->
    @include('website.pages.component.pages-banner', ['page_title' => trans('web_lang.blog')])
    <!-- END SECTION BANNER -->


    <section class="blog-page small_pt">

        {{-- loader --}}
        @include('website.pages.component.loader')

        <div class="container">

            {{--<button id="button" type="button">Click</button>--}}

            @if(count($blogs) > 0)

                <div class="row">

                @foreach($blogs as $blog)
                    <!-- START SECTION ONE BLOG -->
                    @include('website.pages.blog.components.one-blog')
                    <!-- END SECTION ONE BLOG -->
                    @endforeach

                </div>

                <nav>
                    @if ($blogs->lastPage() > 1)
                        <ul class="pagination justify-content-center">

                            <li class="{{ ($blogs->currentPage() == 1) ? ' disabled' : '' }}">
                                @if($blogs->currentPage() == 1)
                                    <a href="#!" style="cursor: default">{{ trans('web_lang.previous') }}</a>
                                @else
                                    <a href="{{ $blogs->url($blogs->currentPage()-1) }}">{{ trans('web_lang.previous') }}</a>
                                @endif
                            </li>

                            @for ($i = 1; $i <= $blogs->lastPage(); $i++)
                                <li class="{{ ($blogs->currentPage() == $i) ? ' active' : '' }}">
                                    <a href="{{ $blogs->url($i) }}">{{ $i }}</a>
                                </li>
                            @endfor

                            <li class="{{ ($blogs->currentPage() == $blogs->lastPage()) ? ' disabled' : '' }}">
                                @if($blogs->currentPage() == $blogs->lastPage())
                                    <a href="#!" style="cursor: default">{{ trans('web_lang.next') }}</a>
                                @else
                                    <a href="{{ $blogs->url($blogs->currentPage()+1) }}">{{ trans('web_lang.next') }}</a>
                                @endif
                            </li>

                        </ul>
                    @endif
                </nav>
            @else
                @include('website.handle-pages.not-found')
            @endif
        </div>
    </section>


    <!-- ================ content ================= -->

@endsection







