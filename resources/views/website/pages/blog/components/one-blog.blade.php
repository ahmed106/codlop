<div class="col-lg-4 col-md-4 col-sm-6">
    <div class="blog_item animation " data-animation="fadeInUp"
         data-animation-delay="0.2s">
        <div class="blog_img">
            <a href="{{ url('blog-detail/'.$blog->id)}}">
                <img src="{{ GetImg($blog->image) }}" alt="blog_small_img1">
                <span><i class="fa fa-calendar"></i> {{ change_date_to_new_format($blog->date, 'full') }} </span>
            </a>
        </div>
        <div class="blog_content">
            <div class="blog_text">
                <h6 class="blog_title">
                    <a href="{{ url('blog-detail/'.$blog->id) }}">
                        {{ $blog->title }}
                    </a>
                </h6>
                <p>
                    {!! strip_tags($blog->text) !!}
                </p>
                <a href="{{ url('blog-detail/'.$blog->id) }}" class="text-capitalize">{{ trans('web_lang.Read more') }}</a>
            </div>
        </div>
    </div>
</div>
