<div class="col-lg-12 col-md-12">
    <article class="blog_content_detail">
        <div class="post_content">
            <h3 class="blog_title"><a href="{{ url('blog-detail/'.$blog->id) }}">{{ $blog->title }}</a>
            </h3>
            <div class="blog-img mt-3 mb-3">
                <img alt="blog_largel_img1.jpg" src="{{ GetImg($blog->image) }}">
            </div>

            <p>
                {!! strip_tags($blog->text) !!}
            </p>

        </div>
    </article>
</div>
