
<div class="widget-post">
    <div class="widget-post-thumb">
        <a href="{{ url('blog-detail/'.$new_blog->id) }}"><img src="{{ GetImg($new_blog->image) }}" alt="post-thumb1"></a>
    </div>
    <div class="widget-post-content">
        <h6>
            <a href="{{ url('blog-detail/'.$new_blog->id) }}">{{ $new_blog->title }}</a>
        </h6>

    </div>
</div>
