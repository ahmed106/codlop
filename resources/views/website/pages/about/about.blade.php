<section id="about" class="about-page small_pt">
    <div class="container">

        @if(count($site_text_abouts) > 0)
            @foreach($site_text_abouts as $key => $site_text_about)
                @if($key % 2 == 0)
                    <div class="row align-items-center pb-50">
                        <div class="col-lg-6 col-md-12 col-sm-12">
                            <div class="text-center res_md_mb_30 res_sm_mb_20">
                            </div>
                        </div>
                        <div class="col-sm-12 about-sec">
                            <img class="animation bounceimg animated zoomIn" data-animation="zoomIn"
                                 data-animation-delay="0.2s"
                                 src="{{ GetImg($site_text_about->image) }}" alt="">
                            <div class="title_default_light">
                                <h4 class="animation " data-animation="fadeInUp" data-animation-delay="0.2s"> {{$site_text_about->title}} </h4>
                                <p class="animation " data-animation="fadeInUp" data-animation-delay="0.4s">
                                    {!! strip_tags($site_text_about->text) !!}
                                </p>
                            </div>
                        </div>
                    </div>
                @else
                    <div class="row align-items-center pb-50">
                        <div class="col-sm-12 about-sec">
                            <div class="title_default_light">
                                <h4 class="animation " data-animation="fadeInUp" data-animation-delay="0.2s"> {{$site_text_about->title}} </h4>
                                <p class="animation " data-animation="fadeInUp" data-animation-delay="0.4s">
                                    {!! strip_tags($site_text_about->text) !!}
                                </p>
                            </div>
                            <img class="animation bounceimg animated zoomIn img-about-none" data-animation="zoomIn"
                                 data-animation-delay="0.2s"
                                 src="{{ GetImg($site_text_about->image) }}" alt="">
                        </div>
                    </div>
                @endif
            @endforeach
        @endif

    </div>


    <div class="col-12">
        <svg class="hero-svg svg3" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 10" preserveAspectRatio="none">
            <defs>
                <linearGradient id="grad1" x1="0%" y1="0%" x2="100%" y2="0%">
                    <stop offset="0%" style="stop-color:#0d249d;stop-opacity:1"></stop>
                    <stop offset="100%" style="stop-color:#4b2596;stop-opacity:1"></stop>
                </linearGradient>
            </defs>
            <path d="M0 10 0 0 A 90 59, 0, 0, 0, 100 0 L 100 10 Z" fill="url(#grad1)"></path>
        </svg>
    </div>


</section>
