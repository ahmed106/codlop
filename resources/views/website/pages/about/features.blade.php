<div class="rs-process modify1 pt-160 pb-120 md-pt-75 md-pb-80">
    <div class="shape-animation">
        <div class="shape-process">
            <img class="dance2" src="{{asset('website')}}/assets/images/circle.png" alt="images">
        </div>
    </div>
    <div class="container">
        <div class="row align-items-center">
            <div class="col-xl-4 pr-40 md-pr-15 md-pb-80">
                <div class="process-wrap md-center">
                    <div class="sec-title mb-30">
                        <div class="sub-text new">{{ trans('web_lang.That is not all') }} .......</div>
                        <h2 class="title white-color">
                            {{ trans('web_lang.Features you get from CodLop') }}
                        </h2>
                    </div>
                </div>
            </div>
            <div class="col-xl-8 sm-pl-40 sm-pr-20">
                <div class="row">

                    @if(count($site_text_features) > 0)
                        @foreach($site_text_features as $key => $site_text_feature)
                            <div class="col-md-6 mb-70">
                                <div class="rs-addon-number">
                                    <div class="number-text">
                                        <div class="number-area" style="background: {{ $site_text_feature->color }}">
                                            {{ ++$key }}
                                        </div>
                                        <div class="number-title">
                                            <h3 class="title"> {{$site_text_feature->title}}</h3>
                                        </div>


                                        <p class="number-txt"> {!! strip_tags($site_text_feature->text) !!} </p>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    @endif

                </div>
            </div>
        </div>
    </div>
    <div class="waveWrapper">
        <div class="wave waveTop"></div>
        <div class="wave waveMiddle"></div>
    </div>
</div>
