@extends('website.layouts.layout')

@section('page_title')
    {{ setting('title') }}
@endsection


@section('content')

    <!-- ================ content ================= -->

    <!-- START SECTION BANNER -->
    @include('website.pages.component.pages-banner', ['page_title' => trans('web_lang.about company')])
    <!-- END SECTION BANNER -->

    <!-- START SECTION Vision and Message -->
    @include('website.pages.about.about')
    <!-- END SECTION Vision and Message -->

    <!-- START SECTION Features -->
    @include('website.pages.about.features')
    <!-- END SECTION Features -->

    <!-- START SECTION Contact -->
    @include('website.pages.component.contact')
    <!-- END SECTION Contact -->

    <!-- ================ content ================= -->

@endsection







