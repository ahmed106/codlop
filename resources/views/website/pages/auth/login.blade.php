@extends('website.layouts.layout')

@section('page_title')
    {{ setting('title') }}
@endsection


@section('content')

    <!-- ================ content ================= -->

    <!-- START SECTION BANNER -->
    @include('website.pages.component.pages-banner', ['page_title' => trans('web_lang.login')])
    <!-- END SECTION BANNER -->


    <section class="small_pt">
        <div class="container">
            <div class="contact-us">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="authorize_box">
                            <div class="field_form authorize_form">
                                <form method="post" action="{{ route('login_action') }}" enctype="multipart/form-data">
                                     @csrf
                                    <div class="form-group col-md-12 animation animated fadeInUp" data-animation="fadeInUp" data-animation-delay="0.2s">
                                        <label for="">{{ trans('web_lang.E-mail') }}</label>
                                        <input type="email" class="form-control" placeholder="{{ trans('web_lang.E-mail') }}" name="email" value="{{ old('email') }}" required>
                                    </div>
                                     <div class="form-group col-md-12 animation animated fadeInUp" data-animation="fadeInUp" data-animation-delay="0.4s">
                                        <label for="">{{ trans('web_lang.password') }}</label>
                                        <input type="password" class="form-control" placeholder="{{ trans('web_lang.password') }}" name="password" required>
                                    </div>
                                    <div class="col-md-12 text-left animation animated fadeInUp" data-animation="fadeInUp" data-animation-delay="0.6s">
                                        <a href="{{ url('rest-password-form') }}"><strong>{{ trans('web_lang.Reset password !') }}</strong></a>
                                    </div>
                                    <div class="form-group col-md-12 text-center animation animated fadeInUp" data-animation="fadeInUp" data-animation-delay="0.5s">
                                        <button class="btn btn-default btn-login" type="submit" style="width: auto">{{ trans('web_lang.login') }}</button>
                                    </div>
                                </form>
                            </div>
                            <hr>

                            <!-- <div class="text-center">
                                <span class="animation animated fadeInUp forgot_pass" data-animation="fadeInUp" data-animation-delay=".8s">
                                   <a href="{{ url('rest-password-form') }}"> {{ trans('web_lang.Reset password !') }}</a>
                                </span>
                            </div> -->

                            <div class="text-center">
                                <span class="animation animated fadeInUp forgot_pass" data-animation="fadeInUp" data-animation-delay=".8s">
                                   <a href="{{ url('order-now') }}"> <strong>{{ trans('web_lang.order now') }}</strong></a>
                                </span>
                            </div>

                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- ================ content ================= -->

@endsection







