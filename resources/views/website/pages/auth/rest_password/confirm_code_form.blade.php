@extends('website.layouts.layout')

@section('page_title')
    {{ setting('title') }}
@endsection


@section('content')

    <!-- ================ content ================= -->

    <!-- START SECTION BANNER -->
    @include('website.pages.component.pages-banner', ['page_title' => trans('web_lang.confirm code') ])


    <section class="small_pt">
        <div class="container">
            <div class="contact-us">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="authorize_box">
                            <div class="field_form authorize_form">

                                <form action="{{route('confirm-code')}}" class="checkout with_border with_padding form-horizontal" role="form" method="post">
                                    @csrf

                                    <input type="hidden" name="id" value="{{ $confirm_user->id }}">

                                    <div class="login-form">
                                        <h4 class="login-title"> {{ trans('web_lang.confirm code') }} </h4>
                                        <div class="row">

                                            <div class="form-group col-md-12 animation animated fadeInUp" data-animation="fadeInUp" data-animation-delay="0.2s">
                                                <label>{{ trans('web_lang.confirm code') }}*</label>
                                                <input type="number" class="form-control" name="forget_password_code" id="username" value="{{ old('confirm_code') }}" required>
                                            </div>


                                            <div class="form-group col-md-12 text-center animation animated fadeInUp" data-animation="fadeInUp" data-animation-delay="0.5s">
                                                <button class="btn btn-default btn-login" type="submit" style="width: auto">{{ trans('web_lang.send') }}</button>
                                            </div>

                                        </div>
                                    </div>
                                </form>


                            </div>


                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>


    <!-- ================ content ================= -->

@endsection


