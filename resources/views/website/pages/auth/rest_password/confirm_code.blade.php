@extends('website.layouts.layout')

@section('page_title')
    {{ setting('title') }}
@endsection

@section('content')

    <!-- ================ content ================= -->

    <!-- START SECTION BANNER -->
    @include('website.pages.component.pages-banner', ['page_title' => trans('web_lang.Update Password') ])



    <section class="ls section_padding_top_50 section_padding_bottom_50 columns_padding_25"  style="margin: 160px;">
        <div class="container">

            <div class="row">
                <div class="col-sm-12">

                    <h3> Confirm Code To Rest Password </h3>
                    <!-- Form -->
                    <form action="{{url('confirm-code')}}" class="checkout with_border with_padding form-horizontal" role="form" method="post">
                    @csrf

                        <div class="form-group">
                            <div class="col-sm-offset-3 col-sm-9">
                                <input type="submit" class="theme_button color1 topmargin_20 float-right"
                                       name="confirm_code" value="Confirm Code To Rest Password">
                            </div>
                        </div>


                    </form>

                </div>
                <!--eof .col-sm-8 (main content)-->


            </div>
        </div>
    </section>


    <!-- ================ content ================= -->

@endsection

