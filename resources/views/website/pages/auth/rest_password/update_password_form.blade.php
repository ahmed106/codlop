@extends('website.layouts.layout')

@section('page_title')
    {{ setting('title') }}
@endsection

@section('content')

    <!-- ================ content ================= -->

    <!-- START SECTION BANNER -->
    @include('website.pages.component.pages-banner', ['page_title' => trans('web_lang.Update Password') ])


    <section class="small_pt">
        <div class="container">
            <div class="contact-us">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="authorize_box">
                            <div class="field_form authorize_form">

                                <!-- Form -->
                                <form action="{{route('update-password')}}" class="checkout with_border with_padding form-horizontal" role="form" method="post">
                                    @csrf

                                    <input type="hidden" name="id" value="{{ $user_code->id }}">

                                    <div class="login-form">
                                        <h4 class="login-title">{{ trans('web_lang.Update Password') }}</h4>
                                        <div class="row">

                                            <div class="col-md-12 col-12 mb-20">
                                                <input type="password" placeholder="{{ trans('web_lang.Password') }}" class="form-control" name="password" id="password" value="" required autocomplete="off">
                                            </div>

                                            <div class="col-md-12 col-12 mb-20">
                                                <input type="password" placeholder="{{ trans('web_lang.Confirm Password') }}" class="form-control" name="password_confirmation" id="password_confirmation" value="" required autocomplete="off">
                                            </div>


                                            <div class="col-md-12">
                                                <button class="btn btn-default">{{ trans('web_lang.Update') }}</button>
                                            </div>
                                        </div>
                                    </div>


                                </form>

                            </div>

                            <div class="divider"></div>

                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>


    <!-- ================ content ================= -->

@endsection



@push('js')

    <script !src="">
        $('#password_confirmation').keyup(function () {

            if( $(this).val() == $('#password').val() )
            {
                event.preventDefault();
                $('#update_pass_btn').prop("disabled", false);
            }else{
                event.preventDefault();
                $('#update_pass_btn').prop("disabled", true);
            }

        })
    </script>

@endpush
