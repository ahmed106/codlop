@extends('website.layouts.layout')

@section('page_title')
    {{ setting('title') }}
@endsection


@section('content')

    <!-- ================ content ================= -->

    <!-- START SECTION BANNER -->
    @include('website.pages.component.pages-banner', ['page_title' => trans('web_lang.Reset Password') ])


    <section class="small_pt">
        <div class="container">
            <div class="contact-us">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="authorize_box">
                            <div class="field_form authorize_form">

                                <!-- Form -->
                                <form action="{{route('rest-password')}}" method="post" enctype="multipart/form-data">
                                    @csrf
                                    <div class="login-form">
                                        <h4 class="login-title">{{ trans('web_lang.Reset Password') }}</h4>
                                        <div class="row">
                                            <div class="form-group col-md-12 animation animated fadeInUp" data-animation="fadeInUp" data-animation-delay="0.2s">
                                                <label for="">{{ trans('web_lang.E-mail') }}</label>
                                                <input type="email" class="form-control" placeholder="{{ trans('web_lang.E-mail') }}" name="email" required>
                                            </div>

                                            <div class="form-group col-md-12 text-center animation animated fadeInUp" data-animation="fadeInUp" data-animation-delay="0.5s">
                                                <button class="btn btn-default btn-login" type="submit" style="width: auto">{{ trans('web_lang.Reset Password') }}</button>
                                            </div>

                                        </div>
                                    </div>
                                </form>

                            </div>


                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>


    <!-- ================ content ================= -->

@endsection




