@extends('website.layouts.layout')

@section('page_title')
    {{ setting('title') }}
@endsection


@section('content')

    <!-- ================ content ================= -->

    <!-- START SECTION BANNER -->
    @include('website.pages.component.pages-banner', ['page_title' => trans('web_lang.contact') ])
    <!-- END SECTION BANNER -->


    <section class="small_pt small_pb">
        <div class="container">
            <div class="contact-us">
                <div class="row">
                    <div class="col-lg-8">

                        <div class="heading">
                            <span>{{ trans('web_lang.Your work just got easier with') }}</span>
                            <h2>{{ setting('title') }}</h2>
                        </div>

                        <form method="post" action="{{ route('send-contact') }}" id="contact-form" class="field_form_s2" enctype="multipart/form-data">
                            <div class="row">

                                @csrf

                                <div class="form-group col-md-12 animation" data-animation="fadeInUp" data-animation-delay="0.4s">
                                    <input type="text" required="required" placeholder="{{ trans('web_lang.full name') }}" class="form-control require-input" name="name">
                                </div>

                                <div class="form-group col-md-12 animation" data-animation="fadeInUp" data-animation-delay="0.6s">
                                    <input type="email" required="required" placeholder="{{ trans('web_lang.E-mail') }}" class="form-control require-input" name="email">
                                </div>

                                <div class="form-group col-md-12 animation" data-animation="fadeInUp" data-animation-delay="0.8s">
                                    <input type="text" required="required" placeholder="{{ trans('web_lang.subject') }}" class="form-control require-input" name="subject">
                                </div>

                                <div class="form-group col-md-4 animation " data-animation="fadeInUp" data-animation-delay="1s">
                                    <select class="form-control require-input" aria-label="Default select example" style="height: 48px; padding: 12px;" name="phone_code">
                                        <option value="" selected disabled>{{ trans('web_lang.Choose the phone code') }}</option>
                                        @foreach($phone_code_allows as $phone_code_allow)
                                            <option value="{{ $phone_code_allow->dial_code }}">{{ str_replace('+','',$phone_code_allow->dial_code).'+' }}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="form-group col-md-8 animation require-input" data-animation="fadeInUp" data-animation-delay="1s">
                                    <input type="number" placeholder="{{ trans('web_lang.phone number') }}" class="form-control require-input" name="phone">
                                </div>

                                <div class="form-group col-md-12 animation" data-animation="fadeInUp" data-animation-delay="1.2s">
                                    <textarea required="required" placeholder="{{ trans('web_lang.Message') }}" class="form-control require-input" name="message" rows="3"></textarea>
                                </div>

                                <div class="col-md-12 text-center animation" data-animation="fadeInUp" data-animation-delay="1.4s">
                                    <button type="submit" title="Submit Your Message!" class="btn btn-default btn-radius" id="submit-form" name="submit" value="Submit">
                                        {{--                                        <i class="fa fa-spinner fa-spin"></i>--}}
                                        {{ trans('web_lang.send') }}
                                        {{--                                        <i class="fa fa-long-arrow-left"></i>--}}
                                    </button>
                                </div>
                                <div class="col-md-12">
                                    <div id="alert-msg" class="alert-msg text-center"></div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>


    <div class="map">

        <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d14300.089485892766!2d43.969122399999996!3d26.358138399999998!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x2d3bb18ac07a3d8e!2z2YPZiNivINmE2YjYqCDZhNiq2YLZhtmK2Kkg2KfZhNmF2LnZhNmI2YXYp9iq!5e0!3m2!1sen!2seg!4v1641457286130!5m2!1sen!2seg"
                width="100%" height="600" style="border:0;" allowfullscreen="" loading="lazy">

        </iframe>


    </div>

    <!-- ================ content ================= -->

@endsection

@push('web_js')
    <script !src="">
        //===================================================================
        $(document).on('click', '#submit-form', function () {
            var url = '{{ route('send-contact') }}';
            var data = $("#contact-form").serialize();
            console.log(url);
            $.ajax({
                url: url,
                dataType: 'json',
                type: "POST",
                data: data,
                beforeSend: function () {
                    $('#submit-form i').removeClass('fa-long-arrow-left').addClass('fa-spinner fa-spin');
                }, success: function (data) {
                    makeCuteToast("{{ setting('title') }}", "{{ trans('web_lang.Sending has been completed successfully') }}", "success", 5000);
                    $('#submit-form i').removeClass('fa-spinner fa-spin').addClass('fa-long-arrow-left');
                    $('.require-input').val('');
                }, error(response) {
                    let errors = JSON.parse(response.responseText).errors;
                    makeCuteToast("error", errors[0], "error", 5000);
                    $('#submit-form i').removeClass('fa-spinner fa-spin').addClass('fa-long-arrow-left');
                }
            });
            return false;
        });
        //====================================================================
    </script>
@endpush






