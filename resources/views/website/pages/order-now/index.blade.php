@extends('website.layouts.layout')

@section('page_title')
    {{ setting('title') }}
@endsection


@section('content')

    <!-- ================ content ================= -->

    <!-- START SECTION BANNER -->
    @include('website.pages.component.pages-banner', ['page_title' => trans('web_lang.order now')])
    <!-- END SECTION BANNER -->


    <section class="small_pt">
        <div class="container">
            <div class="contact-us">
                <div class="row">
                    <div class="col-lg-8">

                        <div class="heading">
                            <span>{{ trans('web_lang.Order your service now and follow all your work easily with') }}</span>
                            <h2>{{ setting('title') }}</h2>
                        </div>

                        <form method="post" action="{{ route('send-order-now') }}" id="order-form" class="field_form_s2"
                              enctype="multipart/form-data">
                            <div class="row">

                                @csrf

                                <div class="form-group col-md-12 animation" data-animation="fadeInUp"
                                     data-animation-delay="0.4s">
                                    <input type="text" required="required" @auth value="{{auth()->user()->name}}" readonly @endauth placeholder="{{ trans('web_lang.full name') }}" class="form-control require-input" name="name">
                                </div>


                                <div class="form-group col-md-9 @auth col-md-12 @endauth animation" data-animation="fadeInUp"
                                     data-animation-delay="0.6s">
                                    <input type="email" id="email" @auth value="{{auth()->user()->email}}" readonly @endauth required="required" placeholder="{{ trans('web_lang.E-mail') }}" class="form-control require-input" name="email">
                                </div>

                                @guest
                                    <div class="col-md-3 text-center animation">
                                        <button type="button" title="Submit Your Email !" class="btn btn-default btn-radius"
                                                id="confirm-email" name="">
                                            {{ trans('web_lang.confirm email') }}
                                            {{--                                        <i class="fa fa-long-arrow-left"></i>--}}
                                        </button>
                                    </div>
                                @endguest

                                <div id="append-confirm" class="form-group col-md-12 animation require-input" data-animation="fadeInUp" data-animation-delay="1s">
                                    {{-- confirm --}}
                                </div>


                                <div class="form-group col-md-4 animation " data-animation="fadeInUp"
                                     data-animation-delay="1s">

                                    <select class="form-control require-input" @auth disabled @endauth aria-label="Default select example" style="height: 48px; padding: 12px;" name="phone_code">
                                        <option value="" selected disabled>{{ trans('web_lang.Choose the phone code') }}</option>

                                        @foreach($phone_code_allows as $phone_code_allow)

                                            {{--                                            <option value="{{ $phone_code_allow->dial_code }}"> {{ $phone_code_allow->dial_code }}</option>--}}
                                            <option @auth{{auth()->user()->phone_code ==$phone_code_allow->dial_code ? 'selected':'' }} @endauth value="{{ $phone_code_allow->dial_code }}"> {{ str_replace('+','',$phone_code_allow->dial_code).'+' }}</option>
                                        @endforeach
                                    </select>
                                    @auth
                                        <input type="hidden" name="phone_code" value="{{auth()->user()->phone_code}}">

                                    @endauth
                                </div>

                                <div class="form-group col-md-8 animation require-input" data-animation="fadeInUp"
                                     data-animation-delay="1s">
                                    <input @auth value="{{auth()->user()->phone}}" readonly @endauth type="number" placeholder="{{ trans('web_lang.phone number') }}" class="form-control require-input" name="phone">
                                </div>


                                <div class="form-group col-md-12 animation " data-animation="fadeInUp" data-animation-delay="1s">
                                    {{--                                    <label class="label-custom" for="">{{ trans('web_lang.Choose the type of package') }}</label>--}}

                                    {{--                                    <ul class="check-box-div">--}}

                                    {{--                                        @if($offer_main_package)--}}
                                    {{--                                            <li>--}}
                                    {{--                                                <input value="{{$offer_main_package->id}}"--}}
                                    {{--                                                       type="radio" id="package-1"--}}
                                    {{--                                                       name="offer_id" {{ request()->get('offer_id') == $offer_main_package->id || !request()->get('offer_id') ? 'checked' : null }}>--}}
                                    {{--                                                <label--}}
                                    {{--                                                    for="package-1">{{ trans('web_lang.Basic Package (web)') }}</label>--}}
                                    {{--                                            </li>--}}
                                    {{--                                        @endif--}}

                                    {{--                                        @if($offer_professional_package)--}}
                                    {{--                                            <li>--}}
                                    {{--                                                <input--}}
                                    {{--                                                    value="{{$offer_professional_package->id}}"--}}
                                    {{--                                                    type="radio" id="package-2"--}}
                                    {{--                                                    name="offer_id" {{ request()->get('offer_id') == $offer_professional_package->id ? 'checked' : null }}>--}}
                                    {{--                                                <label--}}
                                    {{--                                                    for="package-2">{{ trans('web_lang.Professional Package (web)') }}</label>--}}
                                    {{--                                            </li>--}}
                                    {{--                                        @endif--}}

                                    {{--                                        @if($offer_golden_package)--}}
                                    {{--                                            <li>--}}
                                    {{--                                                <input--}}
                                    {{--                                                    value="{{$offer_golden_package->id}}"--}}
                                    {{--                                                    type="radio" id="package-3"--}}
                                    {{--                                                    name="offer_id" {{ request()->get('offer_id') == $offer_golden_package->id ? 'checked' : null }}>--}}
                                    {{--                                                <label--}}
                                    {{--                                                    for="package-3">{{ trans('web_lang.Gold Package (web)') }}</label>--}}
                                    {{--                                            </li>--}}
                                    {{--                                        @endif--}}

                                    {{--                                        --}}{{--                                        <li>--}}
                                    {{--                                        --}}{{--                                            <input value="" type="radio" id="package-4"--}}
                                    {{--                                        --}}{{--                                                   name="offer_id" {{ !isset($offers) || ($offers->count() < 1) ? 'checked' : null }}>--}}
                                    {{--                                        --}}{{--                                            <label for="package-4">{{ trans('web_lang.Apps only') }}</label>--}}
                                    {{--                                        --}}{{--                                        </li>--}}

                                    {{--                                    </ul>--}}

                                    @if(count($programs) > 0)
                                        <div>
                                            <label class="label-custom"
                                                   for="">{{ trans('web_lang.Choose the desired services') }}</label>
                                            <ul class="check-box-div2">
                                                @foreach($programs as $index => $program)

                                                    <li>

                                                        <input name="programs[]" value="{{ $program->id }}" class="require-input mb-5 program" type="checkbox" id="program-{{ $program->id }}">

                                                        <label for="program-{{ $program->id}}">
                                                            <img  src="{{ GetImg($program->icon) }}" alt="{{ $program->title }}">
                                                            {{ $program->title }}
                                                        </label>
                                                        <textarea required="required" placeholder="{{ trans('web_lang.more details') }}" class="form-control mt-3  textarea d-none required-input" name="details[]" rows="3"></textarea>

                                                    </li>

                                                @endforeach
                                            </ul>
                                        </div>
                                    @endif

                                </div>


                                {{--                                <div class="form-group col-md-12 animation">--}}
                                {{--                                    <textarea required="required" placeholder="{{ trans('web_lang.more details') }}" class="form-control require-input" name="more_details" rows="3"></textarea>--}}
                                {{--                                </div>--}}


                                <div class="col-md-12 text-center animation">
                                    <button type="submit" title="Submit Your Message!"
                                            class="btn btn-default btn-radius" id="submit-form" name="submit"
                                            value="Submit">
                                        {{--<i class="fa fa-spinner fa-spin"></i>--}}
                                        {{ trans('web_lang.send') }}
                                        {{--                                        <i class="fa fa-long-arrow-left"></i>--}}
                                    </button>
                                </div>


                                <div class="col-md-12">
                                    <div id="alert-msg" class="alert-msg text-center"></div>
                                </div>

                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- ================ content ================= -->

@endsection


@push('web_js')
    <script !src="">

        //============================= confirm-email ============================
        $(document).on('click', '#confirm-email', function () {

            var url = '{{ route('confirm-email') }}';
            var email = $("#email").val();

            if (email == '') {
                makeCuteToast("error", '{{ trans('web_lang.You must write your email') }}', "error", 10000);
                return false;
            }
            // console.log(email);
            $.ajax({
                url: url,
                dataType: 'json',
                type: "POST",
                data: {_token: '{{ @csrf_token() }}', email: email,},
                beforeSend: function () {
                    $('#confirm-email i').removeClass('fa-long-arrow-left').addClass('fa-spinner fa-spin');
                }, success: function (data) {
                    makeCuteToast("{{ setting('title') }}", "{{ trans('web_lang.A confirmation code has been sent to your email') }}", "success", 10000);
                    $('#confirm-email').attr("disabled", true);
                    $("#email").attr("readonly", true);
                    $('#confirm-email i').removeClass('fa-spinner fa-spin').addClass('fa-check');

                    $('#append-confirm').append(`<input type="number" placeholder="{{ trans('web_lang.confirm email') }}" class="form-control require-input" name="confirm_email">`);

                }, error: function (response) {
                    let errors = JSON.parse(response.responseText).errors;
                    makeCuteToast("error", errors[0], "error", 10000);
                    // $('#append-confirm').append(`<input type="number" placeholder="{{ trans('web_lang.confirm email') }}" class="form-control require-input" name="confirm_email">`);

                    $('#confirm-email i').removeClass('fa-spinner fa-spin').addClass('fa-long-arrow-left');
                }
            });

            return false;
        });
        //====================================================================

        //============================= submit-form ============================
        $(document).on('click', '#submit-form', function () {
            var url = '{{ route('send-order-now') }}';
            var data = $("#order-form").serialize();
            // console.log(data);

            $.ajax({
                url: url,
                dataType: 'json',
                type: "POST",
                data: data,
                beforeSend: function () {
                    $('#submit-form i').removeClass('fa-long-arrow-left').addClass('fa-spinner fa-spin');
                }, success: function (data) {
                    makeCuteToast("{{ setting('title') }}", "{{ trans('web_lang.Your Order has been successfully completed, please wait for a response as soon as possible.') }}", "success", 10000);

                    @auth

                    setTimeout(returnToProfile, 2000)

                    function returnToProfile() {
                        window.location = '{{url('/profile')}}'
                    }

                    @endauth


                    $('#submit-form i').removeClass('fa-spinner fa-spin').addClass('fa-long-arrow-left');
                    $('.require-input').val('');
                    $(".require-input").prop('checked', false);

                }, error(response) {
                    let errors = JSON.parse(response.responseText).errors;
                    makeCuteToast("error", errors[0], "error", 10000);
                    $('#submit-form i').removeClass('fa-spinner fa-spin').addClass('fa-long-arrow-left');
                }
            });

            return false;
        });
        //====================================================================

        $('.program').on('click', function (e) {
            if ($(this).is(':checked')) {
                $(this).parent().find('.textarea').fadeIn(300).removeClass('d-none');
            } else {
                $(this).parent().find('.textarea').fadeOut(300).addClass('d-none')
            }
        })
    </script>
@endpush






