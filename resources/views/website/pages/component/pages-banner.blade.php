<section class="section_breadcrumb gradient_box2" data-z-index="1" data-parallax="scroll"
         data-image-src="{{asset('website')}}/assets/images/banner_bg3.png">
    <canvas id="banner_canvas" class="transparent_effect fixed"></canvas>
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="banner_text text-center">

                    <h1 class="animation" data-animation="fadeInUp" data-animation-delay="1.1s"> {{ request()->segment(2) == 'service' ||request()->segment(2) == 'service-detail'   ?  __('web_lang.services_company'): $page_title  }}</h1>
                </div>
            </div>

        </div>
    </div>
    <div class="col-12">
        <svg class="hero-svg svg-breadcrumb" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 10"
             preserveAspectRatio="none">
            <path d="M0 10 0 0 A 90 59, 0, 0, 0, 100 0 L 100 10 Z"></path>
        </svg>
    </div>
</section>
