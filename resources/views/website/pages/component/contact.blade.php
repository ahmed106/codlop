
<section class="section start-now">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-8">
                <div class="heading">
                    <i class="fa fa-quote-left"></i>
                    <h2>{{ trans('web_lang.We are always happy to start working with you and facilitate the performance of your work') }}</h2>
                </div>
                <a href="{{ url('contact') }}" class="btn btn-lg btn-default btn-radius" title="{{ trans('web_lang.contact') }}">{{ trans('web_lang.contact') }}</a>
            </div>
        </div>
    </div>
</section>
