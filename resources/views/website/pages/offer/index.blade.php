@extends('website.layouts.layout')

@section('page_title')
    {{ setting('title') }}
@endsection


@section('content')

    <!-- ================ content ================= -->

    <!-- START SECTION BANNER -->
    @include('website.pages.component.pages-banner', ['page_title' => trans('web_lang.Prices and packages')])
    <!-- END SECTION BANNER -->


    <section class="offers-page small_pt">
        <div class="container">
            <div class="table-responsive">
                @if(count($offer_details) > 0)
                    <table class="price-comparison-table comparison-table1">
                        <thead>
                        <tr>
                            <th class="titlte-table">
                                <div class="pricing_table7_box_header">
                                    <h3>{{ trans('web_lang.Package details') }}</h3>
                                </div>

                            </td>
                            <th class="border-top-red">
                                <div class="pricing_table7_box_header">
                                    <img class="bronze img-offer" src="{{asset('website')}}/assets/images/icons/1.png" alt="">
                                    {{ trans('web_lang.basic package') }}
                                </div>

                            </th>
                            <th class="border-top-blue">
                                <div class="pricing_table7_box_header">
                                    <img class="silver img-offer" src="{{asset('website')}}/assets/images/icons/2.png" alt="">
                                    {{ trans('web_lang.Professional Package') }}
                                </div>
                            </th>
                            <th class="border-top-green">
                                <div class="pricing_table7_box_header">
                                    <img class="gold img-offer" src="{{asset('website')}}/assets/images/icons/3.png" alt="">
                                    {{ trans('web_lang.Golden Package') }}
                                </div>
                            </th>
                        </tr>
                        </thead>
                        <tbody>

                        @if(count($offer_details) > 0)
                            @foreach($offer_details as $offer_detail)
                                <tr>
                                    <td>{{ $offer_detail->text }}</td>
                                    <td><span class="{{ $offer_detail->main_package == 'yes' ? 'tick' : 'cross' }}">{{ $offer_detail->main_package == 'yes' ? '✔' : '✘' }}</span></td>
                                    <td><span class="{{ $offer_detail->professional_package == 'yes' ? 'tick' : 'cross' }}">{{ $offer_detail->professional_package == 'yes' ? '✔' : '✘' }}</span></td>
                                    <td><span class="{{ $offer_detail->golden_package == 'yes' ? 'tick' : 'cross' }}">{{ $offer_detail->golden_package == 'yes' ? '✔' : '✘' }}</span></td>
                                </tr>
                            @endforeach
                        @endif

                        @if(count($offer_details) > 0)
                            {{-- prices --}}
                            <tr>
                                <td class="price-td"><span class="price-plan">{{ trans('web_lang.Prices') }}</span></td>
                                <td class="bg-red"><span class="price-text">{{ $offer_main_package ? $offer_main_package->price : null }}</span><span class="price-currency">{{ trans('web_lang.SAR') }}</span></td>
                                <td class="bg-blue"><span class="price-text">{{ $offer_professional_package ? $offer_professional_package->price : null }}</span><span class="price-currency">{{ trans('web_lang.SAR') }}</span></td>
                                <td class="bg-green"><span class="price-text">{{ $offer_golden_package ? $offer_golden_package->price : null }}</span><span class="price-currency">{{ trans('web_lang.SAR') }}</span></td>
                            </tr>

                            <tr>
                                <td class="titlte-table titlte2"><h3>{{ trans('web_lang.Choose the right offer') }}
                                        {{--                                        <i class="fa fa-long-arrow-left"></i>--}}
                                    </h3></td>
                                <td class="bg-red-bottom"><a class="btn btn-red" href="{{ url('order-now?offer_id=').($offer_main_package ? $offer_main_package->id : null) }}"> {{ trans('web_lang.order now') }} </a></td>
                                <td class="bg-blue-bottom"><a class="btn btn-blue" href="{{ url('order-now?offer_id=').($offer_professional_package ? $offer_professional_package->id : null) }}"> {{ trans('web_lang.order now') }} </a></td>
                                <td class="bg-green-bottom"><a class="btn btn-green" href="{{ url('order-now?offer_id=').($offer_golden_package ? $offer_golden_package->id : null) }}"> {{ trans('web_lang.order now') }} </a></td>
                            </tr>
                            {{-- prices --}}
                        @endif


                        </tbody>
                    </table>
                @else
                    @include('website.handle-pages.not-found')
                @endif
            </div>
        </div>
    </section>


    <!-- ================ content ================= -->

@endsection







