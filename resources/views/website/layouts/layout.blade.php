<!doctype html>
<html lang="ar">

<head>
    <!-- Meta -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Cryptocash is Professional Creative Template"/>
    <!-- SITE TITLE -->
    <title>@yield('page_title')</title>

    @include('website.inc._css')

    @yield('styles')

</head>

<body class="v_royal_blue">

@include('website.inc._loader')

@include('website.inc._header')

@include('website.inc.toastr')

<!-- start content -->
@yield('content')
<!-- end content -->

@include('website.inc._footer')

@include('website.inc._js')

</body>

</html>
