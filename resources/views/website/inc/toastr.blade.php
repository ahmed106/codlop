
@push('web_js')

    @if( session()->has('success-message') )
        <script>
            $(document).ready(function() {
                makeCuteToast('{{ setting('title') }}', '{{ session()->get('success-message') }}', 'success', timeout = 5000);
            });
        </script>
    @endif

    @if( session()->has('fail-message') )
        <script>
            $(document).ready(function() {
                makeCuteToast('{{ setting('title') }}', '{{ session()->get('fail-message') }}', 'error', timeout = 5000);
            });
        </script>
    @endif

@endpush
