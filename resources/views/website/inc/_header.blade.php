<!-- START HEADER -->
<header class="header_wrap">
    <div class="container-fluid">
        <nav class="navbar navbar-expand-lg">
            <a class="navbar-brand page-scroll animation" href="{{ url('/') }}" data-animation="fadeInDown"
               data-animation-delay="1s">
                <img class="logo_light" src="{{ GetImg( setting('logo') ) }}" alt="logo"/>
            </a>
            <button class="navbar-toggler animation" type="button" data-toggle="collapse"
                    data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false"
                    aria-label="Toggle navigation" data-animation="fadeInDown" data-animation-delay="1s">
                <span class="fa fa-list"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav m-auto">
                    <li class="animation" data-animation="fadeInDown" data-animation-delay="1.1s"><a
                            class="nav-link page-scroll nav_item active"
                            href="{{ url('/') }}">{{ trans('web_lang.Home') }}</a></li>
                    <li class="animation" data-animation="fadeInDown" data-animation-delay="1.2s"><a
                            class="nav-link page-scroll nav_item"
                            href="{{ url('about') }}">{{ trans('web_lang.About company') }}</a></li>
                    <li class="animation" data-animation="fadeInDown" data-animation-delay="1.3s"><a
                            class="nav-link page-scroll nav_item"
                            href="{{ url('service') }}">{{ trans('web_lang.Services') }}</a></li>
                    <li class="animation" data-animation="fadeInDown" data-animation-delay="1.4s"><a
                            class="nav-link page-scroll nav_item"
                            href="{{ url('offers') }}">{{ trans('web_lang.Prices and packages') }}</a></li>

                    <li class="animation" data-animation="fadeInDown" data-animation-delay="1.5s"><a
                            class="nav-link page-scroll nav_item"
                            href="{{ url('blog') }}">{{ trans('web_lang.Blog') }}</a></li>

                    <li class="animation" data-animation="fadeInDown" data-animation-delay="1.5s"><a
                            class="nav-link page-scroll nav_item"
                            href="{{ url('video-motion') }}">{{ trans('web_lang.our work') }}</a></li>

                    @if(auth()->user())
                        <li class="animation" data-animation="fadeInDown" data-animation-delay="1.5s"><a
                                class="nav-link page-scroll nav_item"
                                href="{{ url('profile') }}">{{ trans('web_lang.My_services') }}</a></li>
                    @endif

                    <li class="animation" data-animation="fadeInDown" data-animation-delay="1.5s"><a
                            class="nav-link page-scroll nav_item"
                            href="{{ url('order-now') }}">{{ trans('web_lang.order now') }}</a></li>

                    <li class="animation" data-animation="fadeInDown" data-animation-delay="1.6s"><a
                            class="nav-link page-scroll nav_item"
                            href="{{ url('contact') }}">{{ trans('web_lang.contact us') }}</a></li>

                    <li class="animation" data-animation="fadeInDown" data-animation-delay="1.6s"><a
                            class="nav-link page-scroll nav_item"
                            href="{{url('/training')}}">{{trans('web_lang.training')}}</a></li>

                    <li class="animation" data-animation="fadeInDown" data-animation-delay="1.6s"><a
                            class="nav-link page-scroll nav_item"
                            href="{{url('/team')}}">{{trans('web_lang.team')}}</a></li>


                </ul>

                <ul class="navbar-nav nav_btn align-items-center">
                    <li class="animation" data-animation="fadeInDown" data-animation-delay="1.9s">
                        <div class="lng_dropdown">
                            <select
                                onchange="this.options[this.selectedIndex].value && (window.location = this.options[this.selectedIndex].value);"
                                name="countries" id="lng_select">
                                @foreach(get_language_allows() as $language )
                                    <option value="{{ change_language($language->code) }}"
                                            data-title="{{ $language->native_name }}" {{ $language->code == get_lang() ? 'selected' : '' }}>{{ $language->native_name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </li>

                    @if(auth()->user())
                        <li class="animation" data-animation="fadeInDown" data-animation-delay="2s">
                            <a class="btn btn-default btn-radius nav_item" href="{{ url('logout') }}">
                                <i class="fa fa-sign-out"></i>
                                {{ trans('web_lang.logout') }}
                            </a>
                        </li>
                    @else
                        <li class="animation" data-animation="fadeInDown" data-animation-delay="2s">
                            <a class="btn btn-default btn-radius nav_item" href="{{ url('login') }}">
                                {{ trans('web_lang.login') }}
                            </a>
                        </li>
                    @endif


                </ul>

                {{--                <ul class="navbar-nav nav_btn align-items-center">--}}
                {{--                    <li class="animation" data-animation="fadeInDown" data-animation-delay="1.7s">--}}
                {{--                        <a href="#" class="phone">--}}
                {{--                            <img src="{{asset('website')}}/assets/images/icons/call.png" alt="">--}}
                {{--                            <span>{{ setting('phone') }}</span>--}}
                {{--                        </a>--}}
                {{--                    </li>--}}
                {{--                </ul>--}}

            </div>

        </nav>
    </div>
</header>
<!-- END HEADER -->

