<!-- START FOOTER SECTION -->
<footer>
    <div class="col-12">
        <svg class="cta-svg cta-svg2" viewBox="0 0 500 150" preserveAspectRatio="none">
            <defs>
                <linearGradient id="grad2" x1="0%" y1="0%" x2="130%" y2="0%">
                    <stop offset="0%" style="stop-color:#0d249d;stop-opacity:1"/>
                    <stop offset="130%" style="stop-color:#4b2596;stop-opacity:1"/>
                </linearGradient>
            </defs>
            <path d="M-31.31,170.22 C164.50,33.05 334.36,-32.06 547.11,196.88 L500.00,150.00 L0.00,150.00 Z"
                  fill="url(#grad2)">
            </path>
        </svg>
    </div>
    <div class="top_footer gradient_box">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <a class="logo-footer" href="{{ url('/') }}">
                        <img src="{{ GetImg( setting('logo') ) }}" alt="logo"/>
                    </a>
                </div>

                <div class="col-lg-4 col-md-6 col-sm-12 animation" data-animation="fadeInUp" data-animation-delay="0.1s">
                    <h4 class="title-footer">{{trans('web_lang.links')}}</h4>
                    <ul class="quick-linkes">
                        <li><a href="{{ url('/') }}">{{ trans('web_lang.Home') }}</a></li>
                        <li><a href="{{ url('about') }}">{{ trans('web_lang.About company') }}</a></li>
                        <li><a href="{{ url('service') }}">{{ trans('web_lang.Services') }}</a></li>
                        <li><a href="{{ url('offers') }}">{{ trans('web_lang.Prices and packages') }}</a></li>
                        <li><a href="{{ url('blog') }}">{{ trans('web_lang.Blog') }}</a></li>
                        <li><a href="{{ url('video-motion') }}">{{ trans('web_lang.our work') }}</a></li>
                        <li><a href="{{ url('order-now') }}">{{ trans('web_lang.order now') }}</a></li>
                        <li><a href="{{ url('contact') }}">{{ trans('web_lang.contact us') }}</a></li>
                    </ul>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-12 animation" data-animation="fadeInUp" data-animation-delay="0.2s">
                    <h4 class="title-footer">{{ trans('web_lang.Services') }}</h4>
                    <ul class="services-linkes">
                        @foreach($services->take(6) as $service)
                            <li><a href="{{url('service-detail/'.$service->id)}}">{{$service->title}}</a></li>
                        @endforeach

                    </ul>
                </div>
                <div class="col-lg-4 col-md-12 animation" data-animation="fadeInUp" data-animation-delay="0.3s">
                    <h4 class="title-footer">{{ trans('web_lang.contact us') }}</h4>
                    <ul class="contact-box">
                        <li>
                            <span><img src="{{asset('website')}}/assets/images/icons/location.png" alt=""></span>
                            <div>
                                <h4>{{ trans('web_lang.address') }}</h4>
                                <p>{{ setting('address') }}</p>
                            </div>
                        </li>
                        <li>
                            <span><img src="{{asset('website')}}/assets/images/icons/email.png" alt=""></span>
                            <div>
                                <h4>{{ trans('web_lang.E-mail') }}</h4>
                                <a href="#">{{ setting('email') }}</a>
                            </div>
                        </li>
                        <li>
                            <span><img src="{{asset('website')}}/assets/images/icons/call.png" alt=""></span>
                            <div>
                                <h4>{{ trans('web_lang.phone') }}</h4>
                                <a href="#">{{ setting('phone') }}</a>
                            </div>
                        </li>
                    </ul>
                    <div class="social-media">
                        <ul class="list_none social_icon">
                            <li><a href="{{ setting('facebook') }}"><i class=" fa fa-facebook"></i></a></li>
                            <li><a href="{{ setting('twitter') }}"><i class=" fa fa-twitter"></i></a></li>
                            <li><a href="{{ setting('gmail') }}"><i class=" fa fa-youtube"></i></a></li>
                            <li><a href="{{ setting('linkedin') }}"><i class=" fa fa-linkedin"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="bottom_footer">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <p class="copyright">{{ trans('web_lang.all rights are saved') }} &copy; {{ setting('title') }} 2021
                    </p>
                </div>
                <div class="col-md-6">
                    <ul class="list_none footer_menu">
                        <li><a href="#">{{ trans('web_lang.Usage Policy') }}</a></li>
                        <li><a href="#">{{ trans('web_lang.Privacy Policy') }}</a></li>
                        <li><a href="#">{{ trans('web_lang.Terms and Conditions') }}</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</footer>
<!-- END FOOTER SECTION -->
