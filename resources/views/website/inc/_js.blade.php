
<!-- Latest jQuery -->
<script src="{{asset('website')}}/assets/js/jquery-1.12.4.min.js"></script>
<!-- Latest compiled and minified Bootstrap -->
<script src="{{asset('website')}}/assets/bootstrap/js/bootstrap.min.js"></script>
<!-- owl-carousel min js  -->
<script src="{{asset('website')}}/assets/owlcarousel/js/owl.carousel.min.js"></script>
<!-- magnific-popup min js  -->
<script src="{{asset('website')}}/assets/js/magnific-popup.min.js"></script>
<!-- waypoints min js  -->
<script src="{{asset('website')}}/assets/js/waypoints.min.js"></script>
<!-- parallax js  -->
<script src="{{asset('website')}}/assets/js/parallax.js"></script>
<!-- countdown js  -->
<script src="{{asset('website')}}/assets/js/jquery.countdown.min.js"></script>
<!-- particles min js  -->
<script src="{{asset('website')}}/assets/js/particles.min.js"></script>
<!-- scripts js -->
<script src="{{asset('website')}}/assets/js/jquery.dd.min.js"></script>
<!-- jquery.counterup.min js -->
<script src="{{asset('website')}}/assets/js/jquery.counterup.min.js"></script>

<script src="{{asset('website')}}/assets/js/circular-Progress-Bar/jQuery-plugin-progressbar.js"></script>

<!-- chart js  -->
<script src="{{asset('website')}}/assets/js/Chart.bundle.js"></script>
<script src="{{asset('website')}}/assets/js/Chart.PieceLabel.min.js"></script>
<script src="{{asset('website')}}/assets/js/chart_script3.js"></script>
<!-- modern_canvas js -->
<script src="{{asset('website')}}/assets/js/modern_canvas.js"></script>
<script src="{{asset('website')}}/assets/js/spop.min.js"></script>
<script src="{{asset('website')}}/assets/js/revolution-slider/js/jquery.themepunch.tools.min.js"></script>
<script src="{{asset('website')}}/assets/js/revolution-slider/js/jquery.themepunch.revolution.min.js"></script>
<script src="{{asset('website')}}/assets/js/revolution-slider/js/extensions/revolution.extension.actions.min.js"></script>
<script src="{{asset('website')}}/assets/js/revolution-slider/js/extensions/revolution.extension.carousel.min.js"></script>
<script src="{{asset('website')}}/assets/js/revolution-slider/js/extensions/revolution.extension.kenburn.min.js"></script>
<script src="{{asset('website')}}/assets/js/revolution-slider/js/extensions/revolution.extension.layeranimation.min.js"></script>
<script src="{{asset('website')}}/assets/js/revolution-slider/js/extensions/revolution.extension.migration.min.js"></script>
<script src="{{asset('website')}}/assets/js/revolution-slider/js/extensions/revolution.extension.navigation.min.js"></script>
<script src="{{asset('website')}}/assets/js/revolution-slider/js/extensions/revolution.extension.parallax.min.js"></script>
<script src="{{asset('website')}}/assets/js/revolution-slider/js/extensions/revolution.extension.slideanims.min.js"></script>
<script src="{{asset('website')}}/assets/js/revolution-slider/js/extensions/revolution.extension.video.min.js"></script>
<!-- scripts js -->
<script src="{{asset('website')}}/assets/js/jquery.mixitup.js"></script>
<script src="{{asset('toastr')}}/cute-alert.js"></script>
<script src="{{asset('website')}}/assets/js/scripts.js"></script>

<script>
    $(function () {
        $('.portfolio-grid').mixItUp();
    });
</script>

{{-- ========================  toastr ======================== --}}

<script>

    function makeCuteAlert(title = null, text = null, type = null, redirect_url = '#',timeout = 50000)
    {
        cuteAlert({
            title: title,
            message: text,
            type: type, // or 'info', 'error', 'warning' , 'question'
            confirmText: "{{ trans('web_lang.Okay') }}",
            cancelText: "{{ trans('web_lang.Cancel') }}"
        }).then((e)=>{
            if ( e == ("confirm") ){
                window.location.href = redirect_url;
            } else {
            }
        })
    }

    function makeCuteToast(title = null, text = null, type = null, timeout = 50000)
    {
        cuteToast({
            title: title,
            message: text,
            type: type, // or 'success', 'info', 'error', 'warning'
            timer: timeout
        });
    }

</script>

{{-- ========================  toastr ======================== --}}



<script>
    var tpj = jQuery;
    var revapi34;
    tpj(document).ready(function () {
        if (tpj("#rev_slider_home").revolution == undefined) {
            revslider_showDoubleJqueryError("#rev_slider_home");
        } else {
            revapi34 = tpj("#rev_slider_home").show().revolution({
                sliderType: "standard",
                jsFileLocation: "assets/js/revolution-slider/js/",
                sliderLayout: "fullwidth",
                dottedOverlay: "none",
                delay: 6700,
                navigation: {
                    keyboardNavigation: "on",
                    keyboard_direction: "horizontal",
                    mouseScrollNavigation: "off",
                    onHoverStop: "on",
                    touch: {
                        touchenabled: "on",
                        swipe_threshold: 75,
                        swipe_min_touches: 1,
                        swipe_direction: "horizontal",
                        drag_block_vertical: false
                    },
                    arrows: {
                        style: "zeus",
                        enable: false,
                        hide_onmobile: true,
                        hide_under: 600,
                        hide_onleave: true,
                        hide_delay: 200,
                        hide_delay_mobile: 1200,
                        tmp: '<div class="tp-title-wrap">    <div class="tp-arr-imgholder"></div> </div>',
                        left: {
                            h_align: "left",
                            v_align: "center",
                            h_offset: 30,
                            v_offset: 0
                        },
                        right: {
                            h_align: "right",
                            v_align: "center",
                            h_offset: 30,
                            v_offset: 0
                        }
                    },
                    bullets: {
                        enable: true,
                        hide_onmobile: true,
                        hide_under: 1200,
                        style: "hesperiden",
                        hide_onleave: true,
                        hide_delay: 200,
                        hide_delay_mobile: 1200,
                        direction: "vertical",
                        h_align: "right",
                        v_align: "center",
                        h_offset: 40,
                        v_offset: 0,
                        space: 5
                    }
                },
                viewPort: {
                    enable: true,
                    outof: "pause",
                    visible_area: "100%"
                },
                shadow: 0,
                spinner: "off",
                stopLoop: 'off',
                stopAfterLoops: -1,
                stopAtSlide: -1,
                shuffle: "off",
                autoHeight: "on",
                hideThumbsOnMobile: "off",
                hideSliderAtLimit: 0,
                hideCaptionAtLimit: 0,
                hideAllCaptionAtLilmit: 0,
                debugMode: false,
                fallbacks: {
                    simplifyAll: "off",
                    nextSlideOnWindowFocus: "off",
                    disableFocusListener: false,
                }
            });
        }
    });
</script>

<script>
   $('.clients-img').owlCarousel({
    rtl:true,
    loop:true,
    nav:false,
    dots:false,
    autoplay:true,
    autoplayTimeout:2000,
    responsive:{
        0:{
            items:2
        },
        600:{
            items:3
        },
        1000:{
            items:4
        }
    }
});
$('.clients-img .owl-item').on('mouseenter',function(e){
    $(this).closest('.clients-img').trigger('stop.owl.autoplay');
  })
  $('.clients-img .owl-item').on('mouseleave',function(e){
    $(this).closest('.clients-img').trigger('play.owl.autoplay');
  })
</script> 

{{--new js--}}
@stack('web_js')
