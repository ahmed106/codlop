<!-- Favicon Icon -->
<link rel="shortcut icon" type="image/x-icon" href="{{asset('website')}}/assets/images/logo-icon.svg">
<link rel="stylesheet" href="{{asset('website')}}/assets/css/animate.css">
<link rel="stylesheet" href="{{asset('website')}}/assets/bootstrap/css/bootstrap.min.css">
<link href="https://fonts.googleapis.com/css?family=Poppins:100,200,300,400,500,600,700,800,900" rel="stylesheet">
<link rel="stylesheet" href="{{asset('website')}}/assets/css/font-awesome.min.css">
<link rel="stylesheet" href="{{asset('website')}}/assets/css/ionicons.min.css">
<link rel="stylesheet" href="{{asset('website')}}/assets/owlcarousel/css/owl.carousel.min.css">
<link rel="stylesheet" href="{{asset('website')}}/assets/owlcarousel/css/owl.theme.default.min.css">
<link rel="stylesheet" href="{{asset('website')}}/assets/css/magnific-popup.css">
<link rel="stylesheet" href="{{asset('website')}}/assets/css/spop.min.css">
<link href="{{asset('website')}}/assets/js/revolution-slider/css/settings.css" rel="stylesheet" type="text/css"/>
<link href="{{asset('website')}}/assets/js/revolution-slider/css/layers.css" rel="stylesheet" type="text/css"/>
<link href="{{asset('website')}}/assets/js/revolution-slider/css/navigation.css" rel="stylesheet" type="text/css"/>
<link rel="stylesheet" href="{{asset('website')}}/assets/js/circular-Progress-Bar/jQuery-plugin-progressbar.css">
<link rel="stylesheet" href="{{asset('website')}}/assets/css/style-{{ session_lang() }}.css?v=3">
<link id="layoutstyle" rel="stylesheet" href="{{asset('toastr')}}/cute-alert.css">
<link id="layoutstyle" rel="stylesheet" href="{{asset('website')}}/assets/color/theme.css">

<!-- Start of Async Callbell Code -->
<script>
    window.callbellSettings = {
        token: "a5rLBi5n7oyeawJwKPNZ83Xp"
    };
</script>
<script>
    (function () {
        var w = window;
        var ic = w.callbell;
        if (typeof ic === "function") {
            ic('reattach_activator');
            ic('update', callbellSettings);
        } else {
            var d = document;
            var i = function () {
                i.c(arguments)
            };
            i.q = [];
            i.c = function (args) {
                i.q.push(args)
            };
            w.Callbell = i;
            var l = function () {
                var s = d.createElement('script');
                s.type = 'text/javascript';
                s.async = true;
                s.src = 'https://dash.callbell.eu/include/' + window.callbellSettings.token + '.js';
                var x = d.getElementsByTagName('script')[0];
                x.parentNode.insertBefore(s, x);
            };
            if (w.attachEvent) {
                w.attachEvent('onload', l);
            } else {
                w.addEventListener('load', l, false);
            }
        }
    })()
</script>
<!-- End of Async Callbell Code -->


{{--new css--}}
@stack('web_css')
