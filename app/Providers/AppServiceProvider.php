<?php

namespace App\Providers;

use App\Models\Service;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application service.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Bootstrap any application service.
     *
     * @return void
     */
    public function boot()
    {
        $services = Service::get()->take(6);
        View::composer('*', function ($view) use ($services) {
            $view->with(['services' => $services]);
        });
        Schema::defaultStringLength('300');
    }
}
