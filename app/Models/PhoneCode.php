<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PhoneCode extends Model
{
    protected $table = 'phone_codes';
    protected $guarded = [];
}
