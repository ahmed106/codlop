<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SiteText extends Model
{
    protected $table = 'site_texts';
    protected $guarded = [];
    protected $appends = ['title','text'];

    public function site_text_trans()
    {
        return $this->hasMany("App\Models\SiteTextTran", "site_text_id", "id");
    }

    public function site_text_tran()
    {
        return $this->hasOne("App\Models\SiteTextTran", "site_text_id", "id")->where('lang', session_lang());
    }

    public function getTitleAttribute()
    {
        $title =  ($this->site_text_tran ? $this->site_text_tran->title : "#");
        return $title;
    }

    public function getTextAttribute()
    {
        $text =  ($this->site_text_tran ? $this->site_text_tran->text : "#");
        return $text;
    }

}
