<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ServiceTran extends Model
{
    protected $table = 'service_trans';
    protected $guarded = [];
}
