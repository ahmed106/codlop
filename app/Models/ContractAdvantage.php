<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ContractAdvantage extends Model
{
    protected $table = 'contract_advantages';
    protected $guarded = [];
    protected $appends = ['title','text'];

    public function contract_advantage_trans()
    {
        return $this->hasMany("App\Models\ContractAdvantageTran", "contract_advantage_id", "id");
    }

    public function contract_advantage_tran()
    {
        return $this->hasOne("App\Models\ContractAdvantageTran", "contract_advantage_id", "id")->where('lang', session_lang());
    }

    public function getTitleAttribute()
    {
        $title =  ($this->contract_advantage_tran ? $this->contract_advantage_tran->title : "#");
        return $title;
    }

    public function getTextAttribute()
    {
        $text =  ($this->contract_advantage_tran ? $this->contract_advantage_tran->text : "#");
        return $text;
    }


    public function contract_advantage_details()
    {
        return $this->hasMany("App\Models\ContractAdvantageDetail", "contract_advantage_id", "id");
    }


}
