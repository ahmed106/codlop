<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserProgramChat extends Model
{
    protected $table = 'user_program_chats';
    protected $guarded = [];

    public function user()
    {
        return $this->hasOne("App\Models\User", "id", "from_user_id");
    }


}
