<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ContractAdvantageTran extends Model
{
    protected $table = 'contract_advantage_trans';
    protected $guarded = [];
}
