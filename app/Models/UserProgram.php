<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserProgram extends Model
{
    protected $table = 'user_programs';
    protected $guarded = [];

    public function program()
    {
        return $this->hasOne("App\Models\Program", "id", "program_id");
    }


}
