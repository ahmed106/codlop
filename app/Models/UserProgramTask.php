<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserProgramTask extends Model
{
    protected $table = 'user_program_tasks';
    protected $guarded = [];
}
