<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PhoneCodeAllow extends Model
{
    protected $table = 'phone_code_allows';
    protected $guarded = [];
}
