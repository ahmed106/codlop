<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ServiceHostTran extends Model
{
    protected $table = 'service_host_trans';
    protected $guarded = [];
}
