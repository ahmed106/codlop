<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ServiceText extends Model
{
    protected $table = 'service_texts';
    protected $guarded = [];
    protected $appends = ['title','text'];

    public function service_text_trans()
    {
        return $this->hasMany("App\Models\ServiceTextTran", "service_text_id", "id");
    }

    public function service_text_tran()
    {
        return $this->hasOne("App\Models\ServiceTextTran", "service_text_id", "id")->where('lang', session_lang());
    }

    public function getTitleAttribute()
    {
        $title =  ($this->service_text_tran ? $this->service_text_tran->title : "#");
        return $title;
    }

    public function getTextAttribute()
    {
        $text =  ($this->service_text_tran ? $this->service_text_tran->text : "#");
        return $text;
    }

}
