<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Policies extends Model
{
    protected $table = 'policies';
    protected $guarded = [];
    protected $appends = ['title', 'text'];


    public function policies_trans()
    {
        return $this->hasMany(PoliciesTranslation::class, "policy_id", "id");
    }

    public function policies_tran()
    {
        return $this->hasOne(PoliciesTranslation::class, "policy_id", "id")->where('lang', session_lang());
    }

    public function getTitleAttribute()
    {
        $title = ($this->policies_tran ? $this->policies_tran->title : "#");
        return $title;
    }

    public function getTextAttribute()
    {
        $text = ($this->policies_tran ? $this->policies_tran->text : "#");
        return $text;
    }
}
