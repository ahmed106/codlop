<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SiteTextTran extends Model
{
    protected $table = 'site_text_trans';
    protected $guarded = [];
}
