<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ConfirmEmail extends Model
{
    protected $table = 'confirm_emails';
    protected $guarded = [];
}
