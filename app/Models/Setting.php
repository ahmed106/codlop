<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    protected $table = 'settings';
    protected $guarded = [];
    protected $appends = ['title','address'];

    public function setting_trans()
    {
        return $this->hasMany("App\Models\SettingTran", "setting_id", "id");
    }

    public function setting_tran()
    {
        return $this->hasOne("App\Models\SettingTran", "setting_id", "id")->where('lang', session_lang());
    }

    public function getTitleAttribute()
    {
        $title =  ($this->setting_tran ? $this->setting_tran->title : "#");
        return $title;
    }

    public function getAddressAttribute()
    {
        $title =  ($this->setting_tran ? $this->setting_tran->address : "#");
        return $title;
    }

}
