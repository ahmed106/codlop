<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Blog extends Model
{
    protected $table = 'blogs';
    protected $guarded = [];
    protected $appends = ['title','text'];

    public function blog_trans()
    {
        return $this->hasMany("App\Models\BlogTran", "blog_id", "id");
    }

    public function blog_tran()
    {
        return $this->hasOne("App\Models\BlogTran", "blog_id", "id")->where('lang', session_lang());
    }

    public function getTitleAttribute()
    {
        $title =  ($this->blog_tran ? $this->blog_tran->title : "#");
        return $title;
    }

    public function getTextAttribute()
    {
        $text =  ($this->blog_tran ? $this->blog_tran->text : "#");
        return $text;
    }
}
