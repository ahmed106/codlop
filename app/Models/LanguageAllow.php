<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LanguageAllow extends Model
{
    protected $table = 'language_allows';
    protected $guarded = [];
}
