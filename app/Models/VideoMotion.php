<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VideoMotion extends Model
{
    protected $table = 'video_motions';
    protected $guarded = [];
    protected $appends = ['title','text'];

    public function video_motion_trans()
    {
        return $this->hasMany("App\Models\VideoMotionTran", "video_motion_id", "id");
    }

    public function video_motion_tran()
    {
        return $this->hasOne("App\Models\VideoMotionTran", "video_motion_id", "id")->where('lang', session_lang());
    }

    public function getTitleAttribute()
    {
        $title =  ($this->video_motion_tran ? $this->video_motion_tran->title : "#");
        return $title;
    }

    public function getTextAttribute()
    {
        $text =  ($this->video_motion_tran ? $this->video_motion_tran->text : "#");
        return $text;
    }
}
