<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ServiceHost extends Model
{
    protected $table = 'service_hosts';
    protected $guarded = [];
    protected $appends = ['title', 'text'];

    public function service_host_trans()
    {
        return $this->hasMany("App\Models\ServiceHostTran", "service_host_id", "id");
    }

    public function service_host_tran()
    {
        return $this->hasOne("App\Models\ServiceHostTran", "service_host_id", "id")->where('lang', session_lang());
    }

    public function getTitleAttribute()
    {
        $title = ($this->service_host_tran ? $this->service_host_tran->title : "#");
        return $title;
    }

    public function getTextAttribute()
    {
        $text = ($this->service_host_tran ? $this->service_host_tran->text : "#");
        return $text;
    }


    public function service_host_details()
    {
        return $this->hasMany("App\Models\ServiceHostDetail", "service_host_id", "id");
    }


}
