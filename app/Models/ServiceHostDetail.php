<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ServiceHostDetail extends Model
{
    protected $table = 'service_host_details';
    protected $guarded = [];
    protected $appends = ['title', 'text'];

    public function service_host_detail_trans()
    {
        return $this->hasMany("App\Models\ServiceHostDetailTran", "service_host_detail_id", "id");
    }

    public function service_host_detail_tran()
    {
        return $this->hasOne("App\Models\ServiceHostDetailTran", "service_host_detail_id", "id")->where('lang', session_lang());
    }

    public function service_host()
    {
        return $this->hasOne("App\Models\ServiceHost", "id", "service_host_id");
    }

    public function getTitleAttribute()
    {
        $title = ($this->service_host_detail_tran ? $this->service_host_detail_tran->title : "----");
        return $title;
    }

    public function getTextAttribute()
    {
        $text = ($this->service_host_detail_tran ? $this->service_host_detail_tran->text : "#");
        return $text;
    }

}
