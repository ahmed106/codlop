<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProgramTran extends Model
{
    protected $table = 'program_trans';
    protected $guarded = [];
}
