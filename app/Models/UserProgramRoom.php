<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserProgramRoom extends Model
{
    protected $table = 'user_program_rooms';
    protected $guarded = [];


    public function user_program_chats()
    {
        return $this->hasMany("App\Models\UserProgramChat", "user_program_room_id", "id");
    }

}
