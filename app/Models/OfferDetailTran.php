<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OfferDetailTran extends Model
{
    protected $table = 'offer_detail_trans';
    protected $guarded = [];
}
