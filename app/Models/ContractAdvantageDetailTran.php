<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ContractAdvantageDetailTran extends Model
{
    protected $table = 'contract_advantage_detail_trans';
    protected $guarded = [];
}
