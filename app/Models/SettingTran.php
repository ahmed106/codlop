<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SettingTran extends Model
{
    protected $table = 'setting_trans';
    protected $guarded = [];
}
