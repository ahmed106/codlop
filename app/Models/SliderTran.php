<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SliderTran extends Model
{
    protected $table = 'slider_trans';
    protected $guarded = [];
}
