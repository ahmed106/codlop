<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    protected $table = 'services';
    protected $guarded = [];
    protected $appends = ['title','text'];

    public function service_trans()
    {
        return $this->hasMany("App\Models\ServiceTran", "service_id", "id");
    }

    public function service_tran()
    {
        return $this->hasOne("App\Models\ServiceTran", "service_id", "id")->where('lang', session_lang());
    }

    public function getTitleAttribute()
    {
        $title =  ($this->service_tran ? $this->service_tran->title : "#");
        return $title;
    }

    public function getTextAttribute()
    {
        $text =  ($this->service_tran ? $this->service_tran->text : "#");
        return $text;
    }

    public function service_hosts()
    {
        return $this->hasMany("App\Models\ServiceHost", "service_id", "id");
    }

    public function service_texts()
    {
        return $this->hasMany("App\Models\ServiceText", "service_id", "id");
    }

    public function service_text_normals()
    {
        return $this->hasMany("App\Models\ServiceText", "service_id", "id")->where([['type','=','normal']]);
    }

    public function service_text_lists()
    {
        return $this->hasMany("App\Models\ServiceText", "service_id", "id")->where([['type','=','list']]);
    }


}
