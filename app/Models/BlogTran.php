<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BlogTran extends Model
{
    protected $table = 'blog_trans';
    protected $guarded = [];
}
