<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ContractAdvantageDetail extends Model
{
    protected $table = 'contract_advantage_details';
    protected $guarded = [];
    protected $appends = ['title','text'];

    public function contract_advantage_detail_trans()
    {
        return $this->hasMany("App\Models\ContractAdvantageDetailTran", "contract_detail_id", "id");
    }

    public function contract_advantage_detail_tran()
    {
        return $this->hasOne("App\Models\ContractAdvantageDetailTran", "contract_detail_id", "id")->where('lang', session_lang());
    }

    public function getTitleAttribute()
    {
        $title =  ($this->contract_advantage_detail_tran ? $this->contract_advantage_detail_tran->title : "#");
        return $title;
    }

    public function getTextAttribute()
    {
        $text =  ($this->contract_advantage_detail_tran ? $this->contract_advantage_detail_tran->text : "#");
        return $text;
    }

}
