<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PrivacyTranslation extends Model
{
    protected $table = 'privacy_translations';

    protected $guarded = [];
}
