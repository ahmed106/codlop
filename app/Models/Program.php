<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Program extends Model
{
    protected $table = 'programs';
    protected $guarded = [];
    protected $appends = ['title', 'text'];

    public function program_trans()
    {
        return $this->hasMany("App\Models\ProgramTran", "program_id", "id");
    }

    public function program_tran()
    {
        return $this->hasOne("App\Models\ProgramTran", "program_id", "id")->where('lang', session_lang());
    }

    public function getTitleAttribute()
    {
        $title = ($this->program_tran ? $this->program_tran->title : "#");
        return $title;
    }

    public function getTextAttribute()
    {
        $text = ($this->program_tran ? $this->program_tran->text : "#");
        return $text;
    }

}
