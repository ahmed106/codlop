<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VideoMotionTran extends Model
{
    protected $table = 'video_motion_trans';
    protected $guarded = [];
}
