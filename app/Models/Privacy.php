<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Privacy extends Model
{
    protected $table = 'privacies';
    protected $guarded = [];
    protected $appends = ['title', 'text'];


    public function privacies_trans()
    {
        return $this->hasMany(PrivacyTranslation::class, "privacy_id", "id");
    }

    public function privacies_tran()
    {
        return $this->hasOne(PrivacyTranslation::class, "privacy_id", "id")->where('lang', session_lang());
    }

    public function getTitleAttribute()
    {
        $title = ($this->privacies_tran ? $this->privacies_tran->title : "#");
        return $title;
    }

    public function getTextAttribute()
    {
        $text = ($this->privacies_tran ? $this->privacies_tran->text : "#");
        return $text;
    }
}
