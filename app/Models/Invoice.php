<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
    protected $table = 'invoices';
    protected $guarded = [];

    public function getInvoiceNumber($number)
    {

        return sprintf('#' . "%'.06d", $number);


    }//end of getInvoiceNumber function

}
