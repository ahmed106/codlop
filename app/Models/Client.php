<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    protected $table = 'clients';
    protected $guarded = [];
    protected $appends = ['title','text'];


    public function client_trans()
    {
        return $this->hasMany("App\Models\ClientTran", "client_id", "id");
    }

    public function client_tran()
    {
        return $this->hasOne("App\Models\ClientTran", "client_id", "id")->where('lang', session_lang());
    }

    public function getTitleAttribute()
    {
        $title =  ($this->client_tran ? $this->client_tran->title : "#");
        return $title;
    }

    public function getTextAttribute()
    {
        $text =  ($this->client_tran ? $this->client_tran->text : "#");
        return $text;
    }

}
