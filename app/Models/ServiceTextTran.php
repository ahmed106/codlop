<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ServiceTextTran extends Model
{
    protected $table = 'service_text_trans';
    protected $guarded = [];
}
