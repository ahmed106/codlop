<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ConsiderationsAndRolesTranslation extends Model
{
    protected $table = 'considerations_and_roles_translations';
    protected $guarded = [];
}
