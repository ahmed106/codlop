<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ClientTran extends Model
{
    protected $table = 'client_trans';
    protected $guarded = [];
}
