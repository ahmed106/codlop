<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Team extends Model
{
    protected $table = 'teams';
    protected $guarded  =[];

    public function team_trans()
    {
        return $this->hasMany(TeamTranslation::class, "team_id", "id");
    }

    public function team_tran()
    {
        return $this->hasOne(TeamTranslation::class, "team_id", "id")->where('lang', session_lang());
    }

}
