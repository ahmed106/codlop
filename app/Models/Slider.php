<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Slider extends Model
{
    protected $table = 'sliders';
    protected $guarded = [];
    protected $appends = ['title','text'];


    public function slider_trans()
    {
        return $this->hasMany("App\Models\SliderTran", "slider_id", "id");
    }

    public function slider_tran()
    {
        return $this->hasOne("App\Models\SliderTran", "slider_id", "id")->where('lang', session_lang());
    }

    public function getTitleAttribute()
    {
        $title =  ($this->slider_tran ? $this->slider_tran->title : "#");
        return $title;
    }

    public function getTextAttribute()
    {
        $text =  ($this->slider_tran ? $this->slider_tran->text : "#");
        return $text;
    }

}
