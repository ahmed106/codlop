<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ServiceHostDetailTran extends Model
{
    protected $table = 'service_host_detail_trans';
    protected $guarded = [];
}
