<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ConsiderationsAndRoles extends Model
{
    protected $table = 'considerations_and_roles';
    protected $guarded = [];
    protected $appends = ['title', 'text'];


    public function considerations_and_roles_trans()
    {
        return $this->hasMany(ConsiderationsAndRolesTranslation::class, "consideration_id", "id");
    }

    public function considerations_and_roles_tran()
    {
        return $this->hasOne(ConsiderationsAndRolesTranslation::class, "consideration_id", "id")->where('lang', session_lang());
    }

    public function getTitleAttribute()
    {
        $title = ($this->considerations_and_roles_tran ? $this->considerations_and_roles_tran->title : "#");
        return $title;
    }

    public function getTextAttribute()
    {
        $text = ($this->considerations_and_roles_tran ? $this->considerations_and_roles_tran->text : "#");
        return $text;
    }
}
