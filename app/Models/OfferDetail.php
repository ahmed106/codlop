<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OfferDetail extends Model
{
    protected $table = 'offer_details';
    protected $guarded = [];
    protected $appends = ['title', 'text'];

    public function offer_detail_trans()
    {
        return $this->hasMany("App\Models\OfferDetailTran", "offer_detail_id", "id");
    }

    public function offer_detail_tran()
    {
        return $this->hasOne("App\Models\OfferDetailTran", "offer_detail_id", "id")->where('lang', session_lang());
    }

    public function getTitleAttribute()
    {
        $title = ($this->offer_detail_tran ? $this->offer_detail_tran->title : "#");
        return $title;
    }

    public function getTextAttribute()
    {
        $text = ($this->offer_detail_tran ? $this->offer_detail_tran->text : "#");
        return $text;
    }

}
