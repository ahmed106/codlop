<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PoliciesTranslation extends Model
{
    protected $table = 'policies_translations';
    protected $guarded = [];
}
