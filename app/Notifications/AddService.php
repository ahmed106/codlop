<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;

class AddService extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public $user;
    public $program;
    public $oldProgramsCount;

    public function __construct($user, $program, $oldProgramsCount)
    {

        $this->user = $user;
        $this->program = $program;
        $this->oldProgramsCount = $oldProgramsCount;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param mixed $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
//    public function toMail($notifiable)
//    {
//        return (new MailMessage)
//            ->line('The introduction to the notification.')
//            ->action('Notification Action', url('/'))
//            ->line('Thank you for using our application!');
//    }


    /**
     * Get the array representation of the notification.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function toDatabase($notifiable)
    {
        $text = '';
        if ($this->oldProgramsCount == 0) {
            $text = __('web_lang.has_been_subscribed_and');
        }
        return [
            'author' => ucfirst($this->user->name),
            'user_id' => $this->user->id,
            'message' => $text . __('web_lang.add_new_service'),
            'date' => change_date_to_new_format($this->program->created_at)
        ];
    }
}
