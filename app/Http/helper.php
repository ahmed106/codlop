<?php

use Illuminate\Support\Facades\Storage;

// ============================================================
if (!function_exists('get_lang')) {
    function get_lang()
    {
        return Request()->segment(1);
    }
}
// ============================================================

// ============================================================
if (!function_exists('change_language')) {
    function change_language($lang)
    {
        return LaravelLocalization::getLocalizedURL($lang);
    }
}
// ============================================================

// ============================================================
if (!function_exists('l_url')) {
    function l_url($url)
    {
        return LaravelLocalization::setLocale() . '/' . $url;;
    }
}
// ============================================================

// ============================================================
if (!function_exists('site_text')) {
    function site_text($id)
    {
        $site = \App\Models\SiteText::where('id', $id);
        if ($site->count() > 0) {
            return $site->first();
        }
        return 0;
    }
}
// ============================================================

// ============================================================
if (!function_exists('right_redirect')) {
    function right_redirect($url)
    {

//            function startsWith ($string, $startString)
//            {
        $len = strlen('h');
//                return (substr($url, 0, $len) === 'h');
//            }

        if (substr($url, 0, $len) === 'h') {
            return $url;
        }

        return '#';
    }

}
// ============================================================

// ============================================================
if (!function_exists('setting')) {
    function setting($key = null)
    {
        $setting = \App\Models\Setting::orderBy('id', 'desc')->first();
        return ($setting && $setting->{$key} ? $setting->{$key} : "#");
    }
}
// ============================================================

// ============================================================
if (!function_exists('one_site_text')) {
    function one_site_text($page_type = null, $text_type = null)
    {
        $query = [];

        if ($page_type != null) {
            $query[] = ['page_type', '=', $page_type];
        }

        if ($text_type != null) {
            $query[] = ['text_type', '=', $text_type];;
        }

        $setting = \App\Models\SiteText::where($query)->first();
        return ($setting && $setting->{$key} ? $setting->{$key} : "#");
    }
}
// ============================================================

// ============================================================
if (!function_exists('site_texts')) {
    function site_texts($page_type = null, $text_type = null)
    {
        $query = [];

        if ($page_type != null) {
            $query[] = ['page_type', '=', $page_type];
        }

        if ($text_type != null) {
            $query[] = ['text_type', '=', $text_type];;
        }

        $site_texts = \App\Models\SiteText::where($query)->get();
        return $site_texts;
    }
}
// ============================================================

// ============================================================
if (!function_exists('all_services')) {

    function all_services()
    {
        $all_services = \App\Models\Service::orderBy('id', 'asc')->get();
        return $all_services;
    }
}
// ============================================================

// ============================================================
if (!function_exists('all_setting')) {
    function all_setting()
    {
        $setting = \App\Models\Setting::orderBy('id', 'desc')->first();
        return $setting;
    }
}
// ============================================================

// ============================================================
if (!function_exists('GetImg')) {
    function GetImg($img_src)
    {
        if (filter_var($img_src, FILTER_VALIDATE_URL)) {
            $img = $img_src;
        } else if (Storage::exists($img_src)) {
            $img = url('storage/') . '/' . $img_src;
        } else {
            $img = url('/') . '/empty.png';
        }
        return $img;
    }
}
// ============================================================

// ============================================================
if (!function_exists('language_allows')) {
    function language_allows()
    {
        return \App\Models\LanguageAllow::pluck('code')->toArray();
    }
}
// ============================================================

// ============================================================
if (!function_exists('session_lang')) {
    function session_lang()
    {

        $lang = 'ar';
        $language_allows = language_allows();

        if (session()->get('lang') && in_array(session()->get('lang'), $language_allows)) {
            $lang = session()->get('lang') ? session()->get('lang') : 'default';
        }

        if (get_lang() && in_array(get_lang(), ['ar', 'en'])) {
            $lang = get_lang();
        }

        if (request()->get('lang') && in_array(request()->get('lang'), $language_allows)) {
            $lang = request()->get('lang');
        }

        if (request()->post('lang') && in_array(request()->post('lang'), $language_allows)) {
            $lang = request()->post('lang');
        }

        if (request()->header('lang') && in_array(request()->header('lang'), $language_allows)) {
            $lang = request()->header('lang');
        }

        return $lang;
    }
}
// ============================================================

// ============================================================
if (!function_exists('change_date_to_new_format')) {
    function change_date_to_new_format($current_date, $date_type = '')
    {
        // PHP Arabic Date
        // PHP Arabic Date

        error_reporting(E_ALL ^ E_NOTICE);

        $months = array(
            "Jan" => "يناير",
            "Feb" => "فبراير",
            "Mar" => "مارس",
            "Apr" => "أبريل",
            "May" => "مايو",
            "Jun" => "يونيو",
            "Jul" => "يوليو",
            "Aug" => "أغسطس",
            "Sep" => "سبتمبر",
            "Oct" => "أكتوبر",
            "Nov" => "نوفمبر",
            "Dec" => "ديسمبر"
        );

        $your_date = $current_date; // The Current Date

        $en_month = date("M", strtotime($your_date));

        foreach ($months as $en => $ar) {
            if ($en == $en_month) {
                $ar_month = get_lang() == 'en' ? $en : $ar;
            }
        }

        $find = array(
            "Sat",
            "Sun",
            "Mon",
            "Tue",
            "Wed",
            "Thu",
            "Fri"
        );

        $replace = array(
            "Sat",
            "Sun",
            "Mon",
            "Tue",
            "Wed",
            "Thu",
            "Fri"
        );

        if (get_lang() == 'ar') {
            $replace = array(
                "السبت",
                "الأحد",
                "الإثنين",
                "الثلاثاء",
                "الأربعاء",
                "الخميس",
                "الجمعة"
            );
        }


        $ar_day_format = date("D", strtotime($your_date)); // The Current Day

        $ar_day = str_replace($find, $replace, $ar_day_format);

        header('Content-Type: text/html; charset=utf-8');
        $standard = array("0", "1", "2", "3", "4", "5", "6", "7", "8", "9");

        $eastern_arabic_symbols = array("0", "1", "2", "3", "4", "5", "6", "7", "8", "9");

        if (get_lang() == 'ar') {
            $eastern_arabic_symbols = array("٠", "١", "٢", "٣", "٤", "٥", "٦", "٧", "٨", "٩");
        }

        $current_date = $ar_day . ' - ' . date("d", strtotime($your_date)) . ' / ' . $ar_month . ' / ' . date("Y", strtotime($your_date));

        if ($date_type != '') {
            $current_date = date("d", strtotime($your_date)) . ' / ' . $ar_month . ' / ' . date("Y", strtotime($your_date));
        }

        if ($date_type == 'full') {
            if (session_lang() == 'ar') {
                $current_date = date("d", strtotime($your_date)) . ' / ' . $ar_month . ' / ' . date("Y", strtotime($your_date));
            } else {
                $current_date = date("Y", strtotime($your_date)) . ' / ' . $ar_month . ' / ' . date("d", strtotime($your_date));
            }
        }

        if ($date_type == 'small_date') {
            if (session_lang() == 'ar') {
                $current_date = date("d", strtotime($your_date)) . ' / ' . $ar_month;
            } else {
                $current_date = date("Y", strtotime($your_date)) . ' / ' . $ar_month;
            }
        }

        if ($date_type == 'time_ar') {
            $current_date = date("H", strtotime($your_date)) . ':' . date("i", strtotime($your_date))/*.":".date("s", strtotime($your_date))*/
            ;
        }

        if ($date_type == 'time_ar_pm_am') {
            $am_pm_date = '  ص  ';
            if (date("A", strtotime($your_date)) == 'PM') {
                $am_pm_date = '  م  ';
            }
            $current_date = date("h", strtotime($your_date)) . ':' . date("i", strtotime($your_date)) . $am_pm_date;
        }

        $arabic_date = str_replace($standard, $eastern_arabic_symbols, $current_date);

        // Echo Out the Date


        return $arabic_date;
    }
}
// ============================================================

// ============================================================
if (!function_exists('upload_image')) {
    function upload_image($path_ = '', $img, $plus, $file_name = '', $delete_file = '')
    {
        // delete old file
        $delete_file != '' ? Storage::delete($delete_file) : '';
        $path = $img->store($path_);
        return $path;
    }
}
// ============================================================

// ============================================================
if (!function_exists('delete_image')) {
    function delete_image($img)
    {
        // delete old file
        $img != '' ? Storage::delete($img) : '';
    }
}
// ============================================================

// ============================================================
if (!function_exists('upload_multi_images')) {
    function upload_multi_images($image = '', $id = '', $path = '')
    {

        $data['size'] = $image->getSize();
        $data['mime_type'] = $image->getMimeType();
        $data['name'] = $image->getClientOriginalName();
        $data['hashname'] = $image->hashName();
        $image->store($path);
        return $data;


    }

}
// ============================================================

// ============================================================
if (!function_exists('up')) {
    function up()
    {
        return new \App\Http\Controllers\Upload;
    }
}
// ============================================================

// ============================================================
if (!function_exists('view_language')) {
    function view_language($json_trans = [])
    {
        $json_trans_format = [];

        foreach ($json_trans as $json_tran) {
            $json_trans_format[$json_tran->lang] = $json_tran;
        }
        return $json_trans_format;
    }
}
// ============================================================

// ============================================================
if (!function_exists('aurl')) {
    function aurl($url = null)
    {
        return url('admin/' . $url);
    }
}
// ============================================================

// ============================================================
if (!function_exists('guard_admin')) {
    function guard_admin()
    {
        return auth()->guard('admin');
    }
}
// ============================================================

// ============================================================
if (!function_exists('admin')) {
    function admin()
    {
        return auth()->guard('admin')->user();
    }
}
// ============================================================

// ============================================================
if (!function_exists('get_language_allows')) {
    function get_language_allows()
    {
        $language_allows = \App\Models\LanguageAllow::get();
        return $language_allows;
    }
}
// ============================================================

// ============================================================
if (!function_exists('active_menu')) {
    function active_menu($link)
    {
        if (preg_match('/' . $link . '/i', Request::segment(2))) {
            return ['m-menu__item--open', 'display:block'];
        } else {
            return ['', ''];
        }
    }
}
// ============================================================

// ============================================================
if (!function_exists('datatable_lang')) {
    function datatable_lang()
    {
        return ['sProcessing' => trans('admin.sProcessing'),
            'sLengthMenu' => trans('admin.sLengthMenu'),
            'sZeroRecords' => trans('admin.sZeroRecords'),
            'sEmptyTable' => trans('admin.sEmptyTable'),
            'sInfo' => trans('admin.sInfo'),
            'sInfoEmpty' => trans('admin.sInfoEmpty'),
            'sInfoFiltered' => trans('admin.sInfoFiltered'),
            'sInfoPostFix' => trans('admin.sInfoPostFix'),
            'sSearch' => trans('admin.sSearch'),
            'sUrl' => trans('admin.sUrl'),
            'sInfoThousands' => trans('admin.sInfoThousands'),
            'sLoadingRecords' => trans('admin.sLoadingRecords'),
            'oPaginate' => [
                'sFirst' => trans('admin.sFirst'),
                'sLast' => trans('admin.sLast'),
                'sNext' => trans('admin.sNext'),
                'sPrevious' => trans('admin.sPrevious'),
            ],
            'oAria' => [
                'sSortAscending' => trans('admin.sSortAscending'),
                'sSortDescending' => trans('admin.sSortDescending'),
            ],
        ];
    }
}
// ============================================================

// ============================================================
if (!function_exists('v_image')) {
    function v_image($ext = null)
    {
        if ($ext === null) {
            return 'image|mimes:jpg,jpeg,png,gif,bmp';
        } else {
            return 'image|mimes:' . $ext;
        }
    }
}
// ============================================================

// ============================================================
if (!function_exists('get_file')) {
    function get_file($file)
    {
        if (!is_null($file))
            return asset('storage') . '/' . $file;
        else
            return asset('empty.png');

    }
}
// ============================================================
