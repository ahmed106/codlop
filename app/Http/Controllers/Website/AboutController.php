<?php

namespace App\Http\Controllers\Website;

use App\Http\Controllers\Controller;
use App\Models\SiteText;

class AboutController extends Controller
{

    public function about()
    {
        $site_text_abouts = SiteText::where([['page_type', '=', 'about'], ['text_type', '=', null]])->with(['site_text_tran'])->get();
        $site_text_features = SiteText::where([['page_type', '=', 'about']])->whereIn('text_type', ['features_of_codlop'])->with(['site_text_tran'])->get();


        return view('website.pages.about.index', [
            'site_text_abouts' => $site_text_abouts,
            'site_text_features' => $site_text_features,
        ]);
    }

}


