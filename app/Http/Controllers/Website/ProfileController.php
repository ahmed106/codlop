<?php

namespace App\Http\Controllers\Website;

use App\Http\Controllers\Admin\BasicDepartmentController;
use App\Http\Controllers\Controller;
use App\Models\Program;
use App\Models\UserProgram;
use App\Models\UserProgramChat;
use App\Models\UserProgramRoom;
use App\Models\UserRoomMessage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;


class ProfileController extends Controller
{

    /* ==================================================================== */
    public function profile()
    {


        $user_id = (auth()->user() ? auth()->user()->id : null);

        $user_programs = UserProgram::with(['program'])->where([['user_id', '=', $user_id]])->get();

        // other apps
        $user_program_ids = UserProgram::with(['program'])->where([['user_id', '=', $user_id]])->pluck('program_id')->toArray();
        $programs = Program::whereNotIn('id', $user_program_ids)->get();

        return view('website.pages.profile.profile', ['user_programs' => $user_programs, 'programs' => $programs]);
    }

    /* ==================================================================== */
    public function getDescription()
    {


        $program = UserProgram::find(request()->program_id);

        return response()->json(['data' => $program->details], 200);

    }//end of getDescription function

    /* ==================================================================== */
    public function new_apps()
    {
        $user_id = (auth()->user() ? auth()->user()->id : null);
        $user_program_ids = UserProgram::with(['program'])->where([['user_id', '=', $user_id]])->pluck('program_id')->toArray();
        $programs = Program::whereNotIn('id', $user_program_ids)->get();
        return view('website.pages.profile.new-apps', ['programs' => $programs]);
    }
    /* ==================================================================== */


    /* ==================================================================== */
    public function confirm_program()
    {
        $user_program_id = \request()->get('user_program_id');
        UserProgram::findorfail($user_program_id)->update([
            'status' => 'finished'
        ]);
        return response()->json(['user_program_id' => $user_program_id], 200);
    }

    /* ==================================================================== */

    /* ==================================================================== */
    public function add_apps()
    {
        $validator = Validator::make(request()->all(),
            [
                'programs' => 'required|array|min:1',
                'programs.*' => 'required|numeric|exists:programs,id',
            ], [], []
        );

        if ($validator->fails()) {
            $errors = collect($validator->errors())->flatten(1);
            foreach ($errors as $error) {
                return back()->withInput()->with('fail-message', trans('web_lang.some thing wrong'));
            }
            return back()->withInput();
        }

        $data = $validator->validated();

        $user_id = (auth()->user() ? auth()->user()->id : null);

        if (count(request()->programs) > 0) {
            foreach (request()->programs as $program) {
                $UserProgram = UserProgram::create([
                    'program_id' => $program,
                    'user_id' => $user_id,
                ]);
                UserProgramRoom::create([
                    'user_program_id' => $UserProgram->id,
                ]);
            }
        }

        return redirect(url('profile'))->with('success-message', trans('web_lang.Done successfully.'));
    }

    /* ==================================================================== */

    /* ==================================================================== */
    public function chat()
    {

        $program = UserProgram::find(\request()->get('program_id'));
        $user_program_id = request()->get('user_program_id');
        $user_program_room = UserProgramRoom::with(['user_program_chats', 'user_program_chats.user'])->where('user_program_id', $user_program_id)->firstOrFail();
        return view('website.pages.profile.chat', ['user_program_room' => $user_program_room, 'program' => $program]);
    }
    /* ==================================================================== */

// =====================================================
    public function msg_send(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'user_program_room_id' => 'required|numeric|exists:user_program_rooms,id',
            'from_user_id' => 'required|numeric|exists:users,id',
            'message' => 'required',
        ], []);
        if ($validator->fails()) {
            $code = 422;
            return response()->json(['error' => collect($validator->errors())->flatten(1)], $code);
        }
        $data = $validator->validated();
        $data['user_type'] = 'user';
        $RoomMsg = UserProgramChat::create($data);
        return response()->json(['data' => $RoomMsg, 'msg' => $request->message], 200);
    }
// =====================================================


} //end of class


