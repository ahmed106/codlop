<?php

namespace App\Http\Controllers\Website;

use App\Http\Controllers\Controller;
use App\Http\Traits\SendEmailPHPNative;
use App\Mail\TestEmail;
use App\Models\Admin;
use App\Models\ConfirmEmail;
use App\Models\Offer;
use App\Models\PhoneCodeAllow;
use App\Models\Program;
use App\Models\User;
use App\Models\UserProgram;
use App\Models\UserProgramRoom;
use App\Notifications\AddService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Notification;
use Illuminate\Validation\Rule;
use Validator;


class OrderController extends Controller
{

    use SendEmailPHPNative;

    /* ==================================================== */
    public function order_now()
    {

        $programs = Program::get();
        $offers = Offer::get();
        $offer_main_package = Offer::where([['offer_type', '=', 'main_package']])->first();
        $offer_professional_package = Offer::where([['offer_type', '=', 'professional_package']])->first();
        $offer_golden_package = Offer::where([['offer_type', '=', 'golden_package']])->first();
        $phone_code_allows = PhoneCodeAllow::all();

        return view('website.pages.order-now.index', [
            'programs' => $programs,
            'offers' => $offers,
            'offer_main_package' => $offer_main_package,
            'offer_professional_package' => $offer_professional_package,
            'offer_golden_package' => $offer_golden_package,
            'phone_code_allows' => $phone_code_allows

        ]);

    }
    /* ==================================================== */

    /* ==================================================== */
    public function confirm_email(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email|max:299',
        ], []);


        if ($validator->fails()) {
            return response()->json(['errors' => collect($validator->errors())->flatten(1)], 422);
        }

        $data = collect($validator->validated())->except([])->toArray();


        $ConfirmEmail = ConfirmEmail::where([['email', '=', $request->email]])->first();


        if (!isset($ConfirmEmail)) {
            $ConfirmEmail = ConfirmEmail::create($data);
        }


        $code = $ConfirmEmail->id . rand(1000, 9999);
        $message = trans('web_lang.confirm code') . $code;

        ConfirmEmail::where([['id', '=', $ConfirmEmail->id]])->update([
            'code' => $code
        ]);


        Mail::to($request->email)->send(new TestEmail($code)); // for test email via mailtrap
        //$this->sendEmailPHPNative($request->email, trans('web_lang.confirm email'), $message); // for real sending email

        return response()->json($data, 200);
    }
    /* ==================================================== */

    /* ==================================================== */
    public function send_order_now(Request $request)
    {

        $user = User::where('email', $request->email)->first();
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:299',
            'email' => ['required', 'email', 'max:299', isset($user) ? Rule::unique('users')->ignore($user->id) : ''],
            'confirm_email' => ['sometimes:nullable', "numeric", Rule::exists('confirm_emails', 'code') /*->where('email','=', $request->email)*/],
            'phone_code' => ['required', "numeric", Rule::exists('phone_code_allows', 'dial_code')],
            'phone' => ['required', 'numeric', isset($user) ? Rule::unique('users', 'phone')->ignore($user->id) : ''],
            'offer_id' => ['nullable', "numeric", Rule::exists('offers', 'id')],
            'more_details' => 'nullable',
            'programs' => 'required|array|min:1',
            'programs.*' => ['nullable', "numeric", Rule::exists('programs', 'id')],

        ], []);


        if ($validator->fails()) {
            return response()->json(['errors' => collect($validator->errors())->flatten(1)], 422);
        }
        $data = collect($validator->validated())->except(['programs', 'confirm_email'])->toArray();
        $pass = rand(10000, 9999999);
        $data['password'] = Hash::make($pass);
        if (!$user) {

            $user = User::create($data);
            Mail::to($request->email)->send(new TestEmail($pass)); // for test email via mailtrap
            //mail($request->email, "Login details", 'Your Email : ' . $request->email . ' , password : ' . $pass); // real sendign email
        }//end if statement

        $olderUserProgram = UserProgram::count();

        $details = $request->details;
        foreach ($details as $index => $detail) {

            if ($details[$index] == null) {
                unset($details[$index]);
            }
        }

        $details = array_values($details);
        if (count($request->programs) > 0) {
            foreach ($request->programs as $index => $program) {

                $UserProgram = UserProgram::create([
                    'user_id' => $user->id,
                    'program_id' => $program,
                    'details' => $details[$index]
                ]);

                UserProgramRoom::create([
                    'user_program_id' => $UserProgram->id,
                ]);
            }
        }

        $user->update([

            'is_read' => 'unread'
        ]);

        $userPrograms = UserProgram::orderBy('id', 'desc')->first();
        $admins = Admin::all();
        Notification::send($admins, new AddService($user, $userPrograms, $olderUserProgram));

        return response()->json($data, 200);
    }
    /* ==================================================== */
}


