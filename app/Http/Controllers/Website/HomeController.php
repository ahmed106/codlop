<?php

namespace App\Http\Controllers\Website;

use App\Http\Controllers\Controller;
use App\Models\Client;
use App\Models\ContractAdvantage;
use App\Models\SiteText;
use App\Models\Slider;
use App\Models\Service;

class HomeController extends Controller
{

    public function index()
    {
        $sliders = Slider::all();
        $contract_advantages = ContractAdvantage::with(['contract_advantage_details'])->get();
        $site_text_home_abouts = SiteText::where([['page_type', '=', 'home' ], ['text_type', '=', null]])->with(['site_text_tran'])->get();
        $site_text_service = SiteText::where([['page_type', '=', 'service']])->first();
        $services = Service::get()->take(6);
        $clients = Client::get();

        return view('website.pages.home.index', [
            'sliders' => $sliders,
            'contract_advantages' => $contract_advantages,
            'site_text_home_abouts' => $site_text_home_abouts,
            'site_text_service' => $site_text_service,
            'services' => $services,
            'clients' => $clients,
        ]);
    }


}


