<?php

namespace App\Http\Controllers\Website;

use App\Http\Controllers\Controller;
use App\Models\Service;
use App\Models\SiteText;

class ServiceController extends Controller
{

    public $header_text;

    public function services()
    {

        $site_text_service = SiteText::where([['page_type', '=', 'service']])->first();

        $this->header_text = $site_text_service->title;

        $services_ = Service::get();

        return view('website.pages.service.index', ['services_' => $services_, 'site_text_service' => $site_text_service,
        ]);
    }

    public function service_detail($service_id)
    {


        $service = Service::with(['service_text_normals', 'service_text_lists', 'service_hosts.service_host_details'])->findorfail($service_id);


        return view('website.pages.service.service_detail', [
            'service' => $service,
        ]);
    }


}


