<?php

namespace App\Http\Controllers\Website;

use App\Http\Controllers\Controller;
use App\Models\VideoMotion;

class VideoMotionController extends Controller
{

    public function video_motion()
    {
        $categories = VideoMotion::get()->groupBy('type');
        $all_video_motions = VideoMotion::with(['video_motion_tran'])->orderBy('id', 'desc')->get();
        return view('website.pages.video_motion.our_work', [

            'all_video_motions' => $all_video_motions,
            'categories' => $categories
        ]);
    }

    public function get_video_content()
    {
        $data = request();
        $html = view('website.pages.video_motion.video', compact('data'))->render();
        return response()->json(['html' => $html], 200);
    }

    public function work_details($id)
    {
        $work_detail = VideoMotion::findorfail($id);
        return view('website.pages.video_motion.work_details', ['work_detail' => $work_detail]);
    }


}


