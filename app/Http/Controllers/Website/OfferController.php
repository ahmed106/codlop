<?php

namespace App\Http\Controllers\Website;

use App\Http\Controllers\Controller;
use App\Models\Offer;
use App\Models\OfferDetail;

class OfferController extends Controller
{

    public function offers()
    {
        $offer_details = OfferDetail::get();
        $offer_main_package = Offer::where([['offer_type','=','main_package']])->first();
        $offer_professional_package = Offer::where([['offer_type','=','professional_package']])->first();
        $offer_golden_package = Offer::where([['offer_type','=','golden_package']])->first();

//        return $offer_details;

        return view('website.pages.offer.index', [
            'offer_details' => $offer_details,
            'offer_main_package' => $offer_main_package,
            'offer_professional_package' => $offer_professional_package,
            'offer_golden_package' => $offer_golden_package,
        ]);

    }


}


