<?php

namespace App\Http\Controllers\Website;

use App\Http\Controllers\Controller;
use App\Models\Blog;

class BlogController extends Controller
{

    public function blog()
    {
        $blogs = Blog::with(['blog_tran'])->orderBy('date', 'desc')->paginate(9);
        return view('website.pages.blog.index', [
            'blogs' => $blogs,
        ]);
    }

    public function blog_detail($blog_id)
    {

        $blog = Blog::where([['id', '=', $blog_id]])->with(['blog_tran'])->first();
        $new_blogs = Blog::with(['blog_tran'])->where([['id', '!=', $blog_id]])->orderBy('id', 'desc')->get()->take(6);
        return view('website.pages.blog.blog_detail', [
            'blog' => $blog,
            'new_blogs' => $new_blogs,
        ]);
    }

}


