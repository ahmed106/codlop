<?php

namespace App\Http\Controllers\Website;

use App\Models\User;
use App\Http\Controllers\Controller;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Mail;
use Auth;

class WebAuth extends Controller
{

    public function login()
    {
        if (Auth::user()) {
            return redirect(url('profile'));
        }
        return view('website.pages.auth.login');
    }

    public function profile()
    {
        return view('website.pages.profile.profile');
    }

    public function login_action()
    {

        $rememberme = request('rememberme') == 1 ? true : false;

        if (auth()->attempt(['email' => request('email'), 'password' => request('password')], $rememberme)) {
            toastr()->success('Login Successfully', trans('admin.Message_title_congratulation'));
            $user = Auth::user();
            return redirect(url('profile'))->with('success-message', trans('web_lang.Login Done successfully.'));
        } else {
            return redirect(url('login'))->with('fail-message', trans('web_lang.Error In Email or Password .'))->withInput();
        }

    }

    public function logout()
    {
        auth()->logout();
        toastr()->success(trans('admin.logout_message'));
        return redirect(url('login'));
    }


//==================================================

    public function rest_password_form()
    {
        return view('website.pages.auth.rest_password.rest_password_form');
    }

    public function rest_password(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'email' => 'required|exists:users,email',
        ], []);

        if ($validator->fails()) {
            return back()->with('fail-message', trans('web_lang.Error In Email .'))->withInput();
        }


        $user_email = $validator->validated()['email'];
        $user = User::where('email', $user_email)->first();
        $code = $user->id . rand(10000, 99999);


        User::where('email', $user_email)->update([
            'forget_password_code' => $code
        ]);

        mail($user_email, "Your Reset Code ", 'Your Reset Code is ' . $code);

        $confirm_user = User::where('email', $user_email)->first();

        return view('website.pages.auth.rest_password.confirm_code_form', compact('confirm_user'));
    }

    public function confirm_code(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required',
            'forget_password_code' => 'required',
        ], []);

        if ($validator->fails()) {
            return redirect(url('rest-password-form'))->with('fail-message', trans('web_lang.Error In Email .'))->withInput();
        }

        $user_code = User::find($request->id);

        if ($user_code->forget_password_code != $request->forget_password_code) {
            toastr()->warning('Confirm code is Invalid', trans('admin.Message_title_attention'));
            return redirect(url('rest-password-form'));
        }

        return view('website.pages.auth.rest_password.update_password_form', compact('user_code'));
    }

    public function update_password(Request $request)
    {

//        return $request->all();

        $validator = Validator::make($request->all(), [
            'id' => 'required',
            'password' => 'min:6|required_with:password_confirmation|same:password_confirmation',
//            'password_confirmation' => 'min:6'
        ], []);

        if ($validator->fails()) {
            $errors = collect($validator->errors())->flatten(1);
            foreach ($errors as $error) {
                toastr()->warning($error, trans('admin.Message_title_attention'));
            }
            return redirect(url('rest-password-form'));
        }

        user::where('id', $request->id)->update([
            'password' => Hash::make($request->password),
        ]);

        $user = User::find($request->id);
        Auth::login($user);


        return redirect(url('profile'));

    }

//==================================================


}
