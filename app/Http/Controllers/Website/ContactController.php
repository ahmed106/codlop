<?php

namespace App\Http\Controllers\Website;

use App\Http\Controllers\Controller;
use App\Models\Contact;
use App\Models\PhoneCodeAllow;
use Illuminate\Http\Request;
use Validator;
use Illuminate\Validation\Rule;


class ContactController extends Controller
{

    public function contact()
    {
        $phone_code_allows = PhoneCodeAllow::all();
        return view('website.pages.contact.index',[
            'phone_code_allows' => $phone_code_allows
        ]);
    }

    public function send_contact(Request $request){
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:299',
            'email' => 'required|email|max:299',
            'subject' => 'required|max:299',
            'phone_code' => ['required',"numeric",Rule::exists('phone_code_allows', 'dial_code')],
            'phone' => 'required|numeric',
            'message' => 'required',
        ], []);

        if ($validator->fails()) {
            return response()->json(['errors' => collect($validator->errors())->flatten(1)], 422);
        }
        $contact = collect($validator->validated())->toArray();
        Contact::create($contact);
        return response()->json($contact, 200);
    }

}


