<?php

namespace App\Http\Controllers\Website;

use App\Http\Controllers\Controller;
use App\Models\Team;
use Illuminate\Http\Request;

class TeamController extends Controller
{

    public function index(){

        $teams = Team::get();

        return view('website.pages.teams.index',compact('teams'));

    }//end of index function
}
