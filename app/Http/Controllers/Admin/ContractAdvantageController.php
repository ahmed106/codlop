<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Traits\CheckAllowedLanguageKeys;
use App\Http\Traits\ViewLanguage;
use App\Models\ContractAdvantage;
use App\Models\ContractAdvantageDetail;
use App\Models\ContractAdvantageDetailTran;
use App\Models\ContractAdvantageTran;
use App\Models\LanguageAllow;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

//models

/* traits */

class ContractAdvantageController extends Controller
{

    use CheckAllowedLanguageKeys, ViewLanguage;


    public $page_titles = [
        'index' => 'مميزات التعاقد',
        'create' => 'اضافة مميزات التعاقد جديد',
        'edit' => '',
    ];

//========================================= index =========================================
    public function index()
    {
        $contract_advantages = ContractAdvantage::orderBy('id', 'desc')->get();
        return view('admin.contract_advantages.index', ['contract_advantages' => $contract_advantages, 'page_titles' => $this->page_titles]);
    }

//========================================= create =========================================
    public function create()
    {
        $language_allows = LanguageAllow::get();
        return view('admin.contract_advantages.create', ['page_titles' => $this->page_titles, 'language_allows' => $language_allows]);
    }

//========================================= store =========================================
    public function store()
    {
        $validator = Validator::make(request()->all(),
            [
                'image' => 'nullable|image',
            ], [], []
        );

        if ($validator->fails()) {
            $errors = collect($validator->errors())->flatten(1);
            foreach ($errors as $error) {
                toastr()->warning($error, trans('admin.Message_title_attention'));
            }
            return back()->withInput();
        }

        $data = $validator->validated();

        /* =============== check languages =============== */
        if ((!is_array(request()->titles) || !$this->check_allowed_language_keys(request()->titles)) ||
            (!is_array(request()->texts) || !$this->check_allowed_language_keys(request()->texts))) {
            toastr()->warning('رجاء املاء جميع اللغات حتى لا يحدث مشكلة فيما بعد (title)', setting('title'));
            return back()->withInput();
        }
        /* =============== check languages =============== */

        if (request()->image != '') {
            $data['image'] = upload_image('contract_advantages', request()->image, 1, 'image', '');
        }

        $contract_advantage = ContractAdvantage::create($data);

        /* =============== languages =============== */
        if (count(request()->titles) > 0) {
            foreach (request()->titles as $lang => $title) {
                ContractAdvantageTran::create([
                    'contract_advantage_id' => $contract_advantage->id,
                    'title' => $title,
                    'text' => request()->texts[$lang] ? request()->texts[$lang] : null,
                    'lang' => $lang,
                ]);
            }
        }
        /* =============== languages =============== */

        toastr()->success('تم العملية بنجاح', setting('title'));
        return redirect(aurl('contract_advantage'));
    }

//========================================= show =========================================
    public function show($id)
    {
        //
    }

//========================================= edit =========================================
    public function edit($id)
    {
        $contract_advantage = ContractAdvantage::findorfail($id);
        $language_allows = LanguageAllow::get();
        $title = ' تعديل بيانات ' . $contract_advantage->title;
        $this->page_titles['edit'] = $title;
        $view_languages = $this->view_language($contract_advantage->contract_advantage_trans);
        return view('admin.contract_advantages.edit', ['contract_advantage' => $contract_advantage, 'language_allows' => $language_allows, 'view_languages' => $view_languages, 'page_titles' => $this->page_titles]);
    }


//========================================= update =========================================
    public function update_contract_advantage()
    {

        $validator = Validator::make(request()->all(),
            [
                'image' => 'nullable|image',
            ], [], []
        );

        if ($validator->fails()) {
            $errors = collect($validator->errors())->flatten(1);
            foreach ($errors as $error) {
                toastr()->warning($error, trans('admin.Message_title_attention'));
            }
            return back()->withInput();
        }

        $data = $validator->validated();

        $contract_advantage = ContractAdvantage::findorfail(request()->contract_advantage_id);

        /* =============== check languages =============== */
        if ((!is_array(request()->titles) || !$this->check_allowed_language_keys(request()->titles)) ||
            (!is_array(request()->texts) || !$this->check_allowed_language_keys(request()->texts))) {
            toastr()->warning('رجاء املاء جميع اللغات حتى لا يحدث مشكلة فيما بعد (title)', setting('title'));
            return back()->withInput();
        }
        /* =============== check languages =============== */

        if (request()->image != '') {
            $data['image'] = upload_image('contract_advantages', request()->image, 1, 'image', $contract_advantage->image);
        }

        $contract_advantage->update($data);

        /* =============== languages =============== */
        if (count(request()->titles) > 0) {
            ContractAdvantageTran::where([['contract_advantage_id', '=', $contract_advantage->id]])->delete();
            foreach (request()->titles as $lang => $title) {
                ContractAdvantageTran::create([
                    'contract_advantage_id' => request()->contract_advantage_id,
                    'title' => $title,
                    'text' => request()->texts[$lang] ? request()->texts[$lang] : null,
                    'lang' => $lang,
                ]);
            }
        }
        /* =============== languages =============== */

        toastr()->success('تم العملية بنجاح', setting('title'));
        return redirect(aurl('contract_advantage'));

    }

//========================================= destroy ========================================
    public function delete_contract_advantage()
    {
        $id = \request()->get('id');
        $contract_advantage = ContractAdvantage::findorfail($id);
        isset($contract_advantage->image) ? delete_image($contract_advantage->image) : '';
        $contract_advantage->delete();
        return response()->json(['id' => $id], 200);
    }


//======================================= add_details ======================================
    public function contract_advantage_add_details($id)
    {
        $contract_advantage = ContractAdvantage::findorfail($id);
        $contract_advantage_details = ContractAdvantageDetail::where('contract_advantage_id', $id)->orderBy('id', 'desc')->get();
        $language_allows = LanguageAllow::get();
        $title = ' اضافة تفاصيل ( ' . $contract_advantage->title . ' ) ';
        $this->page_titles['add_details'] = $title;
        $view_languages = $this->view_language($contract_advantage->contract_advantage_trans);


        return view('admin.contract_advantages.add_details', ['contract_advantage' => $contract_advantage, 'contract_advantage_details' => $contract_advantage_details, 'language_allows' => $language_allows, 'view_languages' => $view_languages, 'page_titles' => $this->page_titles]);
    }


//======================================= add_details ======================================
//======================================= add_details ======================================
//======================================= add_details ======================================
//======================================= add_details ======================================
//======================================= add_details ======================================
//======================================= add_details ======================================
//======================================= add_details ======================================

    public function add_contract_advantage_row()
    {
        $validator = Validator::make(request()->all(), [
            'contract_advantage_id' => ['required', "numeric", Rule::exists('contract_advantages', 'id')],
            'icon' => 'image',
        ], [], []
        );

        if ($validator->fails()) {
            $errors = collect($validator->errors())->flatten(1);
            foreach ($errors as $error) {
                toastr()->warning($error, 'انتبه');
            }
            return back()->withInput();
        }


        $data = $validator->validated();

        if (request()->icon != '') {
            $data['icon'] = upload_image('contract_details', request()->icon, 1, 'icon', '');
        }


        $contract_detail = ContractAdvantageDetail::create($data);


        try {
            if (count(request()->titles) > 0) {
                foreach (request()->titles as $lang => $title) {
                    ContractAdvantageDetailTran::create([
                        'contract_detail_id' => $contract_detail->id,
                        'title' => $title,
                        'text' => isset(request()->texts[$lang]) ? request()->texts[$lang] : null,
                        'lang' => $lang,
                    ]);
                }
            }
        } catch (\Exception $e) {
            toastr()->error($e->getMessage(), setting('error'));
        }


        toastr()->success('تم العملية بنجاح', setting('title'));
        return back();
    }


//========================================= update =========================================

    public function editContractAdvantageDetail($id)
    {
        $contract_detail = ContractAdvantageDetail::find($id);
        $language_allows = LanguageAllow::get();
        $title = ' تعديل تفاصيل ( ' . $contract_detail->title . ' ) ';
        $this->page_titles['add_details'] = $title;
        $view_languages = $this->view_language($contract_detail->contract_advantage_detail_trans);

        return view('admin.contract_advantages.components.edit_row', ['contract_advantage' => $contract_detail, 'contract_detail' => $contract_detail, 'language_allows' => $language_allows, 'view_languages' => $view_languages, 'page_titles' => $this->page_titles]);
    }//end of editContractAdvantageDetail function

    public function updateContractAdvantageDetail($id)
    {
        $contract_detail = ContractAdvantageDetail::find($id);


        $validator = Validator::make(request()->all(), [

            'icon' => 'image',
        ], [], []
        );

        if ($validator->fails()) {
            $errors = collect($validator->errors())->flatten(1);
            foreach ($errors as $error) {
                toastr()->warning($error, 'انتبه');
            }
            return back()->withInput();
        }


        $data = $validator->validated();
        if (request()->icon != '') {
            $data['icon'] = upload_image('contract_details', request()->icon, 1, 'icon', $contract_detail->icon);

        }
        $contract_detail->update($data);


        try {
            if (count(request()->titles) > 0) {
                foreach (request()->titles as $lang => $title) {
                    ContractAdvantageDetailTran::create([
                        'contract_detail_id' => $contract_detail->id,
                        'title' => $title,
                        'text' => isset(request()->texts[$lang]) ? request()->texts[$lang] : null,
                        'lang' => $lang,
                    ]);
                }
            }
        } catch (\Exception $e) {
            toastr()->error($e->getMessage(), setting('error'));
        }


        toastr()->success('تم العملية بنجاح', setting('title'));


        return redirect(aurl('contract_advantage/add_details/' . $contract_detail->contract_advantage_id));


    }//end of updateContractAdvantageDetail function


//========================================= destroy =========================================
    public function delete_contract_advantage_details()
    {
        $id = \request()->get('id');
        $contract_advantage_detail = ContractAdvantageDetail::findorfail($id);
        isset($contract_advantage_detail->image) ? delete_image($contract_advantage_detail->image) : '';
        $contract_advantage_detail->delete();
        return response()->json(['id' => $id], 200);
    }


}
