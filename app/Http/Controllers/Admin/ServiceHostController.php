<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Traits\CheckAllowedLanguageKeys;
use App\Http\Traits\ViewLanguage;
use App\Models\LanguageAllow;
use App\Models\ServiceHost;
use App\Models\ServiceHostTran;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ServiceHostController extends Controller
{
    use CheckAllowedLanguageKeys, ViewLanguage;

    public $page_titles = [
        'index' => 'الباقات',
        'create' => 'اضافة باقه جديدة',
        'edit' => '',
    ];

    public function index()
    {

        $service_hosts = ServiceHost::orderBy('id', 'asc')->get();


        return view('admin.service_hosts.index', ['page_titles' => $this->page_titles, 'service_hosts' => $service_hosts]);

    }//end of index function

    public function create()
    {
        $language_allows = LanguageAllow::get();
        return view('admin.service_hosts.create', ['page_titles' => $this->page_titles, 'language_allows' => $language_allows]);

    }//end of create function

    public function store(Request $request)
    {


        $validator = Validator::make(request()->all(),
            [
                'price' => 'required',
                //  'host_type' => 'required',
                'service_id' => 'required',
            ], [], []
        );

        if ($validator->fails()) {
            $errors = collect($validator->errors())->flatten(1);
            foreach ($errors as $error) {
                toastr()->warning($error, trans('admin.Message_title_attention'));
            }
            return back()->withInput();
        }

        $data = $validator->validated();


        /* =============== check languages =============== */
        if ((!is_array(request()->titles) || !$this->check_allowed_language_keys(request()->titles)) ||
            (!is_array(request()->texts) || !$this->check_allowed_language_keys(request()->texts))) {
            toastr()->warning('رجاء املاء جميع اللغات حتى لا يحدث مشكلة فيما بعد (title)', setting('title'));
            return back()->withInput();
        }
        /* =============== check languages =============== */


        $service = ServiceHost::create($data);


        /* =============== languages =============== */
        if (count(request()->titles) > 0) {
            foreach (request()->titles as $lang => $title) {

                ServiceHostTran::create([
                    'service_host_id' => $service->id,
                    'title' => $title,
                    'text' => request()->texts[$lang] ? request()->texts[$lang] : null,
                    'lang' => $lang,
                ]);
            }
        }

        /* =============== languages =============== */

        toastr()->success('تم العملية بنجاح', setting('title'));
        return redirect(aurl('service/' . $request->service_id));

    }//end of edit function

    public function edit($id)
    {
        $service_host = ServiceHost::findOrFail($id);

        $service_texts = ServiceHostTran::where([['service_host_id', '=', $id]])->get();
        $language_allows = LanguageAllow::get();
        $title = ' تعديل بيانات ' . $service_host->title;
        $this->page_titles['edit'] = $title;
        $view_languages = $this->view_language($service_host->service_host_trans);


        return view('admin.service_hosts.edit', ['service_host' => $service_host, 'service_texts' => $service_texts, 'language_allows' => $language_allows, 'view_languages' => $view_languages, 'page_titles', $this->page_titles]);


    }//end of edit function

    public function update(Request $request, $id)
    {
        $service_host = ServiceHost::findOrFail($id);
        $validator = Validator::make(request()->all(),
            [
                'price' => 'required',
                //'host_type' => 'required',

            ], [], []
        );

        if ($validator->fails()) {
            $errors = collect($validator->errors())->flatten(1);
            foreach ($errors as $error) {
                toastr()->warning($error, trans('admin.Message_title_attention'));
            }
            return back()->withInput();
        }

        $data = $validator->validated();

        if ((!is_array(request()->titles) || !$this->check_allowed_language_keys(request()->titles)) ||
            (!is_array(request()->texts) || !$this->check_allowed_language_keys(request()->texts))) {
            toastr()->warning('رجاء املاء جميع اللغات حتى لا يحدث مشكلة فيما بعد (title)', setting('title'));
            return back()->withInput();
        }
        $service_host->update($data);


        if (count(request()->titles) > 0) {
            ServiceHostTran::where([['service_host_id', '=', $service_host->id]])->delete();
            foreach (request()->titles as $lang => $title) {
                ServiceHostTran::create([
                    'service_host_id' => $service_host->id,
                    'title' => $title,
                    'text' => request()->texts[$lang] ? request()->texts[$lang] : null,
                    'lang' => $lang,
                ]);
            }
        }
        /* =============== languages =============== */

        toastr()->success('تم العملية بنجاح', setting('title'));
        return redirect(aurl('service_hosts'));


    }//end of update function

    public function destroy($id)
    {


    }//end of destroy function

    public function deleteServiceHost()
    {
        $id = \request()->get('id');
        $service = ServiceHost::findorfail($id);
        $service->delete();
        return response()->json(['id' => $id], 200);


    }//end of delete_service_host_detail function
}
