<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Program;
use App\Models\User;
use App\Models\UserProgramChat;
use App\Models\UserProgramRoom;
use App\Models\UserProgramTask;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;


//models
use App\Models\UserProgram;

/* traits */

use App\Http\Traits\CheckAllowedLanguageKeys;
use App\Http\Traits\ViewLanguage;
use Illuminate\Validation\Rule;

class UserProgramController extends Controller
{

    use CheckAllowedLanguageKeys, ViewLanguage;


    public $page_titles = [
        'index' => 'تطبيقات المستخدم',
        'create' => ' اضافة تطبيقات جديدة ',
        'edit' => '',
    ];

//========================================= index =========================================
    public function index()
    {
        //
    }

//========================================= create =========================================
    public function create()
    {
        $user_id = \request()->get('user_id');
        $user = User::findorfail($user_id);
        $user_program_ids = UserProgram::with(['program'])->where([['user_id', '=', $user_id]])->pluck('program_id')->toArray();
        $programs = Program::whereNotIn('id', $user_program_ids)->get();
        return view('admin.users.create_user_program', ['page_titles' => $this->page_titles, 'programs' => $programs]);
    }

//========================================= store =========================================
    public function store()
    {
        $validator = Validator::make(request()->all(),
            [
                'user_id' => 'required|numeric|exists:users,id',
                'programs' => 'required|array|min:1',
                'programs.*' => 'required|numeric|exists:programs,id',
            ], [], []
        );

        if ($validator->fails()) {
            $errors = collect($validator->errors())->flatten(1);
            foreach ($errors as $error) {
                return back()->withInput()->with('fail-message', trans('web_lang.some thing wrong'));
            }
            return back()->withInput();
        }

        $data = $validator->validated();

        $user_id = request()->get('user_id');

        if (count(request()->programs) > 0) {
            foreach (request()->programs as $program) {
                $UserProgram = UserProgram::create([
                    'program_id' => $program,
                    'user_id' => $user_id,
                ]);

                UserProgramRoom::create([
                    'user_program_id' => $UserProgram->id,
                ]);
            }
        }

        toastr()->success('تم العملية بنجاح', setting('title'));
        return redirect(aurl('user' . '/' . $user_id));
    }

//========================================= show =========================================
    public function show($id)
    {
        $user_program = UserProgram::findorfail($id);
        $user = User::findorfail($user_program->user_id);
        $user_program_tasks = UserProgramTask::where([['user_program_id', '=', $user_program->id]])->get();
        $create = ' اضافة مهام جديدة ';
        $title = ' مهام تطبيق ' . $user_program->title;
        $this->page_titles['show'] = $title;
        $this->page_titles['create'] = $create;
        return view('admin.users.show_user_program_tasks', ['user_program' => $user_program, 'user' => $user, 'user_program_tasks' => $user_program_tasks, 'page_titles' => $this->page_titles]);
    }

//========================================= edit =========================================
    public function edit($id)
    {
        //
    }

//========================================= update =========================================
    public function update_user_program()
    {
        $validator = Validator::make(request()->all(),
            [
                'user_program_id' => ['required', "numeric", Rule::exists('phone_codes', 'id')],
                'tasks' => 'required|array|min:1',
                'tasks.*' => 'required',
                'status' => 'nullable|array',
                'status.*' => 'nullable|in:check,uncheck',
                'link' => 'nullable',
            ], [], []
        );

        if ($validator->fails()) {
            $errors = collect($validator->errors())->flatten(1);
            foreach ($errors as $error) {
                toastr()->warning($error, trans('admin.Message_title_attention'));
            }
            return back()->withInput();
        }

        $data = collect($validator->validated())->only(['tasks', 'status'])->toArray();

        if (count($data['tasks']) > 0) {
            UserProgramTask::where([['user_program_id', '=', request()->user_program_id]])->delete();
            foreach ($data['tasks'] as $key => $task) {
                UserProgramTask::create([
                    'user_program_id' => request()->user_program_id,
                    'task' => $task,
                    'status' => (isset($data['status'][$key]) ? $data['status'][$key] : 'uncheck'),
                ]);
            }
        }

        $user_program = UserProgram::findorfail(request()->user_program_id);

        $check_tasks = UserProgramTask::where([
            ['user_program_id', '=', request()->user_program_id],
            ['status', '=', 'check'],
        ])->get();

        $tasks = UserProgramTask::where([['user_program_id', '=', request()->user_program_id]])->get();

        $user_program->update([
            'link' => request()->link,
            'progress_bar' => round(($check_tasks->count() / ($tasks->count() > 0 ? $tasks->count() : 1)) * 100, 2),
        ]);

        toastr()->success('تم العملية بنجاح', setting('title'));
        return back();

    }

//========================================= destroy =========================================
    public function delete_user_program()
    {
        $id = \request()->get('id');
        $user_program = UserProgram::findorfail($id);
        isset($user_program->image) ? delete_image($user_program->image) : '';
        $user_program->delete();
        return response()->json(['id' => $id], 200);
    }

//========================================= destroy =========================================
    public function update_user_program_status()
    {
        $id = \request()->get('id');
        $user_program = UserProgram::findorfail($id);
        $user_program->update([
            'status' => 'new'
        ]);
        return response()->json(['id' => $id], 200);
    }


    /* ==================================================================== */
    public function chat()
    {
        $user_program_id = request()->get('user_program_id');
        $user_program_room = UserProgramRoom::with(['user_program_chats', 'user_program_chats.user'])->where('user_program_id', $user_program_id)->firstOrFail();
//        return $user_program_room;
        return view('admin.users.chat', ['user_program_room' => $user_program_room]);
    }
    /* ==================================================================== */

// =====================================================
    public function msg_send(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'user_program_room_id' => 'required|numeric|exists:user_program_rooms,id',
            'user_type' => 'required|in:admin',
            'message' => 'required',
        ], []);

        if ($validator->fails()) {
            $code = 422;
            return response()->json(['error' => collect($validator->errors())->flatten(1)], $code);
        }
        $data = $validator->validated();
        $RoomMsg = UserProgramChat::create($data);
        return response()->json(['data' => $RoomMsg, 'msg' => $request->message], 200);
    }
// =====================================================

}
