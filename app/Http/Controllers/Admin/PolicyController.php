<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Traits\CheckAllowedLanguageKeys;
use App\Http\Traits\ViewLanguage;
use App\Models\LanguageAllow;
use App\Models\Policies;
use App\Models\PoliciesTranslation;
use Illuminate\Http\Request;

class PolicyController extends Controller
{

    use CheckAllowedLanguageKeys, ViewLanguage;


    public $page_titles = [
        'index' => 'سياسه الاستخدام',
        'create' => 'سياسه الاستخدام',
        'edit' => '',
    ];

    public function index()
    {

        $policy = Policies::first();


        if ($policy) {

            $language_allows = LanguageAllow::get();
            $title = ' تعديل بيانات ' . $policy->title;
            $this->page_titles['edit'] = $title;
            $view_languages = $this->view_language($policy->policies_trans);


            return view('admin.policy.index', ['page_titles' => $this->page_titles, 'policy' => $policy, 'language_allows' => $language_allows, 'view_languages' => $view_languages]);
        } else {
            $language_allows = LanguageAllow::get();
            $title = ' تعديل بيانات ';
            $this->page_titles['edit'] = $title;

            return view('admin.policy.index', ['page_titles' => $this->page_titles, 'language_allows' => $language_allows, 'policy' => $policy]);
        }

    }//end of index function


    public function store(Request $request)
    {

        try {
            /* =============== check languages =============== */
            if ((!is_array(request()->titles) || !$this->check_allowed_language_keys(request()->titles)) ||
                (!is_array(request()->texts) || !$this->check_allowed_language_keys(request()->texts))) {
                toastr()->warning('رجاء املاء جميع اللغات حتى لا يحدث مشكلة فيما بعد (title)', setting('title'));
                return back()->withInput();
            }


            $policy = Policies::first();


            $data = $request->only('image');
            if (!$policy) {
                if (request()->image != '') {
                    $data['image'] = upload_image('polices', request()->image, 1, 'image', '');
                }

                $policy = Policies::create($data);
                if (count(request()->titles) > 0) {
                    foreach (request()->titles as $lang => $title) {

                        PoliciesTranslation::create([
                            'policy_id' => $policy->id,
                            'title' => $title,
                            'text' => request()->texts[$lang] ? request()->texts[$lang] : null,
                            'lang' => $lang,
                        ]);
                    }
                }
            } else {
                if (request()->image != '') {
                    $data['image'] = upload_image('polices', request()->image, 1, 'image', $policy->image);
                }
                $policy->update($data);


                if ((!is_array(request()->titles) || !$this->check_allowed_language_keys(request()->titles)) ||
                    (!is_array(request()->texts) || !$this->check_allowed_language_keys(request()->texts))) {
                    toastr()->warning('رجاء املاء جميع اللغات حتى لا يحدث مشكلة فيما بعد (title)', setting('title'));
                    return back()->withInput();
                }

                if (count(request()->titles) > 0) {
                    PoliciesTranslation::where([['policy_id', '=', $policy->id]])->delete();
                    foreach (request()->titles as $lang => $title) {
                        PoliciesTranslation::create([
                            'policy_id' => $policy->id,
                            'title' => $title,
                            'text' => request()->texts[$lang] ? request()->texts[$lang] : null,
                            'lang' => $lang,
                        ]);
                    }
                }

            }

        } catch (\Exception $e) {
            toastr()->success($e->getMessage(), 'title');

            return redirect()->back();
        }

        toastr()->success('تم العملية بنجاح', setting('title'));
        return redirect(aurl('polices'));

    }//end of store function

    public function edit($id)
    {


    }//end of edit function


    public function update(Request $request)
    {

        dd($request->all());

    }//end of edit function
}
