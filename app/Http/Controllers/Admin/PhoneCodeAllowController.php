<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Traits\CheckAllowedLanguageKeys;
use App\Http\Traits\ViewLanguage;
use App\Models\PhoneCode;
use App\Models\PhoneCodeAllow;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

//models

/* traits */

class PhoneCodeAllowController extends Controller
{

    use CheckAllowedLanguageKeys, ViewLanguage;

    public $page_titles = [
        'index' => 'الدول',
        'create' => 'اضافة دولة جديد',
        'edit' => '',
    ];

//========================================= index ======================================
    public function index()
    {
        $phone_code_allows = PhoneCodeAllow::orderBy('id', 'asc')->get();
        return view('admin.phone_code_allows.index', ['phone_code_allows' => $phone_code_allows, 'page_titles' => $this->page_titles]);
    }

//========================================= create ======================================
    public function create()
    {

        $phone_code_allows = PhoneCodeAllow::pluck('code')->toArray();
        $phone_codes = PhoneCode::whereNotIn('code', $phone_code_allows)->get();


        return view('admin.phone_code_allows.create', ['page_titles' => $this->page_titles, 'phone_codes' => $phone_codes]);
    }

//========================================= store ========================================
    public function store()
    {

        $validator = Validator::make(request()->all(),
            [
                'phone_code_id' => ['required', "numeric", Rule::exists('phone_codes', 'id')],
                'icon' => 'nullable|image',
            ], [], []
        );

        if ($validator->fails()) {
            $errors = collect($validator->errors())->flatten(1);
            foreach ($errors as $error) {
                toastr()->warning($error, trans('admin.Message_title_attention'));
            }
            return back()->withInput();
        }

        $data = $validator->validated();

        if (request()->icon != '') {
            $data['icon'] = upload_image('phone_code_allows', request()->icon, 1, 'icon', '');
        }

        $phone_code = PhoneCode::findorfail(request()->phone_code_id);

        $data['phone_code_id'] = request()->phone_code_id;
        $data['name'] = $phone_code->name;
        $data['dial_code'] = $phone_code->dial_code;
        $data['code'] = $phone_code->code;

        PhoneCodeAllow::create($data);

        toastr()->success('تم العملية بنجاح', setting('title'));
        return redirect(aurl('phone_code_allow'));
    }

//========================================= show =========================================
    public function show($id)
    {
        //
    }

//========================================= edit =========================================
    public function edit($id)
    {
        $phone_code_allow = PhoneCodeAllow::findorfail($id);
        $title = ' تعديل بيانات ' . $phone_code_allow->name;
        $this->page_titles['edit'] = $title;
        return view('admin.phone_code_allows.edit', ['phone_code_allow' => $phone_code_allow, 'page_titles' => $this->page_titles]);
    }

//========================================= update =======================================
    public function update_phone_code_allow()
    {
        $validator = Validator::make(request()->all(),
            [
                'phone_code_allow_id' => ['required', "numeric", Rule::exists('phone_code_allows', 'id')],
                'icon' => 'nullable|image',
            ], [], []
        );

        if ($validator->fails()) {
            $errors = collect($validator->errors())->flatten(1);
            foreach ($errors as $error) {
                toastr()->warning($error, trans('admin.Message_title_attention'));
            }
            return back()->withInput();
        }

        $data = collect($validator->validated())->except(['phone_code_allow_id'])->toArray();

        $phone_code_allow = PhoneCodeAllow::findorfail(request()->phone_code_allow_id);

        if (request()->icon != '') {
            $data['icon'] = upload_image('phone_code_allows', request()->icon, 1, 'image', $phone_code_allow->icon);
        }

        $phone_code_allow->update($data);

        toastr()->success('تم العملية بنجاح', setting('title'));
        return redirect(aurl('phone_code_allow'));

    }

//========================================= destroy ======================================
    public function delete_phone_code_allow()
    {
        $id = \request()->get('id');
        $phone_code_allow = PhoneCodeAllow::findorfail($id);
        isset($phone_code_allow->image) ? delete_image($phone_code_allow->image) : '';
        $phone_code_allow->delete();
        return response()->json(['id' => $id], 200);
    }


}
