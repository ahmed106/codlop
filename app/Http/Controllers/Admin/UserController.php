<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Traits\CheckAllowedLanguageKeys;
use App\Http\Traits\ViewLanguage;
use App\Models\User;
use App\Models\UserProgram;
use Illuminate\Support\Facades\Validator;


//models

/* traits */

class UserController extends Controller
{

    use CheckAllowedLanguageKeys, ViewLanguage;


    public $page_titles = [
        'index' => 'العملاء',
        'create' => 'اضافة عميل جديد',
        'edit' => '',
        'show' => '',
    ];

//========================================= index =========================================
    public function index()
    {

        $users = User::orderBy('id', 'desc')->get();
        return view('admin.users.index', ['users' => $users, 'page_titles' => $this->page_titles]);
    }

//========================================= create =========================================
    public function create()
    {
        return view('admin.users.create', ['page_titles' => $this->page_titles]);
    }

//========================================= store =========================================
    public function store()
    {
        $validator = Validator::make(request()->all(),
            [
                'image' => 'nullable|image',
            ], [], []
        );

        if ($validator->fails()) {
            $errors = collect($validator->errors())->flatten(1);
            foreach ($errors as $error) {
                toastr()->warning($error, trans('admin.Message_title_attention'));
            }
            return back()->withInput();
        }

        $data = $validator->validated();


        if (request()->image != '') {
            $data['image'] = upload_image('users', request()->image, 1, 'image', '');
        }

        $user = User::create($data);

        toastr()->success('تم العملية بنجاح', setting('title'));
        return redirect(aurl('user'));
    }

//========================================= show =========================================
    public function show($id)
    {
        $user = User::findorfail($id);
        $user_programs = UserProgram::with(['program'])->where([['user_id', '=', $user->id]])->get();
        $create = ' اضافة خدمات جديدة ';
        $title = ' خدمات ' . $user->name;
        $this->page_titles['show'] = $title;
        $this->page_titles['create'] = $create;

        $data['is_read'] = 'read';

        $user->update([
            'is_read' => 'read'
        ]);

        return view('admin.users.show', ['user' => $user, 'user_programs' => $user_programs, 'page_titles' => $this->page_titles]);
    }

//========================================= edit =========================================
    public function edit($id)
    {
        $user = User::findorfail($id);
        $title = ' تعديل بيانات ' . $user->title;
        $this->page_titles['edit'] = $title;
        return view('admin.users.edit', ['user' => $user, 'page_titles' => $this->page_titles]);
    }

//========================================= update =========================================
    public function update_user()
    {
        $validator = Validator::make(request()->all(),
            [
                'image' => 'nullable|image',
            ], [], []
        );

        if ($validator->fails()) {
            $errors = collect($validator->errors())->flatten(1);
            foreach ($errors as $error) {
                toastr()->warning($error, trans('admin.Message_title_attention'));
            }
            return back()->withInput();
        }

        $data = $validator->validated();

        $user = User::findorfail(request()->user_id);

        if (request()->image != '') {
            $data['image'] = upload_image('users', request()->image, 1, 'image', $user->image);
        }

        $user->update($data);

        toastr()->success('تم العملية بنجاح', setting('title'));
        return redirect(aurl('user'));

    }

//========================================= destroy =========================================
    public function delete_user()
    {
        $id = \request()->get('id');
        $user = User::findorfail($id);
        isset($user->image) ? delete_image($user->image) : '';
        $user->delete();
        return response()->json(['id' => $id], 200);
    }


}
