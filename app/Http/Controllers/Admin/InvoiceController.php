<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Invoice;
use Illuminate\Http\Request;
use SimpleSoftwareIO\QrCode\Facades\QrCode;

class InvoiceController extends Controller
{
    public $page_titles = [
        'index' => 'الفواتير',
        'create' => 'إضافه فاتوره جديده',
        'edit' => '',
    ];

    public function index()
    {
        $invoices = Invoice::all();
        return view('admin.invoices.index', ['page_titles' => $this->page_titles, 'invoices' => $invoices]);
    }


    public function create()
    {

        return view('admin.invoices.create', ['page_titles' => $this->page_titles]);
    }


    public function store(Request $request)
    {
        $data = $request->validate([
            'customer_name' => 'required|string',
            'phone' => 'required|integer',
            'address' => 'required|string',
            'service_details' => 'required|string',
            'total' => 'required|integer',
            'tax_amount' => 'required|integer',
            'total_tax' => 'required|integer'
        ]);


        $data['tax_number'] = 300812355200003;
        $data['invoice_number'] = $this->getInvoiceNumber();
        $data['company_name'] = 'Code loop';


        $sellr_name_hexa = $this->generateStringToHexadecimal('01', strlen('مؤسسة كود لوب لتقنية المعلومات'), 'مؤسسة كود لوب لتقنية المعلومات');
        $vat_number_hexa = $this->generateStringToHexadecimal('02', strlen($data['tax_number']), $data['tax_number']);
        $time_hexa = $this->generateStringToHexadecimal('03', strlen(now()->toDateTimeLocalString()), now()->toDateTimeLocalString());
        $total_hexa = $this->generateStringToHexadecimal('04', strlen($data['total_tax']), $data['total_tax']);
        $vat_hexa = $this->generateStringToHexadecimal('05', strlen($data['tax_amount']), $data['tax_amount']);


        $hex = $sellr_name_hexa . $vat_number_hexa . $time_hexa . $total_hexa . $vat_hexa;
        $ascii = pack("H*", $hex);


        $hex = base64_encode($ascii);

        $qr = QrCode::size(100)->generate($hex);
        $data['qr'] = $qr;
        try {
            Invoice::create($data);
        } catch (\Exception $e) {

            return $e->getMessage();
        }

        return redirect()->route('invoices.index')->with('success', 'تم إضافه البيانات بنجاح');
    }

    public function generateStringToHexadecimal($tag, $length, $value)
    {


        $value = bin2hex($value);
        $length_count = \Str::length(dechex($length)) == 1 ? '0' . dechex($length) : dechex($length);

        return $tag . $length_count . $value;

    }//end of generate function


    public function show(Invoice $invoice)
    {
        return view('admin.invoices.show', compact('invoice'));

    }


    public function edit($id)
    {
        //
    }


    public function update(Request $request, $id)
    {
        //
    }


    public function destroy($id)
    {
        //
    }

    public function getInvoiceNumber()
    {
        $invoice = Invoice::latest()->first();
        if ($invoice) {
            return $invoice->invoice_number + 1;
        }
        return 1;

    }//end of getInvoiceNumber function
}
