<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Admin;
use SimpleSoftwareIO\QrCode\Facades\QrCode;

class TestQrCodeController extends Controller
{
    public function index()
    {

        if (request()->ajax()) {

            $credential = explode(',', base64_decode(str_replace('?ep=', '', request()->search)));

            if (\Auth::guard('admin')->attempt(['email' => $credential[0], 'password' => $credential[1]])) {
                $user = Admin::whereEmail($credential[0])->first();
                \Auth::login($user);
                return response()->json(['success', 'login successfully'], 200);
            } else {
                return response()->json(['error', 'credential Error '], 401);
            }
        }

        $email = \Auth::guard('admin')->user()->email;
        $password = '123456';
        $credential = [
            'email' => $email,
            'password' => $password,
        ];
        $credential = base64_encode(implode(',', $credential));
        $link = url('/power?ep=' . $credential);
        QrCode::format('svg')->size(100)->generate($link, 'images/qr.svg');
        $qr = QrCode::size(100)->generate($link);


        return view('admin.test_qr', compact('qr', 'credential'));

    }//end of index function
}
