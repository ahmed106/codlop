<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Traits\CheckAllowedLanguageKeys;
use App\Http\Traits\ViewLanguage;
use App\Models\ConsiderationsAndRoles;
use App\Models\ConsiderationsAndRolesTranslation;
use App\Models\LanguageAllow;
use Illuminate\Http\Request;

class RoleConditionController extends Controller
{
    use CheckAllowedLanguageKeys, ViewLanguage;


    public $page_titles = [
        'index' => 'الشروط و الاحكام',
        'create' => 'الشروط و الاحكام',
        'edit' => '',
    ];

    public function index()
    {


        $consideration = ConsiderationsAndRoles::first();
        if ($consideration) {
            $language_allows = LanguageAllow::get();
            $title = ' تعديل بيانات ' . $consideration->title;
            $this->page_titles['edit'] = $title;
            $view_languages = $this->view_language($consideration->considerations_and_roles_trans);

            return view('admin.roles_and_conditions.index', ['page_titles' => $this->page_titles, 'consideration' => $consideration, 'language_allows' => $language_allows, 'view_languages' => $view_languages]);
        } else {

            $language_allows = LanguageAllow::get();
            $title = ' تعديل بيانات ';
            $this->page_titles['edit'] = $title;
            return view('admin.roles_and_conditions.index', ['page_titles' => $this->page_titles, 'language_allows' => $language_allows, 'consideration' => $consideration]);
        }


    }//end of index function


    public function store(Request $request)
    {

        try {
            /* =============== check languages =============== */
            if ((!is_array(request()->titles) || !$this->check_allowed_language_keys(request()->titles)) ||
                (!is_array(request()->texts) || !$this->check_allowed_language_keys(request()->texts))) {
                toastr()->warning('رجاء املاء جميع اللغات حتى لا يحدث مشكلة فيما بعد (title)', setting('title'));
                return back()->withInput();
            }


            $consideration = ConsiderationsAndRoles::first();


            $data = $request->only('image');
            if (!$consideration) {
                if (request()->image != '') {
                    $data['image'] = upload_image('considerations', request()->image, 1, 'image', '');
                }

                $consideration = ConsiderationsAndRoles::create($data);
                if (count(request()->titles) > 0) {
                    foreach (request()->titles as $lang => $title) {

                        ConsiderationsAndRolesTranslation::create([
                            'consideration_id' => $consideration->id,
                            'title' => $title,
                            'text' => request()->texts[$lang] ? request()->texts[$lang] : null,
                            'lang' => $lang,
                        ]);
                    }
                }
            } else {
                if (request()->image != '') {
                    $data['image'] = upload_image('considerations', request()->image, 1, 'image', $consideration->image);
                }
                $consideration->update($data);


                if ((!is_array(request()->titles) || !$this->check_allowed_language_keys(request()->titles)) ||
                    (!is_array(request()->texts) || !$this->check_allowed_language_keys(request()->texts))) {
                    toastr()->warning('رجاء املاء جميع اللغات حتى لا يحدث مشكلة فيما بعد (title)', setting('title'));
                    return back()->withInput();
                }

                if (count(request()->titles) > 0) {
                    ConsiderationsAndRolesTranslation::where([['consideration_id', '=', $consideration->id]])->delete();
                    foreach (request()->titles as $lang => $title) {
                        ConsiderationsAndRolesTranslation::create([
                            'consideration_id' => $consideration->id,
                            'title' => $title,
                            'text' => request()->texts[$lang] ? request()->texts[$lang] : null,
                            'lang' => $lang,
                        ]);
                    }
                }

            }


        } catch (\Exception $e) {

            toastr()->error($e->getMessage(), 'title');

            return redirect()->back();
        }

        toastr()->success('تم العملية بنجاح', setting('title'));
        return redirect(aurl('roles-conditions'));

    }//end of store function
}
