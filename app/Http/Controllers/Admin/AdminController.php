<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\PhoneCode;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

//models
use App\Models\Admin;

/* traits */

use App\Http\Traits\CheckAllowedLanguageKeys;
use App\Http\Traits\ViewLanguage;

class AdminController extends Controller
{

    use CheckAllowedLanguageKeys, ViewLanguage;

    public $page_titles = [
        'index' => 'المشرفين',
        'create' => 'اضافة مشرف جديد',
        'edit' => '',
    ];

//========================================= index ======================================
    public function index()
    {
        $admins = Admin::orderBy('id', 'asc')->get();
        return view('admin.admins.index', ['admins' => $admins, 'page_titles' => $this->page_titles]);
    }

//========================================= create ======================================
    public function create()
    {
        return view('admin.admins.create', ['page_titles' => $this->page_titles]);
    }

//========================================= store ========================================
    public function store()
    {
        $validator = Validator::make(request()->all(),
            [
                'name' => 'required',
                'email' => 'required|email|unique:admins',
                'password' => 'required|min:6',
//                'permission_id' => 'required|numeric|exists:permissions,id',


            ], [], [
                'name' => 'الاسم',
                'email' => 'الايميل',
                'password' => 'الباسورد',
                'password.min' => 'الباسورد يجب ان لا يقل عن 6 احرف',
//                'permission_id' => 'الصلاحية مطلوبة',
            ]
        );

        if ($validator->fails()) {
            $errors_keys = $validator->errors()->keys();
            $errors = collect($validator->errors())->flatten(1);
            foreach ($errors as $error) {
                toastr()->warning($error, trans('admin.Message_title_attention'));
            }
            return back()->with('errors_keys', $errors_keys)->withInput();
        }


        $data = $validator->validated();

        $data['password'] = bcrypt(request('password'));


        if (request()->image != '') {
            $data['image'] = upload_image('admins', request()->image, 1, 'image');
        }

        Admin::create($data);
        toastr()->success('تم العملية بنجاح', setting('title'));
        return redirect(aurl('admin'));
    }

//========================================= show =========================================
    public function show($id)
    {
        //
    }

//========================================= edit =========================================
    public function edit($id)
    {
        $admin = Admin::findorfail($id);
        $title = ' تعديل بيانات ' . $admin->name;
        $this->page_titles['edit'] = $title;
        return view('admin.admins.edit', ['admin' => $admin, 'page_titles' => $this->page_titles]);
    }

//========================================= update =======================================
    public function update_admin()
    {
        $validator = Validator::make(request()->all(),
            [
                'name' => 'required',
                'email' => 'required|email|unique:admins,email,' . request()->admin_id,
//                'permission_id' => 'required|numeric|exists:permissions,id',
            ], [], [
                'name' => trans('admin.name'),
                'email' => trans('admin.email'),
//                'group_id' => 'الصلاحية مطلوبة',
            ]
        );

        if ($validator->fails()) {
            $errors = collect($validator->errors())->flatten(1);
            foreach ($errors as $error) {
                toastr()->warning($error, trans('admin.Message_title_attention'));
            }
            return back()->withInput();
        }


        $data = $validator->validated();

        if (request('password') != null) {
            $data['password'] = bcrypt(request('password'));
        }

        if (request()->image != '') {
            $data['image'] = upload_image('admins', request()->image, 1, 'image');
        }


        Admin::where('id', request()->admin_id)->update($data);

        toastr()->success('تم العملية بنجاح', setting('title'));
        return redirect(aurl('admin'));

    }








//========================================= destroy ======================================
    public function delete_admin()
    {
        $id = \request()->get('id');
        $admin = Admin::findorfail($id);
        isset($admin->image) ? delete_image($admin->image) : '';
        $admin->delete();
        return response()->json(['id' => $id], 200);
    }


}
