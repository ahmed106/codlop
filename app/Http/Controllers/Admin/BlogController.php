<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use App\Models\LanguageAllow;
use App\Models\BlogTran;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

//models
use App\Models\Blog;

/* traits */
use App\Http\Traits\CheckAllowedLanguageKeys;
use App\Http\Traits\ViewLanguage;

class BlogController extends Controller
{

    use CheckAllowedLanguageKeys, ViewLanguage;


    public $page_titles = [
        'index' => 'المدونات',
        'create' => 'اضافة مدونه جديد',
        'edit' => '',
    ];

//========================================= index =========================================
    public function index()
    {
        $blogs = Blog::orderBy('id', 'desc')->get();
        return view('admin.blogs.index', ['blogs' => $blogs, 'page_titles' => $this->page_titles]);
    }

//========================================= create =========================================
    public function create()
    {
        $language_allows = LanguageAllow::get();
        return view('admin.blogs.create', ['page_titles' => $this->page_titles, 'language_allows' => $language_allows]);
    }

//========================================= store =========================================
    public function store()
    {
        $validator = Validator::make(request()->all(),
            [
                'date' => 'required|date',
                'image' => 'nullable|image',
            ], [], []
        );

        if ($validator->fails()) {
            $errors = collect($validator->errors())->flatten(1);
            foreach ($errors as $error) {
                toastr()->warning($error, trans('admin.Message_title_attention'));
            }
            return back()->withInput();
        }

        $data = $validator->validated();

        /* =============== check languages =============== */
        if ((!is_array(request()->titles) || !$this->check_allowed_language_keys(request()->titles)) ||
            (!is_array(request()->texts) || !$this->check_allowed_language_keys(request()->texts))) {
            toastr()->warning('رجاء املاء جميع اللغات حتى لا يحدث مشكلة فيما بعد (title)', setting('title'));
            return back()->withInput();
        }
        /* =============== check languages =============== */

        if (request()->image != '') {
            $data['image'] = upload_image('blogs', request()->image, 1, 'image', '');
        }

        $blog = Blog::create($data);

        /* =============== languages =============== */
        if (count(request()->titles) > 0) {
            foreach (request()->titles as $lang => $title) {
                BlogTran::create([
                    'blog_id' => $blog->id,
                    'title' => $title,
                    'text' => request()->texts[$lang] ? request()->texts[$lang] : null,
                    'lang' => $lang,
                ]);
            }
        }
        /* =============== languages =============== */

        toastr()->success('تم العملية بنجاح', setting('title'));
        return redirect(aurl('blog'));
    }

//========================================= show =========================================
    public function show($id)
    {
        //
    }

//========================================= edit =========================================
    public function edit($id)
    {
        $blog = Blog::findorfail($id);
        $language_allows = LanguageAllow::get();
        $title = ' تعديل بيانات ' . $blog->title;
        $this->page_titles['edit'] = $title;
        $view_languages = $this->view_language($blog->blog_trans);
        return view('admin.blogs.edit', ['blog' => $blog, 'language_allows' => $language_allows, 'view_languages' => $view_languages, 'page_titles' => $this->page_titles]);
    }

//========================================= update =========================================
    public function update_blog()
    {
        $validator = Validator::make(request()->all(),
            [
                'date' => 'required|date',
                'image' => 'nullable|image',
            ], [], []
        );

        if ($validator->fails()) {
            $errors = collect($validator->errors())->flatten(1);
            foreach ($errors as $error) {
                toastr()->warning($error, trans('admin.Message_title_attention'));
            }
            return back()->withInput();
        }

        $data = $validator->validated();

        $blog = Blog::findorfail(request()->blog_id);

        /* =============== check languages =============== */
        if ((!is_array(request()->titles) || !$this->check_allowed_language_keys(request()->titles)) ||
            (!is_array(request()->texts) || !$this->check_allowed_language_keys(request()->texts))) {
            toastr()->warning('رجاء املاء جميع اللغات حتى لا يحدث مشكلة فيما بعد (title)', setting('title'));
            return back()->withInput();
        }
        /* =============== check languages =============== */

        if (request()->image != '') {
            $data['image'] = upload_image('blogs', request()->image, 1, 'image', $blog->image);
        }

        $blog->update($data);

        /* =============== languages =============== */
        if (count(request()->titles) > 0) {
            BlogTran::where([['blog_id', '=', $blog->id]])->delete();
            foreach (request()->titles as $lang => $title) {
                BlogTran::create([
                    'blog_id' => request()->blog_id,
                    'title' => $title,
                    'text' => request()->texts[$lang] ? request()->texts[$lang] : null,
                    'lang' => $lang,
                ]);
            }
        }
        /* =============== languages =============== */

        toastr()->success('تم العملية بنجاح', setting('title'));
        return redirect(aurl('blog'));

    }

//========================================= destroy =========================================
    public function delete_blog()
    {
        $id = \request()->get('id');
        $blog = Blog::findorfail($id);
        isset($blog->image) ? delete_image($blog->image) : '';
        $blog->delete();
        return response()->json(['id' => $id], 200);
    }

}
