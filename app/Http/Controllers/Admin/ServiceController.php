<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Traits\CheckAllowedLanguageKeys;
use App\Http\Traits\ViewLanguage;
use App\Models\LanguageAllow;
use App\Models\Service;
use App\Models\ServiceHost;
use App\Models\ServiceHostDetail;
use App\Models\ServiceText;
use App\Models\ServiceTran;
use Illuminate\Support\Facades\Validator;

//models

/* traits */

class ServiceController extends Controller
{

    use CheckAllowedLanguageKeys, ViewLanguage;


    public $page_titles = [
        'index' => 'الخدمات',
        'create' => 'اضافة خدمة جديدة',
        'edit' => '',
    ];

//========================================= index =========================================
    public function index()
    {
        $services_ = Service::orderBy('id', 'asc')->get();
        return view('admin.services.index', ['services_' => $services_, 'page_titles' => $this->page_titles]);
    }

//========================================= create =========================================
    public function create()
    {
        $language_allows = LanguageAllow::get();
        return view('admin.services.create', ['page_titles' => $this->page_titles, 'language_allows' => $language_allows]);
    }

//========================================= store =========================================
    public function store()
    {

        $validator = Validator::make(request()->all(),
            [
                'image' => 'nullable|image',
            ], [], []
        );

        if ($validator->fails()) {
            $errors = collect($validator->errors())->flatten(1);
            foreach ($errors as $error) {
                toastr()->warning($error, trans('admin.Message_title_attention'));
            }
            return back()->withInput();
        }

        $data = $validator->validated();

        /* =============== check languages =============== */
        if ((!is_array(request()->titles) || !$this->check_allowed_language_keys(request()->titles)) ||
            (!is_array(request()->texts) || !$this->check_allowed_language_keys(request()->texts))) {
            toastr()->warning('رجاء املاء جميع اللغات حتى لا يحدث مشكلة فيما بعد (title)', setting('title'));
            return back()->withInput();
        }
        /* =============== check languages =============== */

        if (request()->icon != '') {
            $data['icon'] = upload_image('services', request()->icon, 1, 'icon', '');
        }
        if (request()->image != '') {
            $data['image'] = upload_image('services', request()->image, 1, 'image', '');
        }

        $service = Service::create($data);

        /* =============== languages =============== */
        if (count(request()->titles) > 0) {
            foreach (request()->titles as $lang => $title) {
                ServiceTran::create([
                    'service_id' => $service->id,
                    'title' => $title,
                    'text' => request()->texts[$lang] ? request()->texts[$lang] : null,
                    'lang' => $lang,
                ]);
            }
        }
        /* =============== languages =============== */

        toastr()->success('تم العملية بنجاح', setting('title'));
        return redirect(aurl('service'));
    }

//========================================= show =========================================
    public function show($id)
    {
        $service = Service::findorfail($id);
        $title = ' تفاصيل ' . $service->title;
        $this->page_titles['index'] = $title;
        $this->page_titles['create'] = 'اضافة نص خدمة جديد';

        /* service host */
        $service_host = ServiceHost::first();
        $service_hosts = ServiceHost::get();
        $service_host_id = \request()->get('service_host_id');
        $service_host_details = ServiceHostDetail::where('id', '!=', 0);

        if (!isset($service_host_id)) {
            $service_host_details = $service_host_details->where([['service_host_id', '=', (isset($service_host) ? $service_host->id : "#")]]);
        }

        if (isset($service_host_id)) {
            $service_host_details = $service_host_details->where([['service_host_id', '=', $service_host_id]]);
        }

        $service_host_details = $service_host_details->orderBy('service_host_id', 'asc')->get();
        /* service host */

        return view('admin.services.service_pages.' . $service->service_page, ['page_titles' => $this->page_titles, 'service' => $service, 'service_hosts' => $service_hosts, 'service_host' => $service_host, 'service_host_details' => $service_host_details]);
    }

//========================================= edit =========================================
    public function edit($id)
    {


        $service = Service::findorfail($id);
        $service_texts = ServiceText::where([['service_id', '=', $id]])->get();
        $language_allows = LanguageAllow::get();
        $title = ' تعديل بيانات ' . $service->title;
        $this->page_titles['edit'] = $title;
        $view_languages = $this->view_language($service->service_trans);
        return view('admin.services.edit', ['service' => $service, 'service_texts' => $service_texts, 'language_allows' => $language_allows, 'view_languages' => $view_languages, 'page_titles' => $this->page_titles]);
    }

//========================================= update =========================================
    public function update_service()
    {


        $validator = Validator::make(request()->all(),
            [
                'image' => 'nullable|image',
            ], [], []
        );

        if ($validator->fails()) {
            $errors = collect($validator->errors())->flatten(1);
            foreach ($errors as $error) {
                toastr()->warning($error, trans('admin.Message_title_attention'));
            }
            return back()->withInput();
        }

        $data = $validator->validated();

        $service = Service::findorfail(request()->service_id);

        /* =============== check languages =============== */
        if ((!is_array(request()->titles) || !$this->check_allowed_language_keys(request()->titles)) ||
            (!is_array(request()->texts) || !$this->check_allowed_language_keys(request()->texts))) {
            toastr()->warning('رجاء املاء جميع اللغات حتى لا يحدث مشكلة فيما بعد (title)', setting('title'));
            return back()->withInput();
        }
        /* =============== check languages =============== */

        if (request()->icon != '') {
            $data['icon'] = upload_image('services', request()->icon, 1, 'icon', $service->icon);
        }
        if (request()->image != '') {
            $data['image'] = upload_image('services', request()->image, 1, 'image', $service->image);
        }

        $service->update($data);

        /* =============== languages =============== */
        if (count(request()->titles) > 0) {
            ServiceTran::where([['service_id', '=', $service->id]])->delete();
            foreach (request()->titles as $lang => $title) {
                ServiceTran::create([
                    'service_id' => request()->service_id,
                    'title' => $title,
                    'text' => request()->texts[$lang] ? request()->texts[$lang] : null,
                    'lang' => $lang,
                ]);
            }
        }
        /* =============== languages =============== */

        toastr()->success('تم العملية بنجاح', setting('title'));
        return redirect(aurl('service'));

    }

//========================================= destroy =========================================
    public function delete_service()
    {
        $id = \request()->get('id');
        $service = Service::findorfail($id);
        isset($service->image) ? delete_image($service->image) : '';
        isset($service->icon) ? delete_image($service->icon) : '';
        $service->delete();
        return response()->json(['id' => $id], 200);
    }

}
