<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Traits\CheckAllowedLanguageKeys;
use App\Http\Traits\ViewLanguage;
use App\Models\LanguageAllow;
use App\Models\Slider;
use App\Models\SliderTran;
use App\Models\Team;
use App\Models\TeamTranslation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class TeamController extends Controller
{
    use CheckAllowedLanguageKeys, ViewLanguage;


    public $page_titles = [
        'index' => 'فريق العمل',
        'create' => 'اضافة عضو جديد',
        'edit' => '',
    ];

    public function index()
    {
        $teams = Team::orderBy('id','desc')->get();
        return view('admin.teams.index',['teams'=>$teams,'page_titles'=>$this->page_titles]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $language_allows = LanguageAllow::get();
        return  view('admin.teams.create',['page_titles'=>$this->page_titles,'language_allows'=>$language_allows]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make(request()->all(),
            [
                'photo' => 'nullable|image',
                'is_ceo'=>'nullable'


            ], [], []
        );


        if ($validator->fails()) {
            $errors = collect($validator->errors())->flatten(1);
            foreach ($errors as $error) {
                toastr()->warning($error, trans('admin.Message_title_attention'));
            }
            return back()->withInput();
        }

        $data = $validator->validated();


        /* =============== check languages =============== */
        if ((!is_array(request()->name) || !$this->check_allowed_language_keys(request()->name)) ||
            (!is_array(request()->job_title) || !$this->check_allowed_language_keys(request()->job_title))) {
            toastr()->warning('رجاء املاء جميع اللغات حتى لا يحدث مشكلة فيما بعد (title)', setting('title'));
            return back()->withInput();
        }
        /* =============== check languages =============== */

        if (request()->photo != '') {
            $data['photo'] = upload_image('teams', request()->photo, 1, 'image', '');
        }



        $team = Team::create($data);


        /* =============== languages =============== */
        if (count(request()->name) > 0) {
            foreach (request()->name as $lang => $title) {
                TeamTranslation::create([
                    'team_id' => $team->id,
                    'name' => $title,
                    'job_title' => request()->job_title[$lang] ? request()->job_title[$lang] : null,
                    'lang' => $lang,
                ]);
            }
        }

        /* =============== languages =============== */

        toastr()->success('تم العملية بنجاح', setting('title'));
        return redirect(aurl('teams'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Team  $team
     * @return \Illuminate\Http\Response
     */
    public function show(Team $team)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Team  $team
     * @return \Illuminate\Http\Response
     */
    public function edit(Team $team)
    {
        $language_allows = LanguageAllow::get();
        return  view('admin.teams.edit',['page_titles'=>$this->page_titles,'language_allows'=>$language_allows,'team'=>$team]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Team  $team
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Team $team)
    {
        $validator = Validator::make(request()->all(),
            [
                'image' => 'nullable|image',
                'is_ceo'=>'nullable'
            ], [], []
        );



        if ($validator->fails()) {
            $errors = collect($validator->errors())->flatten(1);
            foreach ($errors as $error) {
                toastr()->warning($error, trans('admin.Message_title_attention'));
            }
            return back()->withInput();
        }

        $data = $validator->validated();




        /* =============== check languages =============== */
        if ((!is_array(request()->name) || !$this->check_allowed_language_keys(request()->name)) ||
            (!is_array(request()->job_title) || !$this->check_allowed_language_keys(request()->job_title))) {
            toastr()->warning('رجاء املاء جميع اللغات حتى لا يحدث مشكلة فيما بعد (title)', setting('title'));
            return back()->withInput();
        }
        /* =============== check languages =============== */

        if (request()->photo != '') {
            $data['photo'] = upload_image('teams', request()->photo, 1, 'image', $team->photo);
        }
        $data['is_ceo']  = $request->is_ceo ? 1: 0;

        $team->update($data);


        /* =============== languages =============== */
        if (count(request()->name) > 0) {
            TeamTranslation::where([['team_id', '=', $team->id]])->delete();
            foreach (request()->name as $lang => $title) {
                TeamTranslation::create([
                    'team_id' => $team->id,
                    'name' => $title,
                    'job_title' => request()->job_title[$lang] ? request()->job_title[$lang] : null,
                    'lang' => $lang,
                ]);
            }
        }
        /* =============== languages =============== */

        toastr()->success('تم العملية بنجاح', setting('title'));
        return redirect(aurl('teams'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Team  $team
     * @return \Illuminate\Http\Response
     */
    public function destroy(Team $team)
    {

    }
    public function delete(){
        $id = \request()->get('id');
        $team = Team::findorfail($id);
        isset($team->photo) ? delete_image($team->photo) : '';
        $team->delete();
        return response()->json(['id' => $id], 200);

    }//end of delete function
}
