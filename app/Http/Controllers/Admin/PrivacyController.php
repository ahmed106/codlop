<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Traits\CheckAllowedLanguageKeys;
use App\Http\Traits\ViewLanguage;
use App\Models\LanguageAllow;
use App\Models\Privacy;
use App\Models\PrivacyTranslation;
use Illuminate\Http\Request;

class PrivacyController extends Controller
{
    use CheckAllowedLanguageKeys, ViewLanguage;


    public $page_titles = [
        'index' => 'سياسه الخصوصيه',
        'create' => 'سياسه الخصوصيه',
        'edit' => '',
    ];

    public function index()
    {
        $privacy = Privacy::first();
        if ($privacy) {
            $language_allows = LanguageAllow::get();
            $title = ' تعديل بيانات ' . $privacy->title;
            $this->page_titles['edit'] = $title;
            $view_languages = $this->view_language($privacy->privacies_trans);

            return view('admin.privacy.index', ['page_titles' => $this->page_titles, 'privacy' => $privacy, 'language_allows' => $language_allows, 'view_languages' => $view_languages]);
        } else {

            $language_allows = LanguageAllow::get();
            $title = ' تعديل بيانات ';
            $this->page_titles['edit'] = $title;
            return view('admin.privacy.index', ['page_titles' => $this->page_titles, 'language_allows' => $language_allows, 'privacy' => $privacy]);
        }


    }//end of index function


    public function store(Request $request)
    {


        try {
            /* =============== check languages =============== */
            if ((!is_array(request()->titles) || !$this->check_allowed_language_keys(request()->titles)) ||
                (!is_array(request()->texts) || !$this->check_allowed_language_keys(request()->texts))) {
                toastr()->warning('رجاء املاء جميع اللغات حتى لا يحدث مشكلة فيما بعد (title)', setting('title'));
                return back()->withInput();
            }


            $privacy = Privacy::first();


            $data = $request->only('image');
            if (!$privacy) {
                if (request()->image != '') {
                    $data['image'] = upload_image('privacy', request()->image, 1, 'image', '');
                }

                $privacy = Privacy::create($data);
                if (count(request()->titles) > 0) {
                    foreach (request()->titles as $lang => $title) {

                        PrivacyTranslation::create([
                            'privacy_id' => $privacy->id,
                            'title' => $title,
                            'text' => request()->texts[$lang] ? request()->texts[$lang] : null,
                            'lang' => $lang,
                        ]);
                    }
                }
            } else {
                if (request()->image != '') {
                    $data['image'] = upload_image('privacy', request()->image, 1, 'image', $privacy->image);
                }
                $privacy->update($data);


                if ((!is_array(request()->titles) || !$this->check_allowed_language_keys(request()->titles)) ||
                    (!is_array(request()->texts) || !$this->check_allowed_language_keys(request()->texts))) {
                    toastr()->warning('رجاء املاء جميع اللغات حتى لا يحدث مشكلة فيما بعد (title)', setting('title'));
                    return back()->withInput();
                }

                if (count(request()->titles) > 0) {
                    PrivacyTranslation::where([['privacy_id', '=', $privacy->id]])->delete();
                    foreach (request()->titles as $lang => $title) {
                        PrivacyTranslation::create([
                            'privacy_id' => $privacy->id,
                            'title' => $title,
                            'text' => request()->texts[$lang] ? request()->texts[$lang] : null,
                            'lang' => $lang,
                        ]);
                    }
                }

            }

        } catch (\Exception $e) {
            toastr()->success($e->getMessage(), 'title');

            return redirect()->back();
        }

        toastr()->success('تم العملية بنجاح', setting('title'));
        return redirect(aurl('privacy'));

    }//end of store function
}
