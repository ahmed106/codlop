<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use App\Models\LanguageAllow;
use App\Models\ClientTran;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

//models
use App\Models\Client;

/* traits */
use App\Http\Traits\CheckAllowedLanguageKeys;
use App\Http\Traits\ViewLanguage;

class ClientController extends Controller
{

    use CheckAllowedLanguageKeys, ViewLanguage;


    public $page_titles = [
        'index' => 'عملاؤنا',
        'create' => 'اضافة عملاء جداد',
        'edit' => '',
    ];

//========================================= index =========================================
    public function index()
    {
        $clients = Client::orderBy('id', 'desc')->get();
        return view('admin.clients.index', ['clients' => $clients, 'page_titles' => $this->page_titles]);
    }

//========================================= create =========================================
    public function create()
    {
        $language_allows = LanguageAllow::get();
        return view('admin.clients.create', ['page_titles' => $this->page_titles, 'language_allows' => $language_allows]);
    }

//========================================= store =========================================
    public function store()
    {
        $validator = Validator::make(request()->all(),
            [
                'image' => 'required|image',
                'link' => 'required',
            ], [], []
        );

        if ($validator->fails()) {
            $errors = collect($validator->errors())->flatten(1);
            foreach ($errors as $error) {
                toastr()->warning($error, trans('admin.Message_title_attention'));
            }
            return back()->withInput();
        }

        $data = $validator->validated();

        /* =============== check languages =============== */
//        if ((!is_array(request()->titles) || !$this->check_allowed_language_keys(request()->titles)) ||
//            (!is_array(request()->texts) || !$this->check_allowed_language_keys(request()->texts))) {
//            toastr()->warning('رجاء املاء جميع اللغات حتى لا يحدث مشكلة فيما بعد (title)', setting('title'));
//            return back()->withInput();
//        }
        /* =============== check languages =============== */

        if (request()->image != '') {
            $data['image'] = upload_image('clients', request()->image, 1, 'image', '');
        }

        $client = Client::create($data);

        /* =============== languages =============== */
//        if (count(request()->titles) > 0) {
//            foreach (request()->titles as $lang => $title) {
//                ClientTran::create([
//                    'client_id' => $client->id,
//                    'title' => $title,
//                    'text' => request()->texts[$lang] ? request()->texts[$lang] : null,
//                    'lang' => $lang,
//                ]);
//            }
//        }
        /* =============== languages =============== */

        toastr()->success('تم العملية بنجاح', setting('title'));
        return redirect(aurl('client'));
    }

//========================================= show =========================================
    public function show($id)
    {
        //
    }

//========================================= edit =========================================
    public function edit($id)
    {
        $client = Client::findorfail($id);
        $language_allows = LanguageAllow::get();
        $title = ' تعديل بيانات ' . $client->title;
        $this->page_titles['edit'] = $title;
        $view_languages = $this->view_language($client->client_trans);
        return view('admin.clients.edit', ['client' => $client, 'language_allows' => $language_allows, 'view_languages' => $view_languages, 'page_titles' => $this->page_titles]);
    }

//========================================= update =========================================
    public function update_client()
    {
        $validator = Validator::make(request()->all(),
            [
                'image' => 'nullable|image',
                'link' => 'required',
            ], [], []
        );

        if ($validator->fails()) {
            $errors = collect($validator->errors())->flatten(1);
            foreach ($errors as $error) {
                toastr()->warning($error, trans('admin.Message_title_attention'));
            }
            return back()->withInput();
        }

        $data = $validator->validated();

        $client = Client::findorfail(request()->client_id);

        /* =============== check languages =============== */
//        if ((!is_array(request()->titles) || !$this->check_allowed_language_keys(request()->titles)) ||
//            (!is_array(request()->texts) || !$this->check_allowed_language_keys(request()->texts))) {
//            toastr()->warning('رجاء املاء جميع اللغات حتى لا يحدث مشكلة فيما بعد (title)', setting('title'));
//            return back()->withInput();
//        }
        /* =============== check languages =============== */

        if (request()->image != '') {
            $data['image'] = upload_image('clients', request()->image, 1, 'image', $client->image);
        }

        $client->update($data);

        /* =============== languages =============== */
//        if (count(request()->titles) > 0) {
//            ClientTran::where([['client_id', '=', $client->id]])->delete();
//            foreach (request()->titles as $lang => $title) {
//                ClientTran::create([
//                    'client_id' => request()->client_id,
//                    'title' => $title,
//                    'text' => request()->texts[$lang] ? request()->texts[$lang] : null,
//                    'lang' => $lang,
//                ]);
//            }
//        }
        /* =============== languages =============== */

        toastr()->success('تم العملية بنجاح', setting('title'));
        return redirect(aurl('client'));

    }

//========================================= destroy =========================================
    public function delete_client()
    {
        $id = \request()->get('id');
        $client = Client::findorfail($id);
        isset($client->image) ? delete_image($client->image) : '';
        $client->delete();
        return response()->json(['id' => $id], 200);
    }

}
