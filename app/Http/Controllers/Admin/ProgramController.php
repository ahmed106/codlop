<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Traits\CheckAllowedLanguageKeys;
use App\Http\Traits\ViewLanguage;
use App\Models\LanguageAllow;
use App\Models\Program;
use App\Models\ProgramTran;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ProgramController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    use CheckAllowedLanguageKeys, ViewLanguage;


    public $page_titles = [
        'index' => 'خدمات اطلب الآن',
        'create' => 'اضافة خدمه جديد',
        'edit' => '',
    ];

    public function index()
    {
        $programs = Program::get();
        return view('admin.programs.index', ['programs' => $programs, 'page_titles' => $this->page_titles]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $language_allows = LanguageAllow::get();
        return view('admin.programs.create', ['page_titles' => $this->page_titles, 'language_allows' => $language_allows]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make(request()->all(),
            [

                'icon' => 'nullable|image',
            ], [], []
        );

        if ($validator->fails()) {
            $errors = collect($validator->errors())->flatten(1);
            foreach ($errors as $error) {
                toastr()->warning($error, trans('admin.Message_title_attention'));
            }
            return back()->withInput();
        }


        $data = $validator->validated();
        /* =============== check languages =============== */
        if ((!is_array(request()->titles) || !$this->check_allowed_language_keys(request()->titles)) ||
            (!is_array(request()->texts) || !$this->check_allowed_language_keys(request()->texts))) {
            toastr()->warning('رجاء املاء جميع اللغات حتى لا يحدث مشكلة فيما بعد (title)', setting('title'));
            return back()->withInput();
        }
        /* =============== check languages =============== */

        if (request()->icon != '') {
            $data['icon'] = upload_image('icons', request()->icon, 1, 'icons', '');
        }
        $program = Program::create($data);


        /* =============== languages =============== */

        try {
            if (count(request()->titles) > 0) {


                foreach (request()->titles as $lang => $title) {
                    ProgramTran::create([
                        'program_id' => $program->id,
                        'title' => $title,
                        'text' => request()->texts[$lang] ? request()->texts[$lang] : null,
                        'lang' => $lang,
                    ]);
                }
            }
        } catch (\Exception $e) {
            toastr()->warning($e->getMessage(), setting('title'));
            return back()->withInput();
        }

        /* =============== languages =============== */

        toastr()->success('تم العملية بنجاح', setting('title'));
        return redirect(aurl('programs'));

    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $program = Program::findorfail($id);
        $language_allows = LanguageAllow::get();
        $title = ' تعديل بيانات ' . $program->title;
        $this->page_titles['edit'] = $title;
        $view_languages = $this->view_language($program->program_trans);
        return view('admin.programs.edit', ['program' => $program, 'language_allows' => $language_allows, 'view_languages' => $view_languages, 'page_titles' => $this->page_titles]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make(request()->all(),
            [
                'icon' => 'nullable|image',
            ], [], []
        );

        if ($validator->fails()) {
            $errors = collect($validator->errors())->flatten(1);
            foreach ($errors as $error) {
                toastr()->warning($error, trans('admin.Message_title_attention'));
            }
            return back()->withInput();
        }

        $data = $validator->validated();

        $program = Program::findorfail($id);


        /* =============== check languages =============== */
        if ((!is_array(request()->titles) || !$this->check_allowed_language_keys(request()->titles)) ||
            (!is_array(request()->texts) || !$this->check_allowed_language_keys(request()->texts))) {
            toastr()->warning('رجاء املاء جميع اللغات حتى لا يحدث مشكلة فيما بعد (title)', setting('title'));
            return back()->withInput();
        }
        /* =============== check languages =============== */

        if (request()->icon != '') {
            $data['icon'] = upload_image('icons', request()->icon, 1, 'image', $program->icon);
        }

        $program->update($data);


        /* =============== languages =============== */
        if (count(request()->titles) > 0) {
            ProgramTran::where([['program_id', '=', $program->id]])->delete();
            foreach (request()->titles as $lang => $title) {
                ProgramTran::create([
                    'program_id' => $program->id,
                    'title' => $title,
                    'text' => request()->texts[$lang] ? request()->texts[$lang] : null,
                    'lang' => $lang,
                ]);
            }
        }
        /* =============== languages =============== */

        toastr()->success('تم العملية بنجاح', setting('title'));
        return redirect(aurl('programs'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */

    public function deleteProgram(Request $request)
    {
        $id = \request()->get('id');
        $program = Program::findorfail($id);
        isset($program->icon) ? delete_image($program->icon) : '';
        $program->delete();
        return response()->json(['id' => $id], 200);

    }//end of deleteProgram function

    public function destroy($id)
    {
        //
    }
}
