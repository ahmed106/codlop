<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use App\Models\LanguageAllow;
use App\Models\SliderTran;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

//models
use App\Models\Slider;

/* traits */
use App\Http\Traits\CheckAllowedLanguageKeys;
use App\Http\Traits\ViewLanguage;

class SliderController extends Controller
{

    use CheckAllowedLanguageKeys, ViewLanguage;


    public $page_titles = [
        'index' => 'الاسليدرات',
        'create' => 'اضافة اسلايدر جديد',
        'edit' => '',
    ];

//========================================= index =========================================
    public function index()
    {
        $sliders = Slider::orderBy('id', 'desc')->get();
        return view('admin.sliders.index', ['sliders' => $sliders, 'page_titles' => $this->page_titles]);
    }

//========================================= create =========================================
    public function create()
    {
        $language_allows = LanguageAllow::get();
        return view('admin.sliders.create', ['page_titles' => $this->page_titles, 'language_allows' => $language_allows]);
    }

//========================================= store =========================================
    public function store()
    {
        $validator = Validator::make(request()->all(),
            [
                'image' => 'nullable|image',
            ], [], []
        );

        if ($validator->fails()) {
            $errors = collect($validator->errors())->flatten(1);
            foreach ($errors as $error) {
                toastr()->warning($error, trans('admin.Message_title_attention'));
            }
            return back()->withInput();
        }

        $data = $validator->validated();

        /* =============== check languages =============== */
        if ((!is_array(request()->titles) || !$this->check_allowed_language_keys(request()->titles)) ||
            (!is_array(request()->texts) || !$this->check_allowed_language_keys(request()->texts))) {
            toastr()->warning('رجاء املاء جميع اللغات حتى لا يحدث مشكلة فيما بعد (title)', setting('title'));
            return back()->withInput();
        }
        /* =============== check languages =============== */

        if (request()->image != '') {
            $data['image'] = upload_image('sliders', request()->image, 1, 'image', '');
        }

        $slider = Slider::create($data);

        /* =============== languages =============== */
        if (count(request()->titles) > 0) {
            foreach (request()->titles as $lang => $title) {
                SliderTran::create([
                    'slider_id' => $slider->id,
                    'title' => $title,
                    'text' => request()->texts[$lang] ? request()->texts[$lang] : null,
                    'lang' => $lang,
                ]);
            }
        }
        /* =============== languages =============== */

        toastr()->success('تم العملية بنجاح', setting('title'));
        return redirect(aurl('slider'));
    }

//========================================= show =========================================
    public function show($id)
    {
        //
    }

//========================================= edit =========================================
    public function edit($id)
    {
        $slider = Slider::findorfail($id);
        $language_allows = LanguageAllow::get();
        $title = ' تعديل بيانات ' . $slider->title;
        $this->page_titles['edit'] = $title;
        $view_languages = $this->view_language($slider->slider_trans);
        return view('admin.sliders.edit', ['slider' => $slider, 'language_allows' => $language_allows, 'view_languages' => $view_languages, 'page_titles' => $this->page_titles]);
    }

//========================================= update =========================================
    public function update_slider()
    {
        $validator = Validator::make(request()->all(),
            [
                'image' => 'nullable|image',
            ], [], []
        );

        if ($validator->fails()) {
            $errors = collect($validator->errors())->flatten(1);
            foreach ($errors as $error) {
                toastr()->warning($error, trans('admin.Message_title_attention'));
            }
            return back()->withInput();
        }

        $data = $validator->validated();

        $slider = Slider::findorfail(request()->slider_id);

        /* =============== check languages =============== */
        if ((!is_array(request()->titles) || !$this->check_allowed_language_keys(request()->titles)) ||
            (!is_array(request()->texts) || !$this->check_allowed_language_keys(request()->texts))) {
            toastr()->warning('رجاء املاء جميع اللغات حتى لا يحدث مشكلة فيما بعد (title)', setting('title'));
            return back()->withInput();
        }
        /* =============== check languages =============== */

        if (request()->image != '') {
            $data['image'] = upload_image('sliders', request()->image, 1, 'image', $slider->image);
        }

        $slider->update($data);

        /* =============== languages =============== */
        if (count(request()->titles) > 0) {
            SliderTran::where([['slider_id', '=', $slider->id]])->delete();
            foreach (request()->titles as $lang => $title) {
                SliderTran::create([
                    'slider_id' => request()->slider_id,
                    'title' => $title,
                    'text' => request()->texts[$lang] ? request()->texts[$lang] : null,
                    'lang' => $lang,
                ]);
            }
        }
        /* =============== languages =============== */

        toastr()->success('تم العملية بنجاح', setting('title'));
        return redirect(aurl('slider'));

    }

//========================================= destroy =========================================
    public function delete_slider()
    {
        $id = \request()->get('id');
        $slider = Slider::findorfail($id);
        isset($slider->image) ? delete_image($slider->image) : '';
        $slider->delete();
        return response()->json(['id' => $id], 200);
    }

}
