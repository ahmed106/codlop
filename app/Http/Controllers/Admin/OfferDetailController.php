<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use App\Models\Offer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

//models
use App\Models\OfferDetail;
use App\Models\OfferDetailTran;
use App\Models\LanguageAllow;

/* traits */
use App\Http\Traits\CheckAllowedLanguageKeys;
use App\Http\Traits\ViewLanguage;


class OfferDetailController extends Controller
{

    use CheckAllowedLanguageKeys, ViewLanguage;


    public $page_titles = [
        'index' => 'العروض',
        'create' => 'اضافة تفاصيل عرض جديدة',
        'edit' => '',
    ];

//========================================= index =========================================
    public function index()
    {
        $offer_main_package = Offer::where([['offer_type','=','main_package']])->first();
        $offer_professional_package = Offer::where([['offer_type','=','professional_package']])->first();
        $offer_golden_package = Offer::where([['offer_type','=','golden_package']])->first();

        $offer_details = OfferDetail::orderBy('id', 'asc')->get();
        return view('admin.offer_details.index', ['offer_main_package' => $offer_main_package, 'offer_professional_package' => $offer_professional_package, 'offer_golden_package' => $offer_golden_package, 'offer_details' => $offer_details, 'page_titles' => $this->page_titles]);
    }

//========================================= create =========================================
    public function create()
    {
        $language_allows = LanguageAllow::get();
        return view('admin.offer_details.create', ['page_titles' => $this->page_titles, 'language_allows' => $language_allows]);
    }

//========================================= store =========================================
    public function store()
    {
        $validator = Validator::make(request()->all(), [
            'main_package' => 'nullable|in:yes,no',
            'professional_package' => 'nullable|in:yes,no',
            'golden_package' => 'nullable|in:yes,no',
        ], [], []
        );

        if ($validator->fails()) {
            $errors = collect($validator->errors())->flatten(1);
            foreach ($errors as $error) {
                toastr()->warning($error, trans('admin.Message_title_attention'));
            }
            return back()->withInput();
        }

        $data = $validator->validated();

        /* =============== check languages =============== */
        if ( (!is_array(request()->texts) || !$this->check_allowed_language_keys(request()->texts)) ) {
            toastr()->warning('رجاء املاء جميع اللغات حتى لا يحدث مشكلة فيما بعد (text)', setting('title'));
            return back()->withInput();
        }
        /* =============== check languages =============== */

        if (request()->main_package == null){
            $data['main_package'] = 'no';
        }

        if (request()->professional_package == null){
            $data['professional_package'] = 'no';
        }

        if (request()->golden_package == null){
            $data['golden_package'] = 'no';
        }

        $offer_detail = OfferDetail::create($data);

        /* =============== languages =============== */
        if (count(request()->texts) > 0) {
            foreach (request()->texts as $lang => $text) {
                OfferDetailTran::create([
                    'offer_detail_id' => $offer_detail->id,
                    'title' => null,
                    'text' => $text,
                    'lang' => $lang,
                ]);
            }
        }
        /* =============== languages =============== */

        toastr()->success('تم العملية بنجاح', setting('title'));
        return redirect(aurl('offer_detail'));
    }

//========================================= show =========================================
    public function show($id)
    {
        //
    }

//========================================= edit =========================================
    public function edit($id)
    {
        $offer_detail = OfferDetail::findorfail($id);
        $language_allows = LanguageAllow::get();
        $title = ' تعديل بيانات ' . $offer_detail->title;
        $this->page_titles['edit'] = $title;
        $view_languages = $this->view_language($offer_detail->offer_detail_trans);
        return view('admin.offer_details.edit', ['offer_detail' => $offer_detail, 'language_allows' => $language_allows, 'view_languages' => $view_languages, 'page_titles' => $this->page_titles]);
    }

//========================================= update =========================================
    public function update_offer_detail()
    {
        $validator = Validator::make(request()->all(), [
            'main_package' => 'nullable|in:yes,no',
            'professional_package' => 'nullable|in:yes,no',
            'golden_package' => 'nullable|in:yes,no',
        ], [], []
        );

        if ($validator->fails()) {
            $errors = collect($validator->errors())->flatten(1);
            foreach ($errors as $error) {
                toastr()->warning($error, trans('admin.Message_title_attention'));
            }
            return back()->withInput();
        }

        $data = $validator->validated();

        if (request()->main_package == null){
            $data['main_package'] = 'no';
        }

        if (request()->professional_package == null){
            $data['professional_package'] = 'no';
        }

        if (request()->golden_package == null){
            $data['golden_package'] = 'no';
        }


        $offer_detail = OfferDetail::findorfail(request()->offer_detail_id);

        /* =============== check languages =============== */
        if ( (!is_array(request()->texts) || !$this->check_allowed_language_keys(request()->texts)) ) {
            toastr()->warning('رجاء املاء جميع اللغات حتى لا يحدث مشكلة فيما بعد (texts)', setting('title'));
            return back()->withInput();
        }
        /* =============== check languages =============== */

        if (request()->image != '') {
            $data['image'] = upload_image('offer_details', request()->image, 1, 'image', $offer_detail->image);
        }

        $offer_detail->update($data);

        /* =============== languages =============== */
        if (count(request()->texts) > 0) {
            OfferDetailTran::where([['offer_detail_id', '=', $offer_detail->id]])->delete();
            foreach (request()->texts as $lang => $text) {
                OfferDetailTran::create([
                    'offer_detail_id' => $offer_detail->id,
                    'title' => null,
                    'text' => $text,
                    'lang' => $lang,
                ]);
            }
        }
        /* =============== languages =============== */

        toastr()->success('تم العملية بنجاح', setting('title'));
        return redirect(aurl('offer_detail'));

    }

//========================================= destroy =========================================
    public function delete_offer_detail()
    {
        $id = \request()->get('id');
        $offer_detail = OfferDetail::findorfail($id);
        isset($offer_detail->image) ? delete_image($offer_detail->image) : '';
        $offer_detail->delete();
        return response()->json(['id' => $id], 200);
    }

//========================================= update =========================================
    public function update_prices()
    {
        $validator = Validator::make(request()->all(), [
            'main_package' => 'required|numeric',
            'professional_package' => 'required|numeric',
            'golden_package' => 'required|numeric',
        ], [], []
        );

        if ($validator->fails()) {
            $errors = collect($validator->errors())->flatten(1);
            foreach ($errors as $error) {
                toastr()->warning($error, trans('admin.Message_title_attention'));
            }
            return back()->withInput();
        }

        $data = $validator->validated();

        Offer::where([['offer_type','=','main_package']])->update([
            'price' => request()->main_package
        ]);

        Offer::where([['offer_type','=','professional_package']])->update([
            'price' => request()->professional_package
        ]);

        Offer::where([['offer_type','=','golden_package']])->update([
            'price' => request()->golden_package
        ]);


        return response()->json(['data' => $data], 200);

    }

}
