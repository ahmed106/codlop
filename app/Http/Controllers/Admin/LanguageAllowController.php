<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Language;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

//models
use App\Models\LanguageAllow;

/* traits */

use App\Http\Traits\CheckAllowedLanguageKeys;
use App\Http\Traits\ViewLanguage;

class LanguageAllowController extends Controller
{

    use CheckAllowedLanguageKeys, ViewLanguage;


    public $page_titles = [
        'index' => 'اللغات',
        'create' => 'اضافة لغة جديد',
        'edit' => '',
    ];


//========================================= index ======================================
    public function index()
    {
        $language_allows = LanguageAllow::orderBy('id', 'asc')->get();
        return view('admin.language_allows.index', ['language_allows' => $language_allows, 'page_titles' => $this->page_titles]);
    }

//========================================= create ======================================
    public function create()
    {
        $language_allows = LanguageAllow::pluck('code')->toArray();
        $languages = Language::whereNotIn('code', $language_allows)->get();
        return view('admin.language_allows.create', ['page_titles' => $this->page_titles, 'languages' => $languages]);
    }

//========================================= store ========================================
    public function store()
    {
        $validator = Validator::make(request()->all(),
            [
                'language_id' => ['required', "numeric", Rule::exists('languages', 'id')],
                'icon' => 'nullable|image',
            ], [], []
        );

        if ($validator->fails()) {
            $errors = collect($validator->errors())->flatten(1);
            foreach ($errors as $error) {
                toastr()->warning($error, trans('admin.Message_title_attention'));
            }
            return back()->withInput();
        }

        $data = $validator->validated();

        if (request()->icon != '') {
            $data['icon'] = upload_image('language_allows', request()->icon, 1, 'icon', '');
        }

        $language = Language::findorfail(request()->language_id);

        $data['language_id'] = request()->language_id;
        $data['code'] = $language->code;
        $data['name'] = $language->name;
        $data['native_name'] = $language->native_name;

        LanguageAllow::create($data);

        toastr()->success('تم العملية بنجاح', setting('title'));
        return redirect(aurl('language_allow'));
    }

//========================================= show =========================================
    public function show($id)
    {
        //
    }

//========================================= edit =========================================
    public function edit($id)
    {
        $language_allow = LanguageAllow::findorfail($id);
        $title = ' تعديل بيانات ' . $language_allow->native_name;
        $this->page_titles['edit'] = $title;
        return view('admin.language_allows.edit', ['language_allow' => $language_allow, 'page_titles' => $this->page_titles]);
    }

//========================================= update =======================================
    public function update_language_allow()
    {
        $validator = Validator::make(request()->all(),
            [
                'language_allow_id' => ['required', "numeric", Rule::exists('language_allows', 'id')],
                'icon' => 'nullable|image',
            ], [], []
        );

        if ($validator->fails()) {
            $errors = collect($validator->errors())->flatten(1);
            foreach ($errors as $error) {
                toastr()->warning($error, trans('admin.Message_title_attention'));
            }
            return back()->withInput();
        }

        $data = collect($validator->validated())->except(['language_allow_id'])->toArray();

        $language_allow = LanguageAllow::findorfail(request()->language_allow_id);

        if (request()->icon != '') {
            $data['icon'] = upload_image('language_allows', request()->icon, 1, 'image', $language_allow->icon);
        }

        $language_allow->update($data);

        toastr()->success('تم العملية بنجاح', setting('title'));
        return redirect(aurl('language_allow'));

    }

//========================================= destroy ======================================
    public function delete_language_allow()
    {
        $id = \request()->get('id');
        $language_allow = LanguageAllow::findorfail($id);
        isset($language_allow->image) ? delete_image($language_allow->image) : '';
        $language_allow->delete();
        return response()->json(['id' => $id], 200);
    }

}
