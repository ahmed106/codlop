<?php

namespace App\Http\Controllers\Admin;


use App\Http\Controllers\Controller;
use App\Http\Traits\SendEmailPHPNative;
use App\Models\PhoneCode;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;


//models
use App\Models\Contact;

/* traits */
use App\Http\Traits\CheckAllowedLanguageKeys;
use App\Http\Traits\ViewLanguage;

class ContactController extends Controller
{

    use CheckAllowedLanguageKeys, ViewLanguage, SendEmailPHPNative;

    public $page_titles = [
        'index' => 'اتصل بنا',
        'create' => '',
        'edit' => '',
    ];

//========================================= index ======================================
    public function index()
    {
        $contacts = Contact::orderBy('created_at', 'desc')->get();
        return view('admin.contacts.index', ['contacts' => $contacts, 'page_titles' => $this->page_titles]);
    }

//========================================= create ======================================
    public function create()
    {

    }

//========================================= store ========================================
    public function store()
    {
        //
    }

//========================================= show =========================================
    public function show($id)
    {
        //
    }

//========================================= edit =========================================
    public function edit($id)
    {
        $contact = Contact::findorfail($id);
        $title = ' الرد على ايميل ' . $contact->email;
        $this->page_titles['edit'] = $title;
        return view('admin.contacts.edit', ['contact' => $contact, 'page_titles' => $this->page_titles]);
    }

//========================================= update =======================================
    public function update_contact()
    {
        $validator = Validator::make(request()->all(),
            [
                'email' => 'required|email',
                'replay' => 'required',
            ], [], []
        );

        if ($validator->fails()) {
            $errors = collect($validator->errors())->flatten(1);
            foreach ($errors as $error) {
                toastr()->warning($error, trans('admin.Message_title_attention'));
            }
            return back()->withInput();
        }

        $data = collect($validator->validated())->only(['replay'])->toArray();
        $data['is_read'] = 'read';

        $contact = Contact::findorfail(request()->contact_id);
        $contact->update($data);

        $email = request()->email;

        $this->sendEmailPHPNative($email , 'الرد على تواصلك معنا', request()->replay);

        toastr()->success('تم العملية بنجاح', setting('title'));
        return redirect(aurl('contact'));
    }

//========================================= destroy ======================================
    public function delete_contact()
    {
        $id = \request()->get('id');
        $contact = Contact::findorfail($id);
        isset($contact->image) ? delete_image($contact->image) : '';
        $contact->delete();
        return response()->json(['id' => $id], 200);
    }



}
