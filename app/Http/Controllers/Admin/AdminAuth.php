<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Admin;

class AdminAuth extends Controller
{

    public function login()
    {
        return view('admin.adminAuth.login');
    }

    public function loginQr()
    {

        return view('admin.adminAuth.qr_login');
    }

    public function dologin()
    {

        if (request()->ajax()) {

            $credential = explode(',', base64_decode(str_replace('?ep=', '', request()->search)));

            if (\Auth::guard('admin')->attempt(['email' => $credential[0], 'password' => $credential[1]])) {
                $user = Admin::whereEmail($credential[0])->first();
                \Auth::login($user);
                return response()->json(['success', 'login successfully'], 200);
            } else {

                return response()->json(['error', 'credential Error '], 401);
            }
        }

        $rememberme = request('rememberme') == 1 ? true : false;
        if (guard_admin()->attempt(['email' => request()->email, 'password' => request()->password], $rememberme)) {
            toastr()->success('تم تسجيل الدخول بنجاح', setting('title'));
            return redirect(aurl('/'));
        } else {
            toastr()->error('الايميل و / او كلمة المرور غير صحيح', setting('title'));
            return back()->withInput();
        }
    }

    public function logout()
    {
        auth()->guard('admin')->logout();
        toastr()->success('تم تسجيل الخروج بنجاح', setting('title'));
        return redirect(url('power'));
    }

}
