<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use App\Http\Traits\ViewLanguage;
use App\Models\LanguageAllow;
use App\Models\SettingTran;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

/* models */

use App\Models\Setting;

/* traits */
use App\Http\Traits\CheckAllowedLanguageKeys;


class SettingController extends Controller
{

    use CheckAllowedLanguageKeys, ViewLanguage;


    public $page_titles = [
        'index' => 'الاعدادات',
        'create' => '',
        'edit' => 'تعديل بيانات الاعدادات',
    ];


//========================================= edit =========================================
    public function edit()
    {
        $setting = Setting::with(['setting_trans'])->first();
        $language_allows = LanguageAllow::get();
        $view_languages = $this->view_language($setting->setting_trans);
        return view('admin.settings.edit', ['setting' => $setting, 'page_titles' => $this->page_titles, 'language_allows' => $language_allows, 'view_languages' => $view_languages]);
    }


//========================================= update =========================================
    public function update_setting()
    {
        $validator = Validator::make(request()->all(),
            [
                'logo' => 'nullable|image',
                'phone' => 'nullable|numeric',
                'email' => 'nullable|email',
                'facebook' => 'nullable',
                'twitter' => 'nullable',
                'gmail' => 'nullable',
                'linkedin' => 'nullable',
                'whatsapp' => 'nullable|numeric',
                'messenger_link' => 'nullable',
                'twitter_link' => 'nullable',
                'latitude' => 'nullable',
                'longitude' => 'nullable',
                'location' => 'nullable',
            ], [], []
        );

        if ($validator->fails()) {
            $errors = collect($validator->errors())->flatten(1);
            foreach ($errors as $error) {
                toastr()->warning($error, trans('admin.Message_title_attention'));
            }
            return back()->withInput();
        }

        $data = $validator->validated();


        /* =============== check languages =============== */
        if ((!is_array(request()->titles) || !$this->check_allowed_language_keys(request()->titles)) ||
            (!is_array(request()->address) || !$this->check_allowed_language_keys(request()->address))) {
            toastr()->warning('رجاء املاء جميع اللغات حتى لا يحدث مشكلة فيما بعد (title)', setting('title'));
            return back()->withInput();
        }
        /* =============== check languages =============== */


        $setting = Setting::first();

        if (request()->logo != '') {
            $data['logo'] = upload_image('settings', request()->logo, 1, 'logo', $setting->logo);
        }

        $setting->update($data);

        /* =============== languages =============== */
        if (count(request()->titles) > 0) {
            SettingTran::where([['setting_id', '=', $setting->id]])->delete();
            foreach (request()->titles as $lang => $title) {
                SettingTran::create([
                    'setting_id' => $setting->id,
                    'title' => $title,
                    'address' => request()->address[$lang] ? request()->address[$lang] : null,
                    'lang' => $lang,
                ]);
            }
        }
        /* =============== languages =============== */


        toastr()->success('تم العملية بنجاح', setting('title'));
        return redirect(aurl('settings'));

    }

//========================================= destroy =========================================
    public function destroy($id)
    {
        //
    }


}
