<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Traits\CheckAllowedLanguageKeys;
use App\Http\Traits\ViewLanguage;
use App\Models\LanguageAllow;
use App\Models\SiteText;
use App\Models\SiteTextTran;
use Illuminate\Support\Facades\Validator;

//models

/* traits */

class SiteTextController extends Controller
{

    use CheckAllowedLanguageKeys, ViewLanguage;


    public $page_titles = [
        'index' => 'نصوص الموقع',
        'create' => 'اضافة نص موقع جديد',
        'edit' => '',
    ];


//========================================= index ======================================
    public function index()
    {
        $site_texts = SiteText::orderBy('page_type', 'asc')->get();
        return view('admin.site_texts.index', ['site_texts' => $site_texts, 'page_titles' => $this->page_titles]);
    }

//========================================= create ======================================
    public function create()
    {
        $language_allows = LanguageAllow::get();
        return view('admin.site_texts.create', ['page_titles' => $this->page_titles, 'language_allows' => $language_allows]);
    }

//========================================= store ========================================
    public function store()
    {
        $validator = Validator::make(request()->all(),
            [
                'page_type' => 'required|in:home,about',
                'image' => 'nullable|image',
            ], [], []
        );


        if ($validator->fails()) {
            $errors = collect($validator->errors())->flatten(1);
            foreach ($errors as $error) {
                toastr()->warning($error, trans('admin.Message_title_attention'));
            }
            return back()->withInput();
        }

        $data = $validator->validated();

        /* =============== check languages =============== */
        if ((!is_array(request()->titles) || !$this->check_allowed_language_keys(request()->titles)) ||
            (!is_array(request()->texts) || !$this->check_allowed_language_keys(request()->texts))) {
            toastr()->warning('رجاء املاء جميع اللغات حتى لا يحدث مشكلة فيما بعد (title)', setting('title'));
            return back()->withInput();
        }
        /* =============== check languages =============== */

        if (request()->image != '') {
            $data['image'] = upload_image('site_texts', request()->image, 1, 'image', '');
        }

        $site_text = SiteText::create($data);

        /* =============== languages =============== */
        if (count(request()->titles) > 0) {
            foreach (request()->titles as $lang => $title) {

                SiteTextTran::create([
                    'site_text_id' => $site_text->id,
                    'title' => $title,
                    'text' => request()->texts[$lang] ? str_replace('&quot;', '', request()->texts[$lang]) : null,
                    'lang' => $lang,
                ]);
            }
        }
        /* =============== languages =============== */

        toastr()->success('تم العملية بنجاح', setting('title'));
        return redirect(aurl('site_text'));
    }

//========================================= show =========================================
    public function show($id)
    {
        //
    }

//========================================= edit =========================================
    public function edit($id)
    {
        $site_text = SiteText::findorfail($id);
        $language_allows = LanguageAllow::get();
        $title = ' تعديل بيانات ' . $site_text->title;
        $this->page_titles['edit'] = $title;
        $view_languages = $this->view_language($site_text->site_text_trans);
        return view('admin.site_texts.edit', ['site_text' => $site_text, 'language_allows' => $language_allows, 'view_languages' => $view_languages, 'page_titles' => $this->page_titles]);
    }

//========================================= update =======================================
    public function update_site_text()
    {
        $validator = Validator::make(request()->all(),
            [
                'image' => 'nullable|image',
            ], [], []
        );

        if ($validator->fails()) {
            $errors = collect($validator->errors())->flatten(1);
            foreach ($errors as $error) {
                toastr()->warning($error, trans('admin.Message_title_attention'));
            }
            return back()->withInput();
        }

        $data = $validator->validated();

        $site_text = SiteText::findorfail(request()->site_text_id);

        /* =============== check languages =============== */
        if ((!is_array(request()->titles) || !$this->check_allowed_language_keys(request()->titles)) ||
            (!is_array(request()->texts) || !$this->check_allowed_language_keys(request()->texts))) {
            toastr()->warning('رجاء املاء جميع اللغات حتى لا يحدث مشكلة فيما بعد (title)', setting('title'));
            return back()->withInput();
        }
        /* =============== check languages =============== */

        if (request()->image != '') {
            $data['image'] = upload_image('site_texts', request()->image, 1, 'image', $site_text->image);
        }

        $site_text->update($data);

        /* =============== languages =============== */
        if (count(request()->titles) > 0) {
            SiteTextTran::where([['site_text_id', '=', $site_text->id]])->delete();
            foreach (request()->titles as $lang => $title) {
                SiteTextTran::create([
                    'site_text_id' => request()->site_text_id,
                    'title' => $title,
                    'text' => request()->texts[$lang] ? request()->texts[$lang] : null,
                    'lang' => $lang,
                ]);
            }
        }
        /* =============== languages =============== */

        toastr()->success('تم العملية بنجاح', setting('title'));
        if (request()->page == 'home') {
            return back();
        }
        return redirect(aurl('site_text'));

    }

//========================================= destroy ======================================
    public function delete_site_text()
    {
        $id = \request()->get('id');
        $site_text = SiteText::findorfail($id);
        isset($site_text->image) ? delete_image($site_text->image) : '';
        $site_text->delete();
        return response()->json(['id' => $id], 200);
    }

}
