<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Traits\CheckAllowedLanguageKeys;
use App\Http\Traits\ViewLanguage;
use App\Models\LanguageAllow;
use App\Models\ServiceHost;
use App\Models\ServiceHostDetail;
use App\Models\ServiceHostDetailTran;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

//models

/* traits */

class ServiceHostDetailController extends Controller
{

    use CheckAllowedLanguageKeys, ViewLanguage;


    public $page_titles = [
        'index' => 'الاستضافات',
        'create' => 'اضافة استضافة جديدة',
        'edit' => 'تعديل',
    ];

//========================================= index =========================================
    public function index()
    {
        $service_hosts = ServiceHost::get();
        $service_host_id = \request()->get('service_host_id');
        $service_host_details = ServiceHostDetail::where('id', '!=', 0);
        if (isset($service_host_id)) {
            $service_host_details = $service_host_details->where([['service_host_id', '=', $service_host_id]]);
        }
        $service_host_details = $service_host_details->orderBy('service_host_id', 'asc')->get();
        return view('admin.service_host_details.index', ['service_host_details' => $service_host_details, 'service_hosts' => $service_hosts, 'page_titles' => $this->page_titles]);
    }

//========================================= create =========================================
    public function create()
    {

        $service_hosts = ServiceHost::get();
        $language_allows = LanguageAllow::get();
        return view('admin.service_host_details.create', ['page_titles' => $this->page_titles, 'language_allows' => $language_allows, 'service_hosts' => $service_hosts]);
    }

//========================================= store =========================================
    public function store()
    {

        $validator = Validator::make(request()->all(), [
            'service_id' => ['required', "numeric", Rule::exists('services', 'id')],
        ], [], []
        );

        if ($validator->fails()) {
            $errors = collect($validator->errors())->flatten(1);
            foreach ($errors as $error) {
                toastr()->warning($error, trans('admin.Message_title_attention'));
            }
            return back()->withInput();
        }

        $data = $validator->validated();

        /* =============== languages =============== */
        if (count(request()->titles) > 0) {
            foreach (request()->titles as $service_host_id => $titles) {
                if (isset($service_host_id) && $service_host_id != '') {
                    $service_host_detail = ServiceHostDetail::create([
                        'service_host_id' => $service_host_id,
                        'service_id' => request()->service_id,
                    ]);
                    foreach ($titles as $lang => $title) {
                        if (isset($title) && $title != '') {
                            ServiceHostDetailTran::create([
                                'service_host_detail_id' => $service_host_detail->id,
                                'title' => $title,
                                'text' => null,
                                'lang' => $lang,
                            ]);
                        }
                    }
                }
            }
        }
        /* =============== languages =============== */

        $service_host = ServiceHost::first();

        toastr()->success('تم العملية بنجاح', setting('title'));
        return redirect(aurl('service') . '/' . request()->service_id . '?service_host_id=' . $service_host->id);
    }

//========================================= show =========================================
    public function show($id)
    {
        //
    }

//========================================= edit =========================================
    public function edit($id)
    {

        $service_host_detail = ServiceHostDetail::find($id);

        $service_hosts = ServiceHost::get();
        $view_languages = LanguageAllow::get();

        $title = ' تعديل بيانات ' . $service_host_detail->title;
        $this->page_titles['edit'] = $title;


        return view('admin.service_host_details.edit', ['page_titles' => $this->page_titles, 'language_allows' => $view_languages, 'service_hosts' => $service_hosts, 'service_host_detail' => $service_host_detail]);
    }

//========================================= update =========================================


    public function update(Request $request, $id)
    {


//    $service_host_detail = ServiceHostDetail::find($id);
//    $validator = Validator::make(request()->all(), [
//        'service_id' => ['required', "numeric", Rule::exists('services', 'id')],
//    ], [], []
//    );
//
//    if ($validator->fails()) {
//        $errors = collect($validator->errors())->flatten(1);
//        foreach ($errors as $error) {
//            toastr()->warning($error, trans('admin.Message_title_attention'));
//        }
//        return back()->withInput();
//    }
//
//    $data = $validator->validated();
//
//    /* =============== languages =============== */
//    if (count(request()->titles) > 0) {
//        foreach (request()->titles as $service_host_id => $titles) {
//            if (isset($service_host_id) && $service_host_id != '') {
//                $service_host_detail->update([
//                    'service_host_id' => $service_host_id,
//                    'service_id' => request()->service_id,
//                ]);
//                foreach ($titles as $lang => $title) {
//                    if (isset($title) && $title != '') {
//                        $service_host_detail->service_host_detail_trans()->update([
//                            'service_host_detail_id' => $service_host_detail->id,
//                            'title' => $title,
//                            'text' => null,
//                            'lang' => $lang,
//                        ]);
//                    }
//                }
//            }
//        }
//    }
//    /* =============== languages =============== */
//
//    $service_host = ServiceHost::first();
//
//    toastr()->success('تم العملية بنجاح', setting('title'));
//    return redirect(aurl('service') . '/' . request()->service_id . '?service_host_id=' . $service_host->id);

    }//end of update function


//========================================= destroy =========================================
    public function delete_service_host_detail()
    {
        $id = \request()->get('id');
        $service_host_detail = ServiceHostDetail::findorfail($id);
        isset($service_host_detail->image) ? delete_image($service_host_detail->image) : '';
        $service_host_detail->delete();
        return response()->json(['id' => $id], 200);
    }

}
