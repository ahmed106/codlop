<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use App\Models\LanguageAllow;
use App\Models\Service;
use App\Models\ServiceTextTran;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

//models
use App\Models\ServiceText;

/* traits */

use App\Http\Traits\CheckAllowedLanguageKeys;
use App\Http\Traits\ViewLanguage;

class ServiceTextController extends Controller
{

    use CheckAllowedLanguageKeys, ViewLanguage;


    public $page_titles = [
        'index' => 'نصوص الخدمات',
        'create' => 'اضافة نص خدمة جديد',
        'edit' => '',
    ];

//========================================= index =========================================
    public function index()
    {
        $service_id = \request()->get('service_id');
        $service_texts = ServiceText::where([['service_id', '=', $service_id]])->orderBy('id', 'asc')->get();
        return view('admin.service_texts.index', ['service_texts' => $service_texts, 'page_titles' => $this->page_titles]);
    }

//========================================= create =========================================
    public function create()
    {
        $language_allows = LanguageAllow::get();
        return view('admin.service_texts.create', ['page_titles' => $this->page_titles, 'language_allows' => $language_allows]);
    }

//========================================= store =========================================
    public function store()
    {
        $validator = Validator::make(request()->all(), [
            'image' => 'nullable|image',
        ], [], []
        );

        if ($validator->fails()) {
            $errors = collect($validator->errors())->flatten(1);
            foreach ($errors as $error) {
                toastr()->warning($error, trans('admin.Message_title_attention'));
            }
            return back()->withInput();
        }

        $data = $validator->validated();

        /* =============== check languages =============== */
        if ((!is_array(request()->titles) || !$this->check_allowed_language_keys(request()->titles)) ||
            (!is_array(request()->texts) || !$this->check_allowed_language_keys(request()->texts))) {
            toastr()->warning('رجاء املاء جميع اللغات حتى لا يحدث مشكلة فيما بعد (title)', setting('title'));
            return back()->withInput();
        }
        /* =============== check languages =============== */

        if (request()->image != '') {
            $data['image'] = upload_image('service_texts', request()->image, 1, 'image', '');
        }

        $service = Service::findorfail(request()->get('service_id'));

        $data['service_id'] = isset($service) ? $service->id : null;
        $data['type'] = 'list';


        $service_text = ServiceText::create($data);

        /* =============== languages =============== */
        if (count(request()->titles) > 0) {
            foreach (request()->titles as $lang => $title) {
                ServiceTextTran::create([
                    'service_text_id' => $service_text->id,
                    'title' => $title,
                    'text' => request()->texts[$lang] ? request()->texts[$lang] : null,
                    'lang' => $lang,
                ]);
            }
        }
        /* =============== languages =============== */

        toastr()->success('تم العملية بنجاح', setting('title'));
        return redirect(aurl('service_text').'?service_id='.$service->id);
    }

//========================================= show =========================================
    public function show($id)
    {
        //
    }

//========================================= edit =========================================
    public function edit($id)
    {
        $service_text = ServiceText::findorfail($id);
        $language_allows = LanguageAllow::get();
        $title = ' تعديل بيانات ' . $service_text->title;
        $this->page_titles['edit'] = $title;
        $view_languages = $this->view_language($service_text->service_text_trans);
        return view('admin.service_texts.edit', ['service_text' => $service_text, 'language_allows' => $language_allows, 'view_languages' => $view_languages, 'page_titles' => $this->page_titles]);
    }

//========================================= update =========================================
    public function update_service_text()
    {
        $validator = Validator::make(request()->all(), [
            'image' => 'nullable|image',
        ], [], []
        );

        if ($validator->fails()) {
            $errors = collect($validator->errors())->flatten(1);
            foreach ($errors as $error) {
                toastr()->warning($error, trans('admin.Message_title_attention'));
            }
            return back()->withInput();
        }

        $data = $validator->validated();

        $service_text = ServiceText::findorfail(request()->service_text_id);

        /* =============== check languages =============== */
        if ((!is_array(request()->titles) || !$this->check_allowed_language_keys(request()->titles)) ||
            (!is_array(request()->texts) || !$this->check_allowed_language_keys(request()->texts))) {
            toastr()->warning('رجاء املاء جميع اللغات حتى لا يحدث مشكلة فيما بعد (title)', setting('title'));
            return back()->withInput();
        }
        /* =============== check languages =============== */

        if (request()->image != '') {
            $data['image'] = upload_image('service_texts', request()->image, 1, 'image', $service_text->image);
        }

        $service_text->update($data);

        /* =============== languages =============== */
        if (count(request()->titles) > 0) {
            ServiceTextTran::where([['service_text_id', '=', $service_text->id]])->delete();
            foreach (request()->titles as $lang => $title) {
                ServiceTextTran::create([
                    'service_text_id' => request()->service_text_id,
                    'title' => $title,
                    'text' => request()->texts[$lang] ? request()->texts[$lang] : null,
                    'lang' => $lang,
                ]);
            }
        }
        /* =============== languages =============== */

        toastr()->success('تم العملية بنجاح', setting('title'));
        return redirect(aurl('service_text').'?service_id='.$service_text->service_id);

    }

//========================================= destroy =========================================
    public function delete_service_text()
    {
        $id = \request()->get('id');
        $service_text = ServiceText::findorfail($id);
        isset($service_text->image) ? delete_image($service_text->image) : '';
        $service_text->delete();
        return response()->json(['id' => $id], 200);
    }

}
