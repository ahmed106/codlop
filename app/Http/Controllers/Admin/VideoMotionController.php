<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Traits\CheckAllowedLanguageKeys;
use App\Http\Traits\ViewLanguage;
use App\Models\LanguageAllow;
use App\Models\VideoMotion;
use App\Models\VideoMotionTran;
use Illuminate\Support\Facades\Validator;

//models

/* traits */

class VideoMotionController extends Controller
{

    use CheckAllowedLanguageKeys, ViewLanguage;


    public $page_titles = [
        'index' => 'اعمالنا',
        'create' => 'اضافة عمل جديد',
        'edit' => '',
    ];

//========================================= index =========================================
    public function index()
    {
        $video_motions = VideoMotion::orderBy('id', 'desc');


        if (request()->get('type')) {
            $video_motions = $video_motions->where('type', '=', request()->get('type'));
        }

        $video_motions = $video_motions->orderBy('id', 'desc')->get();

        return view('admin.video_motions.index', ['video_motions' => $video_motions, 'page_titles' => $this->page_titles]);
    }

//========================================= create =========================================
    public function create()
    {

        $language_allows = LanguageAllow::get();
        return view('admin.video_motions.create', ['page_titles' => $this->page_titles, 'language_allows' => $language_allows]);
    }

//========================================= store =========================================
    public function store()
    {


        $validator = Validator::make(request()->all(),
            [
                'image' => 'required|image',
                'video' => 'required|mimes:mp4,mov,ogg,qt',
                'type' => 'required|in:video_motions,mobile_application,web_design,stores',
                'website_link' => 'sometimes:nullable:url',
                'android_link' => 'sometimes:nullable:url',
                'ios_link' => 'sometimes:nullable:url',
            ], [], []
        );


        if ($validator->fails()) {
            $errors = collect($validator->errors())->flatten(1);
            foreach ($errors as $error) {
                toastr()->warning($error, trans('admin.Message_title_attention'));
            }
            return back()->withInput();
        }

        $data = $validator->validated();


        /* =============== check languages =============== */
        if ((!is_array(request()->titles) || !$this->check_allowed_language_keys(request()->titles)) ||
            (!is_array(request()->texts) || !$this->check_allowed_language_keys(request()->texts))) {
            toastr()->warning('رجاء املاء جميع اللغات حتى لا يحدث مشكلة فيما بعد', setting('title'));
            return back()->withInput();
        }
        /* =============== check languages =============== */

        if (request()->image != '') {
            $data['image'] = upload_image('video_motions', request()->image, 1, 'image', '');
        }

        if (request()->background != '') {
            $data['background'] = upload_image('video_motions', request()->background, 1, 'background', '');
        }

        if (request()->video != '') {
            $data['video'] = upload_image('video_motions', request()->video, 1, 'video', '');
        }


        $video_motion = VideoMotion::create($data);

        /* =============== languages =============== */
        if (count(request()->titles) > 0) {
            foreach (request()->titles as $lang => $title) {
                VideoMotionTran::create([
                    'video_motion_id' => $video_motion->id,
                    'title' => $title,
                    'text' => request()->texts[$lang] ? request()->texts[$lang] : null,
                    'lang' => $lang,
                ]);
            }
        }
        /* =============== languages =============== */

        toastr()->success('تم العملية بنجاح', setting('title'));
        return redirect(aurl('video_motion'));
    }

//========================================= show =========================================
    public function show($id)
    {
        //
    }

//========================================= edit =========================================
    public function edit($id)
    {
        $video_motion = VideoMotion::findorfail($id);
        $language_allows = LanguageAllow::get();
        $title = ' تعديل بيانات ' . $video_motion->title;
        $this->page_titles['edit'] = $title;
        $view_languages = $this->view_language($video_motion->video_motion_trans);
        return view('admin.video_motions.edit', ['video_motion' => $video_motion, 'language_allows' => $language_allows, 'view_languages' => $view_languages, 'page_titles' => $this->page_titles]);
    }

//========================================= update =========================================
    public function update_video_motion()
    {

        $validator = Validator::make(request()->all(),
            [
                'image' => 'nullable|image',
                'video' => 'nullable|mimes:mp4,mov,ogg,qt',
                'type' => 'required|in:video_motions,mobile_application,web_design,stores',
                'website_link' => 'sometimes:nullable:url',
                'android_link' => 'sometimes:nullable:url',
                'ios_link' => 'sometimes:nullable:url',
            ], [], []
        );

        if ($validator->fails()) {
            $errors = collect($validator->errors())->flatten(1);
            foreach ($errors as $error) {
                toastr()->warning($error, trans('admin.Message_title_attention'));
            }
            return back()->withInput();
        }

        $data = $validator->validated();

        $video_motion = VideoMotion::findorfail(request()->video_motion_id);

        /* =============== check languages =============== */
        if ((!is_array(request()->titles) || !$this->check_allowed_language_keys(request()->titles)) ||
            (!is_array(request()->texts) || !$this->check_allowed_language_keys(request()->texts))) {
            toastr()->warning('رجاء املاء جميع اللغات حتى لا يحدث مشكلة فيما بعد', setting('title'));
            return back()->withInput();
        }
        /* =============== check languages =============== */

        if (request()->image != '') {
            $data['image'] = upload_image('video_motions', request()->image, 1, 'image', $video_motion->image);
        }

        if (request()->background != '') {
            $data['background'] = upload_image('video_motions', request()->background, 1, 'background', $video_motion->background);
        }

        if (request()->video != '') {
            $data['video'] = upload_image('video_motions', request()->video, 1, 'video', $video_motion->video);
        }

        $video_motion->update($data);

        /* =============== languages =============== */
        if (count(request()->titles) > 0) {
            VideoMotionTran::where([['video_motion_id', '=', $video_motion->id]])->delete();
            foreach (request()->titles as $lang => $title) {
                VideoMotionTran::create([
                    'video_motion_id' => request()->video_motion_id,
                    'title' => $title,
                    'text' => request()->texts[$lang] ? request()->texts[$lang] : null,
                    'lang' => $lang,
                ]);
            }
        }
        /* =============== languages =============== */

        toastr()->success('تم العملية بنجاح', setting('title'));
        return redirect(aurl('video_motion'));

    }

//========================================= destroy =========================================
    public function delete_video_motion()
    {
        $id = \request()->get('id');
        $video_motion = VideoMotion::findorfail($id);
        isset($video_motion->video) ? delete_image($video_motion->video) : '';
        $video_motion->delete();
        return response()->json(['id' => $id], 200);
    }

}
