<?php


namespace App\Http\Traits;

use App\Models\LanguageAllow;

trait CheckAllowedLanguageKeys
{

    public function check_allowed_language_keys($inputs = [])
    {
        $LanguageAllowCodes = LanguageAllow::pluck('code')->toArray();
        foreach ($LanguageAllowCodes as $lang) {
            if (!isset($inputs[$lang])) {
                return 'false';
            }
        }
        return 'true';
    }

}//end class
