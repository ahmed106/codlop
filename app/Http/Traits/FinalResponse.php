<?php


namespace App\Http\Traits;


trait FinalResponse
{

    private function paginate_case($name = '', $obj_ = '')
    {
        $pagination_status = request()->get('pagination_status');
        $per_link_ = request()->get('per_link_');
        $orderBy = request()->get('orderBy');

        if (!isset($orderBy) || !in_array($orderBy, ['asc', 'desc'])) {
            $orderBy = 'desc';
        }

        $data = $obj_->orderBy('id', $orderBy)->get();

        if ($pagination_status == 'on') {
            if ($per_link_ != '' && is_numeric($per_link_) && $per_link_ > 0) {
                $data = $obj_->orderBy('id', $orderBy)->paginate($per_link_);
            } else {
                $data = $obj_->orderBy('id', $orderBy)->paginate(10);
            }
        }

        return $data;
    }

    private function final_response($data = null , $message = '' , $code = 200 , $in_two_response = 'yes')
    {
        $postman_code = 200;

        if ( $in_two_response == 'yes')
        {
            $postman_code = $code;
        }

        // if array
        $responseJson['data'] = collect( $data );

        if ( request()->get('pagination_status') == 'on')
        {
            $responseJson = collect( $data );
        }

        if ($data == null)
        {
            $responseJson['data']  = null;
        }

        $responseJson['message'] = $message;
        $responseJson['status'] = intval( $code );
        return response()->json($responseJson, $postman_code);
    }

}//end class
