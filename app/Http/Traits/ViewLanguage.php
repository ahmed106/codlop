<?php


namespace App\Http\Traits;


trait ViewLanguage
{

    public function view_language($json_trans = [])
    {
        $json_trans_format = [];

        foreach ($json_trans as $json_tran)
        {
            $json_trans_format[$json_tran->lang] = $json_tran;
        }
        return $json_trans_format;
    }

}//end trait
