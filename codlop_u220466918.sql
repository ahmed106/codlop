-- phpMyAdmin SQL Dump
-- version 4.9.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Dec 26, 2021 at 05:41 PM
-- Server version: 10.2.41-MariaDB
-- PHP Version: 7.3.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `codlop_u220466918`
--

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE `admins` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(300) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(300) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(300) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(300) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `group_id` int(11) DEFAULT NULL,
  `admin_type` varchar(300) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`id`, `name`, `email`, `password`, `image`, `group_id`, `admin_type`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'مصطفى محمد', 'admin@admin.com', '$2y$10$3XdZyuMBeY4aEGKnLXYAm.HfrnWAnFxpuF7pOmwFsdE9gWmsBXAxi', 'admins/ekU76eXmE5rgmDD8uH6Wzg7whh3uxBwHBa3gnjAd.jpeg', NULL, NULL, NULL, '2021-11-20 23:09:14', '2021-11-24 19:40:25'),
(2, 'Ahmed2021', 'ahmed@gmail.com', '$2y$10$stqw7nOZjkBxMOUeuuUOVOGyN.l4EMB5suVI3Dgh0of6QsqAieLaC', 'admins/U0mM90ohNgk7PoDHnShcMCBiQm8fwzkD8B9KuHWB.png', NULL, NULL, NULL, '2021-11-24 19:37:28', '2021-11-24 19:43:09'),
(4, 'user2 hady1', 'd21@d2.com', '$2y$10$Fmw//Bav.AzCByL1H61gWOplJ7yem.TRD2Nk5dFhlXg5swnwgJ4Me', 'admins/6BgdmJzJ5Lwz1o8lepdsN0BvT05e8bVmPgF7Nu3o.png', NULL, NULL, NULL, '2021-11-26 23:02:18', '2021-11-26 23:02:18');

-- --------------------------------------------------------

--
-- Table structure for table `blogs`
--

CREATE TABLE `blogs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `date` date DEFAULT NULL,
  `image` varchar(300) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `blogs`
--

INSERT INTO `blogs` (`id`, `date`, `image`, `created_at`, `updated_at`) VALUES
(1, '2021-11-15', 'blogs/fNQH8BqgSwiT3N8vYuZ0xNNWNFiJjosn7PoZky4Q.png', '2021-11-15 21:42:50', '2021-11-27 00:00:02'),
(2, '2021-11-16', 'blogs/ARK4PSeTzO7bqSyZmoEar9zhBo5Q6I9GLdOJYUIw.png', '2021-11-15 21:42:50', '2021-11-26 23:59:53'),
(3, '2021-11-16', 'blogs/tbCfcnqO7U0CaiVFek6tpGR6saifU75tuZKkSaCl.png', '2021-11-15 21:42:50', '2021-11-26 23:59:39'),
(5, '2021-11-17', 'blogs/m3MGrdPLZ3F2sq5z0qbRtVYkJSK45OKnrkuVECKQ.jpeg', '2021-11-24 20:11:42', '2021-11-24 20:11:42'),
(6, '2021-10-14', 'blogs/3rxP6Fekc7gJOM9BXN3TnyMZuOn0dMaQW3Gr718A.png', '2021-11-24 20:18:22', '2021-12-09 08:55:32'),
(7, '2021-10-14', 'blogs/O3ooVPkXv6NqtSAkzJI4XbKywxx8DJJs81mGfa5l.jpeg', '2021-11-24 20:18:34', '2021-12-09 08:56:44'),
(8, '2021-12-14', 'blogs/R55l4CLH9YnWIKzOUYeym88zXYFalWZdJKSNpW20.png', '2021-11-24 20:18:53', '2021-11-24 20:18:53'),
(9, '2021-12-14', 'blogs/oN1hR5cLg0CpiFCGjoyPLd88cEVZPWIORlZ4PbpE.png', '2021-11-24 20:19:08', '2021-11-24 20:19:08'),
(10, '2021-12-14', 'blogs/XToMX2X7DDIKlCkvORVN8Sf0q6rmNMChrGm9WXks.jpeg', '2021-11-24 20:19:17', '2021-12-09 08:57:43'),
(11, '2021-12-14', 'blogs/roqlVr7RmYaTgl99MCirY6t72BM4OMXQ02WANdXv.gif', '2021-11-24 20:20:53', '2021-11-24 20:20:53'),
(12, '2021-12-14', 'blogs/1E1LEEE9q65w5jyPd00CZwzjO1XW0KyiqULLaxsK.png', '2021-11-24 20:21:05', '2021-12-09 08:55:53');

-- --------------------------------------------------------

--
-- Table structure for table `blog_trans`
--

CREATE TABLE `blog_trans` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `blog_id` bigint(20) UNSIGNED DEFAULT NULL,
  `title` varchar(300) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `text` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lang` varchar(300) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `blog_trans`
--

INSERT INTO `blog_trans` (`id`, `blog_id`, `title`, `text`, `lang`, `created_at`, `updated_at`) VALUES
(8, 5, 'لوريم إيبسوم', 'هناك حقيقة مثبتة منذ زمن طويل وهي أن المحتوى المقروء لصفحة ما سيلهي القارئ عن التركيز على الشكل الخارجي للنص أو شكل توضع الفقرات في الصفحة التي يقرأها. ولذلك يتم استخدام طريقة لوريم إيبسوم لأنها تعطي توزيعاَ طبيعياَ -إلى حد ما- للأحرف عوضاً عن استخدام \"هنا يوجد محتوى نصي، هنا يوجد محتوى نصي\" فتجعلها تبدو (أي الأحرف) وكأنها نص مقروء. العديد من برامح النشر المكتبي وبرامح تحرير صفحات الويب تستخدم لوريم إيبسوم بشكل إفتراضي كنموذج عن النص، وإذا قمت بإدخال \"lorem ipsum\" في أي محرك بحث ستظهر العديد من المواقع الحديثة العهد في نتائج البحث. على مدى السنين ظهرت نسخ جديدة ومختلفة من نص لوريم إيبسوم، أحياناً عن طريق الصدفة، وأحياناً عن عمد كإدخال بعض العبارات الفكاهية إليها.', 'ar', '2021-11-24 20:11:42', '2021-11-24 20:11:42'),
(9, 5, 'Lorem ipsum', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'en', '2021-11-24 20:11:42', '2021-11-24 20:11:42'),
(14, 8, 'لوريم إيبسوم', 'هناك حقيقة مثبتة منذ زمن طويل وهي أن المحتوى المقروء لصفحة ما سيلهي القارئ عن التركيز على الشكل الخارجي للنص أو شكل توضع الفقرات في الصفحة التي يقرأها. ولذلك يتم استخدام طريقة لوريم إيبسوم لأنها تعطي توزيعاَ طبيعياَ -إلى حد ما- للأحرف عوضاً عن استخدام \"هنا يوجد محتوى نصي، هنا يوجد محتوى نصي\" فتجعلها تبدو (أي الأحرف) وكأنها نص مقروء. العديد من برامح النشر المكتبي وبرامح تحرير صفحات الويب تستخدم لوريم إيبسوم بشكل إفتراضي كنموذج عن النص، وإذا قمت بإدخال \"lorem ipsum\" في أي محرك بحث ستظهر العديد من المواقع الحديثة العهد في نتائج البحث. على مدى السنين ظهرت نسخ جديدة ومختلفة من نص لوريم إيبسوم، أحياناً عن طريق الصدفة، وأحياناً عن عمد كإدخال بعض العبارات الفكاهية إليها.', 'ar', '2021-11-24 20:18:53', '2021-11-24 20:18:53'),
(15, 8, 'Lorem ipsum', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'en', '2021-11-24 20:18:53', '2021-11-24 20:18:53'),
(16, 9, 'لوريم إيبسوم', 'هناك حقيقة مثبتة منذ زمن طويل وهي أن المحتوى المقروء لصفحة ما سيلهي القارئ عن التركيز على الشكل الخارجي للنص أو شكل توضع الفقرات في الصفحة التي يقرأها. ولذلك يتم استخدام طريقة لوريم إيبسوم لأنها تعطي توزيعاَ طبيعياَ -إلى حد ما- للأحرف عوضاً عن استخدام \"هنا يوجد محتوى نصي، هنا يوجد محتوى نصي\" فتجعلها تبدو (أي الأحرف) وكأنها نص مقروء. العديد من برامح النشر المكتبي وبرامح تحرير صفحات الويب تستخدم لوريم إيبسوم بشكل إفتراضي كنموذج عن النص، وإذا قمت بإدخال \"lorem ipsum\" في أي محرك بحث ستظهر العديد من المواقع الحديثة العهد في نتائج البحث. على مدى السنين ظهرت نسخ جديدة ومختلفة من نص لوريم إيبسوم، أحياناً عن طريق الصدفة، وأحياناً عن عمد كإدخال بعض العبارات الفكاهية إليها.', 'ar', '2021-11-24 20:19:08', '2021-11-24 20:19:08'),
(17, 9, 'Lorem ipsum', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'en', '2021-11-24 20:19:08', '2021-11-24 20:19:08'),
(20, 11, 'لوريم إيبسوم', 'هناك حقيقة مثبتة منذ زمن طويل وهي أن المحتوى المقروء لصفحة ما سيلهي القارئ عن التركيز على الشكل الخارجي للنص أو شكل توضع الفقرات في الصفحة التي يقرأها. ولذلك يتم استخدام طريقة لوريم إيبسوم لأنها تعطي توزيعاَ طبيعياَ -إلى حد ما- للأحرف عوضاً عن استخدام \"هنا يوجد محتوى نصي، هنا يوجد محتوى نصي\" فتجعلها تبدو (أي الأحرف) وكأنها نص مقروء. العديد من برامح النشر المكتبي وبرامح تحرير صفحات الويب تستخدم لوريم إيبسوم بشكل إفتراضي كنموذج عن النص، وإذا قمت بإدخال \"lorem ipsum\" في أي محرك بحث ستظهر العديد من المواقع الحديثة العهد في نتائج البحث. على مدى السنين ظهرت نسخ جديدة ومختلفة من نص لوريم إيبسوم، أحياناً عن طريق الصدفة، وأحياناً عن عمد كإدخال بعض العبارات الفكاهية إليها.', 'ar', '2021-11-24 20:20:53', '2021-11-24 20:20:53'),
(21, 11, 'Lorem ipsum', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'en', '2021-11-24 20:20:53', '2021-11-24 20:20:53'),
(26, 3, 'ما هو Laravel لارافيل ؟ تعرف على أهم اطار عمل لغة PHP', 'هناك حقيقة مثبتة منذ زمن طويل وهي أن المحتوى المقروء لصفحة ما سيلهي القارئ عن التركيز على الشكل الخارجي للنص أو شكل توضع الفقرات في الصفحة التي يقرأها. ولذلك يتم استخدام طريقة لوريم إيبسوم لأنها تعطي توزيعاَ طبيعياَ -إلى حد ما- للأحرف عوضاً عن استخدام \"هنا يوجد محتوى نصي، هنا يوجد محتوى نصي\" فتجعلها تبدو (أي الأحرف) وكأنها نص مقروء. العديد من برامح النشر المكتبي وبرامح تحرير صفحات الويب تستخدم لوريم إيبسوم بشكل إفتراضي كنموذج عن النص، وإذا قمت بإدخال \"lorem ipsum\" في أي محرك بحث ستظهر العديد من المواقع الحديثة العهد في نتائج البحث. على مدى السنين ظهرت نسخ جديدة ومختلفة من نص لوريم إيبسوم، أحياناً عن طريق الصدفة، وأحياناً عن عمد كإدخال بعض العبارات الفكاهية إليها.', 'ar', '2021-11-26 23:59:39', '2021-11-26 23:59:39'),
(27, 3, 'What is Laravel? Learn about the most important PHP framework', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 'en', '2021-11-26 23:59:39', '2021-11-26 23:59:39'),
(28, 2, 'أهمية دراسة تجربة المستخدم , كيف يمكن أن تؤثر على نتائج منتجك ؟', 'هناك حقيقة مثبتة منذ زمن طويل وهي أن المحتوى المقروء لصفحة ما سيلهي القارئ عن التركيز على الشكل الخارجي للنص أو شكل توضع الفقرات في الصفحة التي يقرأها. ولذلك يتم استخدام طريقة لوريم إيبسوم لأنها تعطي توزيعاَ طبيعياَ -إلى حد ما- للأحرف عوضاً عن استخدام \"هنا يوجد محتوى نصي، هنا يوجد محتوى نصي\" فتجعلها تبدو (أي الأحرف) وكأنها نص مقروء. العديد من برامح النشر المكتبي وبرامح تحرير صفحات الويب تستخدم لوريم إيبسوم بشكل إفتراضي كنموذج عن النص، وإذا قمت بإدخال \"lorem ipsum\" في أي محرك بحث ستظهر العديد من المواقع الحديثة العهد في نتائج البحث. على مدى السنين ظهرت نسخ جديدة ومختلفة من نص لوريم إيبسوم، أحياناً عن طريق الصدفة، وأحياناً عن عمد كإدخال بعض العبارات الفكاهية إليها.', 'ar', '2021-11-26 23:59:53', '2021-11-26 23:59:53'),
(29, 2, 'The importance of studying user experience, how can it affect the results of your product?', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 'en', '2021-11-26 23:59:53', '2021-11-26 23:59:53'),
(30, 1, 'افضل شركة تصميم مواقع في السعودية القصيم بريدة الرياض جدة', 'هناك حقيقة مثبتة منذ زمن طويل وهي أن المحتوى المقروء لصفحة ما سيلهي القارئ عن التركيز على الشكل الخارجي للنص أو شكل توضع الفقرات في الصفحة التي يقرأها. ولذلك يتم استخدام طريقة لوريم إيبسوم لأنها تعطي توزيعاَ طبيعياَ -إلى حد ما- للأحرف عوضاً عن استخدام \"هنا يوجد محتوى نصي، هنا يوجد محتوى نصي\" فتجعلها تبدو (أي الأحرف) وكأنها نص مقروء. العديد من برامح النشر المكتبي وبرامح تحرير صفحات الويب تستخدم لوريم إيبسوم بشكل إفتراضي كنموذج عن النص، وإذا قمت بإدخال \"lorem ipsum\" في أي محرك بحث ستظهر العديد من المواقع الحديثة العهد في نتائج البحث. على مدى السنين ظهرت نسخ جديدة ومختلفة من نص لوريم إيبسوم، أحياناً عن طريق الصدفة، وأحياناً عن عمد كإدخال بعض العبارات الفكاهية إليها.', 'ar', '2021-11-27 00:00:02', '2021-11-27 00:00:02'),
(31, 1, 'The best web design company in Saudi Arabia, Qassim, Buraydah, Riyadh, Jeddah', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 'en', '2021-11-27 00:00:02', '2021-11-27 00:00:02'),
(32, 6, 'لوريم إيبسوم', 'هناك حقيقة مثبتة منذ زمن طويل وهي أن المحتوى المقروء لصفحة ما سيلهي القارئ عن التركيز على الشكل الخارجي للنص أو شكل توضع الفقرات في الصفحة التي يقرأها. ولذلك يتم استخدام طريقة لوريم إيبسوم لأنها تعطي توزيعاَ طبيعياَ -إلى حد ما- للأحرف عوضاً عن استخدام \"هنا يوجد محتوى نصي، هنا يوجد محتوى نصي\" فتجعلها تبدو (أي الأحرف) وكأنها نص مقروء. العديد من برامح النشر المكتبي وبرامح تحرير صفحات الويب تستخدم لوريم إيبسوم بشكل إفتراضي كنموذج عن النص، وإذا قمت بإدخال \"lorem ipsum\" في أي محرك بحث ستظهر العديد من المواقع الحديثة العهد في نتائج البحث. على مدى السنين ظهرت نسخ جديدة ومختلفة من نص لوريم إيبسوم، أحياناً عن طريق الصدفة، وأحياناً عن عمد كإدخال بعض العبارات الفكاهية إليها.', 'ar', '2021-12-09 08:55:32', '2021-12-09 08:55:32'),
(33, 6, 'Lorem ipsum', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'en', '2021-12-09 08:55:32', '2021-12-09 08:55:32'),
(34, 12, 'لوريم إيبسوم', 'هناك حقيقة مثبتة منذ زمن طويل وهي أن المحتوى المقروء لصفحة ما سيلهي القارئ عن التركيز على الشكل الخارجي للنص أو شكل توضع الفقرات في الصفحة التي يقرأها. ولذلك يتم استخدام طريقة لوريم إيبسوم لأنها تعطي توزيعاَ طبيعياَ -إلى حد ما- للأحرف عوضاً عن استخدام \"هنا يوجد محتوى نصي، هنا يوجد محتوى نصي\" فتجعلها تبدو (أي الأحرف) وكأنها نص مقروء. العديد من برامح النشر المكتبي وبرامح تحرير صفحات الويب تستخدم لوريم إيبسوم بشكل إفتراضي كنموذج عن النص، وإذا قمت بإدخال \"lorem ipsum\" في أي محرك بحث ستظهر العديد من المواقع الحديثة العهد في نتائج البحث. على مدى السنين ظهرت نسخ جديدة ومختلفة من نص لوريم إيبسوم، أحياناً عن طريق الصدفة، وأحياناً عن عمد كإدخال بعض العبارات الفكاهية إليها.', 'ar', '2021-12-09 08:55:53', '2021-12-09 08:55:53'),
(35, 12, 'Lorem ipsum', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'en', '2021-12-09 08:55:53', '2021-12-09 08:55:53'),
(38, 7, 'لوريم إيبسوم', 'هناك حقيقة مثبتة منذ زمن طويل وهي أن المحتوى المقروء لصفحة ما سيلهي القارئ عن التركيز على الشكل الخارجي للنص أو شكل توضع الفقرات في الصفحة التي يقرأها. ولذلك يتم استخدام طريقة لوريم إيبسوم لأنها تعطي توزيعاَ طبيعياَ -إلى حد ما- للأحرف عوضاً عن استخدام \"هنا يوجد محتوى نصي، هنا يوجد محتوى نصي\" فتجعلها تبدو (أي الأحرف) وكأنها نص مقروء. العديد من برامح النشر المكتبي وبرامح تحرير صفحات الويب تستخدم لوريم إيبسوم بشكل إفتراضي كنموذج عن النص، وإذا قمت بإدخال \"lorem ipsum\" في أي محرك بحث ستظهر العديد من المواقع الحديثة العهد في نتائج البحث. على مدى السنين ظهرت نسخ جديدة ومختلفة من نص لوريم إيبسوم، أحياناً عن طريق الصدفة، وأحياناً عن عمد كإدخال بعض العبارات الفكاهية إليها.', 'ar', '2021-12-09 08:56:44', '2021-12-09 08:56:44'),
(39, 7, 'Lorem ipsum', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'en', '2021-12-09 08:56:44', '2021-12-09 08:56:44'),
(40, 10, 'لوريم إيبسوم', 'هناك حقيقة مثبتة منذ زمن طويل وهي أن المحتوى المقروء لصفحة ما سيلهي القارئ عن التركيز على الشكل الخارجي للنص أو شكل توضع الفقرات في الصفحة التي يقرأها. ولذلك يتم استخدام طريقة لوريم إيبسوم لأنها تعطي توزيعاَ طبيعياَ -إلى حد ما- للأحرف عوضاً عن استخدام \"هنا يوجد محتوى نصي، هنا يوجد محتوى نصي\" فتجعلها تبدو (أي الأحرف) وكأنها نص مقروء. العديد من برامح النشر المكتبي وبرامح تحرير صفحات الويب تستخدم لوريم إيبسوم بشكل إفتراضي كنموذج عن النص، وإذا قمت بإدخال \"lorem ipsum\" في أي محرك بحث ستظهر العديد من المواقع الحديثة العهد في نتائج البحث. على مدى السنين ظهرت نسخ جديدة ومختلفة من نص لوريم إيبسوم، أحياناً عن طريق الصدفة، وأحياناً عن عمد كإدخال بعض العبارات الفكاهية إليها.', 'ar', '2021-12-09 08:57:43', '2021-12-09 08:57:43'),
(41, 10, 'Lorem ipsum', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'en', '2021-12-09 08:57:43', '2021-12-09 08:57:43');

-- --------------------------------------------------------

--
-- Table structure for table `clients`
--

CREATE TABLE `clients` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `link` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(300) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `clients`
--

INSERT INTO `clients` (`id`, `link`, `image`, `created_at`, `updated_at`) VALUES
(1, 'https://www.google.com', 'clients/fqS0KiOn6E7Lr85t4xxu3nW5iAj5IqjWUrV8pVzf.png', '2021-12-13 18:42:05', '2021-12-13 19:45:36'),
(2, 'https://www.google.com', 'clients/lUXVe1OsOXt43A2nFHVlR0Iv2Zq4OlrLVowxyxml.png', '2021-12-13 18:42:19', '2021-12-13 19:45:13'),
(3, 'https://www.google.com', 'clients/irOuQh2cthvTjnVf7FyAwPj1LyB2FJSFP4Rv9pD3.png', '2021-12-13 19:46:30', '2021-12-13 19:46:30'),
(5, 'www.google.com', 'clients/wfbVQ365FdjA6fe9sGg3u2GHHjdzHbyP7zgLT3Er.jpeg', '2021-12-15 07:47:11', '2021-12-15 07:47:11');

-- --------------------------------------------------------

--
-- Table structure for table `client_trans`
--

CREATE TABLE `client_trans` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `client_id` bigint(20) UNSIGNED DEFAULT NULL,
  `title` varchar(300) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `text` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lang` varchar(300) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `confirm_emails`
--

CREATE TABLE `confirm_emails` (
  `id` bigint(20) NOT NULL,
  `email` varchar(500) COLLATE latin1_general_ci DEFAULT NULL,
  `code` varchar(500) COLLATE latin1_general_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Dumping data for table `confirm_emails`
--

INSERT INTO `confirm_emails` (`id`, `email`, `code`, `created_at`, `updated_at`) VALUES
(6, 'd2@d2.com', '65296', '2021-11-27 01:13:13', '2021-12-17 17:41:20'),
(7, 'd3332@d2.com', '78688', '2021-11-27 01:14:24', '2021-11-27 01:14:24'),
(8, 'devmostafa80@gmail.com', '83662', '2021-11-27 01:22:39', '2021-12-07 21:50:49'),
(9, 'd233@d2.com', '98524', '2021-11-27 01:28:08', '2021-11-27 01:30:28'),
(10, '7oda.elkomy@gmail.com', '109364', '2021-12-08 13:40:17', '2021-12-13 09:36:53'),
(11, 'alitest513@gmail.com', '116270', '2021-12-09 10:16:36', '2021-12-19 07:58:31'),
(12, 'abdohantash9999@gmail.com', '128531', '2021-12-19 08:00:59', '2021-12-19 08:00:59');

-- --------------------------------------------------------

--
-- Table structure for table `contacts`
--

CREATE TABLE `contacts` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(300) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone_code` varchar(300) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` bigint(20) UNSIGNED DEFAULT NULL,
  `email` varchar(300) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `subject` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `message` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_read` enum('read','unread') COLLATE utf8mb4_unicode_ci DEFAULT 'unread',
  `replay` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `contacts`
--

INSERT INTO `contacts` (`id`, `name`, `phone_code`, `phone`, `email`, `subject`, `message`, `is_read`, `replay`, `created_at`, `updated_at`) VALUES
(1, 'احمد على', '+20', 1022544554, 'm@m.com', 'الاستفسار عن الموقع', 'مجموعة من الجمل النموذجية، لتكوين نص لوريم إيبسوم ذو شكل منطقي قريب إلى النص الحقيقي. وبالتالي يكون النص الناتح خالي من التكرار، أو أي كلمات أو عبارات غير لائقة أو ما شابه. وهذا ما يجعله أول مولّد نص لوريم إيبسوم حقيقي على الإنترنت.', 'unread', NULL, '2021-11-17 01:07:09', '2021-11-17 01:07:09'),
(14, 'user2 hady', '+20', 1025130204, 'd2@d2.com', 'اسعار الموقع', 'هناك حقيقة مثبتة منذ زمن طويل وهي أن المحتوى المقروء لصفحة ما سيلهي القارئ عن التركيز على الشكل الخارجي للنص أو شكل توضع الفقرات في الصفحة التي يقرأها. ولذلك يتم استخدام طريقة لوريم إيبسوم لأنها تعطي توزيعاَ طبيعياَ -إلى حد ما- للأحرف عوضاً عن استخدام \"هنا يوجد محتوى نصي، هنا يوجد محتوى نصي\" فتجعلها تبدو (أي الأحرف) وكأنها نص مقروء. العديد من برامح النشر المكتبي وبرامح تحرير صفحات الويب تستخدم لوريم إيبسوم بشكل إفتراضي كنموذج عن النص، وإذا قمت بإدخال \"lorem ipsum\" في أي محرك بحث ستظهر العديد من المواقع الحديثة العهد في نتائج البحث. على مدى السنين ظهرت نسخ جديدة ومختلفة من نص لوريم إيبسوم، أحياناً عن طريق الصدفة، وأحياناً عن عمد كإدخال بعض العبارات الفكاهية إليها.', 'unread', NULL, '2021-11-19 03:18:04', '2021-11-19 03:18:04'),
(15, 'user2', '+20', 1025130288, 'd@d.com', 'عروض جديدة', 'القارئ عن التركيز على الشكل الخارجي للنص أو شكل توضع الفقرات في الصفحة التي يقرأها. ولذلك يتم استخدام طريقة لوريم إيبسوم لأنها تعطي توزيعاَ طبيعياَ -إلى حد ما- للأحرف عوضاً عن استخدام \"هنا يوجد محتوى نصي، هنا يوجد محتوى نصي\" فتجعلها تبدو (أي الأحرف) وكأنها نص مقروء. العديد من برامح النشر المكتبي وبرامح تحرير صفحات الويب تستخدم لوريم إيبسوم بشكل إفتراضي كنموذج عن النص، وإذا قمت بإدخال \"lorem ipsum\" في أي محرك بحث ستظهر العديد من المواقع الحديثة العهد في نتائج البحث. على مدى السنين ظهرت نسخ جديدة ومختلفة من نص لوريم إيبسوم، أحياناً عن طريق الصدفة، وأحياناً عن عمد كإدخال بعض العبارات الفكاهية إليها.', 'unread', NULL, '2021-11-21 02:50:30', '2021-11-21 02:50:30'),
(22, 'TimothyJaisa', '+966', 88565989114, 'you_me.complete@yahoo.com.hk', 'Make Artificial Intelligence bring you more $ 5966 in a day', 'A ready-made system for generating passive income from $ 7697 in a day >>>>>>>>>>>>>>>>>>>>>>>>>>> https://www.google.com/url?q=https%3A%2F%2Fvk.cc%2Fc8ED7K&sa=D&26=21&usg=AFQjCNEY3K5BsQ-mUq_FMcp6hGUAytb7Og <<<<<<<<<<<<<<<<<<<<<<<<', 'unread', NULL, '2021-12-05 20:02:46', '2021-12-05 20:02:46'),
(23, 'Mike Ferguson', '+966', 81697719376, 'no-replyJAKABURO@gmail.com', 'Get more dofollow backlinks for codlop.com', 'Hello \r\n \r\nWe all know the importance that dofollow link have on any website`s ranks. \r\nHaving most of your linkbase filled with nofollow ones is of no good for your ranks and SEO metrics. \r\n \r\nBuy quality dofollow links from us, that will impact your ranks in a positive way \r\nhttps://www.digital-x-press.com/product/150-dofollow-backlinks/ \r\n \r\nBest regards \r\nMike Ferguson\r\n \r\nsupport@digital-x-press.com', 'unread', NULL, '2021-12-06 20:26:53', '2021-12-06 20:26:53'),
(24, 'TimothyJaisa', '+966', 83834412149, 'l.o.v.e._marie@web.de', 'Schnelles Geld von 153000 EUR pro Tag', 'Deine Freunde verdienen bereits Vor 185000 EUR pro Tag >>>>>>>>>>>>>>>>>>>>>>>>>>> http://www.google.com/url?q=http%3A%2F%2Fgo.nihawiwa.com%2F0bnl&sa=D&35=37&usg=AFQjCNEUPijOi1LFtA_rg0rU0qfVGTKd6A <<<<<<<<<<<<<<<<<<<<<<<<', 'unread', NULL, '2021-12-08 02:33:50', '2021-12-08 02:33:50'),
(25, 'محمود', '+20', 1066816711, 'sldsalkdaslk@gmail.com', 'تجربة الدعم الفني', 'تجربة الدعم الفني تجربة الدعم الفنيتجربة الدعم الفنيتجربة الدعم الفنيتجربة الدعم الفنيتجربة الدعم الفنيتجربة الدعم الفني', 'unread', NULL, '2021-12-09 10:13:38', '2021-12-09 10:13:38'),
(26, 'TimothyJaisa', '+966', 85685631642, 'tait.frier@fullline.ca', 'Quit your job and get passive income from $ 7897 per day', 'REGISTER NOW and get more $ 5986 in a day >>>>>>>>>>>>>>>>>>>>>>>>>>> https://www.google.com/url?q=https%3A%2F%2Fvk.cc%2Fc8Prmu&sa=D&73=51&usg=AFQjCNH_EGwAiiB8MpWHxZlE1C27oj3Rvw <<<<<<<<<<<<<<<<<<<<<<<<', 'unread', NULL, '2021-12-09 16:56:45', '2021-12-09 16:56:45'),
(27, 'TimothyJaisa', '+966', 83162912879, 'leon.mueller@gmx.ch', 'Launch Artificial Intelligence with one button and earn more $ 8959 in a day', 'Launch Artificial Intelligence with one button and earn from $ 6957 in a day >>>>>>>>>>>>>>>>>>>>>>>>>>> https://www.google.com/url?q=https%3A%2F%2Fvk.cc%2Fc8Prmu&sa=D&21=21&usg=AFQjCNH_EGwAiiB8MpWHxZlE1C27oj3Rvw <<<<<<<<<<<<<<<<<<<<<<<<', 'unread', NULL, '2021-12-10 16:19:40', '2021-12-10 16:19:40'),
(28, 'TimothyJaisa', '+966', 87825225578, 'svenja.iseli@bluemail.ch', 'Change your life and get passive income from $ 8959 in a day', 'Bitcoin Miiliarder told how he makes money from $ 7868 in a day >>>>>>>>>>>>>>>>>>>>>>>>>>> https://www.google.com/url?q=https%3A%2F%2Fvk.cc%2Fc8Prmu&sa=D&19=19&usg=AFQjCNH_EGwAiiB8MpWHxZlE1C27oj3Rvw <<<<<<<<<<<<<<<<<<<<<<<<', 'unread', NULL, '2021-12-11 11:14:36', '2021-12-11 11:14:36'),
(29, 'Kirillkyw', '+966', 87229483528, 'ro.b.e.rt.st.o.l.t.e.nber.gb.i.g@gmail.com', 'Какая сегодня погода?', 'Как вы прогнозируете погоду? \r\nЛИчно я смотрю вечером на закат, но если ничего не вижу, тогда иду на сайт https://www.gismeteo.ua/ и там получаю весь прогнозируете. И было несколько раз когда сайт был не прав. \r\nТак что доверй, но проверяй.', 'unread', NULL, '2021-12-13 06:28:42', '2021-12-13 06:28:42'),
(30, 'Mike Bawerman', '+966', 88334624573, 'no-replyJAKABURO@gmail.com', 'DA60+ for codlop.com with -35%', 'Greetings \r\n \r\nDo you want a quick boost in ranks and sales for your website? \r\nHaving a high DA score, always helps \r\n \r\nApply this -35% code ( MEGAPROMOTER ) while getting your codlop.com to have a DA above 60 points in Moz with us today and reap the benefits of such a great feat at an affordable rate. \r\n \r\n \r\n \r\nSee our offers here: \r\nhttps://www.strictlydigital.net/product/moz-da50-seo-plan/ \r\nhttps://www.strictlydigital.net/product/moz-da60-seo-plan/ \r\n \r\nNEW: ahrefs DR70 is now possible: \r\nhttps://www.strictlydigital.net/product/ahrefs-seo-plan/ \r\n \r\n \r\nThank you \r\nMike Bawerman\r\n \r\nmike@strictlydigital.net', 'unread', NULL, '2021-12-13 07:29:55', '2021-12-13 07:29:55'),
(31, 'TimothyJaisa', '+966', 88755879765, 'thierry.bretscher@postmail.ch', 'REGISTER NOW and get more $ 6758 in a day', 'Passive income more $ 6888 in a day >>>>>>>>>>>>>>>>>>>>>>>>>>> https://www.google.com/url?q=https%3A%2F%2Fvk.cc%2Fc8Prmu&sa=D&99=54&usg=AFQjCNH_EGwAiiB8MpWHxZlE1C27oj3Rvw <<<<<<<<<<<<<<<<<<<<<<<<', 'unread', NULL, '2021-12-13 09:33:01', '2021-12-13 09:33:01'),
(32, 'Im Holly. My sexy photo here https://hamejule.page.link/7bTe3wthYzZwftaC7#photo_id427624', '+966', 83135769313, 'fluo@hotmail.com', 'Im Penelope. My sexy photo here https://calaziv.page.link/FYR5CaTDwt7LtXpj7#photo_id682874', 'Im Roxana. My sexy photo here https://zuqegubu.page.link/PhHGxCRonxZ9W6NW9#photo_id141148', 'unread', NULL, '2021-12-13 13:44:22', '2021-12-13 13:44:22'),
(33, 'TimothyJaisa', '+966', 83229736131, 'ruth.kluser@jetaviation.ch', 'REGISTER NOW and get more $ 5669 in a day', 'REGISTER NOW and get from $ 6586 in a day >>>>>>>>>>>>>>>>>>>>>>>>>>> http://www.google.com/url?q=http%3A%2F%2Fgestyy.com%2Fep9CR9&sa=D&08=57&usg=AFQjCNHxHb5q4cvsPaKvJfESagxfB_AFOw <<<<<<<<<<<<<<<<<<<<<<<<', 'unread', NULL, '2021-12-14 22:17:22', '2021-12-14 22:17:22'),
(34, 'TimothyJaisa', '+966', 83327818615, 'isbestellungen@ggaweb.ch', 'Register, press one button and get passive income more $ 8896 per day', 'Passive income on the rise and fall of bitcoin from $ 8978 in a day >>>>>>>>>>>>>>>>>>>>>>>>>>> http://www.google.com/url?q=http%3A%2F%2Fgo.nirulugo.com%2F0bnl&sa=D&22=43&usg=AFQjCNErknOO8eaNhDQCQiKaUi6wsp9KfA <<<<<<<<<<<<<<<<<<<<<<<<', 'unread', NULL, '2021-12-15 12:42:43', '2021-12-15 12:42:43'),
(35, 'Im Bridgette. My sexy photo here https://zuqegubu.page.link/Ri61tyg8FzkcZmuf7#photo_id621418', '+966', 88453272468, '2929wycliff@allresco.com', 'Im Mercedes. My sexy photo here https://zyjisa.page.link/cqgXRirhMc3xA3Vw7#photo_id687112', 'Im Roberta. My sexy photo here https://calaziv.page.link/k4eMt3xKGM3AHRh76#photo_id925627', 'unread', NULL, '2021-12-16 01:57:11', '2021-12-16 01:57:11'),
(36, 'TimothyJaisa', '+966', 86299974594, 'carmi112@freenet.de', 'Fast income from one investment more $ 8585 in a day', 'Make Artificial Intelligence bring you more $ 7598 in a day >>>>>>>>>>>>>>>>>>>>>>>>>>> http://www.google.com/url?q=http%3A%2F%2Fgo.nirulugo.com%2F0bnl&sa=D&32=52&usg=AFQjCNErknOO8eaNhDQCQiKaUi6wsp9KfA <<<<<<<<<<<<<<<<<<<<<<<<', 'read', 'asd asd asd as d', '2021-12-16 14:04:02', '2021-12-18 16:43:44'),
(37, 'TimothyJaisa', '+966', 88359353967, 'ghettoasterix@aol.de', 'Fast income from one investment from $ 8879 in a day', 'REGISTER NOW and get more $ 7866 in a day >>>>>>>>>>>>>>>>>>>>>>>>>>> http://www.google.com/url?q=http%3A%2F%2Fgo.nirulugo.com%2F0bnl&sa=D&12=11&usg=AFQjCNErknOO8eaNhDQCQiKaUi6wsp9KfA <<<<<<<<<<<<<<<<<<<<<<<<', 'read', 'asd asd asd', '2021-12-17 14:09:30', '2021-12-18 16:43:10'),
(38, 'Nils Thomas', '+966', 87432371228, 'no-replyJAKABURO@gmail.com', 'Mailing via the feedback form.', 'Hi!  codlop.com \r\n \r\nWe present \r\n \r\nSending your commercial proposal through the Contact us form which can be found on the sites in the Communication section. Contact form are filled in by our program and the captcha is solved. The profit of this method is that messages sent through feedback forms are whitelisted. This method raise the probability that your message will be read. \r\n \r\nOur database contains more than 27 million sites around the world to which we can send your message. \r\n \r\nThe cost of one million messages 49 USD \r\n \r\nFREE TEST mailing Up to 50,000 messages. \r\n \r\n \r\nThis message is created automatically.  Use our contacts for communication. \r\n \r\nContact us. \r\nTelegram - @FeedbackMessages \r\nSkype  live:contactform_18 \r\nWhatsApp - +375259112693 \r\nWe only use chat.', 'read', 'asd asd asd ad asd asd asd asd', '2021-12-17 21:42:48', '2021-12-18 16:41:54'),
(39, 'Im Kristina. My sexy photo here https://fasuqen.page.link/gLvrshQigDUVqf589#photo_id336447', '+966', 84735127599, 'massimos@comcast.net', 'Im Kristina. My sexy photo here https://butewu.page.link/XgaEACndt9wt5TubA#photo_id579587', 'Im Joanna. My sexy photo here https://symupaxo.page.link/34m2EuKQaz8z6J1p6#photo_id155654', 'read', 'asd as dasd', '2021-12-18 00:36:47', '2021-12-18 16:38:18'),
(40, 'TimothyJaisa', '+966', 84562133141, 'araldo77@libero.it', 'Fast income from one investment more $ 6999 in a day', 'Register, press one button and get passive income from $ 9859 per day >>>>>>>>>>>>>>>>>>>>>>>>>>> http://www.google.com/url?q=http%3A%2F%2Fgo.nirulugo.com%2F0bnl&sa=D&01=01&usg=AFQjCNErknOO8eaNhDQCQiKaUi6wsp9KfA <<<<<<<<<<<<<<<<<<<<<<<<', 'read', 'لوريم ايبسوم دولار سيت أميت ,كونسيكتيتور أدايبا يسكينج أليايت,سيت دو أيوسمود تيمبور\r\n', '2021-12-18 13:31:50', '2021-12-18 13:31:50'),
(41, 'محمود الكومي', '+20', 1551059361, '7oda.elkomy@gmail.com', 'تجربة لعمل اتصل بنا', 'لبللبام ككملب كمل كملباكم لاكملب كال مكبل اككملبامكي يكملاملياكم كملاكمبل اكيكبلكميبا كطيلا كميككطكطي الكطاي كطكطكطل', 'read', 'شكرا على تواصلك معنا', '2021-12-19 07:42:47', '2021-12-19 07:43:48'),
(42, 'TimothyJaisa', '+966', 89441971829, 'libri@mediabooks.it', 'Passive income on the rise and fall of bitcoin from $ 9786 per day', 'Launch Artificial Intelligence with one button and earn from $ 8588 in a day >>>>>>>>>>>>>>>>>>>>>>>>>>> http://www.google.com/url?q=http%3A%2F%2Fgo.nirulugo.com%2F0bnl&sa=D&19=18&usg=AFQjCNErknOO8eaNhDQCQiKaUi6wsp9KfA <<<<<<<<<<<<<<<<<<<<<<<<', 'unread', NULL, '2021-12-19 12:50:24', '2021-12-19 12:50:24'),
(43, 'Veila', '+966', 82618377564, '17nsp409@icloud.com', 'I promised.', 'Hi, this is Anna. I am sending you my intimate photos as I promised. https://tinyurl.com/y3tyryq3', 'unread', NULL, '2021-12-20 05:01:13', '2021-12-20 05:01:13'),
(44, 'Mike Gerald', '+966', 85462246944, 'no-replyJAKABURO@gmail.com', 'Local SEO for more business', 'Howdy \r\n \r\nWe will improve your Local Ranks organically and safely, using only whitehat methods, while providing Google maps and website offsite work at the same time. \r\n \r\nPlease check our services below, we offer Local SEO at cheap rates. \r\nhttps://speed-seo.net/product/local-seo-package/ \r\n \r\nNEW: \r\nhttps://www.speed-seo.net/product/zip-codes-gmaps-citations/ \r\n \r\nregards \r\nMike Gerald\r\n \r\nSpeed SEO Digital Agency', 'unread', NULL, '2021-12-20 13:52:50', '2021-12-20 13:52:50'),
(45, 'Im Bridgette. My sexy photo here https://watynot.page.link/FnR1vz38wDjRRLQo9#photo_id319555', '+966', 86117678232, 'shandalest.clair27@yahoo.com', 'Im Esther. My sexy photo here https://vaxaco.page.link/NjYM8SSssYt8FoJLA#photo_id758988', 'Im Tina. My sexy photo here https://pacifon.page.link/dtQD53ABeQeYgSMY7#photo_id522677', 'unread', NULL, '2021-12-20 14:55:56', '2021-12-20 14:55:56'),
(46, 'TimothyJaisa', '+966', 82555844625, 'andreas.mueller2@t-online.de', 'Schnelles Geld Vor 11300 EUR pro Tag', 'Anfangen zu verdienen von 15900 EUR pro Tag >>>>>>>>>>>>>>>>>>>>>>>>>>> https://www.google.com/url?q=https%3A%2F%2Fvk.cc%2Fc9jD1p&sa=D&63=82&usg=AFQjCNE5-l7dYtpQNDLSz9x2TTehrGGJ2w <<<<<<<<<<<<<<<<<<<<<<<<', 'unread', NULL, '2021-12-21 12:53:22', '2021-12-21 12:53:22'),
(47, 'Veila', '+966', 83814911176, '6dz1lkig@hotmail.com', 'I promised.', 'Hi, this is Jenny. I am sending you my intimate photos as I promised. https://tinyurl.com/y5tn97e3', 'unread', NULL, '2021-12-21 15:21:08', '2021-12-21 15:21:08'),
(48, 'JAMES COOK', '+966', 89642135641, 'james_cook78@yahoo.com', 'Loan @ 3%', 'Dear sir/ma \r\nWe are a finance and investment company offering loans at 3% interest rate. We will be happy to make a loan available to your organisation for your project. Our terms and conditions will apply. Our term sheet/loan agreement will be sent to you for review, when we hear from you. Please reply to this email ONLY cookj5939@gmail.com \r\n \r\nRegards. \r\nJames Cook \r\nChairman & CEO Euro Finance & Commercial Ltd', 'unread', NULL, '2021-12-21 22:50:56', '2021-12-21 22:50:56'),
(49, 'Issacses', '+966', 86444177483, 'sarah@fisadco.co.uk', '%%!= Quick earnings from $ 1500 per week )%№&', 'Hello. \r\nIf you are interested in this type of earnings, then you should know that investments of $ 500 are needed here once %-)- \r\nFurther, you will receive from $ 1500 per week %&*# \r\n>>>>> https://www.google.com/url?q=https%3A%2F%2Fvk.cc%2Fc9jD1p&sa=D&60=21&usg=AFQjCNE5-l7dYtpQNDLSz9x2TTehrGGJ2w <<<<< \r\nThe essence of earning is to generate income from trading cryptocurrencies in an automatic mode, even if all markets collapse, you will be guaranteed to receive your money from $ 1500 per week. \r\nManage to take your place in the sun and register *#!^ \r\nLimited offer *^!@ \r\n>>>>> https://www.google.com/url?q=https%3A%2F%2Fvk.cc%2Fc9jD1p&sa=D&51=03&usg=AFQjCNE5-l7dYtpQNDLSz9x2TTehrGGJ2w <<<<<', 'unread', NULL, '2021-12-23 14:42:17', '2021-12-23 14:42:17'),
(50, 'Im Ursula. My sexy photo here https://nyrefig.page.link/hSvxCoQ9osZ9KFSSA#photo_id516242', '+966', 84224381952, 'sharonweckwerth@yahoo.com', 'Im Esther. My sexy photo here https://sogysenil.page.link/J6RbkNizNzy4eZBR6#photo_id511482', 'Im Ursula. My sexy photo here https://varuqep.page.link/t1oDGFkoo9tA4Gks8#photo_id123195', 'unread', NULL, '2021-12-23 21:03:14', '2021-12-23 21:03:14'),
(51, 'Veila', '+966', 81432764222, '0m4xwgnz@hotmail.com', 'I promised.', 'Hi, this is Jeniffer. I am sending you my intimate photos as I promised. https://tinyurl.com/y5qfeb2h', 'unread', NULL, '2021-12-24 19:41:39', '2021-12-24 19:41:39'),
(52, 'Issacses', '+966', 84699186586, 'bee7@hotmail.co.uk', '№^;+ Quick earnings from $ 1500 per week \"*\"=', 'Hello. \r\nIf you are interested in this type of earnings, then you should know that investments of $ 500 are needed here once (#;- \r\nFurther, you will receive from $ 1500 per week :=;+ \r\n>>>>> https://www.google.com/url?q=https%3A%2F%2Fvk.cc%2Fc9jD1p&sa=D&57=83&usg=AFQjCNE5-l7dYtpQNDLSz9x2TTehrGGJ2w <<<<< \r\nThe essence of earning is to generate income from trading cryptocurrencies in an automatic mode, even if all markets collapse, you will be guaranteed to receive your money from $ 1500 per week. \r\nManage to take your place in the sun and register ;^%) \r\nLimited offer \"-:^ \r\n>>>>> https://www.google.com/url?q=https%3A%2F%2Fvk.cc%2Fc9jD1p&sa=D&67=42&usg=AFQjCNE5-l7dYtpQNDLSz9x2TTehrGGJ2w <<<<<', 'unread', NULL, '2021-12-24 20:11:31', '2021-12-24 20:11:31');

-- --------------------------------------------------------

--
-- Table structure for table `contract_advantages`
--

CREATE TABLE `contract_advantages` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `image` varchar(300) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `contract_advantages`
--

INSERT INTO `contract_advantages` (`id`, `image`, `created_at`, `updated_at`) VALUES
(1, 'contract_advantages/KAbzC4s1S4fFSAivkmTKd6Vq1hP06y4jGV6IatWD.png', '2021-11-14 21:54:53', '2021-11-26 23:47:35');

-- --------------------------------------------------------

--
-- Table structure for table `contract_advantage_details`
--

CREATE TABLE `contract_advantage_details` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `contract_advantage_id` bigint(20) UNSIGNED DEFAULT NULL,
  `icon` varchar(300) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `contract_advantage_details`
--

INSERT INTO `contract_advantage_details` (`id`, `contract_advantage_id`, `icon`, `created_at`, `updated_at`) VALUES
(61, 1, 'contract_details/65U9Xk4LJO7st5utVy2EwjURTCX0apRFzZwI1Now.png', '2021-11-26 23:49:48', '2021-11-26 23:49:48'),
(62, 1, 'contract_details/be425t6T4Whzyl7UxCKq8QFkwSN7dVCTeZTXCar2.png', '2021-11-26 23:51:01', '2021-11-26 23:51:01'),
(63, 1, 'contract_details/xGezooOgQQjrJZdjnWBr8iBoDT8LYy3ZPzxB0YmD.png', '2021-11-26 23:51:27', '2021-11-26 23:51:27');

-- --------------------------------------------------------

--
-- Table structure for table `contract_advantage_detail_trans`
--

CREATE TABLE `contract_advantage_detail_trans` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `contract_detail_id` bigint(20) UNSIGNED DEFAULT NULL,
  `title` varchar(300) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `text` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lang` varchar(300) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `contract_advantage_detail_trans`
--

INSERT INTO `contract_advantage_detail_trans` (`id`, `contract_detail_id`, `title`, `text`, `lang`, `created_at`, `updated_at`) VALUES
(87, 61, 'دعم فني متواصل', 'لدينا طاقم من أفضل المهندسين المحترفين والفنيين التقنيين في هذا المجال, مستعدون لمساعدتك دائماً.', 'ar', '2021-11-26 23:49:48', '2021-11-26 23:49:48'),
(88, 61, 'Ongoing technical support', 'We have a staff of the best professional engineers and technical technicians in this field, always ready to help you.', 'en', '2021-11-26 23:49:48', '2021-11-26 23:49:48'),
(89, 62, 'دعم البرمجيات المختلفة', 'نلتزم بدعم موقعك مجاناً حيث نقوم بتركيب وتحديث وحماية برمجيات السيرفر الأساسية وإعدادها بشكل دوري .', 'ar', '2021-11-26 23:51:01', '2021-11-26 23:51:01'),
(90, 62, 'Various software support', 'We are committed to supporting your site for free, as we install, update and protect the basic server software and prepare it periodically.', 'en', '2021-11-26 23:51:01', '2021-11-26 23:51:01'),
(91, 63, 'الدقة والإحترافية', 'لا ينتهي عملنا حتى نتأكد من رضائك عن الخدمات التي نقدمها لك تماماً , نحن نضمن لك الجودة العالية والخدمة الموثوقة وبأسعار مناسبة .', 'ar', '2021-11-26 23:51:27', '2021-11-26 23:51:27'),
(92, 63, 'Accuracy and professionalism', 'Our work does not end until we make sure that you are completely satisfied with the services we provide to you. We guarantee you high quality and reliable service at reasonable prices.', 'en', '2021-11-26 23:51:27', '2021-11-26 23:51:27');

-- --------------------------------------------------------

--
-- Table structure for table `contract_advantage_trans`
--

CREATE TABLE `contract_advantage_trans` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `contract_advantage_id` bigint(20) UNSIGNED DEFAULT NULL,
  `title` varchar(300) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `text` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lang` varchar(300) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `contract_advantage_trans`
--

INSERT INTO `contract_advantage_trans` (`id`, `contract_advantage_id`, `title`, `text`, `lang`, `created_at`, `updated_at`) VALUES
(21, 1, 'أهم مميزات التعاقد معنا', 'فى كود لوب نسعي إلى الارتقاء بمجال تكنولوجيا المعلومات من خلال تقديم تصميمات وبرمجيات فريدة من نوعها والتي تتناسب مع جميع الأفراد والمؤسسات بأعلى جودة وفي أسرع وقت ممكن\r\nلنلبي جميع متطلباتكم ونحصل على إرضاء جميع العملاء .', 'ar', '2021-12-12 10:26:46', '2021-12-12 10:26:46'),
(22, 1, 'The most important advantages of contracting with us', 'At CodLop, we seek to advance the field of information technology by providing unique designs and software that suit all individuals and institutions with the highest quality and as quickly as possible to meet all your requirements and obtain the satisfaction of all customers.', 'en', '2021-12-12 10:26:46', '2021-12-12 10:26:46');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `firebase_tokens`
--

CREATE TABLE `firebase_tokens` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `phone_token` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `software_type` enum('ios','android','web') COLLATE utf8mb4_unicode_ci DEFAULT 'android',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `languages`
--

CREATE TABLE `languages` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `code` varchar(300) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(300) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `native_name` varchar(300) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `languages`
--

INSERT INTO `languages` (`id`, `code`, `name`, `native_name`, `created_at`, `updated_at`) VALUES
(1, 'ab', 'Abkhaz', 'аҧсуа', '2021-11-14 19:46:23', '2021-11-14 19:46:23'),
(2, 'aa', 'Afar', 'Afaraf', '2021-11-14 19:46:23', '2021-11-14 19:46:23'),
(3, 'af', 'Afrikaans', 'Afrikaans', '2021-11-14 19:46:23', '2021-11-14 19:46:23'),
(4, 'ak', 'Akan', 'Akan', '2021-11-14 19:46:23', '2021-11-14 19:46:23'),
(5, 'sq', 'Albanian', 'Shqip', '2021-11-14 19:46:23', '2021-11-14 19:46:23'),
(6, 'am', 'Amharic', 'አማርኛ', '2021-11-14 19:46:23', '2021-11-14 19:46:23'),
(7, 'ar', 'Arabic', 'العربية', '2021-11-14 19:46:23', '2021-11-14 19:46:23'),
(8, 'an', 'Aragonese', 'Aragonés', '2021-11-14 19:46:23', '2021-11-14 19:46:23'),
(9, 'hy', 'Armenian', 'Հայերեն', '2021-11-14 19:46:23', '2021-11-14 19:46:23'),
(10, 'as', 'Assamese', 'অসমীয়া', '2021-11-14 19:46:23', '2021-11-14 19:46:23'),
(11, 'av', 'Avaric', 'авар мацӀ, магӀарул мацӀ', '2021-11-14 19:46:23', '2021-11-14 19:46:23'),
(12, 'ae', 'Avestan', 'avesta', '2021-11-14 19:46:23', '2021-11-14 19:46:23'),
(13, 'ay', 'Aymara', 'aymar aru', '2021-11-14 19:46:23', '2021-11-14 19:46:23'),
(14, 'az', 'Azerbaijani', 'azərbaycan dili', '2021-11-14 19:46:23', '2021-11-14 19:46:23'),
(15, 'bm', 'Bambara', 'bamanankan', '2021-11-14 19:46:23', '2021-11-14 19:46:23'),
(16, 'ba', 'Bashkir', 'башҡорт теле', '2021-11-14 19:46:23', '2021-11-14 19:46:23'),
(17, 'eu', 'Basque', 'euskara, euskera', '2021-11-14 19:46:23', '2021-11-14 19:46:23'),
(18, 'be', 'Belarusian', 'Беларуская', '2021-11-14 19:46:23', '2021-11-14 19:46:23'),
(19, 'bn', 'Bengali', 'বাংলা', '2021-11-14 19:46:23', '2021-11-14 19:46:23'),
(20, 'bh', 'Bihari', 'भोजपुरी', '2021-11-14 19:46:23', '2021-11-14 19:46:23'),
(21, 'bi', 'Bislama', 'Bislama', '2021-11-14 19:46:23', '2021-11-14 19:46:23'),
(22, 'bs', 'Bosnian', 'bosanski jezik', '2021-11-14 19:46:23', '2021-11-14 19:46:23'),
(23, 'br', 'Breton', 'brezhoneg', '2021-11-14 19:46:23', '2021-11-14 19:46:23'),
(24, 'bg', 'Bulgarian', 'български език', '2021-11-14 19:46:23', '2021-11-14 19:46:23'),
(25, 'my', 'Burmese', 'ဗမာစာ', '2021-11-14 19:46:23', '2021-11-14 19:46:23'),
(26, 'ca', 'Catalan; Valencian', 'Català', '2021-11-14 19:46:23', '2021-11-14 19:46:23'),
(27, 'ch', 'Chamorro', 'Chamoru', '2021-11-14 19:46:23', '2021-11-14 19:46:23'),
(28, 'ce', 'Chechen', 'нохчийн мотт', '2021-11-14 19:46:23', '2021-11-14 19:46:23'),
(29, 'ny', 'Chichewa; Chewa; Nyanja', 'chiCheŵa, chinyanja', '2021-11-14 19:46:23', '2021-11-14 19:46:23'),
(30, 'zh', 'Chinese', '中文 (Zhōngwén), 汉语, 漢語', '2021-11-14 19:46:23', '2021-11-14 19:46:23'),
(31, 'cv', 'Chuvash', 'чӑваш чӗлхи', '2021-11-14 19:46:23', '2021-11-14 19:46:23'),
(32, 'kw', 'Cornish', 'Kernewek', '2021-11-14 19:46:23', '2021-11-14 19:46:23'),
(33, 'co', 'Corsican', 'corsu, lingua corsa', '2021-11-14 19:46:23', '2021-11-14 19:46:23'),
(34, 'cr', 'Cree', 'ᓀᐦᐃᔭᐍᐏᐣ', '2021-11-14 19:46:23', '2021-11-14 19:46:23'),
(35, 'hr', 'Croatian', 'hrvatski', '2021-11-14 19:46:23', '2021-11-14 19:46:23'),
(36, 'cs', 'Czech', 'česky, čeština', '2021-11-14 19:46:23', '2021-11-14 19:46:23'),
(37, 'da', 'Danish', 'dansk', '2021-11-14 19:46:23', '2021-11-14 19:46:23'),
(38, 'dv', 'Divehi; Dhivehi; Maldivian;', 'ދިވެހި', '2021-11-14 19:46:23', '2021-11-14 19:46:23'),
(39, 'nl', 'Dutch', 'Nederlands, Vlaams', '2021-11-14 19:46:23', '2021-11-14 19:46:23'),
(40, 'en', 'English', 'English', '2021-11-14 19:46:23', '2021-11-14 19:46:23'),
(41, 'eo', 'Esperanto', 'Esperanto', '2021-11-14 19:46:23', '2021-11-14 19:46:23'),
(42, 'et', 'Estonian', 'eesti, eesti keel', '2021-11-14 19:46:23', '2021-11-14 19:46:23'),
(43, 'ee', 'Ewe', 'Eʋegbe', '2021-11-14 19:46:23', '2021-11-14 19:46:23'),
(44, 'fo', 'Faroese', 'føroyskt', '2021-11-14 19:46:23', '2021-11-14 19:46:23'),
(45, 'fj', 'Fijian', 'vosa Vakaviti', '2021-11-14 19:46:23', '2021-11-14 19:46:23'),
(46, 'fi', 'Finnish', 'suomi, suomen kieli', '2021-11-14 19:46:23', '2021-11-14 19:46:23'),
(47, 'fr', 'French', 'français, langue française', '2021-11-14 19:46:23', '2021-11-14 19:46:23'),
(48, 'ff', 'Fula; Fulah; Pulaar; Pular', 'Fulfulde, Pulaar, Pular', '2021-11-14 19:46:23', '2021-11-14 19:46:23'),
(49, 'gl', 'Galician', 'Galego', '2021-11-14 19:46:23', '2021-11-14 19:46:23'),
(50, 'ka', 'Georgian', 'ქართული', '2021-11-14 19:46:23', '2021-11-14 19:46:23'),
(51, 'de', 'German', 'Deutsch', '2021-11-14 19:46:23', '2021-11-14 19:46:23'),
(52, 'el', 'Greek, Modern', 'Ελληνικά', '2021-11-14 19:46:23', '2021-11-14 19:46:23'),
(53, 'gn', 'Guaraní', 'Avañeẽ', '2021-11-14 19:46:23', '2021-11-14 19:46:23'),
(54, 'gu', 'Gujarati', 'ગુજરાતી', '2021-11-14 19:46:23', '2021-11-14 19:46:23'),
(55, 'ht', 'Haitian; Haitian Creole', 'Kreyòl ayisyen', '2021-11-14 19:46:23', '2021-11-14 19:46:23'),
(56, 'ha', 'Hausa', 'Hausa, هَوُسَ', '2021-11-14 19:46:23', '2021-11-14 19:46:23'),
(57, 'he', 'Hebrew', 'עברית', '2021-11-14 19:46:23', '2021-11-14 19:46:23'),
(58, 'iw', 'Hebrew', 'עברית', '2021-11-14 19:46:23', '2021-11-14 19:46:23'),
(59, 'hz', 'Herero', 'Otjiherero', '2021-11-14 19:46:23', '2021-11-14 19:46:23'),
(60, 'hi', 'Hindi', 'हिन्दी, हिंदी', '2021-11-14 19:46:23', '2021-11-14 19:46:23'),
(61, 'ho', 'Hiri Motu', 'Hiri Motu', '2021-11-14 19:46:23', '2021-11-14 19:46:23'),
(62, 'hu', 'Hungarian', 'Magyar', '2021-11-14 19:46:23', '2021-11-14 19:46:23'),
(63, 'ia', 'Interlingua', 'Interlingua', '2021-11-14 19:46:23', '2021-11-14 19:46:23'),
(64, 'id', 'Indonesian', 'Bahasa Indonesia', '2021-11-14 19:46:23', '2021-11-14 19:46:23'),
(65, 'ie', 'Interlingue', 'Originally called Occidental; then Interlingue after WWII', '2021-11-14 19:46:23', '2021-11-14 19:46:23'),
(66, 'ga', 'Irish', 'Gaeilge', '2021-11-14 19:46:23', '2021-11-14 19:46:23'),
(67, 'ig', 'Igbo', 'Asụsụ Igbo', '2021-11-14 19:46:23', '2021-11-14 19:46:23'),
(68, 'ik', 'Inupiaq', 'Iñupiaq, Iñupiatun', '2021-11-14 19:46:23', '2021-11-14 19:46:23'),
(69, 'io', 'Ido', 'Ido', '2021-11-14 19:46:23', '2021-11-14 19:46:23'),
(70, 'is', 'Icelandic', 'Íslenska', '2021-11-14 19:46:23', '2021-11-14 19:46:23'),
(71, 'it', 'Italian', 'Italiano', '2021-11-14 19:46:23', '2021-11-14 19:46:23'),
(72, 'iu', 'Inuktitut', 'ᐃᓄᒃᑎᑐᑦ', '2021-11-14 19:46:23', '2021-11-14 19:46:23'),
(73, 'ja', 'Japanese', '日本語 (にほんご／にっぽんご)', '2021-11-14 19:46:23', '2021-11-14 19:46:23'),
(74, 'jv', 'Javanese', 'basa Jawa', '2021-11-14 19:46:23', '2021-11-14 19:46:23'),
(75, 'kl', 'Kalaallisut, Greenlandic', 'kalaallisut, kalaallit oqaasii', '2021-11-14 19:46:23', '2021-11-14 19:46:23'),
(76, 'kn', 'Kannada', 'ಕನ್ನಡ', '2021-11-14 19:46:23', '2021-11-14 19:46:23'),
(77, 'kr', 'Kanuri', 'Kanuri', '2021-11-14 19:46:23', '2021-11-14 19:46:23'),
(78, 'ks', 'Kashmiri', 'कश्मीरी, كشميري‎', '2021-11-14 19:46:23', '2021-11-14 19:46:23'),
(79, 'kk', 'Kazakh', 'Қазақ тілі', '2021-11-14 19:46:23', '2021-11-14 19:46:23'),
(80, 'km', 'Khmer', 'ភាសាខ្មែរ', '2021-11-14 19:46:23', '2021-11-14 19:46:23'),
(81, 'ki', 'Kikuyu, Gikuyu', 'Gĩkũyũ', '2021-11-14 19:46:23', '2021-11-14 19:46:23'),
(82, 'rw', 'Kinyarwanda', 'Ikinyarwanda', '2021-11-14 19:46:23', '2021-11-14 19:46:23'),
(83, 'ky', 'Kirghiz, Kyrgyz', 'кыргыз тили', '2021-11-14 19:46:23', '2021-11-14 19:46:23'),
(84, 'kv', 'Komi', 'коми кыв', '2021-11-14 19:46:23', '2021-11-14 19:46:23'),
(85, 'kg', 'Kongo', 'KiKongo', '2021-11-14 19:46:23', '2021-11-14 19:46:23'),
(86, 'ko', 'Korean', '한국어 (韓國語), 조선말 (朝鮮語)', '2021-11-14 19:46:23', '2021-11-14 19:46:23'),
(87, 'ku', 'Kurdish', 'Kurdî, كوردی‎', '2021-11-14 19:46:23', '2021-11-14 19:46:23'),
(88, 'kj', 'Kwanyama, Kuanyama', 'Kuanyama', '2021-11-14 19:46:23', '2021-11-14 19:46:23'),
(89, 'la', 'Latin', 'latine, lingua latina', '2021-11-14 19:46:23', '2021-11-14 19:46:23'),
(90, 'lb', 'Luxembourgish, Letzeburgesch', 'Lëtzebuergesch', '2021-11-14 19:46:23', '2021-11-14 19:46:23'),
(91, 'lg', 'Luganda', 'Luganda', '2021-11-14 19:46:23', '2021-11-14 19:46:23'),
(92, 'li', 'Limburgish, Limburgan, Limburger', 'Limburgs', '2021-11-14 19:46:23', '2021-11-14 19:46:23'),
(93, 'ln', 'Lingala', 'Lingála', '2021-11-14 19:46:23', '2021-11-14 19:46:23'),
(94, 'lo', 'Lao', 'ພາສາລາວ', '2021-11-14 19:46:23', '2021-11-14 19:46:23'),
(95, 'lt', 'Lithuanian', 'lietuvių kalba', '2021-11-14 19:46:23', '2021-11-14 19:46:23'),
(96, 'lu', 'Luba-Katanga', '', '2021-11-14 19:46:24', '2021-11-14 19:46:24'),
(97, 'lv', 'Latvian', 'latviešu valoda', '2021-11-14 19:46:24', '2021-11-14 19:46:24'),
(98, 'gv', 'Manx', 'Gaelg, Gailck', '2021-11-14 19:46:24', '2021-11-14 19:46:24'),
(99, 'mk', 'Macedonian', 'македонски јазик', '2021-11-14 19:46:24', '2021-11-14 19:46:24'),
(100, 'mg', 'Malagasy', 'Malagasy fiteny', '2021-11-14 19:46:24', '2021-11-14 19:46:24'),
(101, 'ms', 'Malay', 'bahasa Melayu, بهاس ملايو‎', '2021-11-14 19:46:24', '2021-11-14 19:46:24'),
(102, 'ml', 'Malayalam', 'മലയാളം', '2021-11-14 19:46:24', '2021-11-14 19:46:24'),
(103, 'mt', 'Maltese', 'Malti', '2021-11-14 19:46:24', '2021-11-14 19:46:24'),
(104, 'mi', 'Māori', 'te reo Māori', '2021-11-14 19:46:24', '2021-11-14 19:46:24'),
(105, 'mr', 'Marathi (Marāṭhī)', 'मराठी', '2021-11-14 19:46:24', '2021-11-14 19:46:24'),
(106, 'mh', 'Marshallese', 'Kajin M̧ajeļ', '2021-11-14 19:46:24', '2021-11-14 19:46:24'),
(107, 'mn', 'Mongolian', 'монгол', '2021-11-14 19:46:24', '2021-11-14 19:46:24'),
(108, 'na', 'Nauru', 'Ekakairũ Naoero', '2021-11-14 19:46:24', '2021-11-14 19:46:24'),
(109, 'nv', 'Navajo, Navaho', 'Diné bizaad, Dinékʼehǰí', '2021-11-14 19:46:24', '2021-11-14 19:46:24'),
(110, 'nb', 'Norwegian Bokmål', 'Norsk bokmål', '2021-11-14 19:46:24', '2021-11-14 19:46:24'),
(111, 'nd', 'North Ndebele', 'isiNdebele', '2021-11-14 19:46:24', '2021-11-14 19:46:24'),
(112, 'ne', 'Nepali', 'नेपाली', '2021-11-14 19:46:24', '2021-11-14 19:46:24'),
(113, 'ng', 'Ndonga', 'Owambo', '2021-11-14 19:46:24', '2021-11-14 19:46:24'),
(114, 'nn', 'Norwegian Nynorsk', 'Norsk nynorsk', '2021-11-14 19:46:24', '2021-11-14 19:46:24'),
(115, 'no', 'Norwegian', 'Norsk', '2021-11-14 19:46:24', '2021-11-14 19:46:24'),
(116, 'ii', 'Nuosu', 'ꆈꌠ꒿ Nuosuhxop', '2021-11-14 19:46:24', '2021-11-14 19:46:24'),
(117, 'nr', 'South Ndebele', 'isiNdebele', '2021-11-14 19:46:24', '2021-11-14 19:46:24'),
(118, 'oc', 'Occitan', 'Occitan', '2021-11-14 19:46:24', '2021-11-14 19:46:24'),
(119, 'oj', 'Ojibwe, Ojibwa', 'ᐊᓂᔑᓈᐯᒧᐎᓐ', '2021-11-14 19:46:24', '2021-11-14 19:46:24'),
(120, 'cu', 'Old Church Slavonic, Church Slavic, Church Slavonic, Old Bulgarian, Old Slavonic', 'ѩзыкъ словѣньскъ', '2021-11-14 19:46:24', '2021-11-14 19:46:24'),
(121, 'om', 'Oromo', 'Afaan Oromoo', '2021-11-14 19:46:24', '2021-11-14 19:46:24'),
(122, 'or', 'Oriya', 'ଓଡ଼ିଆ', '2021-11-14 19:46:24', '2021-11-14 19:46:24'),
(123, 'os', 'Ossetian, Ossetic', 'ирон æвзаг', '2021-11-14 19:46:24', '2021-11-14 19:46:24'),
(124, 'pa', 'Panjabi, Punjabi', 'ਪੰਜਾਬੀ, پنجابی‎', '2021-11-14 19:46:24', '2021-11-14 19:46:24'),
(125, 'pi', 'Pāli', 'पाऴि', '2021-11-14 19:46:24', '2021-11-14 19:46:24'),
(126, 'fa', 'Persian', 'فارسی', '2021-11-14 19:46:24', '2021-11-14 19:46:24'),
(127, 'pl', 'Polish', 'polski', '2021-11-14 19:46:24', '2021-11-14 19:46:24'),
(128, 'ps', 'Pashto, Pushto', 'پښتو', '2021-11-14 19:46:24', '2021-11-14 19:46:24'),
(129, 'pt', 'Portuguese', 'Português', '2021-11-14 19:46:24', '2021-11-14 19:46:24'),
(130, 'qu', 'Quechua', 'Runa Simi, Kichwa', '2021-11-14 19:46:24', '2021-11-14 19:46:24'),
(131, 'rm', 'Romansh', 'rumantsch grischun', '2021-11-14 19:46:24', '2021-11-14 19:46:24'),
(132, 'rn', 'Kirundi', 'kiRundi', '2021-11-14 19:46:24', '2021-11-14 19:46:24'),
(133, 'ro', 'Romanian, Moldavian, Moldovan', 'română', '2021-11-14 19:46:24', '2021-11-14 19:46:24'),
(134, 'ru', 'Russian', 'русский язык', '2021-11-14 19:46:24', '2021-11-14 19:46:24'),
(135, 'sa', 'Sanskrit (Saṁskṛta)', 'संस्कृतम्', '2021-11-14 19:46:24', '2021-11-14 19:46:24'),
(136, 'sc', 'Sardinian', 'sardu', '2021-11-14 19:46:24', '2021-11-14 19:46:24'),
(137, 'sd', 'Sindhi', 'सिन्धी, سنڌي، سندھی‎', '2021-11-14 19:46:24', '2021-11-14 19:46:24'),
(138, 'se', 'Northern Sami', 'Davvisámegiella', '2021-11-14 19:46:24', '2021-11-14 19:46:24'),
(139, 'sm', 'Samoan', 'gagana faa Samoa', '2021-11-14 19:46:24', '2021-11-14 19:46:24'),
(140, 'sg', 'Sango', 'yângâ tî sängö', '2021-11-14 19:46:24', '2021-11-14 19:46:24'),
(141, 'sr', 'Serbian', 'српски језик', '2021-11-14 19:46:24', '2021-11-14 19:46:24'),
(142, 'gd', 'Scottish Gaelic; Gaelic', 'Gàidhlig', '2021-11-14 19:46:24', '2021-11-14 19:46:24'),
(143, 'sn', 'Shona', 'chiShona', '2021-11-14 19:46:24', '2021-11-14 19:46:24'),
(144, 'si', 'Sinhala, Sinhalese', 'සිංහල', '2021-11-14 19:46:24', '2021-11-14 19:46:24'),
(145, 'sk', 'Slovak', 'slovenčina', '2021-11-14 19:46:24', '2021-11-14 19:46:24'),
(146, 'sl', 'Slovene', 'slovenščina', '2021-11-14 19:46:24', '2021-11-14 19:46:24'),
(147, 'so', 'Somali', 'Soomaaliga, af Soomaali', '2021-11-14 19:46:24', '2021-11-14 19:46:24'),
(148, 'st', 'Southern Sotho', 'Sesotho', '2021-11-14 19:46:24', '2021-11-14 19:46:24'),
(149, 'es', 'Spanish; Castilian', 'español, castellano', '2021-11-14 19:46:24', '2021-11-14 19:46:24'),
(150, 'su', 'Sundanese', 'Basa Sunda', '2021-11-14 19:46:24', '2021-11-14 19:46:24'),
(151, 'sw', 'Swahili', 'Kiswahili', '2021-11-14 19:46:24', '2021-11-14 19:46:24'),
(152, 'ss', 'Swati', 'SiSwati', '2021-11-14 19:46:24', '2021-11-14 19:46:24'),
(153, 'sv', 'Swedish', 'svenska', '2021-11-14 19:46:24', '2021-11-14 19:46:24'),
(154, 'ta', 'Tamil', 'தமிழ்', '2021-11-14 19:46:24', '2021-11-14 19:46:24'),
(155, 'te', 'Telugu', 'తెలుగు', '2021-11-14 19:46:24', '2021-11-14 19:46:24'),
(156, 'tg', 'Tajik', 'тоҷикӣ, toğikī, تاجیکی‎', '2021-11-14 19:46:24', '2021-11-14 19:46:24'),
(157, 'th', 'Thai', 'ไทย', '2021-11-14 19:46:24', '2021-11-14 19:46:24'),
(158, 'ti', 'Tigrinya', 'ትግርኛ', '2021-11-14 19:46:24', '2021-11-14 19:46:24'),
(159, 'bo', 'Tibetan Standard, Tibetan, Central', 'བོད་ཡིག', '2021-11-14 19:46:24', '2021-11-14 19:46:24'),
(160, 'tk', 'Turkmen', 'Türkmen, Түркмен', '2021-11-14 19:46:24', '2021-11-14 19:46:24'),
(161, 'tl', 'Tagalog', 'Wikang Tagalog, ᜏᜒᜃᜅ᜔ ᜆᜄᜎᜓᜄ᜔', '2021-11-14 19:46:24', '2021-11-14 19:46:24'),
(162, 'tn', 'Tswana', 'Setswana', '2021-11-14 19:46:24', '2021-11-14 19:46:24'),
(163, 'to', 'Tonga (Tonga Islands)', 'faka Tonga', '2021-11-14 19:46:24', '2021-11-14 19:46:24'),
(164, 'tr', 'Turkish', 'Türkçe', '2021-11-14 19:46:24', '2021-11-14 19:46:24'),
(165, 'ts', 'Tsonga', 'Xitsonga', '2021-11-14 19:46:24', '2021-11-14 19:46:24'),
(166, 'tt', 'Tatar', 'татарча, tatarça, تاتارچا‎', '2021-11-14 19:46:24', '2021-11-14 19:46:24'),
(167, 'tw', 'Twi', 'Twi', '2021-11-14 19:46:24', '2021-11-14 19:46:24'),
(168, 'ty', 'Tahitian', 'Reo Tahiti', '2021-11-14 19:46:24', '2021-11-14 19:46:24'),
(169, 'ug', 'Uighur, Uyghur', 'Uyƣurqə, ئۇيغۇرچە‎', '2021-11-14 19:46:24', '2021-11-14 19:46:24'),
(170, 'uk', 'Ukrainian', 'українська', '2021-11-14 19:46:24', '2021-11-14 19:46:24'),
(171, 'ur', 'Urdu', 'اردو', '2021-11-14 19:46:24', '2021-11-14 19:46:24'),
(172, 'uz', 'Uzbek', 'zbek, Ўзбек, أۇزبېك‎', '2021-11-14 19:46:24', '2021-11-14 19:46:24'),
(173, 've', 'Venda', 'Tshivenḓa', '2021-11-14 19:46:24', '2021-11-14 19:46:24'),
(174, 'vi', 'Vietnamese', 'Tiếng Việt', '2021-11-14 19:46:24', '2021-11-14 19:46:24'),
(175, 'vo', 'Volapük', 'Volapük', '2021-11-14 19:46:24', '2021-11-14 19:46:24'),
(176, 'wa', 'Walloon', 'Walon', '2021-11-14 19:46:24', '2021-11-14 19:46:24'),
(177, 'cy', 'Welsh', 'Cymraeg', '2021-11-14 19:46:24', '2021-11-14 19:46:24'),
(178, 'wo', 'Wolof', 'Wollof', '2021-11-14 19:46:24', '2021-11-14 19:46:24'),
(179, 'fy', 'Western Frisian', 'Frysk', '2021-11-14 19:46:24', '2021-11-14 19:46:24'),
(180, 'xh', 'Xhosa', 'isiXhosa', '2021-11-14 19:46:24', '2021-11-14 19:46:24'),
(181, 'yi', 'Yiddish', 'ייִדיש', '2021-11-14 19:46:24', '2021-11-14 19:46:24'),
(182, 'yo', 'Yoruba', 'Yorùbá', '2021-11-14 19:46:24', '2021-11-14 19:46:24');

-- --------------------------------------------------------

--
-- Table structure for table `language_allows`
--

CREATE TABLE `language_allows` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `language_id` bigint(20) UNSIGNED DEFAULT NULL,
  `code` varchar(300) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(300) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `native_name` varchar(300) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `icon` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `language_allows`
--

INSERT INTO `language_allows` (`id`, `language_id`, `code`, `name`, `native_name`, `icon`, `created_at`, `updated_at`) VALUES
(1, 7, 'ar', 'Arabic', 'العربية', 'language_allows/5RCg1gXC2sO2CMfczrxWCMYc8OtmUD8OopTilwDX.png', '2021-11-14 19:47:32', '2021-11-24 00:41:07'),
(2, 40, 'en', 'English', 'English', 'language_allows/GJovNmRNy0vyvKSDsTsoP0tudvFvg62jHNTwgmR2.png', '2021-11-14 19:47:32', '2021-11-24 00:41:14');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(300) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_100000_create_password_resets_table', 1),
(2, '2016_06_01_000001_create_oauth_auth_codes_table', 1),
(3, '2016_06_01_000002_create_oauth_access_tokens_table', 1),
(4, '2016_06_01_000003_create_oauth_refresh_tokens_table', 1),
(5, '2016_06_01_000004_create_oauth_clients_table', 1),
(6, '2016_06_01_000005_create_oauth_personal_access_clients_table', 1),
(7, '2019_07_31_101142_create_visits_table', 1),
(8, '2019_08_19_000000_create_failed_jobs_table', 1),
(9, '2020_02_09_105529_create_admins_table', 1),
(10, '2020_03_17_203042_create_phone_codes_table', 1),
(11, '2020_03_17_203057_create_phone_code_allows_table', 1),
(12, '2020_04_19_163423_create_contacts_table', 1),
(13, '2020_10_12_000000_create_users_table', 1),
(14, '2021_02_03_140814_create_firebase_tokens_table', 1),
(15, '2021_04_14_093857_create_notification_infos_table', 1),
(16, '2021_04_14_095113_create_notifications_table', 1),
(17, '2021_04_15_184645_create_languages_table', 1),
(18, '2021_04_16_200338_create_language_allows_table', 1),
(19, '2021_11_11_180250_create_sliders_table', 1),
(20, '2021_11_11_180627_create_slider_trans_table', 1),
(21, '2021_11_11_180650_create_settings_table', 1),
(22, '2021_11_11_180704_create_setting_trans_table', 1),
(23, '2021_11_11_180725_create_site_texts_table', 1),
(24, '2021_11_11_180801_create_site_text_trans_table', 1),
(25, '2021_11_11_181026_create_contract_advantages_table', 1),
(26, '2021_11_11_181039_create_contract_advantage_trans_table', 1),
(27, '2021_11_11_181101_create_contract_advantage_details_table', 1),
(28, '2021_11_11_181121_create_contract_advantage_detail_trans_table', 1),
(29, '2021_11_11_181155_create_offers_table', 1),
(30, '2021_11_11_181227_create_offer_details_table', 1),
(31, '2021_11_11_181250_create_offer_detail_trans_table', 1),
(32, '2021_11_11_181324_create_blogs_table', 1),
(33, '2021_11_11_181404_create_blog_trans_table', 1),
(34, '2021_11_11_181417_create_programs_table', 1),
(35, '2021_11_11_181428_create_program_trans_table', 1),
(36, '2021_11_11_181437_create_subscribe_to_offers_table', 1),
(37, '2021_11_11_181625_create_subscribe_to_offer_details_table', 1),
(38, '2021_11_11_182249_create_services_table', 1),
(39, '2021_11_11_182325_create_service_trans_table', 1),
(40, '2021_11_11_182407_create_service_texts_table', 1),
(41, '2021_11_11_182438_create_service_text_trans_table', 1),
(42, '2021_11_11_182519_create_service_text_details_table', 1),
(43, '2021_11_11_182611_create_service_text_detail_trans_table', 1),
(44, '2021_11_11_182718_create_service_hosts_table', 1),
(45, '2021_11_11_182727_create_service_host_trans_table', 1),
(46, '2021_11_11_182805_create_service_details_table', 1),
(47, '2021_11_11_182821_create_service_detail_trans_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `notifications`
--

CREATE TABLE `notifications` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `notification_info_id` bigint(20) UNSIGNED DEFAULT NULL,
  `user_type` enum('client','admin') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'admin',
  `from_user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `to_user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `title` varchar(300) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `message` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `notification_type` enum('admin') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'admin',
  `action_type` enum('nothing') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'nothing',
  `is_read` enum('no','yes') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'no',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `notification_infos`
--

CREATE TABLE `notification_infos` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(300) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `message` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_access_tokens`
--

CREATE TABLE `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(300) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_auth_codes`
--

CREATE TABLE `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_clients`
--

CREATE TABLE `oauth_clients` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `name` varchar(300) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `provider` varchar(300) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_personal_access_clients`
--

CREATE TABLE `oauth_personal_access_clients` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_refresh_tokens`
--

CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `offers`
--

CREATE TABLE `offers` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `price` varchar(300) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `offer_type` enum('main_package','professional_package','golden_package') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `offers`
--

INSERT INTO `offers` (`id`, `price`, `offer_type`, `created_at`, `updated_at`) VALUES
(1, '3000', 'main_package', '2021-11-16 21:09:57', '2021-12-19 07:22:18'),
(2, '5500', 'professional_package', '2021-11-16 21:09:57', '2021-12-19 07:22:18'),
(3, '10000', 'golden_package', '2021-11-16 21:09:57', '2021-12-19 07:22:18');

-- --------------------------------------------------------

--
-- Table structure for table `offer_details`
--

CREATE TABLE `offer_details` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `offer_id` bigint(20) UNSIGNED DEFAULT NULL,
  `main_package` enum('yes','no') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'no',
  `professional_package` enum('yes','no') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'no',
  `golden_package` enum('yes','no') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'no',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `offer_details`
--

INSERT INTO `offer_details` (`id`, `offer_id`, `main_package`, `professional_package`, `golden_package`, `created_at`, `updated_at`) VALUES
(1, NULL, 'yes', 'yes', 'yes', '2021-11-16 21:17:16', NULL),
(2, NULL, 'yes', 'yes', 'yes', '2021-11-16 21:17:16', NULL),
(3, NULL, 'no', 'yes', 'yes', '2021-11-16 21:17:16', NULL),
(4, NULL, 'no', 'yes', 'yes', '2021-11-16 21:17:16', '2021-11-28 00:08:41'),
(5, NULL, 'no', 'yes', 'yes', '2021-11-28 00:01:26', '2021-11-28 00:01:26');

-- --------------------------------------------------------

--
-- Table structure for table `offer_detail_trans`
--

CREATE TABLE `offer_detail_trans` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `offer_detail_id` bigint(20) UNSIGNED DEFAULT NULL,
  `title` varchar(300) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `text` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lang` varchar(300) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `offer_detail_trans`
--

INSERT INTO `offer_detail_trans` (`id`, `offer_detail_id`, `title`, `text`, `lang`, `created_at`, `updated_at`) VALUES
(9, 5, NULL, 'شهر دعم فني مجانا', 'ar', '2021-11-28 00:01:26', '2021-11-28 00:01:26'),
(10, 5, NULL, '1 month free technical support', 'en', '2021-11-28 00:01:26', '2021-11-28 00:01:26'),
(11, 4, NULL, 'SEO محسنات محركات البحث', 'ar', '2021-11-28 00:08:41', '2021-11-28 00:08:41'),
(12, 4, NULL, 'SEO Search Engine Optimization', 'en', '2021-11-28 00:08:41', '2021-11-28 00:08:41'),
(19, 2, NULL, 'عدد لا نهائي من الصفحات والقوائم', 'ar', '2021-12-12 10:27:27', '2021-12-12 10:27:27'),
(20, 2, NULL, 'Unlimited number of pages and lists', 'en', '2021-12-12 10:27:27', '2021-12-12 10:27:27'),
(23, 1, NULL, 'تصميم موقع واحد كامل بتصميم خاص رائع', 'ar', '2021-12-13 09:49:01', '2021-12-13 09:49:01'),
(24, 1, NULL, 'One complete website design with a great special design', 'en', '2021-12-13 09:49:01', '2021-12-13 09:49:01'),
(25, 3, NULL, 'دمج الموقع بشبكات التواصل الإجتماعية (ربط الموقع بـ فيس بوك، تويتر،...)', 'ar', '2021-12-13 09:49:07', '2021-12-13 09:49:07'),
(26, 3, NULL, 'Integrating the site with social networks (linking the site with Facebook - Twitter', 'en', '2021-12-13 09:49:07', '2021-12-13 09:49:07');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(300) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(300) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `phone_codes`
--

CREATE TABLE `phone_codes` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(300) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dial_code` varchar(300) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `code` varchar(300) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `phone_codes`
--

INSERT INTO `phone_codes` (`id`, `name`, `dial_code`, `code`, `created_at`, `updated_at`) VALUES
(1, 'Afghanistan', '+93', 'AF', '2021-11-14 20:04:35', '2021-11-14 20:04:35'),
(2, 'Aland Islands', '+358', 'AX', '2021-11-14 20:04:35', '2021-11-14 20:04:35'),
(3, 'Albania', '+355', 'AL', '2021-11-14 20:04:35', '2021-11-14 20:04:35'),
(4, 'Algeria', '+213', 'DZ', '2021-11-14 20:04:35', '2021-11-14 20:04:35'),
(5, 'AmericanSamoa', '+1684', 'AS', '2021-11-14 20:04:35', '2021-11-14 20:04:35'),
(6, 'Andorra', '+376', 'AD', '2021-11-14 20:04:35', '2021-11-14 20:04:35'),
(7, 'Angola', '+244', 'AO', '2021-11-14 20:04:35', '2021-11-14 20:04:35'),
(8, 'Anguilla', '+1264', 'AI', '2021-11-14 20:04:35', '2021-11-14 20:04:35'),
(9, 'Antarctica', '+672', 'AQ', '2021-11-14 20:04:35', '2021-11-14 20:04:35'),
(10, 'Antigua and Barbuda', '+1268', 'AG', '2021-11-14 20:04:35', '2021-11-14 20:04:35'),
(11, 'Argentina', '+54', 'AR', '2021-11-14 20:04:35', '2021-11-14 20:04:35'),
(12, 'Armenia', '+374', 'AM', '2021-11-14 20:04:35', '2021-11-14 20:04:35'),
(13, 'Aruba', '+297', 'AW', '2021-11-14 20:04:35', '2021-11-14 20:04:35'),
(14, 'Australia', '+61', 'AU', '2021-11-14 20:04:35', '2021-11-14 20:04:35'),
(15, 'Austria', '+43', 'AT', '2021-11-14 20:04:35', '2021-11-14 20:04:35'),
(16, 'Azerbaijan', '+994', 'AZ', '2021-11-14 20:04:35', '2021-11-14 20:04:35'),
(17, 'Bahamas', '+1242', 'BS', '2021-11-14 20:04:35', '2021-11-14 20:04:35'),
(18, 'Bahrain', '+973', 'BH', '2021-11-14 20:04:35', '2021-11-14 20:04:35'),
(19, 'Bangladesh', '+880', 'BD', '2021-11-14 20:04:35', '2021-11-14 20:04:35'),
(20, 'Barbados', '+1246', 'BB', '2021-11-14 20:04:35', '2021-11-14 20:04:35'),
(21, 'Belarus', '+375', 'BY', '2021-11-14 20:04:35', '2021-11-14 20:04:35'),
(22, 'Belgium', '+32', 'BE', '2021-11-14 20:04:35', '2021-11-14 20:04:35'),
(23, 'Belize', '+501', 'BZ', '2021-11-14 20:04:35', '2021-11-14 20:04:35'),
(24, 'Benin', '+229', 'BJ', '2021-11-14 20:04:35', '2021-11-14 20:04:35'),
(25, 'Bermuda', '+1441', 'BM', '2021-11-14 20:04:35', '2021-11-14 20:04:35'),
(26, 'Bhutan', '+975', 'BT', '2021-11-14 20:04:35', '2021-11-14 20:04:35'),
(27, 'Bolivia, Plurinational State of', '+591', 'BO', '2021-11-14 20:04:35', '2021-11-14 20:04:35'),
(28, 'Bosnia and Herzegovina', '+387', 'BA', '2021-11-14 20:04:35', '2021-11-14 20:04:35'),
(29, 'Botswana', '+267', 'BW', '2021-11-14 20:04:35', '2021-11-14 20:04:35'),
(30, 'Brazil', '+55', 'BR', '2021-11-14 20:04:35', '2021-11-14 20:04:35'),
(31, 'British Indian Ocean Territory', '+246', 'IO', '2021-11-14 20:04:35', '2021-11-14 20:04:35'),
(32, 'Brunei Darussalam', '+673', 'BN', '2021-11-14 20:04:35', '2021-11-14 20:04:35'),
(33, 'Bulgaria', '+359', 'BG', '2021-11-14 20:04:35', '2021-11-14 20:04:35'),
(34, 'Burkina Faso', '+226', 'BF', '2021-11-14 20:04:35', '2021-11-14 20:04:35'),
(35, 'Burundi', '+257', 'BI', '2021-11-14 20:04:35', '2021-11-14 20:04:35'),
(36, 'Cambodia', '+855', 'KH', '2021-11-14 20:04:35', '2021-11-14 20:04:35'),
(37, 'Cameroon', '+237', 'CM', '2021-11-14 20:04:35', '2021-11-14 20:04:35'),
(38, 'Canada', '+1', 'CA', '2021-11-14 20:04:35', '2021-11-14 20:04:35'),
(39, 'Cape Verde', '+238', 'CV', '2021-11-14 20:04:35', '2021-11-14 20:04:35'),
(40, 'Cayman Islands', '+ 345', 'KY', '2021-11-14 20:04:35', '2021-11-14 20:04:35'),
(41, 'Central African Republic', '+236', 'CF', '2021-11-14 20:04:35', '2021-11-14 20:04:35'),
(42, 'Chad', '+235', 'TD', '2021-11-14 20:04:35', '2021-11-14 20:04:35'),
(43, 'Chile', '+56', 'CL', '2021-11-14 20:04:35', '2021-11-14 20:04:35'),
(44, 'China', '+86', 'CN', '2021-11-14 20:04:35', '2021-11-14 20:04:35'),
(45, 'Christmas Island', '+61', 'CX', '2021-11-14 20:04:35', '2021-11-14 20:04:35'),
(46, 'Cocos (Keeling) Islands', '+61', 'CC', '2021-11-14 20:04:35', '2021-11-14 20:04:35'),
(47, 'Colombia', '+57', 'CO', '2021-11-14 20:04:35', '2021-11-14 20:04:35'),
(48, 'Comoros', '+269', 'KM', '2021-11-14 20:04:35', '2021-11-14 20:04:35'),
(49, 'Congo', '+242', 'CG', '2021-11-14 20:04:35', '2021-11-14 20:04:35'),
(50, 'Congo, The Democratic Republic of the Congo', '+243', 'CD', '2021-11-14 20:04:35', '2021-11-14 20:04:35'),
(51, 'Cook Islands', '+682', 'CK', '2021-11-14 20:04:36', '2021-11-14 20:04:36'),
(52, 'Costa Rica', '+506', 'CR', '2021-11-14 20:04:36', '2021-11-14 20:04:36'),
(53, 'Cote d\'Ivoire', '+225', 'CI', '2021-11-14 20:04:36', '2021-11-14 20:04:36'),
(54, 'Croatia', '+385', 'HR', '2021-11-14 20:04:36', '2021-11-14 20:04:36'),
(55, 'Cuba', '+53', 'CU', '2021-11-14 20:04:36', '2021-11-14 20:04:36'),
(56, 'Cyprus', '+357', 'CY', '2021-11-14 20:04:36', '2021-11-14 20:04:36'),
(57, 'Czech Republic', '+420', 'CZ', '2021-11-14 20:04:36', '2021-11-14 20:04:36'),
(58, 'Denmark', '+45', 'DK', '2021-11-14 20:04:36', '2021-11-14 20:04:36'),
(59, 'Djibouti', '+253', 'DJ', '2021-11-14 20:04:36', '2021-11-14 20:04:36'),
(60, 'Dominica', '+1767', 'DM', '2021-11-14 20:04:36', '2021-11-14 20:04:36'),
(61, 'Dominican Republic', '+1849', 'DO', '2021-11-14 20:04:36', '2021-11-14 20:04:36'),
(62, 'Ecuador', '+593', 'EC', '2021-11-14 20:04:36', '2021-11-14 20:04:36'),
(63, 'Egypt', '+20', 'EG', '2021-11-14 20:04:36', '2021-11-14 20:04:36'),
(64, 'El Salvador', '+503', 'SV', '2021-11-14 20:04:36', '2021-11-14 20:04:36'),
(65, 'Equatorial Guinea', '+240', 'GQ', '2021-11-14 20:04:36', '2021-11-14 20:04:36'),
(66, 'Eritrea', '+291', 'ER', '2021-11-14 20:04:36', '2021-11-14 20:04:36'),
(67, 'Estonia', '+372', 'EE', '2021-11-14 20:04:36', '2021-11-14 20:04:36'),
(68, 'Ethiopia', '+251', 'ET', '2021-11-14 20:04:36', '2021-11-14 20:04:36'),
(69, 'Falkland Islands (Malvinas)', '+500', 'FK', '2021-11-14 20:04:36', '2021-11-14 20:04:36'),
(70, 'Faroe Islands', '+298', 'FO', '2021-11-14 20:04:36', '2021-11-14 20:04:36'),
(71, 'Fiji', '+679', 'FJ', '2021-11-14 20:04:36', '2021-11-14 20:04:36'),
(72, 'Finland', '+358', 'FI', '2021-11-14 20:04:36', '2021-11-14 20:04:36'),
(73, 'France', '+33', 'FR', '2021-11-14 20:04:36', '2021-11-14 20:04:36'),
(74, 'French Guiana', '+594', 'GF', '2021-11-14 20:04:36', '2021-11-14 20:04:36'),
(75, 'French Polynesia', '+689', 'PF', '2021-11-14 20:04:36', '2021-11-14 20:04:36'),
(76, 'Gabon', '+241', 'GA', '2021-11-14 20:04:36', '2021-11-14 20:04:36'),
(77, 'Gambia', '+220', 'GM', '2021-11-14 20:04:36', '2021-11-14 20:04:36'),
(78, 'Georgia', '+995', 'GE', '2021-11-14 20:04:36', '2021-11-14 20:04:36'),
(79, 'Germany', '+49', 'DE', '2021-11-14 20:04:36', '2021-11-14 20:04:36'),
(80, 'Ghana', '+233', 'GH', '2021-11-14 20:04:36', '2021-11-14 20:04:36'),
(81, 'Gibraltar', '+350', 'GI', '2021-11-14 20:04:36', '2021-11-14 20:04:36'),
(82, 'Greece', '+30', 'GR', '2021-11-14 20:04:36', '2021-11-14 20:04:36'),
(83, 'Greenland', '+299', 'GL', '2021-11-14 20:04:36', '2021-11-14 20:04:36'),
(84, 'Grenada', '+1473', 'GD', '2021-11-14 20:04:36', '2021-11-14 20:04:36'),
(85, 'Guadeloupe', '+590', 'GP', '2021-11-14 20:04:36', '2021-11-14 20:04:36'),
(86, 'Guam', '+1671', 'GU', '2021-11-14 20:04:36', '2021-11-14 20:04:36'),
(87, 'Guatemala', '+502', 'GT', '2021-11-14 20:04:36', '2021-11-14 20:04:36'),
(88, 'Guernsey', '+44', 'GG', '2021-11-14 20:04:36', '2021-11-14 20:04:36'),
(89, 'Guinea', '+224', 'GN', '2021-11-14 20:04:36', '2021-11-14 20:04:36'),
(90, 'Guinea-Bissau', '+245', 'GW', '2021-11-14 20:04:36', '2021-11-14 20:04:36'),
(91, 'Guyana', '+595', 'GY', '2021-11-14 20:04:36', '2021-11-14 20:04:36'),
(92, 'Haiti', '+509', 'HT', '2021-11-14 20:04:36', '2021-11-14 20:04:36'),
(93, 'Holy See (Vatican City State)', '+379', 'VA', '2021-11-14 20:04:36', '2021-11-14 20:04:36'),
(94, 'Honduras', '+504', 'HN', '2021-11-14 20:04:36', '2021-11-14 20:04:36'),
(95, 'Hong Kong', '+852', 'HK', '2021-11-14 20:04:36', '2021-11-14 20:04:36'),
(96, 'Hungary', '+36', 'HU', '2021-11-14 20:04:36', '2021-11-14 20:04:36'),
(97, 'Iceland', '+354', 'IS', '2021-11-14 20:04:36', '2021-11-14 20:04:36'),
(98, 'India', '+91', 'IN', '2021-11-14 20:04:36', '2021-11-14 20:04:36'),
(99, 'Indonesia', '+62', 'ID', '2021-11-14 20:04:36', '2021-11-14 20:04:36'),
(100, 'Iran, Islamic Republic of Persian Gulf', '+98', 'IR', '2021-11-14 20:04:36', '2021-11-14 20:04:36'),
(101, 'Iraq', '+964', 'IQ', '2021-11-14 20:04:36', '2021-11-14 20:04:36'),
(102, 'Ireland', '+353', 'IE', '2021-11-14 20:04:36', '2021-11-14 20:04:36'),
(103, 'Isle of Man', '+44', 'IM', '2021-11-14 20:04:36', '2021-11-14 20:04:36'),
(104, 'Israel', '+972', 'IL', '2021-11-14 20:04:36', '2021-11-14 20:04:36'),
(105, 'Italy', '+39', 'IT', '2021-11-14 20:04:36', '2021-11-14 20:04:36'),
(106, 'Jamaica', '+1876', 'JM', '2021-11-14 20:04:36', '2021-11-14 20:04:36'),
(107, 'Japan', '+81', 'JP', '2021-11-14 20:04:36', '2021-11-14 20:04:36'),
(108, 'Jersey', '+44', 'JE', '2021-11-14 20:04:36', '2021-11-14 20:04:36'),
(109, 'Jordan', '+962', 'JO', '2021-11-14 20:04:36', '2021-11-14 20:04:36'),
(110, 'Kazakhstan', '+77', 'KZ', '2021-11-14 20:04:36', '2021-11-14 20:04:36'),
(111, 'Kenya', '+254', 'KE', '2021-11-14 20:04:36', '2021-11-14 20:04:36'),
(112, 'Kiribati', '+686', 'KI', '2021-11-14 20:04:36', '2021-11-14 20:04:36'),
(113, 'Korea, Democratic People\'s Republic of Korea', '+850', 'KP', '2021-11-14 20:04:36', '2021-11-14 20:04:36'),
(114, 'Korea, Republic of South Korea', '+82', 'KR', '2021-11-14 20:04:36', '2021-11-14 20:04:36'),
(115, 'Kuwait', '+965', 'KW', '2021-11-14 20:04:36', '2021-11-14 20:04:36'),
(116, 'Kyrgyzstan', '+996', 'KG', '2021-11-14 20:04:36', '2021-11-14 20:04:36'),
(117, 'Laos', '+856', 'LA', '2021-11-14 20:04:36', '2021-11-14 20:04:36'),
(118, 'Latvia', '+371', 'LV', '2021-11-14 20:04:36', '2021-11-14 20:04:36'),
(119, 'Lebanon', '+961', 'LB', '2021-11-14 20:04:36', '2021-11-14 20:04:36'),
(120, 'Lesotho', '+266', 'LS', '2021-11-14 20:04:36', '2021-11-14 20:04:36'),
(121, 'Liberia', '+231', 'LR', '2021-11-14 20:04:36', '2021-11-14 20:04:36'),
(122, 'Libyan Arab Jamahiriya', '+218', 'LY', '2021-11-14 20:04:36', '2021-11-14 20:04:36'),
(123, 'Liechtenstein', '+423', 'LI', '2021-11-14 20:04:36', '2021-11-14 20:04:36'),
(124, 'Lithuania', '+370', 'LT', '2021-11-14 20:04:36', '2021-11-14 20:04:36'),
(125, 'Luxembourg', '+352', 'LU', '2021-11-14 20:04:36', '2021-11-14 20:04:36'),
(126, 'Macao', '+853', 'MO', '2021-11-14 20:04:36', '2021-11-14 20:04:36'),
(127, 'Macedonia', '+389', 'MK', '2021-11-14 20:04:36', '2021-11-14 20:04:36'),
(128, 'Madagascar', '+261', 'MG', '2021-11-14 20:04:36', '2021-11-14 20:04:36'),
(129, 'Malawi', '+265', 'MW', '2021-11-14 20:04:36', '2021-11-14 20:04:36'),
(130, 'Malaysia', '+60', 'MY', '2021-11-14 20:04:36', '2021-11-14 20:04:36'),
(131, 'Maldives', '+960', 'MV', '2021-11-14 20:04:36', '2021-11-14 20:04:36'),
(132, 'Mali', '+223', 'ML', '2021-11-14 20:04:36', '2021-11-14 20:04:36'),
(133, 'Malta', '+356', 'MT', '2021-11-14 20:04:36', '2021-11-14 20:04:36'),
(134, 'Marshall Islands', '+692', 'MH', '2021-11-14 20:04:36', '2021-11-14 20:04:36'),
(135, 'Martinique', '+596', 'MQ', '2021-11-14 20:04:36', '2021-11-14 20:04:36'),
(136, 'Mauritania', '+222', 'MR', '2021-11-14 20:04:36', '2021-11-14 20:04:36'),
(137, 'Mauritius', '+230', 'MU', '2021-11-14 20:04:36', '2021-11-14 20:04:36'),
(138, 'Mayotte', '+262', 'YT', '2021-11-14 20:04:36', '2021-11-14 20:04:36'),
(139, 'Mexico', '+52', 'MX', '2021-11-14 20:04:36', '2021-11-14 20:04:36'),
(140, 'Micronesia, Federated States of Micronesia', '+691', 'FM', '2021-11-14 20:04:36', '2021-11-14 20:04:36'),
(141, 'Moldova', '+373', 'MD', '2021-11-14 20:04:36', '2021-11-14 20:04:36'),
(142, 'Monaco', '+377', 'MC', '2021-11-14 20:04:36', '2021-11-14 20:04:36'),
(143, 'Mongolia', '+976', 'MN', '2021-11-14 20:04:36', '2021-11-14 20:04:36'),
(144, 'Montenegro', '+382', 'ME', '2021-11-14 20:04:36', '2021-11-14 20:04:36'),
(145, 'Montserrat', '+1664', 'MS', '2021-11-14 20:04:36', '2021-11-14 20:04:36'),
(146, 'Morocco', '+212', 'MA', '2021-11-14 20:04:36', '2021-11-14 20:04:36'),
(147, 'Mozambique', '+258', 'MZ', '2021-11-14 20:04:36', '2021-11-14 20:04:36'),
(148, 'Myanmar', '+95', 'MM', '2021-11-14 20:04:36', '2021-11-14 20:04:36'),
(149, 'Namibia', '+264', 'NA', '2021-11-14 20:04:36', '2021-11-14 20:04:36'),
(150, 'Nauru', '+674', 'NR', '2021-11-14 20:04:36', '2021-11-14 20:04:36'),
(151, 'Nepal', '+977', 'NP', '2021-11-14 20:04:36', '2021-11-14 20:04:36'),
(152, 'Netherlands', '+31', 'NL', '2021-11-14 20:04:36', '2021-11-14 20:04:36'),
(153, 'Netherlands Antilles', '+599', 'AN', '2021-11-14 20:04:36', '2021-11-14 20:04:36'),
(154, 'New Caledonia', '+687', 'NC', '2021-11-14 20:04:36', '2021-11-14 20:04:36'),
(155, 'New Zealand', '+64', 'NZ', '2021-11-14 20:04:36', '2021-11-14 20:04:36'),
(156, 'Nicaragua', '+505', 'NI', '2021-11-14 20:04:36', '2021-11-14 20:04:36'),
(157, 'Niger', '+227', 'NE', '2021-11-14 20:04:36', '2021-11-14 20:04:36'),
(158, 'Nigeria', '+234', 'NG', '2021-11-14 20:04:36', '2021-11-14 20:04:36'),
(159, 'Niue', '+683', 'NU', '2021-11-14 20:04:36', '2021-11-14 20:04:36'),
(160, 'Norfolk Island', '+672', 'NF', '2021-11-14 20:04:36', '2021-11-14 20:04:36'),
(161, 'Northern Mariana Islands', '+1670', 'MP', '2021-11-14 20:04:36', '2021-11-14 20:04:36'),
(162, 'Norway', '+47', 'NO', '2021-11-14 20:04:36', '2021-11-14 20:04:36'),
(163, 'Oman', '+968', 'OM', '2021-11-14 20:04:36', '2021-11-14 20:04:36'),
(164, 'Pakistan', '+92', 'PK', '2021-11-14 20:04:36', '2021-11-14 20:04:36'),
(165, 'Palau', '+680', 'PW', '2021-11-14 20:04:36', '2021-11-14 20:04:36'),
(166, 'Palestinian Territory, Occupied', '+970', 'PS', '2021-11-14 20:04:36', '2021-11-14 20:04:36'),
(167, 'Panama', '+507', 'PA', '2021-11-14 20:04:36', '2021-11-14 20:04:36'),
(168, 'Papua New Guinea', '+675', 'PG', '2021-11-14 20:04:36', '2021-11-14 20:04:36'),
(169, 'Paraguay', '+595', 'PY', '2021-11-14 20:04:36', '2021-11-14 20:04:36'),
(170, 'Peru', '+51', 'PE', '2021-11-14 20:04:36', '2021-11-14 20:04:36'),
(171, 'Philippines', '+63', 'PH', '2021-11-14 20:04:36', '2021-11-14 20:04:36'),
(172, 'Pitcairn', '+872', 'PN', '2021-11-14 20:04:36', '2021-11-14 20:04:36'),
(173, 'Poland', '+48', 'PL', '2021-11-14 20:04:36', '2021-11-14 20:04:36'),
(174, 'Portugal', '+351', 'PT', '2021-11-14 20:04:36', '2021-11-14 20:04:36'),
(175, 'Puerto Rico', '+1939', 'PR', '2021-11-14 20:04:36', '2021-11-14 20:04:36'),
(176, 'Qatar', '+974', 'QA', '2021-11-14 20:04:36', '2021-11-14 20:04:36'),
(177, 'Romania', '+40', 'RO', '2021-11-14 20:04:36', '2021-11-14 20:04:36'),
(178, 'Russia', '+7', 'RU', '2021-11-14 20:04:36', '2021-11-14 20:04:36'),
(179, 'Rwanda', '+250', 'RW', '2021-11-14 20:04:36', '2021-11-14 20:04:36'),
(180, 'Reunion', '+262', 'RE', '2021-11-14 20:04:36', '2021-11-14 20:04:36'),
(181, 'Saint Barthelemy', '+590', 'BL', '2021-11-14 20:04:36', '2021-11-14 20:04:36'),
(182, 'Saint Helena, Ascension and Tristan Da Cunha', '+290', 'SH', '2021-11-14 20:04:36', '2021-11-14 20:04:36'),
(183, 'Saint Kitts and Nevis', '+1869', 'KN', '2021-11-14 20:04:36', '2021-11-14 20:04:36'),
(184, 'Saint Lucia', '+1758', 'LC', '2021-11-14 20:04:36', '2021-11-14 20:04:36'),
(185, 'Saint Martin', '+590', 'MF', '2021-11-14 20:04:36', '2021-11-14 20:04:36'),
(186, 'Saint Pierre and Miquelon', '+508', 'PM', '2021-11-14 20:04:36', '2021-11-14 20:04:36'),
(187, 'Saint Vincent and the Grenadines', '+1784', 'VC', '2021-11-14 20:04:36', '2021-11-14 20:04:36'),
(188, 'Samoa', '+685', 'WS', '2021-11-14 20:04:36', '2021-11-14 20:04:36'),
(189, 'San Marino', '+378', 'SM', '2021-11-14 20:04:36', '2021-11-14 20:04:36'),
(190, 'Sao Tome and Principe', '+239', 'ST', '2021-11-14 20:04:36', '2021-11-14 20:04:36'),
(191, 'Saudi Arabia', '+966', 'SA', '2021-11-14 20:04:36', '2021-11-14 20:04:36'),
(192, 'Senegal', '+221', 'SN', '2021-11-14 20:04:36', '2021-11-14 20:04:36'),
(193, 'Serbia', '+381', 'RS', '2021-11-14 20:04:36', '2021-11-14 20:04:36'),
(194, 'Seychelles', '+248', 'SC', '2021-11-14 20:04:36', '2021-11-14 20:04:36'),
(195, 'Sierra Leone', '+232', 'SL', '2021-11-14 20:04:36', '2021-11-14 20:04:36'),
(196, 'Singapore', '+65', 'SG', '2021-11-14 20:04:36', '2021-11-14 20:04:36'),
(197, 'Slovakia', '+421', 'SK', '2021-11-14 20:04:36', '2021-11-14 20:04:36'),
(198, 'Slovenia', '+386', 'SI', '2021-11-14 20:04:36', '2021-11-14 20:04:36'),
(199, 'Solomon Islands', '+677', 'SB', '2021-11-14 20:04:36', '2021-11-14 20:04:36'),
(200, 'Somalia', '+252', 'SO', '2021-11-14 20:04:36', '2021-11-14 20:04:36'),
(201, 'South Africa', '+27', 'ZA', '2021-11-14 20:04:36', '2021-11-14 20:04:36'),
(202, 'South Sudan', '+211', 'SS', '2021-11-14 20:04:36', '2021-11-14 20:04:36'),
(203, 'South Georgia and the South Sandwich Islands', '+500', 'GS', '2021-11-14 20:04:36', '2021-11-14 20:04:36'),
(204, 'Spain', '+34', 'ES', '2021-11-14 20:04:36', '2021-11-14 20:04:36'),
(205, 'Sri Lanka', '+94', 'LK', '2021-11-14 20:04:36', '2021-11-14 20:04:36'),
(206, 'Sudan', '+249', 'SD', '2021-11-14 20:04:36', '2021-11-14 20:04:36'),
(207, 'Suriname', '+597', 'SR', '2021-11-14 20:04:36', '2021-11-14 20:04:36'),
(208, 'Svalbard and Jan Mayen', '+47', 'SJ', '2021-11-14 20:04:36', '2021-11-14 20:04:36'),
(209, 'Swaziland', '+268', 'SZ', '2021-11-14 20:04:36', '2021-11-14 20:04:36'),
(210, 'Sweden', '+46', 'SE', '2021-11-14 20:04:36', '2021-11-14 20:04:36'),
(211, 'Switzerland', '+41', 'CH', '2021-11-14 20:04:36', '2021-11-14 20:04:36'),
(212, 'Syrian Arab Republic', '+963', 'SY', '2021-11-14 20:04:36', '2021-11-14 20:04:36'),
(213, 'Taiwan', '+886', 'TW', '2021-11-14 20:04:36', '2021-11-14 20:04:36'),
(214, 'Tajikistan', '+992', 'TJ', '2021-11-14 20:04:36', '2021-11-14 20:04:36'),
(215, 'Tanzania, United Republic of Tanzania', '+255', 'TZ', '2021-11-14 20:04:36', '2021-11-14 20:04:36'),
(216, 'Thailand', '+66', 'TH', '2021-11-14 20:04:36', '2021-11-14 20:04:36'),
(217, 'Timor-Leste', '+670', 'TL', '2021-11-14 20:04:36', '2021-11-14 20:04:36'),
(218, 'Togo', '+228', 'TG', '2021-11-14 20:04:36', '2021-11-14 20:04:36'),
(219, 'Tokelau', '+690', 'TK', '2021-11-14 20:04:36', '2021-11-14 20:04:36'),
(220, 'Tonga', '+676', 'TO', '2021-11-14 20:04:36', '2021-11-14 20:04:36'),
(221, 'Trinidad and Tobago', '+1868', 'TT', '2021-11-14 20:04:36', '2021-11-14 20:04:36'),
(222, 'Tunisia', '+216', 'TN', '2021-11-14 20:04:36', '2021-11-14 20:04:36'),
(223, 'Turkey', '+90', 'TR', '2021-11-14 20:04:36', '2021-11-14 20:04:36'),
(224, 'Turkmenistan', '+993', 'TM', '2021-11-14 20:04:36', '2021-11-14 20:04:36'),
(225, 'Turks and Caicos Islands', '+1649', 'TC', '2021-11-14 20:04:36', '2021-11-14 20:04:36'),
(226, 'Tuvalu', '+688', 'TV', '2021-11-14 20:04:36', '2021-11-14 20:04:36'),
(227, 'Uganda', '+256', 'UG', '2021-11-14 20:04:36', '2021-11-14 20:04:36'),
(228, 'Ukraine', '+380', 'UA', '2021-11-14 20:04:36', '2021-11-14 20:04:36'),
(229, 'United Arab Emirates', '+971', 'AE', '2021-11-14 20:04:36', '2021-11-14 20:04:36'),
(230, 'United Kingdom', '+44', 'GB', '2021-11-14 20:04:36', '2021-11-14 20:04:36'),
(231, 'United States', '+1', 'US', '2021-11-14 20:04:36', '2021-11-14 20:04:36'),
(232, 'Uruguay', '+598', 'UY', '2021-11-14 20:04:36', '2021-11-14 20:04:36'),
(233, 'Uzbekistan', '+998', 'UZ', '2021-11-14 20:04:36', '2021-11-14 20:04:36'),
(234, 'Vanuatu', '+678', 'VU', '2021-11-14 20:04:36', '2021-11-14 20:04:36'),
(235, 'Venezuela, Bolivarian Republic of Venezuela', '+58', 'VE', '2021-11-14 20:04:36', '2021-11-14 20:04:36'),
(236, 'Vietnam', '+84', 'VN', '2021-11-14 20:04:36', '2021-11-14 20:04:36'),
(237, 'Virgin Islands, British', '+1284', 'VG', '2021-11-14 20:04:36', '2021-11-14 20:04:36'),
(238, 'Virgin Islands, U.S.', '+1340', 'VI', '2021-11-14 20:04:36', '2021-11-14 20:04:36'),
(239, 'Wallis and Futuna', '+681', 'WF', '2021-11-14 20:04:36', '2021-11-14 20:04:36'),
(240, 'Yemen', '+967', 'YE', '2021-11-14 20:04:36', '2021-11-14 20:04:36'),
(241, 'Zambia', '+260', 'ZM', '2021-11-14 20:04:36', '2021-11-14 20:04:36'),
(242, 'Zimbabwe', '+263', 'ZW', '2021-11-14 20:04:36', '2021-11-14 20:04:36');

-- --------------------------------------------------------

--
-- Table structure for table `phone_code_allows`
--

CREATE TABLE `phone_code_allows` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `phone_code_id` bigint(20) UNSIGNED DEFAULT NULL,
  `name` varchar(300) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dial_code` varchar(300) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `code` varchar(300) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `icon` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `phone_code_allows`
--

INSERT INTO `phone_code_allows` (`id`, `phone_code_id`, `name`, `dial_code`, `code`, `icon`, `created_at`, `updated_at`) VALUES
(1, 63, 'Egypt', '+20', 'EG', 'phone_code_allows/BpsCmaBfsQseJDtdY57iDREQBDkqAMY7c2p5QkNj.png', '2021-11-14 19:57:16', '2021-11-24 18:48:51'),
(2, 191, 'Saudi Arabia', '+966', 'SA', 'phone_code_allows/j3r2XKwPjVYJBfvGV7bBI4Nv8UNBFvV8apdN0JeG.png', '2021-11-14 19:57:16', '2021-11-24 18:49:02');

-- --------------------------------------------------------

--
-- Table structure for table `programs`
--

CREATE TABLE `programs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `icon` varchar(300) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `programs`
--

INSERT INTO `programs` (`id`, `icon`, `created_at`, `updated_at`) VALUES
(1, 'http://localhost/codlop/public/website/assets/images/icons/android.png', '2021-11-17 22:15:14', NULL),
(2, 'http://localhost/codlop/public/website/assets/images/icons/apple.png', '2021-11-17 22:15:14', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `program_trans`
--

CREATE TABLE `program_trans` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `program_id` bigint(20) UNSIGNED DEFAULT NULL,
  `title` varchar(300) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `text` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lang` varchar(300) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `program_trans`
--

INSERT INTO `program_trans` (`id`, `program_id`, `title`, `text`, `lang`, `created_at`, `updated_at`) VALUES
(1, 1, 'اندرويد', NULL, 'ar', '2021-11-17 22:17:18', NULL),
(2, 1, 'android', NULL, 'en', '2021-11-17 22:17:18', NULL),
(3, 2, 'ios', NULL, 'ar', '2021-11-17 22:17:18', NULL),
(4, 2, 'ios', NULL, 'en', '2021-11-17 22:17:18', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `services`
--

CREATE TABLE `services` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `icon` varchar(300) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` enum('default','new') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'new',
  `service_page` enum('service_1','service_2','service_3','service_4','service_5','service_6','service_7','service_8','other') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'other',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `services`
--

INSERT INTO `services` (`id`, `icon`, `image`, `type`, `service_page`, `created_at`, `updated_at`) VALUES
(1, 'https://codlop.com/website/assets/images/icons/15.png', NULL, 'new', 'service_1', '2021-11-17 19:40:50', NULL),
(2, 'https://codlop.com/website/assets/images/icons/14.png', 'https://codlop.com/website/assets/images/header4.png', 'new', 'service_2', '2021-11-17 19:40:50', NULL),
(3, 'https://codlop.com/website/assets/images/icons/10.png', NULL, 'new', 'service_3', '2021-11-17 19:40:50', NULL),
(4, 'https://codlop.com/website/assets/images/icons/11.png', 'https://codlop.com/website/assets/images/Digital_marketing_1.webp', 'new', 'service_4', '2021-11-17 19:40:50', NULL),
(5, 'https://codlop.com/website/assets/images/icons/12.png', NULL, 'new', 'service_5', '2021-11-17 19:40:50', NULL),
(6, 'https://codlop.com/website/assets/images/icons/13.png', NULL, 'new', 'service_6', '2021-11-17 19:40:50', NULL),
(7, 'https://codlop.com/website/assets/images/icons/16.png', 'https://codlop.com/website/assets/images/motion-graphics2.png', 'new', 'service_7', '2021-11-17 19:40:50', NULL),
(8, 'https://codlop.com/website/assets/images/icons/17.png', 'https://codlop.com/website/assets/images/mark-8.png', 'new', 'service_8', '2021-11-17 19:40:50', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `service_hosts`
--

CREATE TABLE `service_hosts` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `service_id` bigint(20) UNSIGNED DEFAULT NULL,
  `price` bigint(20) UNSIGNED DEFAULT NULL,
  `host_type` enum('economic','professional','golden') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `service_hosts`
--

INSERT INTO `service_hosts` (`id`, `service_id`, `price`, `host_type`, `created_at`, `updated_at`) VALUES
(1, 3, 3000, 'economic', '2021-11-19 00:05:27', NULL),
(2, 3, 5000, 'professional', '2021-11-19 00:05:27', NULL),
(3, 3, 7000, 'golden', '2021-11-19 00:06:07', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `service_host_details`
--

CREATE TABLE `service_host_details` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `service_host_id` bigint(20) UNSIGNED DEFAULT NULL,
  `service_id` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `service_host_details`
--

INSERT INTO `service_host_details` (`id`, `service_host_id`, `service_id`, `created_at`, `updated_at`) VALUES
(1, 1, 3, '2021-11-19 00:24:24', NULL),
(2, 1, 3, '2021-11-19 00:24:24', NULL),
(3, 1, 3, '2021-11-19 00:24:24', NULL),
(4, 1, 3, '2021-11-19 00:24:24', NULL),
(5, 1, 3, '2021-11-19 00:24:24', NULL),
(6, 1, 3, '2021-11-19 00:24:24', NULL),
(7, 2, 3, '2021-11-19 00:24:24', NULL),
(8, 2, 3, '2021-11-19 00:24:24', NULL),
(9, 2, 3, '2021-11-19 00:24:24', NULL),
(10, 2, 3, '2021-11-19 00:24:24', NULL),
(11, 2, 3, '2021-11-19 00:24:24', NULL),
(12, 2, 3, '2021-11-19 00:24:24', NULL),
(13, 3, 3, '2021-11-19 00:24:24', NULL),
(14, 3, 3, '2021-11-19 00:24:24', NULL),
(15, 3, 3, '2021-11-19 00:24:24', NULL),
(16, 3, 3, '2021-11-19 00:24:24', NULL),
(17, 3, 3, '2021-11-19 00:24:24', NULL),
(18, 3, 3, '2021-11-19 00:24:24', NULL),
(20, 1, 3, '2021-12-04 02:34:10', '2021-12-04 02:34:10'),
(21, 2, 3, '2021-12-04 02:34:10', '2021-12-04 02:34:10'),
(22, 3, 3, '2021-12-04 02:34:10', '2021-12-04 02:34:10'),
(32, 1, 3, '2021-12-04 02:57:15', '2021-12-04 02:57:15'),
(33, 2, 3, '2021-12-04 02:57:15', '2021-12-04 02:57:15'),
(34, 3, 3, '2021-12-04 02:57:15', '2021-12-04 02:57:15');

-- --------------------------------------------------------

--
-- Table structure for table `service_host_detail_trans`
--

CREATE TABLE `service_host_detail_trans` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `service_host_detail_id` bigint(20) UNSIGNED DEFAULT NULL,
  `title` varchar(300) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `text` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lang` varchar(300) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `service_host_detail_trans`
--

INSERT INTO `service_host_detail_trans` (`id`, `service_host_detail_id`, `title`, `text`, `lang`, `created_at`, `updated_at`) VALUES
(1, 1, '3GB مساحة تخزين', NULL, 'ar', '2021-11-19 00:26:30', NULL),
(2, 1, '3GB storage space', NULL, 'en', '2021-11-19 00:28:30', NULL),
(3, 2, 'تسجيل نطاق مجاني', NULL, 'ar', '2021-11-19 00:26:30', NULL),
(4, 2, 'Free domain registration', NULL, 'en', '2021-11-19 00:28:02', NULL),
(5, 3, '30GB باندويث', NULL, 'ar', '2021-11-19 00:26:30', NULL),
(6, 3, '30GB Bandwith', NULL, 'en', '2021-11-19 00:28:02', NULL),
(7, 4, '20 بريد الكترونى', NULL, 'ar', '2021-11-19 00:26:30', NULL),
(8, 4, '20 emails', NULL, 'en', '2021-11-19 00:28:02', NULL),
(9, 5, 'دومين فرعى عدد لا نهائي', NULL, 'ar', '2021-11-19 00:26:30', NULL),
(10, 5, 'subdomain infinite number', NULL, 'en', '2021-11-19 00:28:02', NULL),
(11, 6, 'قاعدة البيانات عدد لا نهائي', NULL, 'ar', '2021-11-19 00:26:30', NULL),
(12, 6, 'The database is infinite', NULL, 'en', '2021-11-19 00:28:02', NULL),
(13, 7, '10GB مساحة تخزين', NULL, 'ar', '2021-11-19 00:26:30', NULL),
(14, 7, '10GB storage space', NULL, 'en', '2021-11-19 00:28:30', NULL),
(15, 8, 'تسجيل نطاق مجاني', NULL, 'ar', '2021-11-19 00:26:30', NULL),
(16, 8, 'Free domain registration', NULL, 'en', '2021-11-19 00:28:02', NULL),
(17, 9, '100GB باندويث', NULL, 'ar', '2021-11-19 00:26:30', NULL),
(18, 9, '100GB Bandwith', NULL, 'en', '2021-11-19 00:28:02', NULL),
(19, 10, 'بريد الكترونى عدد لا نهائي', NULL, 'ar', '2021-11-19 00:26:30', NULL),
(20, 10, 'Unlimited e-mail', NULL, 'en', '2021-11-19 00:28:02', NULL),
(21, 11, 'دومين فرعى عدد لا نهائي', NULL, 'ar', '2021-11-19 00:26:30', NULL),
(22, 11, 'subdomain infinite number', NULL, 'en', '2021-11-19 00:28:02', NULL),
(23, 12, 'قاعدة البيانات عدد لا نهائي', NULL, 'ar', '2021-11-19 00:26:30', NULL),
(24, 12, 'The database is infinite', NULL, 'en', '2021-11-19 00:28:02', NULL),
(25, 13, '15GB مساحة تخزين', NULL, 'ar', '2021-11-19 00:26:30', NULL),
(26, 13, '15GB storage space', NULL, 'en', '2021-11-19 00:28:30', NULL),
(27, 14, 'تسجيل نطاق مجاني', NULL, 'ar', '2021-11-19 00:26:30', NULL),
(28, 14, 'Free domain registration', NULL, 'en', '2021-11-19 00:28:02', NULL),
(29, 15, '120GB باندويث', NULL, 'ar', '2021-11-19 00:26:30', NULL),
(30, 15, '120GB Bandwith', NULL, 'en', '2021-11-19 00:28:02', NULL),
(31, 16, 'بريد الكترونى عدد لا نهائي', NULL, 'ar', '2021-11-19 00:26:30', NULL),
(32, 16, 'Unlimited e-mail', NULL, 'en', '2021-11-19 00:28:02', NULL),
(33, 17, 'دومين فرعى عدد لا نهائي', NULL, 'ar', '2021-11-19 00:26:30', NULL),
(34, 17, 'subdomain infinite number', NULL, 'en', '2021-11-19 00:28:02', NULL),
(35, 18, 'قاعدة البيانات عدد لا نهائي', NULL, 'ar', '2021-11-19 00:26:30', NULL),
(36, 18, 'The database is infinite', NULL, 'en', '2021-11-19 00:28:02', NULL),
(37, 20, '5 جيجا', NULL, 'ar', '2021-12-04 02:34:10', '2021-12-04 02:34:10'),
(38, 20, '5Giga', NULL, 'en', '2021-12-04 02:34:10', '2021-12-04 02:34:10'),
(39, 21, '15 جيجا', NULL, 'ar', '2021-12-04 02:34:10', '2021-12-04 02:34:10'),
(40, 21, NULL, NULL, 'en', '2021-12-04 02:34:10', '2021-12-04 02:34:10'),
(41, 22, '50 جيجا', NULL, 'ar', '2021-12-04 02:34:10', '2021-12-04 02:34:10'),
(42, 22, NULL, NULL, 'en', '2021-12-04 02:34:10', '2021-12-04 02:34:10'),
(59, 33, '15  رام', NULL, 'ar', '2021-12-04 02:57:15', '2021-12-04 02:57:15'),
(60, 33, '15Ram', NULL, 'en', '2021-12-04 02:57:15', '2021-12-04 02:57:15'),
(61, 34, '30 رام', NULL, 'ar', '2021-12-04 02:57:15', '2021-12-04 02:57:15'),
(62, 34, '30Ram', NULL, 'en', '2021-12-04 02:57:15', '2021-12-04 02:57:15');

-- --------------------------------------------------------

--
-- Table structure for table `service_host_trans`
--

CREATE TABLE `service_host_trans` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `service_host_id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(500) DEFAULT NULL,
  `text` text DEFAULT NULL,
  `lang` varchar(300) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `service_host_trans`
--

INSERT INTO `service_host_trans` (`id`, `service_host_id`, `title`, `text`, `lang`, `created_at`, `updated_at`) VALUES
(1, 1, 'الاقتصادية', NULL, 'ar', '2021-11-19 01:13:04', NULL),
(2, 1, 'Economic', NULL, 'en', '2021-11-19 01:13:04', NULL),
(3, 2, 'المحترفين', NULL, 'ar', '2021-11-19 01:13:04', NULL),
(4, 2, 'Professionals', NULL, 'en', '2021-11-19 01:13:04', NULL),
(5, 3, 'الذهبية', NULL, 'ar', '2021-11-19 01:13:04', NULL),
(6, 3, 'golden', NULL, 'en', '2021-11-19 01:13:04', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `service_texts`
--

CREATE TABLE `service_texts` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `service_id` bigint(20) UNSIGNED DEFAULT NULL,
  `image` varchar(300) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` enum('normal','list') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'normal',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `service_texts`
--

INSERT INTO `service_texts` (`id`, `service_id`, `image`, `type`, `created_at`, `updated_at`) VALUES
(1, 1, 'http://localhost/codlop/public/website/assets/images/store-1.png', 'list', '2021-11-19 22:57:32', NULL),
(2, 1, 'http://localhost/codlop/public/website/assets/images/web-16.png', 'list', '2021-11-19 22:57:32', NULL),
(3, 1, 'http://localhost/codlop/public/website/assets/images/web-16.png', 'list', '2021-11-19 22:57:32', NULL),
(5, 1, 'http://localhost/codlop/public/website/assets/images/web-16.png', 'list', '2021-11-19 22:57:32', NULL),
(6, 1, 'service_texts/9suIi3mOVaC00G9tk92hkdrHgztsBO94e7eMaarc.png', 'list', '2021-11-19 22:57:32', '2021-12-04 02:06:59'),
(8, 2, 'service_texts/lZP0G2dmHELMVLJCxat5FcSnqSlwLEQUrTr36WpH.png', 'list', '2021-11-19 22:57:32', '2021-12-06 21:15:54'),
(9, 2, 'http://localhost/codlop/public/website/assets/images/mob-4.png', 'list', '2021-11-19 22:57:32', NULL),
(10, 2, 'http://localhost/codlop/public/website/assets/images/mob-7.png', 'list', '2021-11-19 22:57:32', NULL),
(12, 2, 'http://localhost/codlop/public/website/assets/images/mob-8.png', 'list', '2021-11-19 22:57:32', NULL),
(13, 2, 'http://localhost/codlop/public/website/assets/images/mob-6.png', 'list', '2021-11-19 22:57:32', NULL),
(14, 4, 'http://localhost/codlop/public/website/assets/images/Mobile%20Marketing-amico.png', 'normal', '2021-11-19 22:57:32', NULL),
(15, 4, 'http://localhost/codlop/public/website/assets/images/icons/study.png', 'list', '2021-11-19 22:57:32', NULL),
(16, 4, 'http://localhost/codlop/public/website/assets/images/icons/marketing-strategy.png', 'list', '2021-11-19 22:57:32', NULL),
(17, 5, 'http://localhost/codlop/public/website/assets/images/mark-10.png', 'normal', '2021-11-19 22:57:32', NULL),
(18, 6, 'http://localhost/codlop/public/website/assets/images/logo.svg', 'normal', '2021-11-19 22:57:32', NULL),
(19, 7, 'service_texts/w8KPnWPGkfAQFcVGLhqo0jCGcZtDjleFeeqybnPb.jpeg', 'normal', '2021-11-19 22:57:32', '2021-12-09 08:32:09'),
(20, 7, 'http://localhost/codlop/public/website/assets/images/announc_feat1.png', 'list', '2021-11-19 22:57:32', NULL),
(21, 7, 'service_texts/dJM4aaZS0DqkgO0iUQZcXcQvn5WDSTetWdn0N8Rq.png', 'list', '2021-11-19 22:57:32', '2021-12-09 08:37:02'),
(22, 8, NULL, 'normal', '2021-11-19 22:57:32', NULL),
(23, 8, NULL, 'list', '2021-11-19 22:57:32', NULL),
(24, 8, NULL, 'list', '2021-11-19 22:57:32', NULL),
(25, 8, NULL, 'list', '2021-11-19 22:57:32', NULL),
(32, 4, 'service_texts/90Kwr46lsfxRSXfcNv01N1KMfe0mzQZoFZeA6o1s.png', 'list', '2021-11-29 19:46:57', '2021-11-29 19:46:57'),
(34, 8, NULL, 'list', '2021-11-29 20:48:40', '2021-11-29 20:48:40'),
(36, 2, 'service_texts/eXS7HhGDe0jMrH31IXc1yaVLlHxYxehPFGsz4jhW.jpeg', 'list', '2021-12-09 08:04:22', '2021-12-09 08:04:22'),
(37, 4, 'service_texts/gHJIhUqEVnNwC8gFd4RV9kYlTGn9xwnZJafmB5uC.svg', 'list', '2021-12-09 08:22:56', '2021-12-09 08:22:56'),
(41, 7, NULL, 'list', '2021-12-19 07:18:00', '2021-12-19 07:18:00');

-- --------------------------------------------------------

--
-- Table structure for table `service_text_trans`
--

CREATE TABLE `service_text_trans` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `service_text_id` bigint(20) UNSIGNED DEFAULT NULL,
  `title` varchar(300) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `text` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lang` varchar(300) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `service_text_trans`
--

INSERT INTO `service_text_trans` (`id`, `service_text_id`, `title`, `text`, `lang`, `created_at`, `updated_at`) VALUES
(3, 2, 'موقع شركة', 'اطلق نجاحك من خلال تصميم موقع شركة مميز يعبر عنك وعن نشاطك بشكل مميز وابداعي خبرتنا فى مجال تصميم المواقع تجعلنا نقدم خدمه تصميم مواقع الشركات باحترافية تامة لتلبية جميع احتياجاتكم لاطلاق نجاح مبهر عبر الانترنت عبر خدمات تصميم وانشاء و برمجة مواقع الشركات المتخصصة من', 'ar', '2021-11-19 23:01:37', NULL),
(4, 2, 'Company Website', 'Unleash your success by designing a distinctive company website that expresses you and your activity in a distinctive and creative way.', 'en', '2021-11-19 23:01:37', NULL),
(5, 3, 'موقع مدرسة', 'اطلق نجاحك عبر الانترنت من خلال تصميم متجر الكترونى إحترافي بأفضل الاسعار وأفضل جوده حيث تعتبر شركة كود لوب احد الشركات المتخصصة في تصميم وانشاء وبرمجة المتاجر الالكترونية الابداعية', 'ar', '2021-11-19 23:01:37', NULL),
(6, 3, 'school site', 'Launch your success online by designing a professional online store at the best prices and the best quality, as Code Loop is one of the companies specialized in designing, creating and programming creative electronic stores', 'en', '2021-11-19 23:01:37', NULL),
(9, 5, 'موقع عقاري', 'اطلق نجاحك عبر الانترنت من خلال تصميم متجر الكترونى إحترافي بأفضل الاسعار وأفضل جوده حيث تعتبر شركة كود لوب احد الشركات المتخصصة في تصميم وانشاء وبرمجة المتاجر الالكترونية الابداعية', 'ar', '2021-11-19 23:01:37', NULL),
(10, 5, 'real estate website', 'Launch your success online by designing a professional online store at the best prices and the best quality, as Code Loop is one of the companies specialized in designing, creating and programming creative electronic stores', 'en', '2021-11-19 23:01:37', NULL),
(15, 9, 'الحماية', 'التطبيق وقاعدة البيانات محمية وآمنه بنسبة 100%', 'ar', '2021-11-19 23:01:37', NULL),
(16, 9, 'protection', 'The application and database are 100% secure and protected', 'en', '2021-11-19 23:01:37', NULL),
(17, 10, 'التصميم', 'اطلق نجاحك عبر الانترنت من خلال تصميم متجر الكترونى', 'ar', '2021-11-19 23:01:37', NULL),
(18, 10, 'design', 'Launch your success online by designing a professional online', 'en', '2021-11-19 23:01:37', NULL),
(23, 13, 'الدعم الفنى', 'اطلق نجاحك عبر الانترنت من خلال تصميم متجر الكترونى', 'ar', '2021-11-19 23:01:37', NULL),
(24, 13, 'technical support', 'Launch your success online by designing a professional online', 'en', '2021-11-19 23:01:37', NULL),
(29, 16, 'وضع استراتيجية تسويق', 'اطلق نجاحك عبر الانترنت من خلال تصميم متجر الكترونى', 'ar', '2021-11-19 23:01:37', NULL),
(30, 16, 'Develop a marketing strategy', 'Launch your success online by designing a professional online', 'en', '2021-11-19 23:01:37', NULL),
(33, 18, 'ما هي الهوية التجارية؟', 'الهوية التجارية هي أساس نجاح أي شركة سواء كانت كبيرة أو صغيرة، ووضع خطة إستراتيجية للتعريف بالعلامة التجارية هو الحل الامثل في ظل الاسواق المليئة بالعديد من المنافسين فهوية الشركة هي الوعد الذي تخبر به المستهلك، فهي تخبر المستهلك ما تقدمه من جودة وما الذي يفرق بينك وبين المنافسين. هوية الشركة أو العلامة التجارية مشتقة مما أنت عليه، الهدف الذي تريد أن تصل بشركتك إليه.', 'ar', '2021-11-19 23:01:37', NULL),
(34, 18, 'What is a business identity?', 'The commercial identity is the basis for the success of any company, whether it is large or small, and developing a strategic plan to introduce the brand is the best solution in light of the markets filled with many competitors. competitors. Corporate or brand identity is derived from who you are, the goal you want your company to reach.', 'en', '2021-11-19 23:01:37', NULL),
(37, 20, 'القصة', 'حركة التصميمات التي تجذب انتباه الجمهور نابعة من جودة القصة التي وراء التصميم لذلك نعتني بكتابة الإسكريبت جيدًا', 'ar', '2021-11-19 23:01:37', NULL),
(38, 20, 'the story', 'The movement of designs that captures the audience\'s attention stems from the quality of the story behind the design, so we take great care in writing the script.', 'en', '2021-11-19 23:01:37', NULL),
(73, 32, 'qweqwe', 'weqweqeqwe', 'ar', '2021-11-29 19:46:57', '2021-11-29 19:46:57'),
(74, 32, 'qweqwe', 'qweq', 'en', '2021-11-29 19:46:57', '2021-11-29 19:46:57'),
(93, 24, 'كتابة وتعبئة المحتوي بشكل مستمر', '#', 'ar', '2021-11-29 20:12:55', '2021-11-29 20:12:55'),
(94, 24, 'Continuously writing and filling out content', '#', 'en', '2021-11-29 20:12:55', '2021-11-29 20:12:55'),
(95, 25, 'الرد على العملاء و المتابعين للصفحات', '#', 'ar', '2021-11-29 20:13:09', '2021-11-29 20:13:09'),
(96, 25, 'Reply to customers and page followers', '#', 'en', '2021-11-29 20:13:09', '2021-11-29 20:13:09'),
(101, 23, 'تصميم الهوية البصرية على شبكات التواصل الاجتماعي وتصميم البنرات', '#', 'ar', '2021-11-29 20:19:54', '2021-11-29 20:19:54'),
(102, 23, 'Visual identity design on social networks and banner design', '#', 'en', '2021-11-29 20:19:54', '2021-11-29 20:19:54'),
(107, 22, 'ادارة شبكات التواصل الاجتماعى', 'تعد السوشيال ميديا عنصر مهم فى التسويق الالكترونى لنشاطك التجارى نظرا لانها تضم مجموعة هائلة من المستخدمين , حيث يزداد عدد مستخدمين وسائل التواصل الاجتماعى يوميا مما يشجع صاحب اى مشروع تجارى على الاعتماد على هذا التسويق الالكتروني من خلال شركة اعلانات سوشيال ميديا قادرة على تحقيق أهدافك. إبدا بتنمية أعمالك ونشاطك من خلال التسويق الالكتروني بتواجدك الحقيقي والفعال عبر الانترنت والتواصل الاجتماعي , سنقدم لك كل ماتحتاج إليه من البداية وحتى ظهور content marketing الخاص بك على مواقع social media', 'ar', '2021-11-29 20:20:53', '2021-11-29 20:20:53'),
(108, 22, 'Social media management', 'Social media is an important element in the e-marketing of your business because it includes a huge group of users, as the number of social media users increases daily, which encourages the owner of any business to rely on this electronic marketing through a social media advertising company that is able to achieve your goals. Start developing your business and activity through e-marketing with your real and effective online presence and social networking, we will provide you with everything you need from the beginning until the appearance of your content marketing on social media sites', 'en', '2021-11-29 20:20:53', '2021-11-29 20:20:53'),
(109, 34, 'جلب المتابعين و المعجبين الحقيقين بطرق مبتكرة وسليمة 100%.', '#', 'ar', '2021-11-29 20:48:40', '2021-11-29 20:48:40'),
(110, 34, 'Get real followers and fans in 100% innovative and sound ways.', '#', 'en', '2021-11-29 20:48:40', '2021-11-29 20:48:40'),
(111, 17, 'تهيئة محركات البحث SEO1', 'الظهور في النتائج الاولي بشكل طبيعي في جوجل عند البحث عن منتجك او الخدمات التي تقدمها قد يقدم لك فرصة كبيرة جدا لتطوير اعمالك كمثال عند الظهور كأول وكالة سفر مقترحة من جوجل في الصفحة الاولي قد تحصل على اكثر من 35 الف زائر شهرياً اذا تعاقد نسبة 1% فقط من الزوار اي 350 عميل شهرياً فقط من خلال الظهور في النتيجة الاولي في جوجل', 'ar', '2021-12-04 01:55:48', '2021-12-04 01:55:48'),
(112, 17, 'SEO Search Engine Optimization', 'Appearing in the first results naturally in Google when searching for your product or services that you provide may present you with a very big opportunity to develop your business, for example when you appear as the first travel agency proposed by Google on the first page, you may get more than 35,000 visitors per month if you contract 1% Only from visitors i.e. 350 customers per month only by appearing in the first result in Google', 'en', '2021-12-04 01:55:48', '2021-12-04 01:55:48'),
(115, 1, 'تصميم المواقع الالكترونية', 'اطلق نجاحك عبر الانترنت من خلال تصميم متجر الكترونى إحترافي بأفضل الاسعار وأفضل جوده حيث تعتبر شركة كود لوب احد الشركات المتخصصة في تصميم وانشاء وبرمجة المتاجر الالكترونية الابداعية', 'ar', '2021-12-04 02:05:09', '2021-12-04 02:05:09'),
(116, 1, 'Website design', 'Launch your success online by designing a professional online store at the best prices and the best quality. Code Loop is one of the companies specialized in designing, creating and programming creative electronic stores.', 'en', '2021-12-04 02:05:09', '2021-12-04 02:05:09'),
(117, 6, 'تطوير فكرتك الخاصة', 'اطلق نجاحك من خلال تصميم موقع شركة مميز يعبر عنك وعن نشاطك بشكل مميز وابداعي خبرتنا فى مجال تصميم المواقع تجعلنا نقدم خدمه تصميم مواقع الشركات باحترافية تامة لتلبية جميع احتياجاتكم لاطلاق نجاح مبهر عبر الانترنت عبر خدمات تصميم وانشاء و برمجة مواقع الشركات المتخصصة من', 'ar', '2021-12-04 02:06:59', '2021-12-04 02:06:59'),
(118, 6, 'Develop your own idea', 'Unleash your success by designing a distinctive company website that expresses you and your activity in a distinctive and creative way.', 'en', '2021-12-04 02:06:59', '2021-12-04 02:06:59'),
(133, 36, 'لامنلكمن كمنيلكم ك ليكمنليب', 'بيليبليب يبليبلييبليب يببليب', 'ar', '2021-12-09 08:04:22', '2021-12-09 08:04:22'),
(134, 36, 'Title to try the sliders', 'dsf;lksdf;lksdf;lk flk;ds;lk fsd;lsfd k;', 'en', '2021-12-09 08:04:22', '2021-12-09 08:04:22'),
(137, 15, 'بسم الله', 'يبلكطيلميكميب ل كطمكيكطجبيكطكطيبكطيبلك كمينكملنمينكم كملنمك كل كمككمنبلمنك بكينم لنكميبلكمن يبنكمل يلكمنبكملنكمن كمنيبكمنكمنليكمنلكمننكملكمن كمن يمكنلي كمنلي كمنلي من يلكمنين ليكمنلبكمل نكملكم لمنكلنمكنكمكمنليكمنلكمنلنكم', 'ar', '2021-12-09 08:18:14', '2021-12-09 08:18:14'),
(138, 15, 'besm allah', 'dsfldsfkljk dsflk;dfs; kl;fds k;lfd s;lkfsd ;kldsf;lkd f;sld;skl ;kl sdf;kl fds;lkfsd ;lkdsf ;lksfd ;lkfsd;lksdf ;lkdfs ;lk;sd fk;kldfsk;ldsf k;ldf ss;lkjsgjk;lsskjlg ;kjl s;jkl skj;lskjls;kljg;sg;kjlskl;', 'en', '2021-12-09 08:18:14', '2021-12-09 08:18:14'),
(139, 37, 'من اعمالنا وسطة', 'يبلطميبل طميبل كيبل طيبمل بيطم لطميبطميبل كيبطيب طيب مطيبلطيبطمل يبط لطيطب', 'ar', '2021-12-09 08:22:56', '2021-12-09 08:22:56'),
(140, 37, 'wasta men fd.sf,ds;lfds', 'sdfsdl;jkfk ;ldfd;klsdkfl ;klsdf k;ldsf;klfdkl ;ldsflk; sd;lkfds', 'en', '2021-12-09 08:22:56', '2021-12-09 08:22:56'),
(141, 12, 'عنوان خدمة جديدة 123', 'عنوان خدمة جديدةعنوان خدمة جديدةعنوان خدمة جديدةعنوان خدمة جديدةعنوان خدمة جديدةعنوان خدمة جديدةعنوان خدمة جديدةعنوان خدمة جديدةعنوان خدمة جديدةعنوان خدمة جديدة', 'ar', '2021-12-09 08:29:08', '2021-12-09 08:29:08'),
(142, 12, 'New service address', 'New service addressnew service addressnew service addressnew service addressnew service addressnew service addressnew service addressnew service addressnew service addressnew service address', 'en', '2021-12-09 08:29:08', '2021-12-09 08:29:08'),
(145, 19, 'موشن جرافيكس', 'الموشن هو تقنية تحريك الرسومات لخلق الوهم في الحركة، وعادة ما تضاف لها بعض أنواع الصوت مثل المؤثرات الصوتية والموسيقى الخلفية، وتستخدم الرسوم المتحركة عادة في صنع الصور والفيديو، ولأننا في عصر الترابط والاتصالات، والرسوم المتحركة تعتبر واحدة من أفضل وسائل الاتصال لأن يمكنها توصيل الرسالة بطريقة ممتعة من خلال الصور، الحركة، الصوت وأكثر، وهذا يجعل من الرسوم المتحركة وسيلة تفاعلية لها القدرة على جذب جمهورك لعلامتك التجارية', 'ar', '2021-12-09 08:32:09', '2021-12-09 08:32:09'),
(146, 19, 'motion graphics', 'Motion is the technique of moving graphics to create an illusion in movement, and some types of sound are usually added to it, such as sound effects and background music, and animation is usually used in making images and videos, and because we are in the era of interconnectedness and communication, animation is considered one of the best means of communication because it can deliver the message In a fun way through images, animation, sound and more, this makes animation an interactive tool that has the ability to attract your audience to your brand', 'en', '2021-12-09 08:32:09', '2021-12-09 08:32:09'),
(151, 21, 'المرئيات 11', 'اختياررر النمط المرئي مهم للغاية فمن خلاله تعكس روح العلامة التجارية وتجعلها مميزة وفريدة', 'ar', '2021-12-09 08:37:02', '2021-12-09 08:37:02'),
(152, 21, 'visuals', 'Choosing the visual style is very important because it reflects the spirit of the brand and makes it distinctive and unique', 'en', '2021-12-09 08:37:02', '2021-12-09 08:37:02'),
(153, 8, 'ادارة شبكات التواصل الاجتماعي 1', 'تعد السوشيال ميديا عنصر مهم فى التسويق الالكترونى لنشاطك التجارى', 'ar', '2021-12-09 08:37:31', '2021-12-09 08:37:31'),
(154, 8, 'Managing social networks', 'Social media is an important element in the online marketing of your business', 'en', '2021-12-09 08:37:31', '2021-12-09 08:37:31'),
(157, 14, 'يبليبكملكم كم يبكمل لكميب', 'يبليبلبيتل منتلت منيبمتن ليبمنتلييسبمك يسبمكن نيسمكب منيسنكمب كمنسيبن كمسيبنمسيكمبننكميسب', 'ar', '2021-12-09 10:06:18', '2021-12-09 10:06:18'),
(158, 14, 'sdfsdfsdfdsds', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 'en', '2021-12-09 10:06:18', '2021-12-09 10:06:18'),
(191, 41, 'اا', 'اا', 'ar', '2021-12-19 07:18:00', '2021-12-19 07:18:00'),
(192, 41, 'Title to try the slidersا', 'اا', 'en', '2021-12-19 07:18:00', '2021-12-19 07:18:00');

-- --------------------------------------------------------

--
-- Table structure for table `service_trans`
--

CREATE TABLE `service_trans` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `service_id` bigint(20) UNSIGNED DEFAULT NULL,
  `title` varchar(300) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `text` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lang` varchar(300) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `service_trans`
--

INSERT INTO `service_trans` (`id`, `service_id`, `title`, `text`, `lang`, `created_at`, `updated_at`) VALUES
(1, 1, 'تصميم المواقع الالكترونية', 'اطلق نجاحك عبر الانترنت من خلال تصميم متجر الكترونى إحترافي بأفضل الاسعار وأفضل جوده حيث تعتبر شركة كود لوب احد الشركات المتخصصة في تصميم وانشاء وبرمجة المتاجر الالكترونية الابداعية', 'ar', '2021-11-17 19:54:54', NULL),
(2, 1, 'Website design', 'Launch your success online by designing a professional online store at the best prices and the best quality. Code Loop is one of the companies specialized in designing, creating and programming creative electronic stores.', 'en', '2021-11-17 19:58:00', NULL),
(3, 2, 'تصميم و برمجة تطبيقات الجوال', 'نقوم بتصميم وتطوير تطبيقات تلبي احتياجات الشركات والافراد و تحسين أداء انشطتك وتقديم حلول مبتكرة وفعالة.', 'ar', '2021-11-17 19:54:54', NULL),
(4, 2, 'Mobile app design and programming', 'We design and develop applications that meet the needs of companies and individuals, improve the performance of your activities, and provide innovative and effective solutions.', 'en', '2021-11-17 19:58:00', NULL),
(5, 3, 'استضافة وحجز الدومينات', 'احصل على استضافة امنة و سريعة تتناسب مع احتياجاتك و بأسعار منافسة تجعلك توجد على الانترنت دائما متميز.', 'ar', '2021-11-17 19:54:54', NULL),
(6, 3, 'Domain hosting and reservation', 'Get secure and fast hosting that suits your needs and at competitive prices that make you always distinguished on the Internet.', 'en', '2021-11-17 19:58:00', NULL),
(9, 5, ' السيو للمواقع والشركات SEO', 'يمكنك من خلال خدمة تحسين نتائج محركات البحث ان تقوم بوضع موقعك الالكتروني أعلى قمة المواقع الالكترونية المشابهة .', 'ar', '2021-11-17 19:54:54', NULL),
(10, 5, 'SEO for websites and companies SEO', 'Through the search engine optimization service, you can place your website at the top of similar websites.', 'en', '2021-11-17 19:58:00', NULL),
(13, 7, 'فيديو انيميشن', 'فريق عمل متخصص في تنفيذ وابتكار فيديوهات معبرة تصل الفكرة الي المشاهد وتلفت انتباهه', 'ar', '2021-11-17 19:54:54', NULL),
(14, 7, 'animation video', 'A team specializing in the implementation and creation of expressive videos that reach the idea to the viewer and draw his attention', 'en', '2021-11-17 19:58:00', NULL),
(17, 8, 'ادارة شبكات التواصل الاجتماعي', 'تعد السوشيال ميديا عنصر مهم فى التسويق الالكترونى لنشاطك التجارى', 'ar', '2021-11-27 14:35:59', '2021-11-27 14:35:59'),
(18, 8, 'Managing social networks', 'Social media is an important element in the online marketing of your business', 'en', '2021-11-27 14:35:59', '2021-11-27 14:35:59'),
(55, 4, 'التسويق الإلكتروني', 'لدينا الخبرة للوصول بأعلانك لعملائك المستهدفين بشكل مؤكد وفعال', 'ar', '2021-12-19 07:07:28', '2021-12-19 07:07:28'),
(56, 4, 'E-Marketing', 'We have the experience to reach your advertisements to your target customers in a sure and effective manner', 'en', '2021-12-19 07:07:28', '2021-12-19 07:07:28'),
(59, 6, 'تصميم شعار و هوية', 'الهوية التجارية ما يميزك فى عالم اعمالك ووسط منافسيك.', 'ar', '2021-12-19 07:08:06', '2021-12-19 07:08:06'),
(60, 6, 'Logo and identity design', 'Business identity is what distinguishes you in your business world and among your competitors.', 'en', '2021-12-19 07:08:06', '2021-12-19 07:08:06');

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `logo` varchar(300) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(300) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(300) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `facebook` varchar(300) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `twitter` varchar(300) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gmail` varchar(300) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `linkedin` varchar(300) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `whatsapp` varchar(300) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `messenger_link` varchar(300) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `twitter_link` varchar(300) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `latitude` varchar(300) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `longitude` varchar(300) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `location` varchar(300) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `logo`, `phone`, `email`, `facebook`, `twitter`, `gmail`, `linkedin`, `whatsapp`, `messenger_link`, `twitter_link`, `latitude`, `longitude`, `location`, `created_at`, `updated_at`) VALUES
(1, 'settings/3RR5V6Q41YJO37zHCgpyIMQ2oeBF7XR2cUdrPoJu.svg', '0558706532', 'info@codlop.com', 'https://www.facebook.com/Cod-lop-103298078820332/', 'https://twitter.com/cod_lop', 'https://www.youtube.com/channel/UChYETiYfFnpAwXzVulCpgoQ', 'https://www.linkedin.com/company/cod-lop/mycompany/', '01005555551', '#', '#', '30.5605', '31.0079', 'Shebeen El-Kom', '2021-11-14 19:29:28', '2021-12-26 08:28:34');

-- --------------------------------------------------------

--
-- Table structure for table `setting_trans`
--

CREATE TABLE `setting_trans` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `setting_id` bigint(20) UNSIGNED DEFAULT NULL,
  `title` varchar(300) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lang` varchar(300) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `setting_trans`
--

INSERT INTO `setting_trans` (`id`, `setting_id`, `title`, `address`, `lang`, `created_at`, `updated_at`) VALUES
(52, 1, 'كود لوب شركة برمجيات في بريدة القصيم', 'المملكة العربية السعودية - القصيم - بريدة', 'ar', '2021-12-26 08:28:34', '2021-12-26 08:28:34'),
(53, 1, 'codlop', 'Kingdom of Saudi Arabia - Qassim - Buraydah', 'en', '2021-12-26 08:28:34', '2021-12-26 08:28:34');

-- --------------------------------------------------------

--
-- Table structure for table `site_texts`
--

CREATE TABLE `site_texts` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `image` varchar(300) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `color` varchar(300) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `page_type` enum('home','about','service','offer','blog','contact') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'about',
  `text_type` enum('years_of_experience','our_vision','our_message','features_of_codlop') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_shown` enum('yes','no') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'yes',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `site_texts`
--

INSERT INTO `site_texts` (`id`, `image`, `color`, `page_type`, `text_type`, `is_shown`, `created_at`, `updated_at`) VALUES
(1, 'site_texts/mHbIZRPAdwBJon4a0RNmp50eH2BsA0qWZVD7yvVl.jpeg', NULL, 'home', NULL, 'yes', '2021-11-15 20:50:59', '2021-12-09 06:34:50'),
(2, 'site_texts/5ZAts4i15B9MZhsxnFm5Fg4m3BrWUoKZmtom7dCq.png', NULL, 'about', NULL, 'yes', '2021-11-15 20:50:59', '2021-12-19 06:57:02'),
(3, 'site_texts/NiPAtqvuIjTWI08VGtzA6ocxTwfehVwbGI8iMaDo.png', NULL, 'about', NULL, 'yes', '2021-11-15 20:50:59', '2021-11-26 23:55:38'),
(4, 'site_texts/BoNfOhABrrpt1HHReuf7pzXuLv31CK1ZrrsiFJiq.png', NULL, 'about', NULL, 'yes', '2021-11-15 20:50:59', '2021-11-26 23:55:42'),
(5, NULL, '#293eaf', 'about', 'features_of_codlop', 'yes', '2021-11-15 20:50:59', NULL),
(6, NULL, '#f55445', 'about', 'features_of_codlop', 'yes', '2021-11-15 20:50:59', NULL),
(7, NULL, '#d3991b', 'about', 'features_of_codlop', 'yes', '2021-11-15 20:50:59', '2021-12-09 07:45:03'),
(8, NULL, '#3bb359', 'about', 'features_of_codlop', 'yes', '2021-11-15 20:50:59', '2021-12-09 07:40:39'),
(9, NULL, '', 'service', NULL, 'yes', '2021-11-15 20:50:59', NULL),
(10, 'site_texts/LvTuOHNoyvjgU7z1kIfqFgtpMrtsa2HNhcesb60o.png', NULL, 'home', NULL, 'yes', '2021-11-23 23:29:46', '2021-12-09 06:36:21');

-- --------------------------------------------------------

--
-- Table structure for table `site_text_trans`
--

CREATE TABLE `site_text_trans` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `site_text_id` bigint(20) UNSIGNED DEFAULT NULL,
  `title` varchar(300) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `text` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lang` varchar(300) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `site_text_trans`
--

INSERT INTO `site_text_trans` (`id`, `site_text_id`, `title`, `text`, `lang`, `created_at`, `updated_at`) VALUES
(11, 6, 'سيرفرات عالية الكفاءة', 'احصل علي اقوي سيرفرات عالية كفاءة دعم فني طوال ايام الاسبوع', 'ar', '2021-11-15 20:52:53', NULL),
(12, 6, 'high efficiency servers', 'Get the most powerful high-efficiency servers, technical support throughout the week', 'en', '2021-11-15 20:52:53', NULL),
(54, 3, 'رؤيتنا وتطلعاتنا', 'أهلا بكم في مؤسسة كود لوب الرائدة في خدمات الويب وتصميم و برمجة مواقع الإنترنت المتقدمة كود لوب تقدم خدمات استضافة المواقع وتصميم وبمرجة مواقع الإنترنت وبرمجة تطبيقات الأجهزة الذكية خبرة أكثر من 10 أعوام بالإضافة إلى العديد من الخدمات التقنية التي ساعدت شريحة كبيرة من المجتمع في إنجاز أعمالهم على أكمل وجه وفي أسرع وقت ممكن , وقد تشرفنا في كود لوب بتقديم خدماتنا للعديد من الهيئات والجهات المختلفة لأكثر من عشر سنوات متواصلة وبكل احترافية عالية', 'ar', '2021-11-26 23:55:38', '2021-11-26 23:55:38'),
(55, 3, 'Our vision and aspirations', 'Welcome to Code Loop, the leading institution in web services, design and programming of advanced web sites Code Loop provides web hosting services, design and programming of web sites and programming of applications for smart devices, with more than 10 years of experience, in addition to many technical services that helped a large segment of society accomplish their work To the fullest and as quickly as possible, and we have been honored at Code Loop to provide our services to many different bodies and authorities for more than ten continuous years and with high professionalism', 'en', '2021-11-26 23:55:38', '2021-11-26 23:55:38'),
(56, 4, 'رسالتنا', 'أهلا بكم في مؤسسة كود لوب الرائدة في خدمات الويب وتصميم و برمجة مواقع الإنترنت المتقدمة كود لوب تقدم خدمات استضافة المواقع وتصميم وبمرجة مواقع الإنترنت وبرمجة تطبيقات الأجهزة الذكية خبرة أكثر من 10 أعوام بالإضافة إلى العديد من الخدمات التقنية التي ساعدت شريحة كبيرة من المجتمع في إنجاز أعمالهم على أكمل وجه وفي أسرع وقت ممكن , وقد تشرفنا في كود لوب بتقديم خدماتنا للعديد من الهيئات والجهات المختلفة لأكثر من عشر سنوات متواصلة وبكل احترافية عالية', 'ar', '2021-11-26 23:55:42', '2021-11-26 23:55:42'),
(57, 4, 'Our message', 'Welcome to Code Loop, the leading institution in web services, design and programming of advanced web sites Code Loop provides web hosting services, design and programming of web sites and programming of applications for smart devices, with more than 10 years of experience, in addition to many technical services that helped a large segment of society accomplish their work To the fullest and as quickly as possible, and we have been honored at Code Loop to provide our services to many different bodies and authorities for more than ten continuous years and with high professionalism', 'en', '2021-11-26 23:55:42', '2021-11-26 23:55:42'),
(86, 8, 'عقد لضمان حق الطرفين', 'شركة مسجلة ولها اوراق قانونية تجعل اتفاقك في امان', 'ar', '2021-12-09 07:40:39', '2021-12-09 07:40:39'),
(87, 8, 'Contract to guarantee the right of both parties', 'A registered company with legal papers that make your agreement safe', 'en', '2021-12-09 07:40:39', '2021-12-09 07:40:39'),
(90, 7, 'شركة مسجلة جديدة', 'شركة مسجلة لها سجل تجاري و بطاقة ضريبية لضمان حقك', 'ar', '2021-12-09 07:45:03', '2021-12-09 07:45:03'),
(91, 7, 'registered company', 'A registered company with a commercial register and tax card to guarantee your right', 'en', '2021-12-09 07:45:03', '2021-12-09 07:45:03'),
(94, 10, 'إنجازاتنا خلال الفترة السابقة', 'أهلا بكم في مؤسسة كود لوب الرائدة في خدمات الويب وتصميم و برمجة مواقع الإنترنت المتقدمة\r\nكود لوب تقدم خدمات استضافة المواقع وتصميم وبمرجة مواقع الإنترنت وبرمجة تطبيقات الأجهزة الذكية بخبرة أكثر من 10 أعوام\r\n بالإضافة إلى العديد من الخدمات التقنية التي ساعدت شريحة كبيرة من المجتمع في إنجاز أعمالهم على أكمل وجه وفي أسرع وقت ممكن، وقد تشرفنا في كود لوب بتقديم الخدمات للعديد من الهيئات والجهات المختلفة لأكثر من عشر سنوات متواصلة وبكل احترافية عالية', 'ar', '2021-12-12 10:24:35', '2021-12-12 10:24:35'),
(95, 10, 'Our achievements during the previous period', 'Welcome to Code Loop, the leading institution in web services, design and programming of advanced web sites Code Loop provides web hosting services, design and programming of web sites and programming of applications for smart devices, with more than 10 years of experience in addition to many technical services that have helped a large segment of society in completing their work To the fullest and as quickly as possible, and we have been honored in Code Loop to provide services to many different bodies and authorities for more than ten continuous years and with high professionalism', 'en', '2021-12-12 10:24:35', '2021-12-12 10:24:35'),
(96, 1, 'أكثر من 10 أعوام من الخبرة', 'كود لوب هي شركة برمجيات في بريدة القصيم أهلا بكم في مؤسسة كود لوب الرائدة في خدمات الويب وتصميم وبرمجة مواقع الإنترنت المتقدمة\r\nكود لوب تقدم خدمات استضافة المواقع وتصميم وبرمجة مواقع الإنترنت وبرمجة تطبيقات الأجهزة الذكية\r\nخبرة أكثر من 10 أعوام بالإضافة إلى العديد من الخدمات التقنية التي ساعدت شريحة كبيرة من المجتمع في إنجاز أعمالهم على أكمل وجه وفي أسرع وقت ممكن، وقد تشرفنا في كود لوب بتقديم الخدمات للعديد من الهيئات والجهات المختلفة لأكثر من عشر سنوات متواصلة وبكل احترافية عالية', 'ar', '2021-12-13 09:50:44', '2021-12-13 09:50:44'),
(97, 1, 'More than 10 years of experience', 'Welcome to Code Loop, the leading institution in web services, design and programming of advanced web sites Code Loop provides web hosting services, design and programming of web sites and programming of applications for smart devices, with more than 10 years of experience in addition to many technical services that have helped a large segment of society in completing their work To the fullest and as quickly as possible, and we have been honored in Code Loop to provide services to many different bodies and authorities for more than ten continuous years and with high professionalism', 'en', '2021-12-13 09:50:44', '2021-12-13 09:50:44'),
(100, 5, 'دعم فني شامل', 'دعم فني ممتد لحل اي مشكلة تقابلك في خلال 24 ساعة فقط', 'ar', '2021-12-19 06:55:00', '2021-12-19 06:55:00'),
(101, 5, 'Comprehensive technical support', 'Extended technical support to solve any problem you meet within only 24 hours', 'en', '2021-12-19 06:55:00', '2021-12-19 06:55:00'),
(102, 9, NULL, 'نقدم حلول للأفراد و الشركات و الجهات الحكومية من واقع خبرتنا و رؤيتنا الحديثة و نعمل على تطوير منتجاتنا لكى تتلاءم مع احتياجات عملائنا حول العالم العربى.', 'ar', '2021-12-19 06:55:41', '2021-12-19 06:55:41'),
(103, 9, NULL, 'We provide solutions for individuals, companies and government agencies based on our experience and modern vision, and we are working to develop our products to suit the needs of our customers around the Arab world.', 'en', '2021-12-19 06:55:41', '2021-12-19 06:55:41'),
(104, 2, 'أكثر من 10 أعوام من الخبرة', 'أهلا بكم في مؤسسة كود لوب الرائدة في خدمات الويب وتصميم و برمجة مواقع الإنترنت المتقدمة كود لوب تقدم خدمات استضافة المواقع وتصميم وبمرجة مواقع الإنترنت وبرمجة تطبيقات الأجهزة الذكية خبرة أكثر من 10 أعوام بالإضافة إلى العديد من الخدمات التقنية التي ساعدت شريحة كبيرة من المجتمع في إنجاز أعمالهم على أكمل وجه وفي أسرع وقت ممكن , وقد تشرفنا في كود لوب بتقديم الخدمات للعديد من الهيئات والجهات المختلفة لأكثر من عشر سنوات متواصلة وبكل احترافية عالية', 'ar', '2021-12-19 06:57:02', '2021-12-19 06:57:02'),
(105, 2, 'More than 10 years of experience', 'Welcome to Code Loop, the leading institution in web services, design and programming of advanced web sites Code Loop provides web hosting services, design and programming of web sites and programming of applications for smart devices, with more than 10 years of experience in addition to many technical services that have helped a large segment of society in completing their work To the fullest and as quickly as possible, and we have been honored in Code Loop to provide services to many different bodies and authorities for more than ten continuous years and with high professionalism', 'en', '2021-12-19 06:57:02', '2021-12-19 06:57:02');

-- --------------------------------------------------------

--
-- Table structure for table `sliders`
--

CREATE TABLE `sliders` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `image` varchar(300) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `background` varchar(300) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sliders`
--

INSERT INTO `sliders` (`id`, `image`, `background`, `created_at`, `updated_at`) VALUES
(1, 'sliders/vK2fWQnM3R5pBC1QCde1pEQMuqfIKxcady8xP39T.png', NULL, '2021-11-14 21:03:36', '2021-12-26 13:34:42'),
(2, 'sliders/D7CthJJ1GF1eVWnHMbkOF5qMv50QpknssEXxXCjq.png', NULL, '2021-11-14 21:03:52', '2021-12-26 13:33:40');

-- --------------------------------------------------------

--
-- Table structure for table `slider_trans`
--

CREATE TABLE `slider_trans` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `slider_id` bigint(20) UNSIGNED DEFAULT NULL,
  `title` varchar(300) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `text` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lang` varchar(300) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `slider_trans`
--

INSERT INTO `slider_trans` (`id`, `slider_id`, `title`, `text`, `lang`, `created_at`, `updated_at`) VALUES
(55, 2, 'كود لوب لتصميم وبرمجة المواقع والتطبيقات', 'نحن مؤسسة تصميم وبرمجة مواقع وتطبيقات رقمية قائمة على نجاح تجربة المستخدم، نعمل على خلق حلول تصميمية وبرمجية مبتكرة لكي نساعد نشاطك التجاري على النجاح وتحقيق المزيد من المبيعات والربح في عالم الانترنت .', 'ar', '2021-12-26 13:33:40', '2021-12-26 13:33:40'),
(56, 2, 'codlop for designing and programming websites and applications', 'We are a design and programming organization for digital websites and applications based on the success of the user experience. We work on creating innovative design and software solutions to help your business succeed and achieve more sales and profit in the Internet world.', 'en', '2021-12-26 13:33:40', '2021-12-26 13:33:40'),
(57, 1, 'كود لوب لتصميم وبرمجة المواقع والتطبيقات', 'نحن مؤسسة تصميم وبرمجة مواقع وتطبيقات رقمية قائمة على نجاح تجربة المستخدم، نعمل على خلق حلول تصميمية وبرمجية مبتكرة لكي نساعد نشاطك التجاري على النجاح وتحقيق المزيد من المبيعات والربح في عالم الانترنت .', 'ar', '2021-12-26 13:34:42', '2021-12-26 13:34:42'),
(58, 1, 'codlop for designing and programming websites and applications', 'We are a design and programming organization for digital websites and applications based on the success of the user experience. We work on creating innovative design and software solutions to help your business succeed and achieve more sales and profit in the Internet world.', 'en', '2021-12-26 13:34:42', '2021-12-26 13:34:42');

-- --------------------------------------------------------

--
-- Table structure for table `subscribe_to_offers`
--

CREATE TABLE `subscribe_to_offers` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `full_name` varchar(300) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(300) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone_code` varchar(300) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` bigint(20) UNSIGNED DEFAULT NULL,
  `offer_id` bigint(20) UNSIGNED DEFAULT NULL,
  `extra_text` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `subscribe_to_offer_details`
--

CREATE TABLE `subscribe_to_offer_details` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `subscribe_to_offer_id` bigint(20) UNSIGNED DEFAULT NULL,
  `program_id` bigint(20) UNSIGNED DEFAULT NULL COMMENT 'like ios, android, web, ui-ux',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `code` varchar(300) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_read` enum('unread','read') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'unread',
  `user_type` enum('client') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'client',
  `phone_code` varchar(300) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` bigint(20) UNSIGNED DEFAULT NULL,
  `name` varchar(300) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(300) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(300) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `offer_id` bigint(20) UNSIGNED DEFAULT NULL,
  `more_details` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `latitude` double NOT NULL DEFAULT 0,
  `longitude` double NOT NULL DEFAULT 0,
  `address` varchar(300) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gender` enum('male','female') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'male',
  `birthday` date DEFAULT NULL,
  `logo` varchar(300) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `banner` varchar(300) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `approved_status` enum('new','accepted','not_accepted') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'new',
  `approved_by` int(11) DEFAULT NULL,
  `is_blocked` enum('blocked','not_blocked') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'not_blocked',
  `is_login` enum('online','offline') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'offline',
  `logout_time` int(11) DEFAULT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `confirmation_code` varchar(300) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `forget_password_code` varchar(300) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `software_type` enum('ios','android','web') COLLATE utf8mb4_unicode_ci DEFAULT 'web',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `code`, `is_read`, `user_type`, `phone_code`, `phone`, `name`, `email`, `password`, `offer_id`, `more_details`, `latitude`, `longitude`, `address`, `gender`, `birthday`, `logo`, `banner`, `approved_status`, `approved_by`, `is_blocked`, `is_login`, `logout_time`, `email_verified_at`, `confirmation_code`, `forget_password_code`, `software_type`, `deleted_at`, `remember_token`, `created_at`, `updated_at`) VALUES
(20, NULL, 'read', 'client', '+20', 10312231331, 'Mostafa elnagar', 'devmostafa80@gmail.com', '$2y$10$3XdZyuMBeY4aEGKnLXYAm.HfrnWAnFxpuF7pOmwFsdE9gWmsBXAxi', 1, 'لوريم ايبسوم دولار سيت أميت ,كونسيكتيتور أدايبا يسكينج أليايت,سيت دو أيوسمود تيمبور\r\n\r\nأنكايديديونتيوت لابوري ات دولار ماجنا أليكيوا . يوت انيم أد مينيم فينايم,كيواس نوستريد\r\n\r\nأكسير سيتاشن يللأمكو لابورأس نيسي يت أليكيوب أكس أيا كوممودو كونسيكيوات . ديواس\r\n\r\nأيوتي أريري دولار إن ريبريهينديرأيت فوليوبتاتي فيلايت أيسسي كايلليوم دولار أيو فيجايت\r\n\r\nنيولا باراياتيور. أيكسسيبتيور ساينت أوككايكات كيوبايداتات نون بروايدينت ,سيونت ان كيولبا\r\n\r\nكيو أوفيسيا ديسيريونتموليت انيم أيدي ايست لابوريوم.\"\r\n\r\n\"سيت يتبيرسبايكياتيس يوندي أومنيس أستي ناتيس أيررور سيت فوليبتاتيم أكيسأنتييوم\r\n\r\nدولاريمكيو لايودانتيوم,توتام ريم أبيرأم,أيكيو أبسا كيواي أب أللو أنفينتوري فيرأتاتيس ايت\r\n\r\nكياسي أرشيتيكتو بيتاي فيتاي ديكاتا سيونت أكسبليكابو. نيمو أنيم أبسام فوليوباتاتيم كيواي\r\n\r\nفوليوبتاس سايت أسبيرناتشر أيوت أودايت أيوت فيوجايت, سيد كيواي كونسيكيونتشر ماجناي\r\n\r\nدولارس أيوس كيواي راتاشن فوليوبتاتيم سيكيواي نيسكايونت. نيكيو بوررو كيوايسكيوم', 0, 0, NULL, 'male', NULL, NULL, NULL, 'new', NULL, 'not_blocked', 'offline', NULL, NULL, NULL, NULL, 'web', NULL, NULL, '2021-12-07 21:51:15', '2021-12-19 07:53:54'),
(21, NULL, 'read', 'client', '+20', 1066816711, 'محمود الكومي', '7oda.elkomy@gmail.com', '$2y$10$7FdyqHsfVFaidrGt5QGmae5apgtXo5B75ehx9Wrp3bOGEPZTP.YKa', 1, 'لوريم ايبسوم دولار سيت أميت ,كونسيكتيتور أدايبا يسكينج أليايت,سيت دو أيوسمود تيمبور\r\n\r\nأنكايديديونتيوت لابوري ات دولار ماجنا أليكيوا . يوت انيم أد مينيم فينايم,كيواس نوستريد\r\n\r\nأكسير سيتاشن يللأمكو لابورأس نيسي يت أليكيوب أكس أيا كوممودو كونسيكيوات . ديواس\r\n\r\nأيوتي أريري دولار إن ريبريهينديرأيت فوليوبتاتي فيلايت أيسسي كايلليوم دولار أيو فيجايت\r\n\r\nنيولا باراياتيور. أيكسسيبتيور ساينت أوككايكات كيوبايداتات نون بروايدينت ,سيونت ان كيولبا\r\n\r\nكيو أوفيسيا ديسيريونتموليت انيم أيدي ايست لابوريوم.\"\r\n\r\n\"سيت يتبيرسبايكياتيس يوندي أومنيس أستي ناتيس أيررور سيت فوليبتاتيم أكيسأنتييوم\r\n\r\nدولاريمكيو لايودانتيوم,توتام ريم أبيرأم,أيكيو أبسا كيواي أب أللو أنفينتوري فيرأتاتيس ايت\r\n\r\nكياسي أرشيتيكتو بيتاي فيتاي ديكاتا سيونت أكسبليكابو. نيمو أنيم أبسام فوليوباتاتيم كيواي\r\n\r\nفوليوبتاس سايت أسبيرناتشر أيوت أودايت أيوت فيوجايت, سيد كيواي كونسيكيونتشر ماجناي\r\n\r\nدولارس أيوس كيواي راتاشن فوليوبتاتيم سيكيواي نيسكايونت. نيكيو بوررو كيوايسكيوم', 0, 0, NULL, 'male', NULL, NULL, NULL, 'new', NULL, 'not_blocked', 'offline', NULL, NULL, NULL, '2184616', 'web', NULL, NULL, '2021-12-08 13:42:31', '2021-12-19 07:38:46');

-- --------------------------------------------------------

--
-- Table structure for table `user_programs`
--

CREATE TABLE `user_programs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `program_id` bigint(20) UNSIGNED DEFAULT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `link` text DEFAULT '#!',
  `status` enum('new','finished') NOT NULL DEFAULT 'new',
  `progress_bar` double NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `user_programs`
--

INSERT INTO `user_programs` (`id`, `program_id`, `user_id`, `link`, `status`, `progress_bar`, `created_at`, `updated_at`) VALUES
(44, 1, 20, '#!', 'new', 0, '2021-12-07 21:51:15', '2021-12-07 21:51:15'),
(47, 2, 21, NULL, 'new', 0, '2021-12-09 10:12:29', '2021-12-19 07:40:35');

-- --------------------------------------------------------

--
-- Table structure for table `user_program_chats`
--

CREATE TABLE `user_program_chats` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_program_room_id` bigint(20) UNSIGNED DEFAULT NULL,
  `from_user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `user_type` enum('user','admin') NOT NULL DEFAULT 'user',
  `message` text DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `user_program_chats`
--

INSERT INTO `user_program_chats` (`id`, `user_program_room_id`, `from_user_id`, `user_type`, `message`, `created_at`, `updated_at`) VALUES
(23, 6, NULL, 'admin', 'بسم الله', '2021-12-23 06:46:25', '2021-12-23 06:46:25'),
(24, 6, NULL, 'admin', 'اهلا وسهلا', '2021-12-23 06:46:32', '2021-12-23 06:46:32'),
(25, 9, NULL, 'admin', 'بسم الله', '2021-12-23 06:50:35', '2021-12-23 06:50:35'),
(26, 9, NULL, 'admin', 'اهلا وسهلا', '2021-12-23 06:50:43', '2021-12-23 06:50:43'),
(27, 9, 21, 'user', 'مرحبا بك', '2021-12-23 06:51:04', '2021-12-23 06:51:04'),
(28, 9, 21, 'user', 'اهلا وسهلا', '2021-12-23 06:51:10', '2021-12-23 06:51:10'),
(29, 9, 21, 'user', 'بسم الله', '2021-12-23 06:51:49', '2021-12-23 06:51:49');

-- --------------------------------------------------------

--
-- Table structure for table `user_program_rooms`
--

CREATE TABLE `user_program_rooms` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_program_id` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `user_program_rooms`
--

INSERT INTO `user_program_rooms` (`id`, `user_program_id`, `created_at`, `updated_at`) VALUES
(6, 44, '2021-12-07 21:51:15', '2021-12-07 21:51:15'),
(9, 47, '2021-12-09 10:12:29', '2021-12-09 10:12:29');

-- --------------------------------------------------------

--
-- Table structure for table `user_program_tasks`
--

CREATE TABLE `user_program_tasks` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `task` text DEFAULT NULL,
  `status` enum('uncheck','check') NOT NULL DEFAULT 'uncheck',
  `user_program_id` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `user_program_tasks`
--

INSERT INTO `user_program_tasks` (`id`, `task`, `status`, `user_program_id`, `created_at`, `updated_at`) VALUES
(348, 'sdfds', 'uncheck', 47, '2021-12-19 07:40:35', '2021-12-19 07:40:35'),
(349, 'سي', 'uncheck', 47, '2021-12-19 07:40:35', '2021-12-19 07:40:35'),
(350, 'يس', 'uncheck', 47, '2021-12-19 07:40:35', '2021-12-19 07:40:35'),
(351, 'سي', 'uncheck', 47, '2021-12-19 07:40:35', '2021-12-19 07:40:35'),
(352, 'يس', 'uncheck', 47, '2021-12-19 07:40:35', '2021-12-19 07:40:35');

-- --------------------------------------------------------

--
-- Table structure for table `video_motions`
--

CREATE TABLE `video_motions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` enum('video_motions','mobile_application','web_design') COLLATE latin1_general_ci NOT NULL DEFAULT 'video_motions',
  `image` varchar(500) COLLATE latin1_general_ci DEFAULT NULL,
  `background` text COLLATE latin1_general_ci DEFAULT NULL,
  `video` text COLLATE latin1_general_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Dumping data for table `video_motions`
--

INSERT INTO `video_motions` (`id`, `type`, `image`, `background`, `video`, `created_at`, `updated_at`) VALUES
(5, 'video_motions', 'video_motions/E37AUDxodIdZxI0VmPjtF3gll7rirTZXsdSommy9.png', 'video_motions/POFjmCBrt4COgUk44SNMHLabJjWK5sGWVjRbM21Z.jpeg', 'video_motions/JmlmH5wrLWpiH08fYWmaVV61nge6QnXk0W3z3LqS.mp4', '2021-12-09 10:11:15', '2021-12-23 06:27:36'),
(6, 'mobile_application', 'video_motions/OutdmaMrWiAdRLVvvL3fX8kAJHHzY89rRaLATP9u.png', 'video_motions/acJeiKxDl5K3ktOuZC2KGbyI4mwNf2GTdbkDjfdH.png', 'video_motions/sVqC6xat4CLBWfZGDSmFMZENmFR2LhkKZs2l6iVA.mp4', '2021-12-09 10:20:08', '2021-12-22 13:16:32'),
(7, 'video_motions', 'video_motions/VqCFfqODvxsAihbbWtFpnLAampDiT48ONx1zalpF.png', 'video_motions/YsBFOqmb02PWmHL5EoDJw9PV0dTQ7dY7l4Ijp8k9.png', 'video_motions/EVJaVcBDZoHvzDqGXzssmAE2TRgCO354CLEENf7E.mp4', '2021-12-09 10:27:29', '2021-12-23 06:38:48'),
(8, 'video_motions', 'video_motions/Ld15l2vgtjX0V5qEqFkxr3Q4e6gHY2CDx2GrffkS.png', 'video_motions/8vCinFY6qjmO9UXIfwWtGbEa8kGVODQAWuUx2ZM3.png', 'video_motions/1zq9N6hhZJA0m4OSXyHRIZ88YlXLCRVX5Z4pwQTQ.mp4', '2021-12-09 10:34:00', '2021-12-26 08:26:12'),
(16, 'mobile_application', 'video_motions/hzin9mIQjZo3NxqelkEwWCiiE1LlxtEoPMh2ispP.png', 'video_motions/eM4L0pE5EOAtSrcx7UmBjAyFg5ONLmrkr277jABL.png', 'video_motions/tHJsqdPACkArvxhe9ylUYaWiu5raofiMykfjBjgd.mp4', '2021-12-21 08:44:08', '2021-12-22 13:38:36'),
(17, 'mobile_application', 'video_motions/QDKBWxyZMxY36dYo0hG2GOTeVbRPPQhWls7rja0m.png', 'video_motions/0Mmbd1BTvOj7vELyAcOsuBmH6fBkoLvtkUdRYXvA.png', 'video_motions/jmUWPKFUKlJcpHXCq9IvRc4kxbuLD1Rp0fL2sdyg.mp4', '2021-12-21 10:22:31', '2021-12-22 13:10:23'),
(18, 'mobile_application', 'video_motions/g2plAG73XsEx8ZTStCMNlbtjbWWBqYTDuigTXgrA.png', 'video_motions/JuoDuKmzRfXMdHEosEohCThSInA6my8upTo7AoY9.png', 'video_motions/q7NQEkslXqIPHBE4qP21MFfCo51GbeOCIpGIEI7c.mp4', '2021-12-21 10:24:39', '2021-12-22 12:38:40'),
(19, 'mobile_application', 'video_motions/NCB3wtmQQeIb0pGPcJ2naBAHmUUb1nrgdbR7Ueow.png', 'video_motions/jSRO77Jc3so9e4mp9fC2ewkuPUL5fPydIF9uyW4c.png', 'video_motions/jgRyB0Ppy3l6zc7oBzR095kw3j0vI4CKLAwScTQz.mp4', '2021-12-21 10:49:56', '2021-12-22 12:37:53'),
(20, 'mobile_application', 'video_motions/7KtfJsFcHqP24DBCnNcaYEfiQlMMrDn6iGd5FYRP.png', 'video_motions/z6fyNpPI7Um6ONLazR8cRGstaV0boZEYMyooaMuV.png', 'video_motions/OviMuIrBWoN421oDTJ30SywQHHihqZez1YPrmqG4.mp4', '2021-12-21 11:56:12', '2021-12-22 13:14:55');

-- --------------------------------------------------------

--
-- Table structure for table `video_motion_trans`
--

CREATE TABLE `video_motion_trans` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `video_motion_id` bigint(20) UNSIGNED DEFAULT NULL,
  `title` varchar(500) CHARACTER SET utf8mb4 DEFAULT NULL,
  `text` text CHARACTER SET utf8mb4 DEFAULT NULL,
  `lang` varchar(300) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Dumping data for table `video_motion_trans`
--

INSERT INTO `video_motion_trans` (`id`, `video_motion_id`, `title`, `text`, `lang`, `created_at`, `updated_at`) VALUES
(165, 19, 'تطبيق \"مندوبي\"', 'تطبيق يوفر لك عروض لتوصيل كل شيء وفي أي وقت من مناديب بأكثر من عرض سعر تختار اللي يناسبك\r\n\r\n\r\nرحلة تطبيق مندوبي داخل شركة كود لوب :\r\n- بعد الاجتماع مع العميل، وتحليل متطلباته في التطبيق \r\n- مرحلة التخطيط، ثم مرحلة التنفيذ بأفضل فريق وبخبرة أكثر من 7 سنوات \r\n- فريق تصميم متخصص، صمم الموقع والتطبيق، تطبيق واحد للمندوب والعميل، بواجهة سهلة وواضحة لكلا منهم\r\n- فريق المطورين، قاموا بتنفيذ التصور بأفضل برمجة بواجهة سريعة وخفيفة \r\n- ومن ثم مرحلة الاختبار والتسليم ، بعد التأكد من تنفيذ جميع متطلبات العميل بأفضل طريقة وفي أسرع وقت', 'ar', '2021-12-22 12:37:53', '2021-12-22 12:37:53'),
(166, 19, 'website and mobile application \"mandoby\"', 'An app that provides you with offers to plug everything in, and at any time from, more than one price offer, you choose what suits you.\r\n\r\n\r\nMandouby application journey within Cod Lop:\r\n- After meeting with the client and analyzing its requirements in the application\r\n- The planning phase, then the implementation phase with the best team with more than 7 years\' experience\r\n- Specialized design team, site and application design, one delegate, and client application, with an easy and clear interface for each of them.\r\n- The team of developers implemented the concept of best programming with a fast and light interface\r\n- Then the test and delivery phase, after ensuring that all client requirements are implemented in the best way and in the fastest time', 'en', '2021-12-22 12:37:53', '2021-12-22 12:37:53'),
(167, 18, 'موقع وتطبيق \"وسطه\"', 'موقع وتطبيق لبيع وشراء كل شيء في أكثر من دولة، بأمان كامل وتحت مسؤولية الشركة المؤسسة من خلال مقراتها\r\n\r\n\r\nرحلة موقع وتطبيق وسطة داخل شركة كود لوب :\r\n- بداية من اختيار الاسم، وتطوير الفكرة وتصميم الشعار\r\n- مرحلة التخطيط لكل خصائص التطبيق، بأن يصبح البيع والشراء والاستلام والتسليم من خلال اختيارين:\r\n(الشخص المعلن، الشركة المؤسسة لتطبيق وسطة)\r\nوجود مميزات إضافية لواجهة المستخدم:\r\n1- الإعلانات الممولة، وهو القسم الخاص بالإعلانات المدفوعة للمعلنين \r\n2- المعلنين الأكثر متابعة من قبل المستخدمين\r\n3- عروض الفلاش لفترة محدودة\r\n4- أحدث الإعلانات المضافة\r\nإتاحة خصائص جديدة للمعلنين: \r\n*عمل عروض لفترات محددة على منتجاتهم\r\n*خاصية المزاد على منتج معين بسعر مبدئي\r\nإمكانية التواصل بأكثر من طريقة بين المستخدم والمعلن: \r\n(تعليقات، اتصال هاتفي)\r\n- مرحلة التصميم، بواجهة سهلة وبسيطة لكل مستخدم سواء المعلن أو المشتري\r\n- مرحلة التنفيذ البرمجي، لكل من الاجهزة الأندرويد والأيفون وموقع التطبيق\r\n- ومن ثم مرحلة الاختبار والتسليم ، بعد التأكد من تنفيذ جميع متطلبات العميل بأفضل طريقة وفي أسرع وقت', 'ar', '2021-12-22 12:38:40', '2021-12-22 12:38:40'),
(168, 18, 'website and mobile application \"wasta\"', 'Website and application for the sale and purchase of everything in more than one State, in full safety, and under the responsibility of the founding company through \r\nits headquarters\r\n\r\n\r\nSite trip and Wasta application within Cod Lop:\r\n- Starting with name selection, idea development, and logo design\r\n- The planning stage of all the characteristics of the application, with sale, purchase, receipt, and delivery through two options:\r\n(Person advertised, company establishing a medium application)\r\nAdditional user interface features:\r\nFunded declarations, the section on advertising paid to advertisers\r\nAdvertisers most followed by users\r\nFlash displays for a limited period\r\nLatest declarations added\r\nMaking new features available to advertisers:\r\n* Fixed-term offers on their products\r\n* Auction feature on a particular product at an initial price\r\nThe possibility of communicating in more than one way between the user and the advertiser:\r\n(Comments, phone call)\r\n- Design phase, with an easy and simple interface for each user, whether advertised or purchased\r\n- Software implementation phase, for Android, iPhone, and application location\r\n- Then the test and delivery phase, after ensuring that all client requirements are implemented in the best way and in the fastest time', 'en', '2021-12-22 12:38:40', '2021-12-22 12:38:40'),
(177, 17, 'موقع وتطبيق \"Deal Map\"', 'موقع وتطبيق لبيع وتأجير العقارات بكل أنواعها\r\n\r\n\r\nرحلة موقع وتطبيق\"Deal Map | ديل ماب\" داخل شركة كود لوب :\r\n- بداية من اختيار الاسم، وتطوير الفكرة وتصميم الشعار\r\n- مرحلة التخطيط لكل خصائص التطبيق، بأن يصبح متاح استخدام التطبيق لـ 3 مستخدمين :\r\n( المستخدم العادي، شركة عقارية، وسيط)\r\n-وجود واجهتين مختلفتين لعرض العقارات:\r\n*واجهة الخريطة: ظهور العقارات حسب موقعها على الخريطة \r\n*واجهة بسيطة: ظهور العقارات بشكل متتالي بالصور وتفاصيل العقار\r\nإمكانية التواصل بأكثر من طريقة بين المستخدم والمعلن: \r\n(شات، واتس آب، اتصال هاتفي)\r\n- مرحلة التصميم، بواجهة سهلة وبسيطة لجميع المستخدمين\r\n- مرحلة التنفيذ البرمجي، فريق المطورين قاموا بتنفيذ التصور بأفضل برمجة بواجهة سريعة وخفيفة  لكل من الأجهزة الأندرويد والأيفون وموقع التطبيق \r\n- ومن ثم مرحلة الاختبار والتسليم ، بعد التأكد من تنفيذ جميع متطلبات العميل بأفضل طريقة وفي أسرع وقت', 'ar', '2021-12-22 13:10:23', '2021-12-22 13:10:23'),
(178, 17, 'Website and application for the sale and rental of real estate of all kinds', 'The Deal Map app trip within Cod Lop:\r\n- Starting with name selection, idea development and logo design\r\n- The planning stage of all the characteristics of the application, by making the application available to 3 users:\r\n(Regular user, real estate company, broker)\r\nThere are two different real estate showings:\r\n* Map interface: The appearance of real estate by location on the map\r\n* Simple interface: The appearance of real estate sequentially with photographs and details of the property\r\nThe possibility of communicating in more than one way between the user and the advertiser:\r\n(Chat, WhatsApp, phone call)\r\n- Design phase, with a simple interface for all users\r\n- Software implementation phase. The developers\' team implemented the concept of the best quick and light interface programming for Android, iPhone and app location.\r\n- Then the test and delivery phase, after ensuring that all client requirements are implemented in the best way and in the fastest time', 'en', '2021-12-22 13:10:23', '2021-12-22 13:10:23'),
(179, 20, 'موقع وتطبيق \"on off \"', 'موقع وتطبيق on off لتسويق تشكيلة واسهة بأسعار مخفضة، واستلام طلباتك بسرعة بضغطة واحدة \r\n\r\n\r\nرحلة تطبيق on off داخل شركة كود لوب :\r\n- بعد الاجتماع مع العميل، وتحليل متطلباته في التطبيق \r\n- مرحلة التخطيط، ثم مرحلة التنفيذ بأفضل فريق وبخبرة أكثر من 7 سنوات \r\n- فريق تصميم متخصص، صمم الموقع والتطبيق\r\n- فريق المطورين، قاموا بتنفيذ التصور بأفضل برمجة بواجهة سريعة وخفيفة \r\n- ومن ثم مرحلة الاختبار والتسليم ، بعد التأكد من تنفيذ جميع متطلبات العميل بأفضل طريقة وفي أسرع وقت', 'ar', '2021-12-22 13:14:55', '2021-12-22 13:14:55'),
(180, 20, 'website and mobile application \"on off\"', 'Website and application on off to market a low-priced menu and quickly receive your orders with one pressure\r\n\r\n\r\nJourney application on off inside Code Loop:\r\n- After meeting with the client and analysing its requirements in the application\r\n- The planning phase, then the implementation phase with the best team with more than 7 years\' experience\r\n- Specialized design team, site, and application design\r\n- The team of developers implemented the concept of best programming with a fast and light interface\r\n- Then the test and delivery phase, after ensuring that all client requirements are implemented in the best way and in the fastest time', 'en', '2021-12-22 13:14:55', '2021-12-22 13:14:55'),
(181, 6, 'موقع وتطبيق جوال \"رخيص\"', 'موقع وتطبيق جوال لبيع و شراء كل شيء حول المملكة العربية السعودية \r\n\r\n\r\nرحلة موقع وتطبيق \"رخيص\" داخل شركة كود لوب :\r\n- كانت بداية الفكرة، مثل موقع حراج فقط\r\n- بعد الاجتماع مع العميل، وتحليل السوق والمنافسين توصلنا لزيادة ميزات للفكرة مثل :\r\n- إتاحة خصائص جديدة للمعلنين: \r\n*عمل عروض لفترات محددة على منتجاتهم\r\n*خاصية المزاد على منتج معين بسعر مبدئي\r\n- بعد مرحلة التخطيط، بدأت مرحلة التنفيذ بأفضل فريق وبخبرة أكثر من 7 سنوات \r\n- مرحلة التصميم، فريق تصميم متخصص صمم الموقع والتطبيق بواجهة مستخدم سهلة وواضحة \r\n- مرحلة التنفيذ البرمجي، فريق المطورين قاموا بتنفيذ التصور بأفضل برمجة بواجهة سريعة وخفيفة  لكل من الأجهزة الأندرويد والأيفون وموقع التطبيق\r\n- ومن ثم مرحلة الاختبار والتسليم ، بعد التأكد من تنفيذ جميع متطلبات العميل بأفضل طريقة وفي أسرع وقت', 'ar', '2021-12-22 13:16:32', '2021-12-22 13:16:32'),
(182, 6, 'Rkhis\" website and app', '\" trip within Cod Lop:\r\nIt was the beginning of the idea, just like an embarrassment site.\r\nAfter meeting with the client, market analysis and competitors, we have been able to increase the advantages of the idea, such as:\r\n- New characteristics for advertisers:\r\n* Fixed-term offers on their products\r\n* Auction feature on a particular product at an initial price\r\nAfter the planning phase, the implementation phase began with the best team with more than 7 years\' experience.\r\n- Design phase, specialized design team designed site and application with easy and clear user interface\r\n- Software implementation phase. The team of developers implemented the concept of best programming with a quick and light interface for each android device.', 'en', '2021-12-22 13:16:32', '2021-12-22 13:16:32'),
(197, 16, 'موقع وتطبيق \"باب صدقة\"', 'هدفه: صدقة الجارية لكل من توفى من أحبابك، من خلال واجهة إلكترونية بإسم المتوفى، بها جميع ما ينفع المسلمين\r\n\r\n\r\nرحلة موقع وتطبيق \"باب صدقة\" داخل شركة كود لوب :\r\n- بداية من كيفية اختيار الاسم، استنادًا إلى أن للجنة لها 8 أبواب، منهم باب الصدقة، يدخل منه المتصدقون.\r\n- إلى تصميم الهوية كاملة، بألوان تناسب طبيعة المشروع وهدفه\r\n- مرحلة التخطيط، بدأت بأفضل فريق وبخبرة أكثر من 7 سنوات، لكل خصائص التطبيق بأن يصبح لكل متوفى واجهة خاصة باسمه ويمكن مشاركتها للأهل والأصدقاء بكافة الأشكال\r\n- مرحلة التصميم، فريق تصميم متخصص صمم الموقع والتطبيق بواجهة مستخدم سهلة وواضحة \r\n- مرحلة التنفيذ البرمجي، فريق المطورين قاموا بتنفيذ التصور بأفضل برمجة بواجهة سريعة وخفيفة  لكل من الأجهزة الأندرويد والأيفون وموقع التطبيق', 'ar', '2021-12-22 13:38:36', '2021-12-22 13:38:36'),
(198, 16, 'website and mobile application \"bab sadaka\"', 'Website and application of ongoing charity for all who died from your loved ones, through an electronic interface in the name of the deceased, with all that benefits Muslims\r\n\r\nSite trip and \"Bab Sadaka\" application within Cod Lop:\r\n- Starting with how to choose a name, based on the fact that the Committee has 8 doors, including charity, from which the believers enter.\r\n- To design the full identity, in colors appropriate to the nature and purpose of the project\r\n- The planning phase began with the best team, with more than 7 years of experience, for all the characteristics of the application, with each deceased having a special interface in his name that could be shared by parents and friends in all forms.\r\n- Design phase, specialized design team designed site and application with easy and clear user interface\r\n- Software implementation phase. The developers\' team implemented the concept of the best quick and light interface programming for Android, iPhone and app location.\r\n- Then the test and delivery phase, after ensuring that all client requirements are implemented in the best way and in the fastest time', 'en', '2021-12-22 13:38:36', '2021-12-22 13:38:36'),
(213, 5, '\" موقع وتطبيق رخيص لبيع وشراء كل شيء| فيديو موجه للشاري\"', '\" مشروع تم تنفيذه برمجيا من خالل شركة كود لوب، وتم العمل على خصائصه المميزة\r\nعن مشاريع - حراج- المشابهة وإبرازها في فيديو موشن تسويقي\r\nيخاطب الشاري ويعرض عليه مميزات الموقع \"', 'ar', '2021-12-23 06:27:36', '2021-12-23 06:27:36'),
(214, 5, '\"A Rkhis site and app for buying and selling everything | Buyer-oriented video\"', 'A project implemented programmatically by CodLop, and its distinctive features have been worked on\r\nAbout similar projects - Haraj and highlighting them in a marketing motion video\r\nHe addresses the buyer and shows him the site features.', 'en', '2021-12-23 06:27:36', '2021-12-23 06:27:36'),
(215, 7, '\" فيديو تعريفي بشركة كود لوب\"', '\"التعريف بالشركة وخدماتها ومواكبتها لرؤية المملكة 2030، من خلال:\r\n- عرض أهمية التطور الرقمي، وتأثيره على سلوك العميل السعودي\r\n- عرض نبذة من خدمات كود لوب \r\n- معلومات عن الشركة وفريق العمل \r\n- كيفية التواصل مع الشركة \"', 'ar', '2021-12-23 06:38:48', '2021-12-23 06:38:48'),
(216, 7, '\"Introductory Video of Cod Lop\"', ':Introducing the company and its services and keeping pace with the vision of the Kingdom 2030, through\r\nPresenting the importance of digital development, and its impact on the behavior of the Saudi customer-\r\n View an overview of Code Loop services-\r\nInformation about the company and the team-\r\nHow to contact the company-', 'en', '2021-12-23 06:38:48', '2021-12-23 06:38:48'),
(221, 8, 'خدمة الأنظمة الإدارية| ERP System', 'التعريف بالخدمة من خلال أسلوب القصة، فهو أقرب وأسرع أسلوب يوضح الأفكار\r\nمن خلال قصة توضح مميزات إنشاء نظام برمجي وسلبيات عدم وجوده:\r\n- عرض الآثار السلبية للاعتماد على الأوراق بشكل أساسي في المشاريع\r\n- عرض مميزات خدمة ERP System، ومناسبتها لكل أنواع المشاريع\r\n- مرحلة تنفيذها داخل شركة كود لوب \r\n- كيفية التواصل معنا لطلب الخدمة', 'ar', '2021-12-26 08:26:12', '2021-12-26 08:26:12'),
(222, 8, 'An introductory video for the ERP System service.', 'Introducing the service through the story style, as it is the closest and fastest way to clarify ideas\r\nThrough:\r\n a story that explains the advantages of creating a software system and the disadvantages of not having it\r\nPresenting the negative effects of relying on papers mainly in projects-\r\nPresenting the advantages of the ERP System service, and its suitability for all types of projects-\r\nImplementation phase within Cod Lop-\r\nHow to contact us to request the service-', 'en', '2021-12-26 08:26:12', '2021-12-26 08:26:12');

-- --------------------------------------------------------

--
-- Table structure for table `visits`
--

CREATE TABLE `visits` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` tinyint(1) DEFAULT NULL,
  `count` int(11) NOT NULL DEFAULT 0,
  `date` date DEFAULT NULL,
  `year` int(11) DEFAULT NULL,
  `month` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `admins_email_unique` (`email`);

--
-- Indexes for table `blogs`
--
ALTER TABLE `blogs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `blog_trans`
--
ALTER TABLE `blog_trans`
  ADD PRIMARY KEY (`id`),
  ADD KEY `blog_trans_blog_id_foreign` (`blog_id`),
  ADD KEY `blog_trans_lang_foreign` (`lang`);

--
-- Indexes for table `clients`
--
ALTER TABLE `clients`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `client_trans`
--
ALTER TABLE `client_trans`
  ADD PRIMARY KEY (`id`),
  ADD KEY `client_id` (`client_id`),
  ADD KEY `lang` (`lang`);

--
-- Indexes for table `confirm_emails`
--
ALTER TABLE `confirm_emails`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contacts`
--
ALTER TABLE `contacts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `contacts_phone_code_foreign` (`phone_code`);

--
-- Indexes for table `contract_advantages`
--
ALTER TABLE `contract_advantages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contract_advantage_details`
--
ALTER TABLE `contract_advantage_details`
  ADD PRIMARY KEY (`id`),
  ADD KEY `contract_advantage_details_contract_advantage_id_foreign` (`contract_advantage_id`);

--
-- Indexes for table `contract_advantage_detail_trans`
--
ALTER TABLE `contract_advantage_detail_trans`
  ADD PRIMARY KEY (`id`),
  ADD KEY `contract_advantage_detail_trans_contract_detail_id_foreign` (`contract_detail_id`),
  ADD KEY `contract_advantage_detail_trans_lang_foreign` (`lang`);

--
-- Indexes for table `contract_advantage_trans`
--
ALTER TABLE `contract_advantage_trans`
  ADD PRIMARY KEY (`id`),
  ADD KEY `contract_advantage_trans_contract_advantage_id_foreign` (`contract_advantage_id`),
  ADD KEY `contract_advantage_trans_lang_foreign` (`lang`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `firebase_tokens`
--
ALTER TABLE `firebase_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `firebase_tokens_user_id_foreign` (`user_id`);

--
-- Indexes for table `languages`
--
ALTER TABLE `languages`
  ADD PRIMARY KEY (`id`),
  ADD KEY `languages_code_index` (`code`);

--
-- Indexes for table `language_allows`
--
ALTER TABLE `language_allows`
  ADD PRIMARY KEY (`id`),
  ADD KEY `language_allows_language_id_foreign` (`language_id`),
  ADD KEY `language_allows_code_index` (`code`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notifications`
--
ALTER TABLE `notifications`
  ADD PRIMARY KEY (`id`),
  ADD KEY `notifications_notification_info_id_foreign` (`notification_info_id`),
  ADD KEY `notifications_to_user_id_foreign` (`to_user_id`);

--
-- Indexes for table `notification_infos`
--
ALTER TABLE `notification_infos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_access_tokens`
--
ALTER TABLE `oauth_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_access_tokens_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_auth_codes`
--
ALTER TABLE `oauth_auth_codes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_auth_codes_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_clients_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_refresh_tokens`
--
ALTER TABLE `oauth_refresh_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_refresh_tokens_access_token_id_index` (`access_token_id`);

--
-- Indexes for table `offers`
--
ALTER TABLE `offers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `offer_details`
--
ALTER TABLE `offer_details`
  ADD PRIMARY KEY (`id`),
  ADD KEY `offer_details_offer_id_foreign` (`offer_id`);

--
-- Indexes for table `offer_detail_trans`
--
ALTER TABLE `offer_detail_trans`
  ADD PRIMARY KEY (`id`),
  ADD KEY `offer_detail_trans_offer_detail_id_foreign` (`offer_detail_id`),
  ADD KEY `offer_detail_trans_lang_foreign` (`lang`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `phone_codes`
--
ALTER TABLE `phone_codes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `phone_codes_name_index` (`name`),
  ADD KEY `phone_codes_dial_code_index` (`dial_code`);

--
-- Indexes for table `phone_code_allows`
--
ALTER TABLE `phone_code_allows`
  ADD PRIMARY KEY (`id`),
  ADD KEY `phone_code_allows_phone_code_id_foreign` (`phone_code_id`),
  ADD KEY `phone_code_allows_name_index` (`name`),
  ADD KEY `phone_code_allows_dial_code_index` (`dial_code`);

--
-- Indexes for table `programs`
--
ALTER TABLE `programs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `program_trans`
--
ALTER TABLE `program_trans`
  ADD PRIMARY KEY (`id`),
  ADD KEY `program_trans_program_id_foreign` (`program_id`),
  ADD KEY `program_trans_lang_foreign` (`lang`);

--
-- Indexes for table `services`
--
ALTER TABLE `services`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `service_hosts`
--
ALTER TABLE `service_hosts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `service_hosts_service_id_foreign` (`service_id`);

--
-- Indexes for table `service_host_details`
--
ALTER TABLE `service_host_details`
  ADD PRIMARY KEY (`id`),
  ADD KEY `service_host_id` (`service_host_id`),
  ADD KEY `service_id` (`service_id`);

--
-- Indexes for table `service_host_detail_trans`
--
ALTER TABLE `service_host_detail_trans`
  ADD PRIMARY KEY (`id`),
  ADD KEY `service_host_trans_lang_foreign` (`lang`),
  ADD KEY `service_host_detail_id` (`service_host_detail_id`);

--
-- Indexes for table `service_host_trans`
--
ALTER TABLE `service_host_trans`
  ADD PRIMARY KEY (`id`),
  ADD KEY `lang` (`lang`),
  ADD KEY `service_host_trans_ibfk_2` (`service_host_id`);

--
-- Indexes for table `service_texts`
--
ALTER TABLE `service_texts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `service_texts_service_id_foreign` (`service_id`);

--
-- Indexes for table `service_text_trans`
--
ALTER TABLE `service_text_trans`
  ADD PRIMARY KEY (`id`),
  ADD KEY `service_text_trans_service_text_id_foreign` (`service_text_id`),
  ADD KEY `service_text_trans_lang_foreign` (`lang`);

--
-- Indexes for table `service_trans`
--
ALTER TABLE `service_trans`
  ADD PRIMARY KEY (`id`),
  ADD KEY `service_trans_service_id_foreign` (`service_id`),
  ADD KEY `service_trans_lang_foreign` (`lang`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `setting_trans`
--
ALTER TABLE `setting_trans`
  ADD PRIMARY KEY (`id`),
  ADD KEY `setting_trans_setting_id_foreign` (`setting_id`),
  ADD KEY `setting_trans_lang_foreign` (`lang`);

--
-- Indexes for table `site_texts`
--
ALTER TABLE `site_texts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `site_text_trans`
--
ALTER TABLE `site_text_trans`
  ADD PRIMARY KEY (`id`),
  ADD KEY `site_text_trans_site_text_id_foreign` (`site_text_id`),
  ADD KEY `site_text_trans_lang_foreign` (`lang`);

--
-- Indexes for table `sliders`
--
ALTER TABLE `sliders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `slider_trans`
--
ALTER TABLE `slider_trans`
  ADD PRIMARY KEY (`id`),
  ADD KEY `slider_trans_slider_id_foreign` (`slider_id`),
  ADD KEY `slider_trans_lang_foreign` (`lang`);

--
-- Indexes for table `subscribe_to_offers`
--
ALTER TABLE `subscribe_to_offers`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `subscribe_to_offers_email_unique` (`email`),
  ADD KEY `subscribe_to_offers_phone_code_foreign` (`phone_code`),
  ADD KEY `subscribe_to_offers_offer_id_foreign` (`offer_id`);

--
-- Indexes for table `subscribe_to_offer_details`
--
ALTER TABLE `subscribe_to_offer_details`
  ADD PRIMARY KEY (`id`),
  ADD KEY `subscribe_to_offer_details_subscribe_to_offer_id_foreign` (`subscribe_to_offer_id`),
  ADD KEY `subscribe_to_offer_details_program_id_foreign` (`program_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD KEY `users_phone_code_foreign` (`phone_code`),
  ADD KEY `offer_id` (`offer_id`);

--
-- Indexes for table `user_programs`
--
ALTER TABLE `user_programs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `program_id` (`program_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `user_program_chats`
--
ALTER TABLE `user_program_chats`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_program_room_id` (`user_program_room_id`),
  ADD KEY `from_user_id` (`from_user_id`);

--
-- Indexes for table `user_program_rooms`
--
ALTER TABLE `user_program_rooms`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_program_id` (`user_program_id`);

--
-- Indexes for table `user_program_tasks`
--
ALTER TABLE `user_program_tasks`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_program_id` (`user_program_id`);

--
-- Indexes for table `video_motions`
--
ALTER TABLE `video_motions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `video_motion_trans`
--
ALTER TABLE `video_motion_trans`
  ADD PRIMARY KEY (`id`),
  ADD KEY `lang` (`lang`),
  ADD KEY `video_motion_id` (`video_motion_id`);

--
-- Indexes for table `visits`
--
ALTER TABLE `visits`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admins`
--
ALTER TABLE `admins`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `blogs`
--
ALTER TABLE `blogs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `blog_trans`
--
ALTER TABLE `blog_trans`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=50;

--
-- AUTO_INCREMENT for table `clients`
--
ALTER TABLE `clients`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `client_trans`
--
ALTER TABLE `client_trans`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `confirm_emails`
--
ALTER TABLE `confirm_emails`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `contacts`
--
ALTER TABLE `contacts`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;

--
-- AUTO_INCREMENT for table `contract_advantages`
--
ALTER TABLE `contract_advantages`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `contract_advantage_details`
--
ALTER TABLE `contract_advantage_details`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=72;

--
-- AUTO_INCREMENT for table `contract_advantage_detail_trans`
--
ALTER TABLE `contract_advantage_detail_trans`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=109;

--
-- AUTO_INCREMENT for table `contract_advantage_trans`
--
ALTER TABLE `contract_advantage_trans`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `firebase_tokens`
--
ALTER TABLE `firebase_tokens`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `languages`
--
ALTER TABLE `languages`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=184;

--
-- AUTO_INCREMENT for table `language_allows`
--
ALTER TABLE `language_allows`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=48;

--
-- AUTO_INCREMENT for table `notifications`
--
ALTER TABLE `notifications`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `notification_infos`
--
ALTER TABLE `notification_infos`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `offers`
--
ALTER TABLE `offers`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `offer_details`
--
ALTER TABLE `offer_details`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `offer_detail_trans`
--
ALTER TABLE `offer_detail_trans`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `phone_codes`
--
ALTER TABLE `phone_codes`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=243;

--
-- AUTO_INCREMENT for table `phone_code_allows`
--
ALTER TABLE `phone_code_allows`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `programs`
--
ALTER TABLE `programs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `program_trans`
--
ALTER TABLE `program_trans`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `services`
--
ALTER TABLE `services`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `service_hosts`
--
ALTER TABLE `service_hosts`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5002;

--
-- AUTO_INCREMENT for table `service_host_details`
--
ALTER TABLE `service_host_details`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;

--
-- AUTO_INCREMENT for table `service_host_detail_trans`
--
ALTER TABLE `service_host_detail_trans`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=65;

--
-- AUTO_INCREMENT for table `service_host_trans`
--
ALTER TABLE `service_host_trans`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `service_texts`
--
ALTER TABLE `service_texts`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;

--
-- AUTO_INCREMENT for table `service_text_trans`
--
ALTER TABLE `service_text_trans`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=193;

--
-- AUTO_INCREMENT for table `service_trans`
--
ALTER TABLE `service_trans`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=65;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `setting_trans`
--
ALTER TABLE `setting_trans`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=54;

--
-- AUTO_INCREMENT for table `site_texts`
--
ALTER TABLE `site_texts`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `site_text_trans`
--
ALTER TABLE `site_text_trans`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=108;

--
-- AUTO_INCREMENT for table `sliders`
--
ALTER TABLE `sliders`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `slider_trans`
--
ALTER TABLE `slider_trans`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=59;

--
-- AUTO_INCREMENT for table `subscribe_to_offers`
--
ALTER TABLE `subscribe_to_offers`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `subscribe_to_offer_details`
--
ALTER TABLE `subscribe_to_offer_details`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `user_programs`
--
ALTER TABLE `user_programs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=50;

--
-- AUTO_INCREMENT for table `user_program_chats`
--
ALTER TABLE `user_program_chats`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT for table `user_program_rooms`
--
ALTER TABLE `user_program_rooms`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `user_program_tasks`
--
ALTER TABLE `user_program_tasks`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=353;

--
-- AUTO_INCREMENT for table `video_motions`
--
ALTER TABLE `video_motions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `video_motion_trans`
--
ALTER TABLE `video_motion_trans`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=223;

--
-- AUTO_INCREMENT for table `visits`
--
ALTER TABLE `visits`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `blog_trans`
--
ALTER TABLE `blog_trans`
  ADD CONSTRAINT `blog_trans_blog_id_foreign` FOREIGN KEY (`blog_id`) REFERENCES `blogs` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `blog_trans_lang_foreign` FOREIGN KEY (`lang`) REFERENCES `language_allows` (`code`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `client_trans`
--
ALTER TABLE `client_trans`
  ADD CONSTRAINT `client_trans_ibfk_1` FOREIGN KEY (`client_id`) REFERENCES `clients` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `client_trans_ibfk_2` FOREIGN KEY (`lang`) REFERENCES `language_allows` (`code`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `contacts`
--
ALTER TABLE `contacts`
  ADD CONSTRAINT `contacts_phone_code_foreign` FOREIGN KEY (`phone_code`) REFERENCES `phone_code_allows` (`dial_code`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `contract_advantage_details`
--
ALTER TABLE `contract_advantage_details`
  ADD CONSTRAINT `contract_advantage_details_contract_advantage_id_foreign` FOREIGN KEY (`contract_advantage_id`) REFERENCES `contract_advantages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `contract_advantage_detail_trans`
--
ALTER TABLE `contract_advantage_detail_trans`
  ADD CONSTRAINT `contract_advantage_detail_trans_contract_detail_id_foreign` FOREIGN KEY (`contract_detail_id`) REFERENCES `contract_advantage_details` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `contract_advantage_detail_trans_lang_foreign` FOREIGN KEY (`lang`) REFERENCES `language_allows` (`code`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `contract_advantage_trans`
--
ALTER TABLE `contract_advantage_trans`
  ADD CONSTRAINT `contract_advantage_trans_contract_advantage_id_foreign` FOREIGN KEY (`contract_advantage_id`) REFERENCES `contract_advantages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `contract_advantage_trans_lang_foreign` FOREIGN KEY (`lang`) REFERENCES `language_allows` (`code`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `firebase_tokens`
--
ALTER TABLE `firebase_tokens`
  ADD CONSTRAINT `firebase_tokens_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `language_allows`
--
ALTER TABLE `language_allows`
  ADD CONSTRAINT `language_allows_language_id_foreign` FOREIGN KEY (`language_id`) REFERENCES `languages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `notifications`
--
ALTER TABLE `notifications`
  ADD CONSTRAINT `notifications_notification_info_id_foreign` FOREIGN KEY (`notification_info_id`) REFERENCES `notification_infos` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `notifications_to_user_id_foreign` FOREIGN KEY (`to_user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `offer_details`
--
ALTER TABLE `offer_details`
  ADD CONSTRAINT `offer_details_offer_id_foreign` FOREIGN KEY (`offer_id`) REFERENCES `offers` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `offer_detail_trans`
--
ALTER TABLE `offer_detail_trans`
  ADD CONSTRAINT `offer_detail_trans_lang_foreign` FOREIGN KEY (`lang`) REFERENCES `language_allows` (`code`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `offer_detail_trans_offer_detail_id_foreign` FOREIGN KEY (`offer_detail_id`) REFERENCES `offer_details` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `phone_code_allows`
--
ALTER TABLE `phone_code_allows`
  ADD CONSTRAINT `phone_code_allows_phone_code_id_foreign` FOREIGN KEY (`phone_code_id`) REFERENCES `phone_codes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `program_trans`
--
ALTER TABLE `program_trans`
  ADD CONSTRAINT `program_trans_lang_foreign` FOREIGN KEY (`lang`) REFERENCES `language_allows` (`code`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `program_trans_program_id_foreign` FOREIGN KEY (`program_id`) REFERENCES `programs` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `service_hosts`
--
ALTER TABLE `service_hosts`
  ADD CONSTRAINT `service_hosts_service_id_foreign` FOREIGN KEY (`service_id`) REFERENCES `services` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `service_host_details`
--
ALTER TABLE `service_host_details`
  ADD CONSTRAINT `service_host_details_ibfk_1` FOREIGN KEY (`service_host_id`) REFERENCES `service_hosts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `service_host_details_ibfk_2` FOREIGN KEY (`service_id`) REFERENCES `services` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `service_host_detail_trans`
--
ALTER TABLE `service_host_detail_trans`
  ADD CONSTRAINT `service_host_detail_trans_ibfk_1` FOREIGN KEY (`service_host_detail_id`) REFERENCES `service_host_details` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `service_host_trans_lang_foreign` FOREIGN KEY (`lang`) REFERENCES `language_allows` (`code`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `service_host_trans`
--
ALTER TABLE `service_host_trans`
  ADD CONSTRAINT `service_host_trans_ibfk_1` FOREIGN KEY (`lang`) REFERENCES `language_allows` (`code`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `service_host_trans_ibfk_2` FOREIGN KEY (`service_host_id`) REFERENCES `service_hosts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `service_texts`
--
ALTER TABLE `service_texts`
  ADD CONSTRAINT `service_texts_service_id_foreign` FOREIGN KEY (`service_id`) REFERENCES `services` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `service_text_trans`
--
ALTER TABLE `service_text_trans`
  ADD CONSTRAINT `service_text_trans_lang_foreign` FOREIGN KEY (`lang`) REFERENCES `language_allows` (`code`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `service_text_trans_service_text_id_foreign` FOREIGN KEY (`service_text_id`) REFERENCES `service_texts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `service_trans`
--
ALTER TABLE `service_trans`
  ADD CONSTRAINT `service_trans_lang_foreign` FOREIGN KEY (`lang`) REFERENCES `language_allows` (`code`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `service_trans_service_id_foreign` FOREIGN KEY (`service_id`) REFERENCES `services` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `setting_trans`
--
ALTER TABLE `setting_trans`
  ADD CONSTRAINT `setting_trans_lang_foreign` FOREIGN KEY (`lang`) REFERENCES `language_allows` (`code`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `setting_trans_setting_id_foreign` FOREIGN KEY (`setting_id`) REFERENCES `settings` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `site_text_trans`
--
ALTER TABLE `site_text_trans`
  ADD CONSTRAINT `site_text_trans_lang_foreign` FOREIGN KEY (`lang`) REFERENCES `language_allows` (`code`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `site_text_trans_site_text_id_foreign` FOREIGN KEY (`site_text_id`) REFERENCES `site_texts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `slider_trans`
--
ALTER TABLE `slider_trans`
  ADD CONSTRAINT `slider_trans_lang_foreign` FOREIGN KEY (`lang`) REFERENCES `language_allows` (`code`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `slider_trans_slider_id_foreign` FOREIGN KEY (`slider_id`) REFERENCES `sliders` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `subscribe_to_offers`
--
ALTER TABLE `subscribe_to_offers`
  ADD CONSTRAINT `subscribe_to_offers_offer_id_foreign` FOREIGN KEY (`offer_id`) REFERENCES `offers` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `subscribe_to_offers_phone_code_foreign` FOREIGN KEY (`phone_code`) REFERENCES `phone_code_allows` (`dial_code`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `subscribe_to_offer_details`
--
ALTER TABLE `subscribe_to_offer_details`
  ADD CONSTRAINT `subscribe_to_offer_details_program_id_foreign` FOREIGN KEY (`program_id`) REFERENCES `programs` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `subscribe_to_offer_details_subscribe_to_offer_id_foreign` FOREIGN KEY (`subscribe_to_offer_id`) REFERENCES `subscribe_to_offers` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_ibfk_1` FOREIGN KEY (`offer_id`) REFERENCES `offers` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `users_phone_code_foreign` FOREIGN KEY (`phone_code`) REFERENCES `phone_code_allows` (`dial_code`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `user_programs`
--
ALTER TABLE `user_programs`
  ADD CONSTRAINT `user_programs_ibfk_1` FOREIGN KEY (`program_id`) REFERENCES `programs` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `user_programs_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `user_program_chats`
--
ALTER TABLE `user_program_chats`
  ADD CONSTRAINT `user_program_chats_ibfk_1` FOREIGN KEY (`user_program_room_id`) REFERENCES `user_program_rooms` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `user_program_chats_ibfk_2` FOREIGN KEY (`from_user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `user_program_rooms`
--
ALTER TABLE `user_program_rooms`
  ADD CONSTRAINT `user_program_rooms_ibfk_1` FOREIGN KEY (`user_program_id`) REFERENCES `user_programs` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `user_program_tasks`
--
ALTER TABLE `user_program_tasks`
  ADD CONSTRAINT `user_program_tasks_ibfk_1` FOREIGN KEY (`user_program_id`) REFERENCES `user_programs` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `video_motion_trans`
--
ALTER TABLE `video_motion_trans`
  ADD CONSTRAINT `video_motion_trans_ibfk_1` FOREIGN KEY (`lang`) REFERENCES `language_allows` (`code`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `video_motion_trans_ibfk_2` FOREIGN KEY (`video_motion_id`) REFERENCES `video_motions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
