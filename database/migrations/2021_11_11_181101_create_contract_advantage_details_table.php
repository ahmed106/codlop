<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContractAdvantageDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contract_advantage_details', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('contract_advantage_id')->nullable();
            $table->foreign('contract_advantage_id')->references('id')->on('contract_advantages')->onDelete('cascade')->onUpdate('cascade');
            $table->string('icon')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contract_advantage_details');
    }
}
