<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePhoneCodeAllowsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('phone_code_allows', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('phone_code_id')->nullable();
            $table->foreign('phone_code_id')->references('id')->on('phone_codes')->onDelete('cascade')->onUpdate('cascade');
            $table->string('name')->index()->nullable();
            $table->string('dial_code')->index()->nullable();
            $table->string('code')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('phone_code_allows');
    }
}
