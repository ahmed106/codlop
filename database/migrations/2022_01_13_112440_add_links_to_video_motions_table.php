<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddLinksToVideoMotionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('video_motions', function (Blueprint $table) {
            $table->string('website_link')->nullable();
            $table->string('android_link')->nullable();
            $table->string('ios_link')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('video_motions', function (Blueprint $table) {
            $table->dropColumn('website_link');
            $table->dropColumn('android_link');
            $table->dropColumn('ios_link');
        });
    }
}
