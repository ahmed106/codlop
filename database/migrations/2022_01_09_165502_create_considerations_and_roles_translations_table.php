<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateConsiderationsAndRolesTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('considerations_and_roles_translations', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('consideration_id');
            $table->string('title');
            $table->text('text');
            $table->string('lang');
            $table->foreign('consideration_id')->references('id')->on('considerations_and_roles')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('considerations_and_roles_translations');
    }
}
