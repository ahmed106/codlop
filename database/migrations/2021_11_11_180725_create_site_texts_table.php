<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSiteTextsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('site_texts', function (Blueprint $table) {
            $table->id();
            $table->string('image')->nullable();
            $table->string('color')->nullable();
            $table->enum('page_type',['home','about','service','offer','blog','contact'])->default('about');
            $table->enum('text_type',['years_of_experience','our_vision','our_message','features_of_codlop'])->nullable();
            $table->enum('is_shown', ['yes', 'no'])->default('yes');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('site_texts');
    }
}
