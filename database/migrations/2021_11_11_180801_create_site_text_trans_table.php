<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSiteTextTransTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('site_text_trans', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('site_text_id')->nullable();
            $table->foreign('site_text_id')->references('id')->on('site_texts')->onDelete('cascade')->onUpdate('cascade');
            $table->string('title')->nullable();
            $table->text('text')->nullable();
            $table->string('lang')->nullable();
            $table->foreign('lang')->references('code')->on('language_allows')->onDelete('cascade')->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('site_text_trans');
    }
}
