<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('code')->nullable();
            $table->enum('user_type', ['client'])->default('client');
            $table->string('phone_code')->nullable();
            $table->foreign('phone_code')->references('dial_code')->on('phone_code_allows')->onDelete('cascade')->onUpdate('cascade');
            $table->unsignedBigInteger('phone')->nullable();
            $table->string('name')->nullable();
            $table->string('email')->nullable()->unique();
            $table->string('password')->nullable();
            $table->unsignedBigInteger('offer_id')->nullable();
            $table->foreign('offer_id')->references('id')->on('offers')->onDelete('cascade')->onUpdate('cascade');
            $table->text('more_details')->nullable();
            $table->double('latitude')->default(0);
            $table->double('longitude')->default(0);
            $table->string('address')->nullable();
            $table->enum('gender', ['male', 'female'])->default('male');
            $table->date('birthday')->nullable();
            $table->string('logo')->nullable();
            $table->string('banner')->nullable();
            $table->enum('approved_status', ['new', 'accepted', 'not_accepted'])->default('new');
            $table->integer('approved_by')->nullable();
            $table->enum('is_blocked', ['blocked', 'not_blocked'])->default('not_blocked');
            $table->enum('is_login', ['online', 'offline'])->default('offline');
            $table->integer('logout_time')->nullable();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('confirmation_code')->nullable();
            $table->string('forget_password_code')->nullable();
            $table->enum('software_type', ['ios', 'android', 'web'])->default('web')->nullable();
            $table->softDeletes();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
