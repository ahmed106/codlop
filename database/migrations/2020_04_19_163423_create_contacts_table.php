<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContactsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contacts', function (Blueprint $table) {
            $table->id();
            $table->string('name')->nullable();
            $table->string('phone_code')->nullable();
            $table->foreign('phone_code')->references('dial_code')->on('phone_code_allows')->onDelete('cascade')->onUpdate('cascade');
            $table->unsignedBigInteger('phone')->nullable();
            $table->string('email')->nullable();
            $table->text('subject')->nullable();
            $table->text('message')->nullable();
            $table->enum('is_read',['read','unread'])->default('unread');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contacts');
    }
}
