<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContractAdvantageDetailTransTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contract_advantage_detail_trans', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('contract_detail_id')->nullable();
            $table->foreign('contract_detail_id')->references('id')->on('contract_advantage_details')->onDelete('cascade')->onUpdate('cascade');
            $table->string('title')->nullable();
            $table->text('text')->nullable();
            $table->string('lang')->nullable();
            $table->foreign('lang')->references('code')->on('language_allows')->onDelete('cascade')->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contract_advantage_detail_trans');
    }
}
