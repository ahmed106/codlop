<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateServiceTextTransTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('service_text_trans', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('service_text_id')->nullable();
            $table->foreign('service_text_id')->references('id')->on('service_texts')->onDelete('cascade')->onUpdate('cascade');
            $table->string('page_title')->nullable();
            $table->string('title')->nullable();
            $table->text('text')->nullable();
            $table->string('lang')->nullable();
            $table->foreign('lang')->references('code')->on('language_allows')->onDelete('cascade')->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('service_text_trans');
    }
}
