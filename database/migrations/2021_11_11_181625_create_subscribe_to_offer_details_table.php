<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSubscribeToOfferDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subscribe_to_offer_details', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('subscribe_to_offer_id')->nullable();
            $table->foreign('subscribe_to_offer_id')->references('id')->on('subscribe_to_offers')->onDelete('cascade')->onUpdate('cascade');
            $table->unsignedBigInteger('program_id')->nullable()->comment('like ios, android, web, ui-ux');
            $table->foreign('program_id')->references('id')->on('programs')->onDelete('cascade')->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('subscribe_to_offer_details');
    }
}
