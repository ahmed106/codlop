<?php

use Illuminate\Support\Facades\Route;

Route::get('power', 'Admin\AdminAuth@login');
Route::view('power-qr', 'admin.adminAuth.qr_login');


Route::group(['prefix' => 'admin', 'namespace' => 'Admin'], function () {

    Config::set('auth.defines', 'admin');
    app()->setLocale('ar');

    //================ Auth ================
//            Route::get('login', 'AdminAuth@login');
    Route::post('power', 'AdminAuth@dologin')->name('admin.login');


    //********************* Auth *********************
    Route::group(['middleware' => 'admin:admin'], function () {

        Route::get('/', function () {
            return view('admin.index');
        });

        Route::get('/test', function () {
            return site_texts('about');
        });


        //================ admin ================
        Route::resource('admin', 'AdminController');
        Route::post('delete-admin', 'AdminController@delete_admin')->name('delete-admin');
        Route::post('update-admin', 'AdminController@update_admin');


        //================ slider ================
        Route::resource('slider', 'SliderController');
        Route::post('delete-slider', 'SliderController@delete_slider')->name('delete-slider');
        Route::post('update-slider', 'SliderController@update_slider');


        //================ contract_advantage ================
        Route::resource('contract_advantage', 'ContractAdvantageController');
        Route::post('delete-contract_advantage', 'ContractAdvantageController@delete_contract_advantage')->name('delete-contract_advantage');
        Route::post('update-contract_advantage', 'ContractAdvantageController@update_contract_advantage');
        Route::get('contract_advantage/add_details/{id}', 'ContractAdvantageController@contract_advantage_add_details');
        Route::post('store_contract_advantage_add_details', 'ContractAdvantageController@store_contract_advantage_add_details');
        Route::post('add-contract_advantage-row', 'ContractAdvantageController@add_contract_advantage_row');
        Route::get('edit_contract_advantage_detail/{id}', 'ContractAdvantageController@editContractAdvantageDetail')->name('edit_contract_advantage_detail');
        Route::put('update_contract_advantage_detail/{id}', 'ContractAdvantageController@updateContractAdvantageDetail')->name('update_contract_advantage_detail');
        Route::post('delete-contract_advantage_details', 'ContractAdvantageController@delete_contract_advantage_details')->name('delete-contract_advantage_details');

        //================ blog ================
        Route::resource('blog', 'BlogController');
        Route::post('delete-blog', 'BlogController@delete_blog')->name('delete-blog');
        Route::post('update-blog', 'BlogController@update_blog');

        //================ video_motion ================
        Route::resource('video_motion', 'VideoMotionController');
        Route::post('delete-video_motion', 'VideoMotionController@delete_video_motion')->name('delete-video_motion');
        Route::post('update-video_motion', 'VideoMotionController@update_video_motion');

        //================ service ================
        Route::resource('service', 'ServiceController');
        Route::post('delete-service', 'ServiceController@delete_service')->name('delete-service');
        Route::post('update-service', 'ServiceController@update_service');

        //service_hosts

        Route::resource('service_hosts', 'ServiceHostController');
        Route::post('delete-service-host', 'ServiceHostController@deleteServiceHost');
        //


        // polices

        Route::resource('polices', 'PolicyController');

        // end polices

        // privacy

        Route::resource('privacy', 'PrivacyController');

        // end privacy

        // roles and conditions

        Route::resource('roles-conditions', 'RoleConditionController');

        // end roles and conditions


        //================ service_text ================
        Route::resource('service_text', 'ServiceTextController');
        Route::post('delete-service_text', 'ServiceTextController@delete_service_text')->name('delete-service_text');
        Route::post('update-service_text', 'ServiceTextController@update_service_text');

        //================ language_allow ================
        Route::resource('language_allow', 'LanguageAllowController');
        Route::post('delete-language_allow', 'LanguageAllowController@delete_language_allow')->name('delete-language_allow');
        Route::post('update-language_allow', 'LanguageAllowController@update_language_allow');

        //================ phone_code_allow ================
        Route::resource('phone_code_allow', 'PhoneCodeAllowController');
        Route::post('delete-phone_code_allow', 'PhoneCodeAllowController@delete_phone_code_allow')->name('delete-phone_code_allow');
        Route::post('update-phone_code_allow', 'PhoneCodeAllowController@update_phone_code_allow');

        //================ site_text ================
        Route::resource('site_text', 'SiteTextController');
        Route::post('delete-site_text', 'SiteTextController@delete_site_text')->name('delete-site_text');
        Route::post('update-site_text', 'SiteTextController@update_site_text');


        //================ offer_detail ================
        Route::resource('offer_detail', 'OfferDetailController');
        Route::post('delete-offer_detail', 'OfferDetailController@delete_offer_detail')->name('delete-offer_detail');
        Route::post('update-offer_detail', 'OfferDetailController@update_offer_detail');
        Route::post('update-prices', 'OfferDetailController@update_prices');


        //================ service_host_detail ================
        Route::resource('service_host_detail', 'ServiceHostDetailController');
        Route::post('delete-service_host_detail', 'ServiceHostDetailController@delete_service_host_detail')->name('delete-service_host_detail');
        Route::post('update-service_host_detail', 'ServiceHostDetailController@update_service_host_detail');


        //================ user ================
        Route::resource('user', 'UserController');
        Route::post('delete-user', 'UserController@delete_user')->name('delete-user');
        Route::post('update-user', 'UserController@update_user');


        //================ client ================
        Route::resource('client', 'ClientController');
        Route::post('delete-client', 'ClientController@delete_client')->name('delete-client');
        Route::post('update-client', 'ClientController@update_client');

        //=================Invoices=====================

        Route::resource('invoices', 'InvoiceController');

        //================ user_program ================
        Route::resource('user_program', 'UserProgramController');
        Route::post('delete-user_program', 'UserProgramController@delete_user_program')->name('delete-user_program');
        Route::post('update-user_program', 'UserProgramController@update_user_program');
        Route::post('update-update_user_program_status', 'UserProgramController@update_user_program_status');
        Route::get('chat', 'UserProgramController@chat');
        Route::post('msg_send', 'UserProgramController@msg_send')->name('msg.send');


        //================ contact ================
        Route::resource('contact', 'ContactController');
        Route::post('delete-contact', 'ContactController@delete_contact')->name('delete-contact');
        Route::post('update-contact', 'ContactController@update_contact');


        //================ settings ================
        Route::get('settings', 'SettingController@edit');
        Route::post('update-setting', 'SettingController@update_setting');



        //================ Teams ================
        Route::resource('teams','TeamController');
        Route::post('teams/delete-team','TeamController@delete');


        Route::post('delete-program', 'ProgramController@deleteProgram');
        Route::resource('programs', 'ProgramController');

        Route::any('logout', 'AdminAuth@logout');

        // test  qr code

        Route::get('/test-qr', 'TestQrCodeController@index');

        Route::view('/test-login-qr', 'admin.test_login_qr');


    });

});
