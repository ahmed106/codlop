<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::group(
    [
        'prefix' => LaravelLocalization::setLocale(),
        'middleware' => ['localeSessionRedirect', 'localizationRedirect', 'localeViewPath'],
        'namespace' => 'Website'
    ], function () {

    config(['auth.defaults.guard' => 'web']);


    /* Without auth */
    Route::get('login', 'WebAuth@login');
    Route::post('login_action', 'WebAuth@login_action')->name('login_action');
    Route::get('/', 'HomeController@index');
    Route::get('about', 'AboutController@about');
    Route::get('service', 'ServiceController@services');
    Route::get('service-detail/{service_id}', 'ServiceController@service_detail');
    Route::get('offers', 'OfferController@offers');
    Route::get('blog', 'BlogController@blog');
    Route::get('blog-detail/{id}', 'BlogController@blog_detail');
    Route::get('video-motion', 'VideoMotionController@video_motion');
    Route::get('work_details/{id}', 'VideoMotionController@work_details');
    Route::get('get-video-content', 'VideoMotionController@get_video_content');
    Route::get('order-now', 'OrderController@order_now');
    Route::post('confirm-email', 'OrderController@confirm_email')->name('confirm-email');
    Route::post('send-order-now', 'OrderController@send_order_now')->name('send-order-now');
    Route::get('contact', 'ContactController@contact');
    Route::post('send-contact', 'ContactController@send_contact')->name('send-contact');
    /* Without auth */


    //==============================  rest password =================================
    Route::get('rest-password-form', 'WebAuth@rest_password_form')->name('rest-password-form');
    Route::post('rest-password', 'WebAuth@rest_password')->name('rest-password');
    Route::post('confirm-code', 'WebAuth@confirm_code')->name('confirm-code');
    Route::post('update-password', 'WebAuth@update_password')->name('update-password');
    //==============================  rest password =================================
    Route::view('/training', 'website.pages.training.index');
    Route::get('/team', 'TeamController@index');

    /* With auth */
    Route::group(['middleware' => ['auth']], function () {

        Route::get('profile', 'ProfileController@profile');
        Route::get('get-description', 'ProfileController@getDescription')->name('getUserServiceDescription');
        Route::post('confirm-program', 'ProfileController@confirm_program')->name('confirm-program');
        Route::get('new-apps', 'ProfileController@new_apps');
        Route::post('add-apps', 'ProfileController@add_apps')->name('add-apps');
        Route::get('chat', 'ProfileController@chat');
        Route::post('send_msg', 'ProfileController@msg_send')->name('msg.send_msg');


        Route::get('logout', 'WebAuth@logout');
    });
    /* With auth */

});


//Clear route cache:
Route::get('/clear_cache', function () {
//    $exitCode = Artisan::call('route:cache');
    $exitCode = Artisan::call('config:cache');
    $exitCode = Artisan::call('cache:clear');
    $exitCode = Artisan::call('view:clear');

    return 'cache cleared';
});

Route::get('/dump-autoload', function () {
    shell_exec('composer dump-autoload');
//    Artisan::call('dump-autoload');
    echo 'dump-autoload complete';
});

Route::get("/storage-link", function () {
    $targetFolder = storage_path("app/public");
    $linkFolder = $_SERVER['DOCUMENT_ROOT'] . '/storage';
    symlink($targetFolder, $linkFolder);
    return 'done';
});
