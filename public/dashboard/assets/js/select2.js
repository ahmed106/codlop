$(function() {
	
	$('.select2').select2({
		placeholder: 'اختر',
		searchInputPlaceholder: 'Search',
		minimumResultsForSearch: Infinity,
		width: '100%'
	});

	$('.select2-with-search').select2({
		searchInputPlaceholder: 'ابحث هنا',
		width: '100%'
	});
});
